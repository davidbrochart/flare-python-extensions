# Flare Python Extensions

Welcome to [Flare](https://www.cresset-group.com/flare/) Python Extensions repository. This repository contains numerous scripts to add additional functionality to Flare (TM).

The scripts are divided into 2 categories. Featured contains scripts which can be used out of the box and should work well for most workflows. Additional contains extra scripts which are developed for specific use-cases that may need to be edited to meet your needs.

See the README.md files in each subdirectory for more information.

Since Flare 5 the most useful extensions are installed with Flare. These extensions are also included in this repository so that they can be updated separately from Flare.
If an extension is both included in Flare and installed from this repository then Flare will load the version from this repository on startup.
The extensions included with Flare are:
* Cresset Integration
* Export Model Data
* Loop Modeling
* Plot
* QSAR Models
* Tours

Since Flare 5 the command line scripts, which allow Flare functions to be accessed from the command line, are installed with Flare. They are found in the "pyflare-scripts" directory within Flare's installation directory.

Updates to these scripts may be found at the [flare-python-pyflare repository](https://gitlab.com/cresset/flare-python-pyflare).

For examples on how to write your own extensions visit the [flare-python-developer repository](https://gitlab.com/cresset/flare-python-developer).

The Python extensions in this repository will only work with Flare 7.1. If you do not have Flare 7.1, please contact us at <enquiries@cresset-group.com> to request a free evaluation. The installation instructions for Flare can be found [here](https://www.cresset-group.com/support/installation/).

# Downloading the extensions

The easiest way to get the extension is to download the whole repository by clicking the download button (which looks like a cloud with a down arrow) and selecting "Download zip".

# Installing the extensions

## Automatically

Extract the zip file, and run the installextensions.py script using the pyflare executable as shown below:

> pyflare installextensions.py

You will be prompted for the install location. Leave this blank to install to the default location.

## Manual

Extract the zip file. The main extensions are in a directory called "featured". To install them, copy the content of this directory to one of the Flare search directories (see below). Less frequently used extensions are located in the "additional" directory. These too can be installed by copying the contents of the "additional" directory to one of the Flare search directories.

**Flare Search Directories**

The directories where Flare loads extensions can be found in the "Extension Manager" dialog which is opened by pressing the "Python" > "Manager" button in the ribbon. Additional directories can be specified by setting the FLARE_PYTHON_EXTENSIONS_PATH environment variable, which consists of one or more directory pathnames separated by colons on Unix or semicolons on Windows.

The default locations of the search directories are:

| OS       | Location |
| ---      | --- |
| Windows: | %LOCALAPPDATA%\Cresset BMD\Flare\python\v7.1\extensions |
|          | \<Flare Installation Directory>\python-extensions |
| Linux:   | $HOME/.com.cresset-bmd/Flare/python/v7.1/extensions |
|          | \<Flare Installation Directory>/python-extensions |
| macOS:   | $HOME/Library/Application Support/Cresset BMD/Flare/python/v7.1/extensions |
|          | \<Flare Installation Directory>.app/Contents/Resources/python-extensions |

**Extensions  Prerequisites**

Some extensions require additional Python packages which are not installed by Flare. Installing these packages is optional, but the extensions which depend on these packages will not be loaded by Flare if these packages are missing.

These packages can be installed from the command line by using the pyflare executable which is located in the Flare installation directory.

> pyflare -m pip install --user -r featured/requirements.txt

Alternatively these packages can be installed from within the Flare "Python Console" window.

```python
import sys
import subprocess
subprocess.check_call([sys.executable, "-m", "pip", "install", "--user", "-r", "featured/requirements.txt"])
```

## Finishing Up

Once the extensions have been copied in the appropriate location, restart Flare. A new ribbon tab should appear titled "Extensions". If the extensions do not load properly, check the Python log which is opened by pressing the "Python" > "Log" button and contact  [Cresset support](https://www.cresset-group.com/about-us/contact-us/).
Descriptions of what each extension does can be found in the [featured](featured/) and [additional](additional/) README.md files.
