# Additional Extensions

The additional extensions is a collection of free Python scripts which adds extra functionality to Flare (TM). Unlike the featured extensions, these extensions may only be useful for specific use cases and may need editing to fit into your workflows. After installing these extensions, a new ribbon tab called "Extensions" will be added to Flare. The "Extensions" should be added to the Flare GUI, containing buttons to access this new functionality.

The additional extensions are provided to you on an "AS-IS" basis, however if you need help or have suggestions of your own please contact [Cresset support](https://www.cresset-group.com/about-us/contact-us/).

## Installing

See the [installation guide](../README.md).

## Extensions Descriptions

### Export (export)
Adds functions for exporting data.

**New Ribbon Controls:**

*3D Pose -> Export -> Export Residues ΔG Values* - Export the residues' WaterSwap ΔG values for the selected protein.


### Open PyMOL (openpymol.py)
Opens the selected molecules in PyMOL.

This extension shows an example of how to open molecules in an external application such as PyMOL.

**New Ribbon Controls:**

*Extensions -> Import/Export -> Send to PyMOL* - Open the selected molecules in PyMOL. When this button is pressed for the first time, Flare will ask for the location of the PyMol executable.

*Extensions -> Import/Export -> Send to PyMOL -> Configure PyMOL Executable* - Allows the location of the PyMOL executable to be changed.

**Requirements:**

This extension requires PyMOL from https://pymol.org to be installed.


### Prep, Align and View (prepalignview)
Aligns, superposes, prepares and adds surfaces to selected proteins in the project.

**New Ribbon Controls:**

*Extensions -> Protein -> Prep and Align* - Runs the following steps for selected proteins in the project.
* Prepare selected proteins and extract ligands.
* Align sequences of selected proteins to the first.
* Superpose selected proteins to the first.
* Add surface to active site and focus on ligands.

*Extensions -> Protein -> Ligand View* - Runs the following steps for selected proteins in the project.
* Extract ligands from the selected proteins.
* Align sequences of selected proteins to the first.
* Superpose selected proteins to the first.
* Add surface with specified settings to active site.
* Change the display style for ligands to the specified style and focus on them.
* Enable the clip planes.
* Hide the ribbons for the selected proteins.
