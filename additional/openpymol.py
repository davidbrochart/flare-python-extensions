# -*- coding: utf-8 -*-
# Copyright (C) 2023 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Opens the selected molecules in PyMOL.

This extension shows an example of how to open molecules in an external application such as PyMOL.

Ribbon Controls:
    Extensions -> Import/Export -> Send to PyMOL
        Open the selected molecules in PyMOL. When this button is pressed for the first time,
        Flare will ask for the location of the PyMol executable.

    Extensions -> Import/Export -> Send to PyMOL -> Configure PyMOL Executable
        Allows the location of the PyMOL executable to be changed.

Requirements:
    This extension requires PyMOL from https://pymol.org to be installed.
"""
import shutil
import os
import atexit
import subprocess
import tempfile

from PySide2 import QtWidgets

from cresset import flare


@flare.extension
class OpenPyMOLExtension:
    """Add a button to the ribbon which starts PyMOL."""

    def __init__(self):
        self.pymol_path = ""
        self._temp_dir = None

    def load(self):
        """Load the extension."""
        tab = flare.main_window().ribbon["Extensions"]
        tab.tip_key = "X"
        group = tab["Import/Export"]

        menu = QtWidgets.QMenu()
        send_to_pymol = menu.addAction("Send to PyMOL")
        send_to_pymol.triggered.connect(self._send_to_pymol)

        configure_pymol = menu.addAction("Configure PyMOL Executable")
        configure_pymol.triggered.connect(self._configure_pymol)

        control = group.add_button("Send to PyMOL", self._send_to_pymol, menu)
        control.tooltip = "Opens the selected molecules in PyMol."
        control.tip_key = "Y"

        atexit.register(self._cleanup)

        print(f"Loaded {self.__class__.__module__}.{self.__class__.__name__}")

    def _cleanup(self):
        """Remove the temp files for the last time PyMOL was run."""
        if self._temp_dir:
            shutil.rmtree(self._temp_dir)
            self._temp_dir = None

    def load_settings(self, settings):
        """Read the PyMol executable path from the settings."""
        self.pymol_path = settings.get("pymol_path", "")

    def save_settings(self, settings):
        """Save the PyMol executable path to the settings."""
        settings["pymol_path"] = self.pymol_path

    def _send_to_pymol(self):
        """Open PyMol with the selected ligands, proteins and poses."""
        # If needed ask the user where PyMol is
        if not self.pymol_path:
            self._configure_pymol()

        # Has the user canceled the dialog shown by self._configure_pymol()
        if not self.pymol_path:
            return

        # Remove the temp files for the last time PyMOL was run
        self._cleanup()

        self._temp_dir = tempfile.mkdtemp()

        command = []
        command.append(self.pymol_path)

        # Write the selected ligands, poses and poses a temp directory
        main_window = flare.main_window()
        for ligand in main_window.selected_ligands:
            path = os.path.join(self._temp_dir, f"{ligand.title}.sdf")
            ligand.write_file(path)
            command.append(path)

        for pose in main_window.selected_poses:
            index = main_window.selected_poses.index(pose)
            path = os.path.join(self._temp_dir, f"{pose.ligand.title}-{index}.sdf")
            pose.write_file(path)
            command.append(path)

        for protein in main_window.selected_proteins:
            path = os.path.join(self._temp_dir, f"{protein.title}.pdb")
            protein.write_file(path)
            command.append(path)

        # Launch PyMol
        env = os.environ.copy()
        if "PYTHONPATH" in env:
            del env["PYTHONPATH"]  # Clear the PYTHONPATH set by flare
        cwd = None
        pymol_dir = os.path.dirname(self.pymol_path)
        if os.path.isdir(pymol_dir):
            cwd = pymol_dir
        subprocess.Popen(command, env=env, cwd=cwd)

    def _configure_pymol(self):
        """Ask where the PyMol executable is located."""
        parent = flare.main_window().widget()
        new_path, _ = QtWidgets.QFileDialog.getOpenFileName(
            parent, "PyMol Executable Path", self.pymol_path
        )

        if new_path:
            self.pymol_path = new_path
