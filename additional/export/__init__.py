# -*- coding: utf-8 -*-
# Copyright (C) 2023 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Adds functions for exporting data.

Ribbon Controls:
    3D Pose -> Export -> Export Residues ΔG Values
        Export the residues' WaterSwap ΔG values for the selected protein.

"""
import os
import csv

from PySide2 import QtWidgets

from cresset import flare


@flare.extension
class ExportExtension:
    """Add a buttons to export data."""

    def __init__(self):
        self._last_save_path = ""

    def load(self):
        """Load the extension."""
        tab = flare.main_window().ribbon["3D Pose"]

        group = tab["Export"]

        control = group.add_button("Export Residues ΔG Values", self._write_water_swap_results)
        control.tooltip = "Export the residues' WaterSwap ΔG values for the selected protein."
        control.tip_key = "G"
        control.load_icon(_package_file("export_dg.png"))

        print(f"Loaded {self.__class__.__module__}.{self.__class__.__name__}")

    def load_settings(self, settings):
        """Read the last saved location to the settings."""
        self._last_save_path = settings.get("last_save_path", "")

    def save_settings(self, settings):
        """Save the last saved location to the settings."""
        settings["last_save_path"] = self._last_save_path

    def _write_water_swap_results(self):
        """Ask where to write the WaterSwap delta G then writes the file."""
        parent = flare.main_window().widget()

        if len(flare.main_window().selected_proteins) != 1:
            QtWidgets.QMessageBox.critical(
                parent,
                "Select Protein",
                "Select the protein to export the WaterSwap Residue Delta G values for.",
            )
            return

        protein = flare.main_window().selected_proteins[0]

        if protein.water_swap_result is None:
            QtWidgets.QMessageBox.critical(
                parent, "Select Protein", "The selected protein does not have WaterSwap results."
            )
            return

        file_path, _ = QtWidgets.QFileDialog.getSaveFileName(
            parent,
            "Save WaterSwap ΔG values",
            self._last_save_path,
            "Comma-separated value (*.csv);;All files (*)",
        )
        if file_path:
            self._last_save_path = file_path
            self._write_water_swap_results_to_csv(protein, file_path)

    @staticmethod
    def _write_water_swap_results_to_csv(protein, file_path):
        """Write the residues WaterSwap delta G values to `file_path`."""
        with open(file_path, "w", newline="") as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow([protein.title])
            writer.writerow([])
            writer.writerow(["Residue", "Delta G", "Error"])

            for residue in protein.residues:
                delta_g, error = protein.water_swap_result.residue_delta_g(residue)
                if delta_g is not None and error is not None:
                    writer.writerow([str(residue), round(delta_g, 1), round(error, 1)])

    def _write_displayed_qsar_model_data(self):
        """Ask where to write the QSAR model data to then writes the file."""
        main_window = flare.main_window()
        parent = main_window.widget()

        file_path, _ = QtWidgets.QFileDialog.getSaveFileName(
            parent,
            "Save QSAR Model Data",
            self._last_save_path,
            "Comma-separated value (*.csv);;All files (*)",
        )
        if file_path:
            self._last_save_path = file_path
            self._write_displayed_qsar_model_data_to_csv(main_window.selected_qsar_model, file_path)


def _package_file(file_name):
    """Return the path to the file `file_name` in this package."""
    return os.path.join(os.path.dirname(os.path.realpath(__file__)), file_name)
