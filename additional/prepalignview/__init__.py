# -*- coding: utf-8 -*-
# Copyright (C) 2023 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Aligns, superposes, prepares and adds surfaces to selected proteins in the project.

Ribbon Controls:
    Extensions -> Protein -> Prep and Align
        Runs the following steps for selected proteins in the project.
        * Prepare selected proteins and extract ligands.
        * Align sequences of selected proteins to the first.
        * Superpose selected proteins to the first.
        * Add surface to active site and focus on ligands.

    Extensions -> Protein -> Ligand View
        Runs the following steps for selected proteins in the project.
        * Extract ligands from the selected proteins.
        * Align sequences of selected proteins to the first.
        * Superpose selected proteins to the first.
        * Add surface with specified settings to active site.
        * Change the display style for ligands to the specified style and focus on them.
        * Enable the clip planes.
        * Hide the ribbons for the selected proteins.
"""
import os
from PySide2 import QtWidgets

from cresset import flare


@flare.extension
class PrepAndAlignExtension:
    """Add a button to the ribbon which runs the "Prep And Align" and "Ligand View" workflow."""

    def __init__(self):
        """Create the extension."""
        self.surface_type_combo_box_index = 1
        self.surface_quality_combo_box_index = 0
        self.display_style_combo_box_index = 4
        self.z_clipping_check_box_checked = False

    def load(self):
        """Load the extension."""
        tab = flare.main_window().ribbon["Extensions"]
        tab.tip_key = "X"
        control = tab["Protein"].add_button("Prep and Align", self._prep_and_align)
        control.tooltip = (
            "Prep, align, superpose, extract ligands then "
            + "add a molecular surface to selected proteins."
        )
        control.load_icon(
            os.path.join(os.path.dirname(os.path.realpath(__file__)), "prep_and_align.png")
        )
        control.tip_key = "A"

        control = tab["Protein"].add_button("Ligand View", self._ligand_view)
        control.tooltip = "Align, superpose and extract ligands."
        control.load_icon(
            os.path.join(os.path.dirname(os.path.realpath(__file__)), "Ligand-view.png")
        )
        control.tip_key = "L"

        print(f"Loaded {self.__class__.__module__}.{self.__class__.__name__}")

    def load_settings(self, settings):
        """Read the dialog configuration from the settings."""
        self.surface_type_combo_box_index = settings.get(
            "surface_type_combo_box_index", self.surface_type_combo_box_index
        )
        self.surface_quality_combo_box_index = settings.get(
            "surface_quality_combo_box_index", self.surface_quality_combo_box_index
        )
        self.display_style_combo_box_index = settings.get(
            "display_style_combo_box_index", self.display_style_combo_box_index
        )
        self.z_clipping_check_box_checked = settings.get(
            "z_clipping_check_box_checked", self.z_clipping_check_box_checked
        )

    def save_settings(self, settings):
        """Save the dialog configuration to the settings."""
        settings["surface_type_combo_box_index"] = self.surface_type_combo_box_index
        settings["surface_quality_combo_box_index"] = self.surface_quality_combo_box_index
        settings["display_style_combo_box_index"] = self.display_style_combo_box_index
        settings["z_clipping_check_box_checked"] = self.z_clipping_check_box_checked

    def _prep_and_align(self):
        """Align, superpose, prepares and adds surfaces to selected proteins in the project."""
        project = flare.main_window().project

        parent = flare.main_window().widget()
        selected_proteins = flare.main_window().selected_proteins
        if not selected_proteins:
            QtWidgets.QMessageBox.critical(
                parent, "Proteins Error", "One or more proteins must be selected."
            )
            return

        self._prep(selected_proteins)
        self._extract_ligands(project, selected_proteins)
        self._align_all(project, selected_proteins)
        self._overlay(project, selected_proteins)
        self._show_ligands(selected_proteins)
        self._center_on_ligands(selected_proteins)
        self._add_active_site_surface(selected_proteins)

        QtWidgets.QMessageBox.information(parent, "Complete", "Complete.")

    def _ligand_view(self):
        """Show the config dialog, then align, superpose and add surfaces to selected proteins."""
        project = flare.main_window().project

        parent = flare.main_window().widget()
        selected_proteins = flare.main_window().selected_proteins
        if not selected_proteins:
            QtWidgets.QMessageBox.critical(
                parent, "Proteins Error", "One or more proteins must be selected."
            )
            return

        surface_type, surface_quality, display_style, z_clipping = self._show_configuration_dialog()
        if (
            surface_type is None
            or surface_quality is None
            or display_style is None
            or z_clipping is None
        ):
            # Cancel was pressed
            return

        self._extract_ligands(project, selected_proteins)
        self._align_all(project, selected_proteins)
        self._overlay(project, selected_proteins)
        self._show_ligands(selected_proteins, display_style)
        self._adjust_clip_planes(z_clipping)
        self._center_on_ligands(selected_proteins)
        self._add_active_site_surface(selected_proteins, surface_type, surface_quality)
        self._hide_ribbon(selected_proteins)

        QtWidgets.QMessageBox.information(parent, "Complete", "Complete.")

    def _show_configuration_dialog(self):
        """Show the config dialog."""
        parent_widget = flare.main_window().widget()
        dialog = QtWidgets.QDialog(parent_widget)

        button_box = QtWidgets.QDialogButtonBox(
            QtWidgets.QDialogButtonBox.Ok | QtWidgets.QDialogButtonBox.Cancel, dialog
        )

        button_box.accepted.connect(dialog.accept)
        button_box.rejected.connect(dialog.reject)

        surface_type = QtWidgets.QComboBox(dialog)
        surface_type.addItem("Solid")
        surface_type.addItem("Wireframe")
        surface_type.setCurrentIndex(self.surface_type_combo_box_index)
        surface_type.setToolTip("The polygon mode used to display the active site surfaces.")

        surface_quality = QtWidgets.QComboBox(dialog)
        surface_quality.addItem("Low")
        surface_quality.addItem("Medium")
        surface_quality.addItem("High")
        surface_quality.addItem("Very High")
        surface_quality.setCurrentIndex(self.surface_quality_combo_box_index)
        surface_quality.setToolTip("The display quality for the active site surfaces.")

        display_style = QtWidgets.QComboBox(dialog)
        display_style.addItem("Lines")
        display_style.addItem("Thin Stick")
        display_style.addItem("Capped Stick")
        display_style.addItem("Ball and Stick")
        display_style.addItem("CPK")
        display_style.setCurrentIndex(self.display_style_combo_box_index)
        display_style.setToolTip("The style used to display the ligands.")

        z_clipping = QtWidgets.QCheckBox(dialog)
        z_clipping.setChecked(self.z_clipping_check_box_checked)
        z_clipping.setToolTip(
            "If checked, the front clip plane is adjusted to allow a better view into the active "
            "site surfaces. Otherwise, all of the atoms and surfaces are visible."
        )

        form_layout = QtWidgets.QFormLayout()
        form_layout.addRow("Surface Polygon Mode:", surface_type)
        form_layout.addRow("Surface Quality:", surface_quality)
        form_layout.addRow("Ligand Display Style:", display_style)
        form_layout.addRow("Enable Z-Clipping:", z_clipping)

        layout = QtWidgets.QVBoxLayout(dialog)
        layout.addLayout(form_layout)
        layout.addStretch(1)
        layout.addWidget(button_box)

        surf_type = None
        surf_quality = None
        disp_style = None
        z_clip = None

        # Show the dialog and get the data out of the dialog if OK was pressed.
        if dialog.exec_():
            surf_type = surface_type.currentText()
            surf_quality = surface_quality.currentText()
            disp_style = display_style.currentText()
            z_clip = z_clipping.isChecked()

            self.surface_type_combo_box_index = surface_type.currentIndex()
            self.surface_quality_combo_box_index = surface_quality.currentIndex()
            self.display_style_combo_box_index = display_style.currentIndex()
            self.z_clipping_check_box_checked = z_clipping.isChecked()

        dialog.deleteLater()
        return surf_type, surf_quality, disp_style, z_clip

    @staticmethod
    def _prep(selected_proteins):
        """Prepare selected proteins."""
        prep = flare.ProteinPrep()

        prep.proteins = [protein for protein in selected_proteins if not protein.is_prepared]
        if not prep.proteins:
            return

        if prep.setup_errors():
            raise Exception("\n".join(prep.setup_errors()))

        prep.start()
        prep.wait()  # Block until done or error

        if prep.errors():
            raise Exception("\n".join(prep.errors()))

    @staticmethod
    def _extract_ligands(project, selected_proteins):
        """Extract ligands from selected proteins."""
        for protein in selected_proteins:
            ligand_seq = [
                seq for seq in protein.sequences if seq.type == flare.Sequence.Type.Ligand
            ]

            # Copy each ligand residue as a ligand
            for seq in ligand_seq:
                for residue in seq:
                    project.ligands.append(residue)

            # Removed the ligand residue from the protein
            protein.sequences.remove(ligand_seq)

    @staticmethod
    def _align_all(project, selected_proteins):
        """Align the sequences."""
        # Set the alignment group for the first chain of each protein
        for protein in project.proteins:
            if protein.sequences:
                seq = protein.sequences[0]
                if seq.type == flare.Sequence.Type.Natural and protein in selected_proteins:
                    seq.alignment_group = 1
                else:
                    seq.alignment_group = 0

        # Now align the proteins
        project.proteins.align()

    @staticmethod
    def _overlay(project, selected_proteins):
        """Superpose selected proteins on the first one."""
        # Do the overlay - operates on each pair of proteins with the first protein as reference
        rms_values = []
        ref_protein = selected_proteins[0]
        for protein in selected_proteins[1:]:
            # Find the alpha carbons of the 2 proteins.
            # The superpose will be done using the location of these atoms
            ref_protein_alpha_carbons = []
            protein_alpha_carbons = []
            for residue1, residue2 in zip(ref_protein.sequences[0], protein.sequences[0]):
                # Skip Gaps
                if residue1 is not None and residue2 is not None:
                    alpha_carbon1 = residue1.alpha_carbon()
                    alpha_carbon2 = residue2.alpha_carbon()

                    # Skip residues with missing alpha carbons
                    if alpha_carbon1 is not None and alpha_carbon2 is not None:
                        ref_protein_alpha_carbons.append(alpha_carbon1)
                        protein_alpha_carbons.append(alpha_carbon2)

            # Move the protein
            matrix, rms = flare.fit_atoms(protein_alpha_carbons, ref_protein_alpha_carbons)
            protein.apply_transform(matrix)
            rms_values.append((protein, rms))

            # Move the ligand which are linked to this protein
            for ligand in protein.ligands:
                ligand.apply_transform(matrix)

        if rms_values:
            header = f'Superimposed to "{ref_protein.title}" using alpha carbons'
            summary = []
            body = "Superimposing on all residues"
            for protein, rms in rms_values:
                summary.append(f'RMS: {rms:.3f} for "{protein.title}"')
            project.log.append(header, "\n".join(summary), body, selected_proteins)

    @staticmethod
    def _show_ligands(selected_proteins, display_style="Capped Stick"):
        """Select the ligands and display them in the specified style."""
        ligands = [ligand for protein in selected_proteins for ligand in protein.ligands]
        flare.main_window().selected_ligands = ligands

        if display_style == "Lines":
            style = flare.DisplayStyle.Line
        elif display_style == "Thin Stick":
            style = flare.DisplayStyle.CapStickThin
        elif display_style == "Capped Stick":
            style = flare.DisplayStyle.CapStick
        elif display_style == "Ball and Stick":
            style = flare.DisplayStyle.BallStick
        elif display_style == "CPK":
            style = flare.DisplayStyle.CPK
        else:
            raise ValueError("unrecognized molecule display style")
        for ligand in ligands:
            ligand.style.display_style = style

    @staticmethod
    def _center_on_ligands(selected_proteins):
        """Center the 3D display to the ligand."""
        ligands = [ligand for protein in selected_proteins for ligand in protein.ligands]
        camera = flare.main_window().camera
        camera.fit_to_window(ligands)

    @staticmethod
    def _add_active_site_surface(selected_proteins, surface_type="Solid", surface_quality="High"):
        """Add surfaces to selected proteins and focus on the ligand pocket."""
        ligands = [ligand for protein in selected_proteins for ligand in protein.ligands]
        for protein in selected_proteins:
            if protein.sequences and not protein.surfaces:
                active_site = flare.atom_pick.active_site(ligands, [protein], 3.0)

                s_quality = None
                if surface_quality == "Low":
                    s_quality = flare.SurfaceQuality.Low
                elif surface_quality == "Medium":
                    s_quality = flare.SurfaceQuality.Medium
                elif surface_quality == "High":
                    s_quality = flare.SurfaceQuality.High
                elif surface_quality == "Very High":
                    s_quality = flare.SurfaceQuality.VeryHigh

                surface = protein.surfaces.new(
                    flare.SurfaceType.Molecular,
                    [protein.sequences[0]],
                    active_site,
                    quality=s_quality,
                )

                if surface_type == "Wireframe":
                    surface.polygon_mode = flare.SurfacePolygonMode.Wireframe
                elif surface_type == "Solid":
                    surface.polygon_mode = flare.SurfacePolygonMode.Solid
                else:
                    raise ValueError("unrecognized surface polygon mode")

    @staticmethod
    def _adjust_clip_planes(z_clipping):
        """If z-clipping is enabled, set the front clip plane. Otherwise, reset the clip planes."""
        camera = flare.main_window().camera
        if z_clipping:
            camera.front_clip = -5
        else:
            camera.reset_clips()

    @staticmethod
    def _hide_ribbon(selected_proteins):
        """Hide the ribbons for the selected proteins so that the ligands can be seen properly."""
        for protein in selected_proteins:
            protein.style.ribbon_visible = False
