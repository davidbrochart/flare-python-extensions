# -*- coding: utf-8 -*-
# Copyright (C) 2023 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Adds buttons and menus which allow other Cresset products to be access from Flare.

Context Menu Items:
    Ligands table -> Send to Forge
        Open the selected molecules in Forge.
        This control is only shown if a Forge installation is detected.

    Ligands table -> Send to Spark
        Open the selected ligand to Spark.
        This menu item is only shown if a Spark installation is detected.
"""
import os
import sys
import pathlib
import tempfile
import shutil
import atexit

from PySide2 import QtGui, QtWidgets

from cresset import flare


def _get_exe_from_reg_hive(hive, product, binary, redirect):
    """Return the path to the `binary` for `product` based on the reg key.

    Returns
    -------
    str
       The path to the binary or None if the reg key was not found.
    """
    import winreg

    try:
        sam = winreg.KEY_READ
        if redirect:
            sam = sam | winreg.KEY_WOW64_32KEY

        with winreg.OpenKey(hive, f"SOFTWARE\\Cresset-BMD\\{product}", 0, sam) as key:
            value, reg_type = winreg.QueryValueEx(key, "Install_Dir")
            if reg_type == winreg.REG_SZ:
                exe_path = f"{value}\\{binary}.exe"
                if os.path.isfile(exe_path):
                    return exe_path

    except Exception:
        # The software is not installed
        pass

    return None


def _get_exe_from_reg(product, binary):
    r"""Return the path to the `binary` for `product` by searching the registry.

    First HKCU\SOFTWARE\Cresset-BMD\{product}\Install_Dir is checked, if it
    is not set then HKLM\SOFTWARE\Cresset-BMD\{product}\Install_Dir is checked.

    Returns
    -------
    str
       The path to the binary or None if the reg key was not found.
    """
    import winreg

    # Search for 64 bit installs
    path = _get_exe_from_reg_hive(winreg.HKEY_CURRENT_USER, product, binary, False)
    if path is None:
        path = _get_exe_from_reg_hive(winreg.HKEY_LOCAL_MACHINE, product, binary, False)

    # Search for 32 bit installs
    if path is None:
        path = _get_exe_from_reg_hive(winreg.HKEY_CURRENT_USER, product, binary, True)
    if path is None:
        path = _get_exe_from_reg_hive(winreg.HKEY_LOCAL_MACHINE, product, binary, True)

    return path


def _get_path_for_linux(product, binary):
    """Return the path to the `binary` for `product` in the neighbouring directory."""
    dir_path = pathlib.Path(os.path.dirname(os.path.abspath(sys.executable)))
    dir_path = dir_path / ".." / ".." / product / "bin" / binary
    if os.path.isfile(dir_path):
        return str(dir_path)
    return None


def _get_path_for_macos(product, binary):
    """Return the path to the `binary` for `product` in the neighbouring app."""
    dir_path = pathlib.Path(os.path.dirname(os.path.abspath(sys.executable)))
    dir_path = dir_path / ".." / ".." / ".." / f"{product}.app" / "Contents" / "MacOS" / binary
    if os.path.isfile(dir_path):
        return str(dir_path)
    return None


def find_exe(product, binary):
    """Return the path to the `binary` for `product`."""
    if sys.platform == "win32":
        return _get_exe_from_reg(product, binary)
    elif sys.platform == "darwin":
        return _get_path_for_macos(product, binary)
    return _get_path_for_linux(product, binary)


@flare.extension
class CressetIntegrationExtension:
    """Add controls to use functionality of other Cresset programs from Flare."""

    def __init__(self):
        self._temp_dir = None

        self._forge_path = find_exe("Forge", "Forge")
        self._spark_path = find_exe("Spark", "Spark")

    def load(self):
        """Load the extension."""
        # Only add the control if the program is installed
        if self._spark_path is not None or self._forge_path is not None:
            flare.callbacks.ligand_context_menu.add(self._ligand_menu)

        atexit.register(self._cleanup)

        print(f"Loaded {self.__class__.__module__}.{self.__class__.__name__}")

    def _cleanup(self):
        """Remove generated temp files."""
        if self._temp_dir is not None:
            shutil.rmtree(self._temp_dir)
            self._temp_dir = None

    def _send_to_forge(self):
        """Send the selected ligands and protein to Forge."""
        parent = flare.main_window().widget()
        proteins = flare.main_window().selected_proteins

        if len(proteins) > 1:
            QtWidgets.QMessageBox.critical(
                parent,
                "Protein Error",
                f"Only 1 protein can be sent, {len(proteins)} are selected.",
            )
            return

        self._cleanup()
        self._temp_dir = tempfile.mkdtemp()

        args = [f'"{self._forge_path}"', "--charge", "n", "--read-mode", "auto", "--training"]

        # Write the molecules being transfered to temp files
        if flare.main_window().selected_ligands:
            sdf_path = f"{str(self._temp_dir)}{os.sep}molecules.sdf"
            with open(sdf_path, "w") as file:
                self._write_file_without_fields(file, flare.main_window().selected_ligands, "sdf")
                args.append(sdf_path)

        if len(proteins) == 1:
            pdb_path = f"{str(self._temp_dir)}{os.sep}protein.sdf"
            with open(pdb_path, "w") as file:
                flare.write_file(file, [flare.main_window().selected_proteins[0]], "pdb")
                args.append("--protein")
                args.append(pdb_path)

        # Launch Forge
        os.spawnv(os.P_NOWAIT, self._forge_path, args)

    def _ligand_menu(self, menu, ligand):
        """Add a menu item to send a ligand to Spark."""
        if self._forge_path is not None:
            action = menu.addAction("Send to Forge")
            icon = QtGui.QIcon(_package_file("send_to_forge.png"))
            action.setIcon(icon)
            action.triggered.connect(self._send_to_forge)

        if self._spark_path is not None:
            ligands = flare.main_window().selected_ligands
            if len(ligands) == 1:
                action = menu.addAction("Send to Spark")
                icon = QtGui.QIcon(_package_file("send_to_spark.png"))
                action.setIcon(icon)
                action.triggered.connect(lambda: self._send_to_spark(ligands[0]))

    def _send_to_spark(self, ligand):
        """Send the `ligand` to Spark."""
        self._cleanup()
        self._temp_dir = tempfile.mkdtemp()

        args = [f'"{self._spark_path}"']

        # Write the molecules being transfered to temp files
        sdf_path = f"{str(self._temp_dir)}{os.sep}molecule.sdf"
        with open(sdf_path, "w") as file:
            self._write_file_without_fields(file, [ligand], "sdf")
            args.append(sdf_path)

        # Launch Spark
        os.spawnv(os.P_NOWAIT, self._spark_path, args)

    @staticmethod
    def _add_ligands_as_poses(ligand, poses):
        """Copy the `poses` into ligand `pose` list."""
        for pose in poses:
            new_pose = ligand.poses.append(pose)
            # Copy the properties too
            for key, value in pose.properties.items():
                try:
                    new_pose.properties[key].value = value.value
                except flare.StateError:
                    pass  # Skip columns which are read only

    @staticmethod
    def _write_file_without_fields(file, ligands, format):
        """Write the ligands to the file without any field points.

        As Spark uses a different algorithm to Flare for generating field points it is
        necessary to remove the field points so Spark regenerates the field points using its
        algorithm.
        """
        project = flare.Project()
        project.ligands.extend(ligands)
        for ligand in project.ligands:
            ligand.field_points.clear()
        flare.write_file(file, project.ligands, format)


def _package_file(file_name):
    """Return the path to the file `file_name` in this package."""
    return os.path.join(os.path.dirname(os.path.realpath(__file__)), file_name)
