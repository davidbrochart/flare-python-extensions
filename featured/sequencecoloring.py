# -*- coding: utf-8 -*-
# Copyright (C) 2023 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Allows proteins atoms to be colored by the sequence alignment.

Ribbon Controls:
    Sequences -> Sequences View -> Color by Sequence
        Colors the selected proteins atoms by the sequence similarity to another protein.
"""
import itertools
from collections import namedtuple

from PySide2 import QtCore, QtGui, QtWidgets

from cresset import flare
from cressetfeaturedutils import ProgressDialog


@flare.extension
class SequenceColoringExtension:
    """Add a button color proteins by the sequence alignment."""

    BLOSUM62 = """
   A  R  N  D  C  Q  E  G  H  I  L  K  M  F  P  S  T  W  Y  V  B  Z  X  *
A  4 -1 -2 -2  0 -1 -1  0 -2 -1 -1 -1 -1 -2 -1  1  0 -3 -2  0 -2 -1  0 -4
R -1  5  0 -2 -3  1  0 -2  0 -3 -2  2 -1 -3 -2 -1 -1 -3 -2 -3 -1  0 -1 -4
N -2  0  6  1 -3  0  0  0  1 -3 -3  0 -2 -3 -2  1  0 -4 -2 -3  3  0 -1 -4
D -2 -2  1  6 -3  0  2 -1 -1 -3 -4 -1 -3 -3 -1  0 -1 -4 -3 -3  4  1 -1 -4
C  0 -3 -3 -3  9 -3 -4 -3 -3 -1 -1 -3 -1 -2 -3 -1 -1 -2 -2 -1 -3 -3 -2 -4
Q -1  1  0  0 -3  5  2 -2  0 -3 -2  1  0 -3 -1  0 -1 -2 -1 -2  0  3 -1 -4
E -1  0  0  2 -4  2  5 -2  0 -3 -3  1 -2 -3 -1  0 -1 -3 -2 -2  1  4 -1 -4
G  0 -2  0 -1 -3 -2 -2  6 -2 -4 -4 -2 -3 -3 -2  0 -2 -2 -3 -3 -1 -2 -1 -4
H -2  0  1 -1 -3  0  0 -2  8 -3 -3 -1 -2 -1 -2 -1 -2 -2  2 -3  0  0 -1 -4
I -1 -3 -3 -3 -1 -3 -3 -4 -3  4  2 -3  1  0 -3 -2 -1 -3 -1  3 -3 -3 -1 -4
L -1 -2 -3 -4 -1 -2 -3 -4 -3  2  4 -2  2  0 -3 -2 -1 -2 -1  1 -4 -3 -1 -4
K -1  2  0 -1 -3  1  1 -2 -1 -3 -2  5 -1 -3 -1  0 -1 -3 -2 -2  0  1 -1 -4
M -1 -1 -2 -3 -1  0 -2 -3 -2  1  2 -1  5  0 -2 -1 -1 -1 -1  1 -3 -1 -1 -4
F -2 -3 -3 -3 -2 -3 -3 -3 -1  0  0 -3  0  6 -4 -2 -2  1  3 -1 -3 -3 -1 -4
P -1 -2 -2 -1 -3 -1 -1 -2 -2 -3 -3 -1 -2 -4  7 -1 -1 -4 -3 -2 -2 -1 -2 -4
S  1 -1  1  0 -1  0  0  0 -1 -2 -2  0 -1 -2 -1  4  1 -3 -2 -2  0  0  0 -4
T  0 -1  0 -1 -1 -1 -1 -2 -2 -1 -1 -1 -1 -2 -1  1  5 -2 -2  0 -1 -1  0 -4
W -3 -3 -4 -4 -2 -2 -3 -2 -2 -3 -2 -3 -1  1 -4 -3 -2 11  2 -3 -4 -3 -2 -4
Y -2 -2 -2 -3 -2 -1 -2 -3  2 -1 -1 -2 -1  3 -3 -2 -2  2  7 -1 -3 -2 -1 -4
V  0 -3 -3 -3 -1 -2 -2 -3 -3  3  1 -2  1 -1 -2 -2  0 -3 -1  4 -3 -2 -1 -4
B -2 -1  3  4 -3  0  1 -1  0 -3 -4  0 -3 -3 -2  0 -1 -4 -3 -3  4  1 -1 -4
Z -1  0  0  1 -3  3  4 -2  0 -3 -3  1 -1 -3 -1  0 -1 -3 -2 -2  1  4 -1 -4
X  0 -1 -1 -1 -2 -1 -1 -1 -1 -1 -1 -1 -1 -1 -2  0  0 -2 -1 -1 -1 -1 -1 -4
* -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4  1
""".strip()

    WAC = """
   A  R  N  D  C  Q  E  G  H  I  L  K  M  F  P  S  T  W  Y  V  B  Z  X  *
A  4 -1  0 -2  0  0 -3 -1  0  0  0 -1  1  0  0  0  0  0  0  1 -4 -4 -4 -4
R -1  4  0 -1 -1  0 -1 -1  0 -2 -1  2  0 -2  0 -1  0  0 -1 -2 -4 -4 -4 -4
N  0  0  4  0  0  1  0  0  0 -1 -1  0  0 -1  0  1  1 -1  0 -1 -4 -4 -4 -4
D -2 -1  0  4 -2 -1  0 -2 -1 -4 -3 -1 -1 -4 -1 -1 -1 -3 -3 -3 -4 -4 -4 -4
C  0 -1  0 -2  4  0 -2  0  0  0  0 -2  1  0 -1  0  0  0  0  0 -4 -4 -4 -4
Q  0  0  1 -1  0  4  0  0  0  0  0  0  1  0  0  0  1  0  1 -1 -4 -4 -4 -4
E -3 -1  0  0 -2  0  4 -2 -1 -4 -3 -1 -1 -4 -1 -2 -1 -2 -4 -4 -4 -4 -4 -4
G -1 -1  0 -2  0  0 -2  4 -1 -2 -1 -1  0 -2  0  0  0 -2 -2 -2 -4 -4 -4 -4
H  0  0  0 -1  0  0 -1 -1  4 -1 -1  0  0  0  0 -1 -1  0  0 -1 -4 -4 -4 -4
I  0 -2 -1 -4  0  0 -4 -2 -1  4  1 -2  2  0  0 -1  0  0  0  2 -4 -4 -4 -4
L  0 -1 -1 -3  0  0 -3 -1 -1  1  4 -2  2  1  0 -1  0  1  0  1 -4 -4 -4 -4
K -1  2  0 -1 -2  0 -1 -1  0 -2 -2  4  0 -2  0  0  0 -2 -2 -3 -4 -4 -4 -4
M  1  0  0 -1  1  1 -1  0  0  2  2  0  4  2  1  0  0  2  0  1 -4 -4 -4 -4
F  0 -2 -1 -4  0  0 -4 -2  0  0  1 -2  2  4  0 -2 -2  2  1  0 -4 -4 -4 -4
P  0  0  0 -1 -1  0 -1  0  0  0  0  0  1  0  4  0  0  0 -1  0 -4 -4 -4 -4
S  0 -1  1 -1  0  0 -2  0 -1 -1 -1  0  0 -2  0  4  1 -1 -1 -1 -4 -4 -4 -4
T  0  0  1 -1  0  1 -1  0 -1  0  0  0  0 -2  0  1  4 -1 -1  0 -4 -4 -4 -4
W  0  0 -1 -3  0  0 -2 -2  0  0  1 -2  2  2  0 -1 -1  4  1  0 -4 -4 -4 -4
Y  0 -1  0 -3  0  1 -4 -2  0  0  0 -2  0  1 -1 -1 -1  1  4  0 -4 -4 -4 -4
V  1 -2 -1 -3  0 -1 -4 -2 -1  2  1 -3  1  0  0 -1  0  0  0  4 -4 -4 -4 -4
B -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4  4 -4 -4 -4
Z -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4  4 -4 -4
X -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4  4 -4
* -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4  4
""".strip()

    IDENT = """
   A  R  N  D  C  Q  E  G  H  I  L  K  M  F  P  S  T  W  Y  V  B  Z  X  *
A  4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4
R -4  4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4
N -4 -4  4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4
D -4 -4 -4  4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4
C -4 -4 -4 -4  4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4
Q -4 -4 -4 -4 -4  4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4
E -4 -4 -4 -4 -4 -4  4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4
G -4 -4 -4 -4 -4 -4 -4  4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4
H -4 -4 -4 -4 -4 -4 -4 -4  4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4
I -4 -4 -4 -4 -4 -4 -4 -4 -4  4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4
L -4 -4 -4 -4 -4 -4 -4 -4 -4 -4  4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4
K -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4  4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4
M -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4  4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4
F -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4  4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4
P -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4  4 -4 -4 -4 -4 -4 -4 -4 -4 -4
S -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4  4 -4 -4 -4 -4 -4 -4 -4 -4
T -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4  4 -4 -4 -4 -4 -4 -4 -4
W -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4  4 -4 -4 -4 -4 -4 -4
Y -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4  4 -4 -4 -4 -4 -4
V -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4  4 -4 -4 -4 -4
B -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4  4 -4 -4 -4
Z -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4  4 -4 -4
X -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4  4 -4
* -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4  4
""".strip()

    Settings = namedtuple("Settings", ["reference", "matrix", "high_color", "low_color"])

    def __init__(self):
        self._low_color = None
        self._high_color = None
        self._matrix_name = None

    def load(self):
        """Load the extension."""
        tab = flare.main_window().ribbon["Sequences"]
        control = tab["Sequences View"].add_button("Color by Sequence", self._color_by_sequence)
        control.size = flare.UIRibbonControl.Size.Small
        control.tooltip = (
            "Colors the selected proteins by similarity to "
            + "another protein using the sequence alignment."
        )
        control.tip_key = "EC"

        print(f"Loaded {self.__class__.__module__}.{self.__class__.__name__}")

    def load_settings(self, settings):
        """Read the settings from the config."""
        self._low_color = settings.get("low_color", (1.0, 0.0, 0.0))
        self._high_color = settings.get("high_color", (0.0, 1.0, 0.0))
        self._matrix_name = settings.get("matrix_name", "")

    def save_settings(self, settings):
        """Save the settings to the config."""
        settings["low_color"] = self._low_color
        settings["high_color"] = self._high_color
        settings["matrix_name"] = self._matrix_name

    def _color_by_sequence(self):
        """Show the configuration dialog then colors the proteins."""
        selected_proteins = flare.main_window().selected_proteins
        parent = flare.main_window().widget()

        if not selected_proteins:
            QtWidgets.QMessageBox.critical(
                parent, "Color Sequence", "Select the proteins to color."
            )
            return

        settings = self._show_coloring_dialog()
        if settings is None:
            return  # Canceled

        self._color_proteins_by_sequence(selected_proteins, settings)

    @classmethod
    def _color_proteins_by_sequence(cls, proteins, settings):
        """Color `proteins` by their sequence alignment to `reference`.

        For each protein in `proteins` each residue is compared with the corresponding residue
        in the `settings.reference` protein based on the sequence alignment. The residues are
        compared using 'settings.matrix' and a score is generated. This score is converted to a
        color between `settings.low_color` and `settings.high_color`.
        """
        headings, matrix = cls._parse_matrix(settings.matrix)
        min_score, max_score = cls._max_min(matrix)

        # Get the reference protein sequences which we will be aligning to
        ref_sequences = {}
        for ref_sequence in cls._natural_aligned_sequences(settings.reference):
            ref_sequences[ref_sequence.alignment_group] = ref_sequence

        with ProgressDialog("Coloring Proteins") as progress_dialog:
            # For each protein sequence compare the residues with the reference
            # and calculate the color
            for i, protein in enumerate(proteins):
                progress_dialog.set_text(f"Coloring {protein.title}")
                progress_dialog.set_value(i)
                if progress_dialog.was_canceled():
                    break

                residue_color = {}

                for sequence in cls._natural_aligned_sequences(protein, ref_sequences.keys()):
                    # Get the corresponding sequence in the reference protein
                    ref_sequence = ref_sequences[sequence.alignment_group]

                    # Compare the residues between the sequences.
                    # Gaps in the sequence alignment appear as None values
                    for ref_res, res in itertools.zip_longest(ref_sequence, sequence):
                        if res is not None:  # Skip gaps
                            ref_char = cls._residue_char(headings, ref_res)
                            char = cls._residue_char(headings, res)
                            # Get the BLOSUM 62 score
                            score = matrix[(ref_char, char)]
                            color = cls._score_to_color(
                                min_score, max_score, settings.low_color, settings.high_color, score
                            )
                            residue_color[res] = color

                # Change the ribbon style so you can see the colors in the ribbon
                protein.style.ribbon_style = flare.RibbonStyle.Oval

                # Color the atoms by the residue score
                with protein.batch_edit():
                    for atom in protein.atoms:
                        progress_dialog.set_value(i)
                        try:
                            atom.style.color = residue_color[atom.residue]
                        except KeyError:
                            pass  # Skip atoms which do not have a score

    @staticmethod
    def _natural_aligned_sequences(protein, groups=None):
        """Return natural sequences in `protein` which have been aligned.

        Parameters
        ----------
        groups : list(int), optional
            If not None then only sequences in one of these groups are returned.
        """
        sequences = []
        for sequence in protein.sequences:
            if sequence.type == flare.Sequence.Type.Natural:
                group = sequence.alignment_group
                if sequence.alignment_group != 0 and (groups is None or group in groups):
                    sequences.append(sequence)
        return sequences

    @staticmethod
    def _score_to_color(min_score, max_score, low_color, high_color, score):
        """Convert a `score` to a color.

        Returns
        -------
        tuple
            A tuple of RGB values each in the range 0.0 to 1.0.
        """
        diff = max_score - min_score
        n_score = (score - min_score) / diff
        i_n_score = 1.0 - n_score
        red = (i_n_score * low_color[0]) + (n_score * high_color[0])
        green = (i_n_score * low_color[1]) + (n_score * high_color[1])
        blue = (i_n_score * low_color[2]) + (n_score * high_color[2])
        return (red, green, blue)

    @staticmethod
    def _from_qcolor(color):
        """Convert a QtGui.QColor object to tuple.

        Parameters
        ----------
        color : QtGui.QColor
            A Qt color object.

        Returns
        -------
        tuple
            A tuple of RGB values each in the range 0.0 to 1.0.
        """
        red = color.redF()
        green = color.greenF()
        blue = color.blueF()
        return (red, green, blue)

    @staticmethod
    def _to_qcolor(color):
        """Convert a tuple to a QtGui.QColor.

        Parameters
        ----------
        color : tuple
            A tuple of RGB values each in the range 0.0 to 1.0.

        Returns
        -------
        QtGui.QColor
            A Qt color object.
        """
        return QtGui.QColor.fromRgbF(color[0], color[1], color[2])

    @staticmethod
    def _residue_char(headings, residue):
        """Convert the residue to the name of a column for the matrix.

        If the residue is not supported by the matrix then '*' is returned.
        """
        char = residue.one_letter_code() if residue is not None else "*"
        if char not in headings:
            char = "*"
        return char

    @staticmethod
    def _max_min(matrix):
        """Return tuple of the min and max score in the matrix."""
        min_score = max_score = list(matrix.values())[0]
        for score in matrix.values():
            min_score = min(min_score, score)
            max_score = max(max_score, score)
        return min_score, max_score

    @staticmethod
    def _parse_matrix(matrix):
        """Convert the matrix into a dict object.

        Returns
        -------
        dict
            The matrix in the format of tuple(row_header,col_header) -> score.
        """
        matrix_data = {}
        rows = matrix.split("\n")
        headings = rows[0].split()
        for row in rows[1:]:
            cells = row.split()
            row_heading = cells[0]
            for col_heading, cell in zip(headings, cells[1:]):
                matrix_data[(col_heading, row_heading)] = int(cell)
        return headings, matrix_data

    def _show_coloring_dialog(self):
        """Display a dialog for setting the reference protein and coloring options.

        Returns
        -------
        cresset.flare.Protein
            The reference protein.
        tuple
            The color to use for low scoring residues as a RGB tuple.
        tuple
            The color to use for high scoring residues as a RGB tuple.
        """
        # Create the settings dialog
        main_window = flare.main_window()
        project = flare.main_window().project

        dialog = QtWidgets.QDialog(main_window.widget())

        matrix = QtWidgets.QComboBox()
        matrix.addItem("Ident", self.IDENT)
        matrix.addItem("BLOSUM62", self.BLOSUM62)
        matrix.addItem("WAC", self.WAC)

        # Select the last used matrix
        index = matrix.findText(self._matrix_name)
        if index >= 0:
            matrix.setCurrentIndex(index)

        reference_protein = QtWidgets.QComboBox()
        protein_names = [protein.title for protein in project.proteins]
        reference_protein.addItems(protein_names)

        # A bar which shows the color range
        color_bar = ColorBoxWidget(
            self._to_qcolor(self._low_color), self._to_qcolor(self._high_color)
        )

        low_button = QtWidgets.QPushButton("Low Score...")
        low_button.clicked.connect(lambda: self._set_low_color(color_bar))

        high_button = QtWidgets.QPushButton("High Score...")
        high_button.clicked.connect(lambda: self._set_high_color(color_bar))

        buttons = QtWidgets.QDialogButtonBox(
            QtWidgets.QDialogButtonBox.Ok | QtWidgets.QDialogButtonBox.Cancel
        )
        buttons.accepted.connect(dialog.accept)
        buttons.rejected.connect(dialog.reject)

        # Layout out the widget
        color_layout = QtWidgets.QHBoxLayout()
        color_layout.addWidget(low_button)
        color_layout.addWidget(color_bar, 1)
        color_layout.addWidget(high_button)

        form_layout = QtWidgets.QFormLayout()
        form_layout.addRow("Matrix:", matrix)
        form_layout.addRow("Reference Protein:", reference_protein)
        form_layout.addRow("Color:", color_layout)

        layout = QtWidgets.QVBoxLayout(dialog)
        layout.addLayout(form_layout)
        layout.addLayout(color_layout)
        layout.addStretch(1)
        layout.addWidget(buttons)

        settings = None

        # Show the dialog, get the settings if Ok was pressed
        if dialog.exec_():
            settings = self.Settings(
                reference=project.proteins[reference_protein.currentIndex()],
                low_color=self._from_qcolor(color_bar.low_color()),
                high_color=self._from_qcolor(color_bar.high_color()),
                matrix=matrix.currentData(),
            )
            self._matrix_name = matrix.currentText()
            self._low_color = settings.low_color
            self._high_color = settings.high_color

        dialog.deleteLater()
        return settings

    @staticmethod
    def _show_color_dialog(color):
        """Show a dialog for picking a color.

        Parameters
        ----------
        color : tuple
            The initial color to show in the dialog as a
            tuple of RGB values each in the range 0.0 to 1.0.

        Returns
        -------
        tuple
            The color the user picked as a RGB tuple. If the dialog was
            canceled then the `color` is returned.
        """
        parent = flare.main_window().widget()
        color_dialog = QtWidgets.QColorDialog(color, parent)
        if color_dialog.exec_():
            color = color_dialog.currentColor()
        color_dialog.deleteLater()
        return color

    def _set_low_color(self, color_bar):
        """Show a dialog for picking the low score color then update `color_bar`."""
        color_bar.set_low_color(self._show_color_dialog(color_bar.low_color()))

    def _set_high_color(self, color_bar):
        """Show a dialog for picking the high score color then update `color_bar`."""
        color_bar.set_high_color(self._show_color_dialog(color_bar.high_color()))


class ColorBoxWidget(QtWidgets.QFrame):
    """A widget which shows a linear gradient color."""

    def __init__(self, low_color, high_color, parent=None):
        """Create the widget. The bar will show `low_color` and `high_color`.

        Parameters
        ----------
        low_color : QtGui.QColor
            The color to use for low scoring residues.

        high_color : QtGui.QColor
            The color to use for high scoring residues.
        """
        super().__init__(parent)
        self._low_color = low_color
        self._high_color = high_color
        self.setFrameStyle(QtWidgets.QFrame.Box)

    def paintEvent(self, event):
        """Paint the linear gradient color."""
        painter = QtGui.QPainter()
        painter.begin(self)

        gradient = QtGui.QLinearGradient(event.rect().topLeft(), event.rect().topRight())
        gradient.setColorAt(0, self._low_color)
        gradient.setColorAt(1, self._high_color)
        painter.fillRect(event.rect(), gradient)

        painter.end()

        # Draw the frame over the gradient
        super().paintEvent(event)

    def sizeHint(self):
        """Return the default size of the dialog."""
        # Expand the size so the color bar is visible.
        hint = super().sizeHint()
        return hint.expandedTo(QtCore.QSize(300, 0))

    def low_color(self):
        """Return the low scoring color.

        Returns
        -------
        QtGui.QColor
            The low scoring color.
        """
        return self._low_color

    def set_low_color(self, score):
        """Set the low scoring color.

        Parameters
        ----------
        score : QtGui.QColor
            The color.
        """
        self._low_color = score
        self.update()

    def high_color(self):
        """Return the high scoring color.

        Returns
        -------
        QtGui.QColor
            The high scoring color.
        """
        return self._high_color

    def set_high_color(self, score):
        """Set the high scoring color.

        Parameters
        ----------
        score : QtGui.QColor
            The color.
        """
        self._high_color = score
        self.update()
