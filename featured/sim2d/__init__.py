# -*- coding: utf-8 -*-
# Copyright (C) 2023 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Calculates the 2D similarity of all ligands to the selected ligands.

Ribbon Controls:
    Extensions -> Ligand -> Calculate 2D Sim
        Calculates the 2D Sim between the selected ligands and all the other ligands.
        A "2D Sim" column will be added to the Ligands table with the results.
"""
import statistics

from PySide2 import QtWidgets

from cresset import flare

from rdkit import Chem, DataStructs
from rdkit.Chem.AtomPairs import Pairs, Torsions
from rdkit.Chem import AllChem, MACCSkeys


@flare.extension
class Calculate2DSimExtension:
    """Add a button to the ribbon which calculates the 2D Sim."""

    def __init__(self):
        parent_widget = flare.main_window().widget()
        self._dialog = QtWidgets.QDialog(parent_widget)

        button_box = QtWidgets.QDialogButtonBox(
            QtWidgets.QDialogButtonBox.Ok | QtWidgets.QDialogButtonBox.Cancel, self._dialog
        )

        button_box.accepted.connect(self._dialog.accept)
        button_box.rejected.connect(self._dialog.reject)

        self._tanimoto = QtWidgets.QRadioButton("Topological (Tanimoto)", self._dialog)
        self._dice = QtWidgets.QRadioButton("Topological (Dice)", self._dialog)
        self._maccs = QtWidgets.QRadioButton("MACCS 166 keys (Dice)", self._dialog)
        self._atom_pairs = QtWidgets.QRadioButton("Atom Pairs (Dice)", self._dialog)
        self._topological_torsion = QtWidgets.QRadioButton(
            "Topological Torsion (Dice)", self._dialog
        )
        self._morgan_2 = QtWidgets.QRadioButton(
            "Morgan Fingerprints - Radius 2 (Dice)", self._dialog
        )
        self._morgan_4 = QtWidgets.QRadioButton(
            "Morgan Fingerprints - Radius 4 (Dice)", self._dialog
        )

        self._tanimoto.setChecked(True)

        self._message_label = QtWidgets.QLabel("", self._dialog)
        self._message_label.setWordWrap(True)

        layout = QtWidgets.QVBoxLayout(self._dialog)
        layout.addWidget(self._message_label)
        layout.addWidget(self._tanimoto)
        layout.addWidget(self._dice)
        layout.addWidget(self._maccs)
        layout.addWidget(self._atom_pairs)
        layout.addWidget(self._topological_torsion)
        layout.addWidget(self._morgan_2)
        layout.addWidget(self._morgan_4)
        layout.addStretch(1)
        layout.addWidget(button_box)

    def load(self):
        """Load the extension."""
        tab = flare.main_window().ribbon["Extensions"]
        tab.tip_key = "X"
        group = tab["Ligand"]

        control = group.add_button("Calculate 2D Sim", self._show_dialog)
        control.tooltip = (
            "Calculate the 2D similarity of all ligands to the selected ligands.\n"
            + "When selecting multiple ligands, the 2D similarity will be calculated as the "
            + "average of the similarity values towards each selected ligand.\n"
            + "A '2D Sim' column will be added to the ligand table with the results."
        )
        control.tip_key = "2"

        print(f"Loaded {self.__class__.__module__}.{self.__class__.__name__}")

    def _show_dialog(self):
        """Show a dialog asking for the 2D sim method to use."""
        main_window = flare.main_window()
        references = main_window.selected_ligands

        if len(references) == 0:
            QtWidgets.QMessageBox.critical(
                main_window.widget(), "Error", "One or more ligands must be selected."
            )
            return

        project = main_window.project
        message = (
            f"Calculate the 2D similarity of all {len(project.ligands)} ligands to the "
            + f"selected {len(references)} reference ligand(s). "
            + 'The results will be placed in the "2D Sim" column.'
        )
        self._message_label.setText(message)

        # Show the dialog and get the data out of the dialog if Ok was pressed.
        if self._dialog.exec_():
            sim_func = None
            if self._tanimoto.isChecked():
                sim_func = self._tanimoto_sim
            elif self._dice.isChecked():
                sim_func = self._dice_sim
            elif self._maccs.isChecked():
                sim_func = self._maccs_sim
            elif self._atom_pairs.isChecked():
                sim_func = self._atom_pairs_sim
            elif self._topological_torsion.isChecked():
                sim_func = self._topological_torsion_sim
            elif self._morgan_2.isChecked():
                sim_func = self._morgan_2_sim
            elif self._morgan_4.isChecked():
                sim_func = self._morgan_4_sim

            self._calculate_2d_sim(references, project.ligands, sim_func)

    @staticmethod
    def _tanimoto_sim(rd_mol1, rd_mol2):
        # https://www.rdkit.org/docs/GettingStartedInPython.html#topological-fingerprints
        fp1 = Chem.RDKFingerprint(rd_mol1)
        fp2 = Chem.RDKFingerprint(rd_mol2)
        return DataStructs.FingerprintSimilarity(fp1, fp2, metric=DataStructs.TanimotoSimilarity)

    @staticmethod
    def _dice_sim(rd_mol1, rd_mol2):
        # https://www.rdkit.org/docs/GettingStartedInPython.html#topological-fingerprints
        fp1 = Chem.RDKFingerprint(rd_mol1)
        fp2 = Chem.RDKFingerprint(rd_mol2)
        return DataStructs.FingerprintSimilarity(fp1, fp2, metric=DataStructs.DiceSimilarity)

    @staticmethod
    def _maccs_sim(rd_mol1, rd_mol2):
        # https://www.rdkit.org/docs/GettingStartedInPython.html#maccs-keys
        fp1 = MACCSkeys.GenMACCSKeys(rd_mol1)
        fp2 = MACCSkeys.GenMACCSKeys(rd_mol2)
        return DataStructs.FingerprintSimilarity(fp1, fp2)

    @staticmethod
    def _atom_pairs_sim(rd_mol1, rd_mol2):
        # https://www.rdkit.org/docs/GettingStartedInPython.html#atom-pairs-and-topological-torsions
        fp1 = Pairs.GetAtomPairFingerprint(rd_mol1)
        fp2 = Pairs.GetAtomPairFingerprint(rd_mol2)
        return DataStructs.DiceSimilarity(fp1, fp2)

    @staticmethod
    def _topological_torsion_sim(rd_mol1, rd_mol2):
        # https://www.rdkit.org/docs/GettingStartedInPython.html#atom-pairs-and-topological-torsions
        fp1 = Torsions.GetTopologicalTorsionFingerprintAsIntVect(rd_mol1)
        fp2 = Torsions.GetTopologicalTorsionFingerprintAsIntVect(rd_mol2)
        return DataStructs.DiceSimilarity(fp1, fp2)

    @staticmethod
    def _morgan_2_sim(rd_mol1, rd_mol2):
        # https://www.rdkit.org/docs/GettingStartedInPython.html#morgan-fingerprints-circular-fingerprints
        fp1 = AllChem.GetMorganFingerprint(rd_mol1, 2)
        fp2 = AllChem.GetMorganFingerprint(rd_mol2, 2)
        return DataStructs.DiceSimilarity(fp1, fp2)

    @staticmethod
    def _morgan_4_sim(rd_mol1, rd_mol2):
        # https://www.rdkit.org/docs/GettingStartedInPython.html#morgan-fingerprints-circular-fingerprints
        fp1 = AllChem.GetMorganFingerprint(rd_mol1, 4)
        fp2 = AllChem.GetMorganFingerprint(rd_mol2, 4)
        return DataStructs.DiceSimilarity(fp1, fp2)

    def _calculate_2d_sim(self, references, ligands, sim_func):
        """Calculate the 2D between the selected ligands and all other ligands."""
        rd_mol_references = [mol.to_rdmol() for mol in references]

        for ligand in ligands:
            sims = [sim_func(ref, ligand.to_rdmol()) for ref in rd_mol_references]
            sim = statistics.mean(sims)
            ligand.properties["2D Sim"].value = round(sim, 3)
