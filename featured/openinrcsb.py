# -*- coding: utf-8 -*-
# Copyright (C) 2023 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Adds functionality to show the RCSB web page for a protein.

Context Menu Items:
    Protein -> Show in RCSB
        Opens a new dialog which shows the RCSB web page for the protein.
"""
from PySide2 import QtCore, QtWidgets, QtWebEngineWidgets

from cresset import flare


@flare.extension
class OpenInRcsbExtension:
    """Adds a menu item to the protein context menu that shows the protein RCSB web page."""

    def load(self):
        """Load the extension."""
        flare.callbacks.protein_context_menu.add(self._protein_menu)

        print(f"Loaded {self.__class__.__module__}.{self.__class__.__name__}")

    def _protein_menu(self, menu, protein):
        """Add a item to the protein context menu to show the protein RCSB web page."""
        action = menu.addAction("Show in RCSB")
        url = f"https://www.rcsb.org/structure/{protein.pdb.id_code}"
        action.triggered.connect(lambda: self._show_web_page(url, protein.pdb.id_code))

    @staticmethod
    def _show_web_page(url, id_code):
        """Show the website in a new dialog."""
        window = flare.main_window().widget()

        # Create a dialog to show the web page
        # The dialog will be deleted when it is closed
        dialog = QtWidgets.QDialog(window)
        dialog.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        dialog.resize(640, 480)
        dialog.setWindowTitle(f"RCSB - {id_code}")

        # Load the web page
        view = QtWebEngineWidgets.QWebEngineView(dialog)
        view.load(url)

        # Add the web page to the dialog
        layout = QtWidgets.QVBoxLayout(dialog)
        layout.addWidget(view)

        dialog.show()
