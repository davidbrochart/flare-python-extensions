# -*- coding: utf-8 -*-
# Copyright (C) 2023 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Calculates the RMSD of ligands and poses using RDKit.

Ribbon Controls:
    Extensions -> Ligand -> Calculate RMSD
        Calculates the RMSD between the selected ligand or pose and all the other ligands and
        poses with the same structure. A "RMSD" column will be added to the Ligands table with
        the results.
"""
import os
import math

from PySide2 import QtWidgets

from cresset import flare


@flare.extension
class CalculateRMSDExtension:
    """Add a button to the ribbon which calculates the RMSD."""

    def load(self):
        """Load the extension."""
        tab = flare.main_window().ribbon["Extensions"]
        tab.tip_key = "X"
        group = tab["Ligand"]

        control = group.add_button("Calculate RMSD", self._calculate_rmsd)
        control.tooltip = (
            "Calculate the RMSD between the selected ligand or pose "
            + "and all other ligands and poses with the same structure. "
            + "A 'RMSD' column will be added to the ligand table with the results."
        )
        control.load_icon(
            os.path.join(os.path.dirname(os.path.realpath(__file__)), "calculate_rmsd.png")
        )
        control.tip_key = "U"

        print(f"Loaded {self.__class__.__module__}.{self.__class__.__name__}")

    def _calculate_rmsd(self):
        """Calculate the RMSD between the selected ligand/pose and all other ligands/poses."""
        # Find the selected reference or pose
        main_window = flare.main_window()
        selection = [*main_window.selected_ligands, *main_window.selected_poses]
        if len(selection) != 1:
            QtWidgets.QMessageBox.critical(
                main_window.widget(), "Error", "One ligand or pose must be selected."
            )
            return

        reference = selection[0]
        self._compute_rmsd_for_ligands(reference, main_window.project.ligands)

    @classmethod
    def _compute_rmsd_for_ligands(cls, reference, ligands):
        """Loop over all ligands and poses and calculate the RMDS to the reference.

        The result is stored in the in the "RMSD" property which will appear as a column
        in the ligand table.
        """
        reference_rdmol = reference.to_rdmol()

        for ligand in ligands:
            rmsd = cls._compute_rmsd(reference_rdmol, ligand.to_rdmol())
            if rmsd >= 0:
                ligand.properties["RMSD"].value = round(rmsd, 3)

            for pose in ligand.poses:
                rmsd = cls._compute_rmsd(reference_rdmol, pose.to_rdmol())
                if rmsd >= 0:
                    pose.properties["RMSD"].value = round(rmsd, 3)

    @staticmethod
    def _compute_rmsd(ref, prb):
        """Calculate the RMSD between 2 RDKit molecules.

        Parameters
        ----------
        ref : rdkit.Chem.rdchem.Mol
            The reference molecule.
        prb : rdkit.Chem.rdchem.Mol
            The molecule to compare to the reference.

        Returns
        -------
        float
            The RMSD value or -1 if the structures of the 2 molecules do not match.
        """
        matches = ref.GetSubstructMatches(prb, uniquify=False)
        rmsd_list = []
        ref_conf = ref.GetConformer()
        prb_conf = prb.GetConformer()
        for match in matches:
            length = len(match)
            if length != ref_conf.GetNumAtoms():
                return math.nan
            rmsd = 0.0
            for prb_index, ref_index in enumerate(match):
                ref_pos = ref_conf.GetAtomPosition(ref_index)
                prb_pos = prb_conf.GetAtomPosition(prb_index)
                rmsd += (ref_pos - prb_pos).LengthSq()
            rmsd /= float(length)
            rmsd_list.append(math.sqrt(rmsd))
        return min(rmsd_list) if rmsd_list else -1
