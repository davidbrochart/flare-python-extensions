# -*- coding: utf-8 -*-
# Copyright (C) 2023 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Predicts loop structures using PyFREAD and a specified database.

Ribbon Controls:
    Protein -> Structure -> Loop Modeling
        Predicts loop structures using the database search loop modeling
        method PyFREAD.

        The calculation can run in automatic or manual mode.

        Automatic mode runs on all selected proteins, all gaps in
        the protein structure are found and a new protein is created
        with the gaps filled in.

        Manual mode generates multiple loops for a single gap in a protein structure.
        The picked residue defines the start of the loop as
        the residue following the picked one. If more than one residue is
        picked, the residues between the first and last are defined as the loop
        sequence for replacement. The specified N best generated loop models are loaded into
        Flare as proteins. The anchor RMSD cutoff and substitution score cutoff
        parameters may also be adjusted.

        Databases can be downloaded from
        https://www.cresset-group.com/support/support-resources/data-files-flare-python-api-extensions/
        or generated using the scripts featured/loopmodeling/fread/fread_db_add.py and
        featured/loopmodeling/fread/fread_db_optimise.py in the extensions
        directory.
"""
import os
import sys
import threading
import tempfile
import shutil
import traceback
from pathlib import Path
from dataclasses import dataclass, field

from PySide2 import QtCore, QtUiTools, QtWidgets

from cresset import flare

PYFREAD_PATH = os.path.join(os.path.realpath(os.path.dirname(__file__)), "fread")
sys.path.insert(0, PYFREAD_PATH)
import pyfread  # noqa: E402

AMINO_ACID_CODES = "ACDEFGHIKLMNPQRSTVWY"


@flare.extension
class LoopModelingExtension(QtWidgets.QDialog):
    """Add a button to the ribbon for predicting loop structures."""

    def __init__(self, parent=None):
        """Create the extension."""
        super().__init__(parent)
        self.structure_directory = None
        self.loop_database_path = str(Path.home())
        self.loop_sequence = ""
        self.n_best_output_structures = 0  # All
        self.anchor_rmsd_cutoff = 1.0
        self.substitution_score_cutoff = 25
        self.auto_mode = True
        self.dialog = None
        self.progress = None
        self.project = None
        self.auto_mode_result_proteins = []
        self.auto_mode_canceled = False
        self.start_residue = None

    def load(self):
        """Load the extension."""
        control = (
            flare.main_window()
            .ribbon["Protein"]["Structure"]
            .add_button("Loop Modeling", self._do_loop_modeling)
        )
        control.size = flare.UIRibbonControl.Size.Small
        control.tooltip = (
            "Build loops using the database loop modeling method FREAD. In automatic mode "
            "a duplicate will be made of each selected protein with the gaps filled in. "
            "In manual mode the loop sequence for "
            "replacement is defined as starting after the first picked residue. If more than one "
            "residue is picked, the full sequence for replacement is defined as the residues "
            "between the first and last in the picked sequence. The loop sequence can be manually "
            "edited in the extension dialog."
        )
        control.tip_key = "L"
        control.load_icon(
            os.path.join(os.path.dirname(os.path.realpath(__file__)), "Build-loops-with-FREAD.png")
        )

        print(f"Loaded {self.__class__.__module__}.{self.__class__.__name__}")

    def load_settings(self, settings):
        """Read the loop database path from the settings."""
        self.loop_database_path = settings.get("loop_database_path", self.loop_database_path)
        self.n_best_output_structures = settings.get(
            "n_best_output_structures", self.n_best_output_structures
        )
        self.anchor_rmsd_cutoff = settings.get("anchor_rmsd_cutoff", self.anchor_rmsd_cutoff)
        self.substitution_score_cutoff = settings.get(
            "substitution_score_cutoff", self.substitution_score_cutoff
        )
        self.auto_mode = settings.get("auto_mode", self.auto_mode)

    def save_settings(self, settings):
        """Save the loop database path to the settings."""
        settings["loop_database_path"] = self.loop_database_path
        settings["n_best_output_structures"] = self.n_best_output_structures
        settings["anchor_rmsd_cutoff"] = self.anchor_rmsd_cutoff
        settings["substitution_score_cutoff"] = self.substitution_score_cutoff
        settings["auto_mode"] = self.auto_mode

    def _do_loop_modeling(self):
        """Show the config dialog, perform the loop prediction and load in loop models."""
        # If PyFREAD is already running reshow the progress dialog
        if self.progress is not None:
            self.progress.show()
            return

        self.auto_mode_result_proteins = []
        self.structure_directory = tempfile.mkdtemp()

        picked_residues = flare.main_window().picked_residues
        selected_proteins = flare.main_window().selected_proteins

        if len(picked_residues) < 1 and len(selected_proteins) == 0:
            QtWidgets.QMessageBox.critical(
                flare.main_window().widget(),
                "Error",
                "Either proteins must be selected or at least one residue must be picked. "
                "In manual mode the beginning of the loop is defined "
                "as the residue after the first picked residue, and the loop sequence for "
                "replacement is defined as those residues between the first and last picked "
                "residues, or alternatively can be typed into the dialog manually.",
            )
            return

        self.start_residue = None
        if len(picked_residues) >= 1:
            picked_residues = self._expand_past_caps(picked_residues)
            self.start_residue = picked_residues[0]
            self.loop_sequence = self._intermediate_residues(picked_residues)

        ok_clicked = self._show_configuration_dialog(selected_proteins)

        if ok_clicked:
            self.project = flare.main_window().project

            self.loop_sequence = self.loop_sequence.upper()
            if not self.auto_mode and not self._is_loop_sequence_valid():
                return

            self.progress = QtWidgets.QProgressDialog(
                "Loop Modeling", None, 0, 0, flare.main_window().widget()
            )
            self.progress.setModal(True)
            self.progress.setWindowFlags(
                self.progress.windowFlags() & ~QtCore.Qt.WindowCloseButtonHint
                | QtCore.Qt.CustomizeWindowHint
            )
            self.progress.show()

            thread = None
            # Start the loop modeling process on the background thread
            # As flare objects cannot be accessed on threads,
            # the data needs to be obtained and passed into the thread
            if self.auto_mode:
                self.auto_mode_canceled = False
                self.progress.setCancelButtonText("Cancel")

                def progress_canceled():
                    # QProgressDialog cancel button hides the dialog, but
                    # the calculation will keep running as PyFREAD cannot be stopped.
                    # When the current PyFREAD ends then the calculation will be stopped.
                    # The cancel button also hidden
                    self.progress.setCancelButtonText(None)
                    self.progress.setLabelText("Canceling")
                    self.progress.show()
                    self.auto_mode_canceled = True

                self.progress.canceled.connect(progress_canceled)

                auto_data = self._find_missing_loops(selected_proteins)
                thread = threading.Thread(
                    target=self._run_pyfread_auto_mode,
                    args=(auto_data,),
                )
            else:  # manual mode
                start_residue_number = str(self.start_residue.seq_num) + self.start_residue.icode
                start_residue_name = self.start_residue.name
                start_residue_chain = self.start_residue.chain

                protein = self.start_residue.molecule
                protein_title = protein.title
                input_pdb_path = self._write_input_pdb_file(protein)

                thread = threading.Thread(
                    target=self._run_pyfread_manual_mode,
                    args=(
                        self.start_residue,
                        protein_title,
                        input_pdb_path,
                        start_residue_name,
                        start_residue_chain,
                        start_residue_number,
                    ),
                )
            # Allow thread to be killed if Flare is closed as we can't cancel PyFREAD
            thread.daemon = True
            thread.start()

    def _expand_past_caps(self, picked_residues):
        """If caps are picked then expand the select beyond them.

        As PyFREAD ignores caps
        """
        if self._is_cap(picked_residues[0]):
            # If a cap is selected move one residue back so the cap is replaced
            index = picked_residues[0].sequence.index(picked_residues[0])
            if index > 0:
                residue = picked_residues[0].sequence[index - 1]
                picked_residues.insert(0, residue)

        if self._is_cap(picked_residues[-1]):
            # If a cap is selected move one residue forwards so the cap is replaced
            index = picked_residues[-1].sequence.index(picked_residues[-1])
            if 0 <= index < len(picked_residues[-1].sequence):
                residue = picked_residues[-1].sequence[index + 1]
                picked_residues.append(residue)

        return picked_residues

    def _intermediate_residues(self, picked_residues):
        """Return the residue sequence between the first and last picked residues."""
        protein = picked_residues[0].molecule
        loop_residue_sequence = ""
        loop_residue_gap_sequence = ""
        for seq_n in range(picked_residues[0].seq_num + 1, picked_residues[-1].seq_num):
            loop_residue = protein.residues.find(picked_residues[0].chain + " " + str(seq_n))
            if loop_residue:
                if loop_residue[0].alpha_carbon():
                    loop_residue_sequence += loop_residue[0].one_letter_code()
                else:
                    loop_residue_gap_sequence += loop_residue[0].one_letter_code()
            else:
                QtWidgets.QMessageBox.critical(
                    flare.main_window().widget(),
                    "Error",
                    "Unable to find loop sequence for selected residues.",
                )
                return
        if loop_residue_gap_sequence:
            loop_residue_sequence = loop_residue_gap_sequence
        return loop_residue_sequence

    def _show_configuration_dialog(self, selected_proteins):
        """Show the config dialog.

        The dialog asks for the loop database path, the loop sequence, the number of the best
        output structures to keep, the anchor RMSD cutoff and the substitution score cutoff,
        and sets the appropriate variables to the values obtained.
        """
        # Load the dialog from the .ui file. This file was created using Qt Designer.
        ui_file_path = os.path.join(
            os.path.dirname(os.path.realpath(__file__)), "LoopModelingConfigDialog.ui"
        )

        file = QtCore.QFile(ui_file_path)
        file.open(QtCore.QFile.ReadOnly)

        loader = QtUiTools.QUiLoader()
        parent = flare.main_window().widget()
        self.dialog = loader.load(file, parent)

        self.dialog.manual_mode.toggled.connect(self.dialog.manual_mode_group_box.setEnabled)
        self.dialog.manual_mode_group_box.setEnabled(self.dialog.manual_mode.isChecked())
        if self.start_residue is not None:
            self.dialog.manual_mode.setText(
                "Manual mode starting from residue " + str(self.start_residue)
            )

        # OK button is only enabled if both loop database path and loop sequence are not empty
        ok_button = self.dialog.buttonBox.button(QtWidgets.QDialogButtonBox.Ok)
        ok_button.setEnabled(False)

        self.dialog.loop_database_path.textChanged[str].connect(self._update_ok_button)
        self.dialog.loop_sequence.textChanged[str].connect(self._update_ok_button)
        self.dialog.auto_mode.toggled.connect(self._update_ok_button)

        # Set fields in dialog to previous values from settings
        self.dialog.loop_database_path.setText(self.loop_database_path)
        self.dialog.loop_sequence.setText(self.loop_sequence)
        self.dialog.n_best_output_structures.setValue(self.n_best_output_structures)
        self.dialog.anchor_rmsd_cutoff.setValue(self.anchor_rmsd_cutoff)
        self.dialog.substitution_score_cutoff.setValue(self.substitution_score_cutoff)
        self.dialog.auto_mode.setChecked(self.auto_mode)
        self.dialog.manual_mode.setChecked(not self.auto_mode)

        if self.start_residue is None:
            self.dialog.auto_mode.setChecked(True)
        self.dialog.manual_mode.setEnabled(self.start_residue is not None)

        if len(selected_proteins) == 0:
            self.dialog.manual_mode.setChecked(True)
        self.dialog.auto_mode.setEnabled(len(selected_proteins) > 0)

        self.dialog.browse_path_button.clicked.connect(self._open_file_browser)

        self._update_ok_button()

        # Show the dialog - if OK was pressed then get the required values
        ok_clicked = False
        if self.dialog.exec_():
            self.loop_database_path = self.dialog.loop_database_path.text()
            self.loop_sequence = self.dialog.loop_sequence.text()
            self.n_best_output_structures = self.dialog.n_best_output_structures.value()
            self.anchor_rmsd_cutoff = self.dialog.anchor_rmsd_cutoff.value()
            self.substitution_score_cutoff = self.dialog.substitution_score_cutoff.value()
            self.auto_mode = self.dialog.auto_mode.isChecked()
            ok_clicked = True

        self.dialog.deleteLater()

        return ok_clicked

    def _update_ok_button(self):
        """Enable or disable the ok button based on the dialog settings. Also
        check that the sequence is connected properly if in manual mode
        """
        ok_button = self.dialog.buttonBox.button(QtWidgets.QDialogButtonBox.Ok)
        ok_button.setEnabled(
            self.dialog.loop_database_path.text() != ""
            and (self.dialog.loop_sequence.text() != "" or self.dialog.auto_mode.isChecked())
        )

        if self.dialog.manual_mode.isChecked():
            err_message = self._check_manual_sequence()
            if err_message is not None:
                self.dialog.error_label.setText(err_message)
                ok_button.setEnabled(False)
            else:
                self.dialog.error_label.setText("")

    def _check_manual_sequence(self):
        """Check that the sequence is going to work. Returns None for success, and an error message
        otherwise.
        """
        nres = len(self.dialog.loop_sequence.text())
        r = self.start_residue
        if r.alpha_carbon() is None:
            return "Start residue is invalid: needs to contain an alpha carbon"
        # If the second residue is virtual (or nonexistent) then FREAD copes fine with that
        if r.next() is None or r.next().alpha_carbon() is None:
            return None
        for i in range(1, nres):
            r = r.next()
            if r is None or r.alpha_carbon() is None:
                return "FREAD requires that the picked residue be just before the gap to be filled."
        return None

    def _is_loop_sequence_valid(self):
        """Check whether the loop sequence contains valid amino acid codes."""
        for character in self.loop_sequence:
            if character not in AMINO_ACID_CODES:
                QtWidgets.QMessageBox.critical(
                    flare.main_window().widget(),
                    "Error",
                    "Invalid amino acid code entered.",
                )
                return False
        return True

    def _open_file_browser(self):
        """Open file browser and set loop database path in dialog to that selected."""
        parent = flare.main_window().widget()
        new_path = QtWidgets.QFileDialog.getExistingDirectory(
            parent, "Loop Database Path", self.dialog.loop_database_path.text()
        )
        if new_path:
            self.dialog.loop_database_path.setText(new_path)

    def _write_input_pdb_file(self, protein):
        """Write the input PDB file for PyFREAD and return its path."""
        input_pdb_path = os.path.join(self.structure_directory, protein.title + ".pdb")
        try:
            protein.write_file(input_pdb_path, "pdb")
        except OSError:
            QtWidgets.QMessageBox.critical(
                flare.main_window().widget(),
                "Error",
                "Unable to write protein to PDB file.",
            )
            return
        return input_pdb_path

    def _run_pyfread_manual_mode(
        self,
        start_residue,
        protein_title,
        input_pdb_path,
        start_residue_name,
        start_residue_chain,
        start_residue_number,
    ):
        """Run PyFREAD using specified arguments and call function to load in structures."""
        try:
            log_header = f'Loop Modeling of "{protein_title}"'

            col_width = 30
            settings_used = (
                "Loop database path:".ljust(col_width)
                + self.loop_database_path
                + "\n"
                + "Input PDB:".ljust(col_width)
                + protein_title
                + "\n"
                + "Pre-loop start residue:".ljust(col_width)
                + start_residue_name
                + " "
                + start_residue_number
                + "\n"
                + "Loop sequence:".ljust(col_width)
                + self.loop_sequence
                + "\n"
                + "Chain:".ljust(col_width)
                + start_residue_chain
                + "\n"
                + "Anchor RMSD cutoff:".ljust(col_width)
                + str(round(self.anchor_rmsd_cutoff, 1))
                + "\n"
                + "Substitution score cutoff:".ljust(col_width)
                + str(self.substitution_score_cutoff)
                + "\n"
            )

            messages, summary, command_line = self._run_pyfread(
                input_pdb_path,
                start_residue_number,
                self.loop_sequence,
                start_residue_chain,
                self.structure_directory,
                self.anchor_rmsd_cutoff,
                self.substitution_score_cutoff,
                protein_title,
            )

            log_comment = (
                settings_used
                + "\n"
                + "PyFREAD arguments:\n"
                + " ".join(command_line)
                + "\n\n"
                + "".join(messages)
            )

            # Write log message on main thread
            flare.invoke_and_wait(
                target=self._write_flare_log_message, args=(log_header, log_comment)
            )

            # Load in structures on main thread
            flare.invoke_and_wait(
                target=self._load_loop_model_pdb_files_into_project_manual_mode,
                args=(start_residue, summary),
            )
        except Exception as e:
            flare.invoke_and_wait(
                target=self._show_error_dialog, args=(str(e), traceback.format_exc())
            )
            raise
        finally:
            flare.invoke_and_wait(target=self._done)

    def _run_pyfread(
        self,
        input_pdb_path,
        start_residue_number,
        loop_sequence,
        input_protein_chain,
        structure_directory,
        anchor_rmsd_cutoff,
        substitution_score_cutoff,
        input_protein_name,
    ):
        """Run PyFREAD for the given gap in the given protein

        Parameters
        ----------
        input_pdb_path : str
            the path to the pdb file which contains the protein structure
        start_residue_number : str
            The residue number and the icode. For example 13 or 36A.
        loop_sequence : str
            The one letter codes of the residues which make up the gap. For example "IREISLLKELN".
        input_protein_chain : str
            The letter of the chain the protein is in.
        structure_directory : str
            The directory to store temp files
        anchor_rmsd_cutoff : float
            Set the anchor RMSD cut-off to.
        substitution_score_cutoff : int
            Set substitution score cut-off to specified integer.
            If the given sequence cannot possibly attain this cut-off score, the cut-off is lowered
            to allow perfect matches to be found
        input_protein_name : str
            The name of the protein

        Return
        ------
        messages : str
            The calculation log.
        summary : str
            Log containing the file paths to the result proteins.
        args : list(str)
            The args passed to PyFREAD.
        """
        summary_file_path = os.path.join(structure_directory, "summary.txt")
        message_file_path = os.path.join(structure_directory, "messages.txt")

        args = [
            self.loop_database_path,
            input_pdb_path,
            start_residue_number,
            loop_sequence,
            "--chain=" + input_protein_chain,
            "--strucdir=" + structure_directory,
            "--mutate",
            "--loop_sequence",
            "--open_rmsd=" + str(anchor_rmsd_cutoff),
            "--score=" + str(substitution_score_cutoff),
            "--decoy_name_prefix=" + input_protein_name + "_",
            "--summary=" + summary_file_path,
            "--verbose",
            "--messages=" + message_file_path,
        ]

        pyfread.main(args)

        messages = None
        try:
            with open(message_file_path, "r") as message_file:
                messages = message_file.readlines()
        except OSError:
            pass  # returning messages as None signals the error

        summary = None
        try:
            with open(summary_file_path, "r") as summary_file:
                summary = summary_file.readlines()
        except OSError:
            pass  # returning messages as None signals the error

        return (messages, summary, args)

    def _load_loop_model_pdb_files_into_project_manual_mode(self, start_residue, summary):
        """Load N best loop model PDB files generated by PyFREAD into project as proteins."""
        if not summary:
            self._show_no_models_error_message(summary)
            return

        log_header = f'Load in Loop Models for "{start_residue.molecule.title}"'
        log_help = (
            "Summary of all database hits (in order, columns are loop model name,\n"
            'environment-specific substitution score, "internal" RMSD of C-alpha\n'
            "separations of loop anchors, all-backbone-atom RMSD of loop anchors,\n"
            "number of CCD iterations needed to close the loop, database loop sequence,\n"
            "all-backbone-atom RMSD of the loop before closure (if input already had\n"
            "loop coordinates), all-backbone-atom RMSD of the loop after closure\n"
            "(if input already had loop coordinates) - the models are sorted by anchor\n"
            "RMSD, score, internal anchor RMSD and CCD iterations):\n\n"
        )

        added_proteins = []
        num_proteins_to_add = 0

        log_comment = log_help
        i = 0
        for line in summary:
            num_proteins_to_add += 1
            model_file_path, model_id = self._extract_model_file_name(line)
            if os.path.exists(model_file_path):
                added = self.project.proteins.extend(flare.read_file(model_file_path, "pdb"))
                if added:
                    log_comment += line
                    added_proteins.extend(added)
                    self._fill_in_missing_empty_residues(
                        start_residue.molecule, self.project.proteins[-1]
                    )
                    i += 1
            if i == self.n_best_output_structures:  # 0 is "All" in dialog QSpinBox
                break
        if added_proteins:
            self._write_flare_log_message(log_header, log_comment)

        if len(added_proteins) < num_proteins_to_add:
            QtWidgets.QMessageBox.warning(
                flare.main_window().widget(),
                "Warning",
                "Not all of the generated loop models could be loaded into Flare.",
            )

        self._run_protein_prep_manual_mode(start_residue.molecule, added_proteins)

    def _extract_model_file_name(self, line):
        """Return the file path from a line in the summary file."""
        model_id = line.split("\t")[0]
        model_file_name = model_id + ".model.pdb"
        model_file_path = os.path.join(self.structure_directory, model_file_name)
        return model_file_path, model_id

    class Residue:
        def __init__(self, residue):
            self.chain = residue.chain
            self.seq_num = residue.seq_num
            self.icode = residue.icode

    def _will_fail_protein_prep(self, file):
        """Return true if this structure will fail protein prep.

        This is normally caused by overlapping atoms.
        """
        project = flare.Project()
        project.proteins.extend(flare.read_file(file))

        prep = flare.ProteinPrep()
        prep.proteins = project.proteins

        return len(prep.setup_errors()) > 0

    def _show_no_models_error_message(self, summary):
        """Show error dialog for case that no loop models were generated."""
        if not summary:
            QtWidgets.QMessageBox.warning(
                flare.main_window().widget(),
                "Warning",
                "No loop models were generated - "
                "see the log for more calculation details. "
                "Try increasing the anchor RMSD cutoff slightly and restarting the calculation.",
            )

    @staticmethod
    def _residue_matches(src_res, dest_seq, res_index):
        """Return True if `src_res` matches the residue at `res_index` in `dest_seq`.

        They match if:
            They are both gaps.
            `src_res` is a gap and `res_index` is >= len(dest_seq)
            They are both empty residues
            They are both not empty residues and have the same chain and name. PyFREAD may change
            residues seq num and icode so these are not checked.
        """
        dest_res = None
        if res_index < len(dest_seq):
            dest_res = dest_seq[res_index]

        if src_res is None and dest_res is None:
            return True  # They are both gaps, should not happen since PyFREAD deletes gaps
        if src_res is None or dest_res is None:
            return False  # One is a gap

        is_src_res_empty = len(src_res.atoms) == 0
        is_dest_res_empty = len(dest_res.atoms) == 0

        if is_src_res_empty and is_dest_res_empty:
            # They are both empty residues, should not happen since PyFREAD deletes empty residues
            return True
        if is_src_res_empty or is_dest_res_empty:
            return False  # One is a empty residues

        # The seq num may have changed if the loop has more residues then the gap it is replacing
        return src_res.chain == dest_res.chain and src_res.name == dest_res.name

    @staticmethod
    def _is_cap(res):
        """Return True if this reidue is a cap"""
        return res.name in ("ACE", "NME") and len(res.atoms) > 0

    def _fill_in_missing_empty_residues(self, input_protein, protein):
        """Put gaps and residues with no atoms back into the protein

        Theses residues are deleted by PyFREAD.
        """
        with protein.batch_edit():
            src_sequences = [
                seq for seq in input_protein.sequences if seq.type == flare.Sequence.Type.Natural
            ]
            dest_sequences = [
                seq for seq in protein.sequences if seq.type == flare.Sequence.Type.Natural
            ]
            assert len(src_sequences) == len(dest_sequences)

            # For each natual sequence match the sequence of the dest protein with the new loop
            # to the sequence of the source protein
            # The differences between the 2 sequences are:
            # * All gaps have been removed from dest
            # * All empty residues have been removed from dest
            # * 0-* Loops have been added to dest, they may have replaced empty residues
            # * All caps have been removed from dest, some of these caps may be added
            #   later when protein prep runs

            for src_seq, dest_seq in zip(src_sequences, dest_sequences):
                assert src_seq.chain == dest_seq.chain

                src_seq_index = 0
                dest_seq_index = 0

                def chain_residues_equal(src_res, dest_res):
                    return (src_res is None and dest_res is None) or (
                        dest_res is not None
                        and src_res is not None
                        and src_res.seq_num == dest_res.seq_num
                        and src_res.icode == dest_res.icode
                    )

                # Loop over each residue in the src and dest sequence transforming the dest
                # sequence to match the src sequence
                while dest_seq_index < len(dest_seq) or src_seq_index < len(src_seq):
                    src_res = src_seq[src_seq_index] if src_seq_index < len(src_seq) else None
                    dest_res = dest_seq[dest_seq_index] if dest_seq_index < len(dest_seq) else None
                    prev_dest_res = (
                        dest_seq[dest_seq_index - 1] if dest_seq_index - 1 < len(dest_seq) else None
                    )

                    prev_res_had_atoms = prev_dest_res is not None and len(prev_dest_res.atoms) > 0
                    current_res_has_atoms = dest_res is not None and len(dest_res.atoms) > 0

                    if chain_residues_equal(src_res, dest_res):
                        # Residues are the same, move to the next one
                        src_seq_index += 1
                        dest_seq_index += 1
                    elif src_res is None:
                        # A gap is in the source protein, add it to the dest protein
                        dest_seq.insert_gap(dest_seq_index)
                        src_seq_index += 1
                        dest_seq_index += 1
                    elif self._is_cap(src_res) and prev_res_had_atoms and current_res_has_atoms:
                        # Caps are removed when inserting a loop
                        # Caps will be inserted by protein prep if the prev or current residues
                        # are missing atoms, done insert a gap as the sequences will be aligned
                        # when prep runs
                        # Where the loop has been inserted caps will not be added by protein prep
                        # instead insert a gap.
                        dest_seq.insert_gap(dest_seq_index)
                        src_seq_index += 1
                        dest_seq_index += 1
                    else:
                        # The residues do not match

                        # The source protein may have a bunch of empty residues
                        # Find the first non empty residue, this will be the residue which signals
                        # the end of the loop
                        src_seq_empty_end_index = src_seq_index
                        contains_cap = False
                        while src_seq_empty_end_index < len(src_seq) and (
                            not src_seq[src_seq_empty_end_index].atoms
                            or self._is_cap(src_seq[src_seq_empty_end_index])
                        ):
                            contains_cap = contains_cap or self._is_cap(
                                src_seq[src_seq_empty_end_index]
                            )
                            src_seq_empty_end_index += 1

                        # Now find the residue at the end of the loop
                        dest_seq_loop_end = dest_seq_index
                        while (
                            dest_seq_loop_end < len(dest_seq)
                            and src_seq_empty_end_index < len(src_seq)
                            and not chain_residues_equal(
                                src_seq[src_seq_empty_end_index], dest_seq[dest_seq_loop_end]
                            )
                        ):
                            dest_seq_loop_end += 1

                        has_loop_been_inserted = dest_seq_loop_end - dest_seq_index > 0
                        if has_loop_been_inserted:
                            # Loop residues have been inserted, ignore empty residues
                            src_seq_index = src_seq_empty_end_index
                            dest_seq_index = dest_seq_loop_end

                            # If there a gap it will have been removed, replace it with a gap
                            if contains_cap:
                                dest_seq.insert_gap(dest_seq_index)
                                dest_seq_index += 1

                            # If the loop is smaller then expected add gaps to make it the same size
                            while dest_seq_index < src_seq_index:
                                dest_seq.insert_gap(dest_seq_index)
                                dest_seq_index += 1
                        else:
                            # A loop has not been added here, copy across the empty residues
                            # or insert gaps for the caps
                            if src_res.atoms:
                                # Will prep insert a cap here
                                if prev_res_had_atoms and current_res_has_atoms:
                                    dest_seq.insert_gap(dest_seq_index)
                                    dest_seq_index += 1
                            else:
                                # Copy accross the empty residue
                                dest_seq.insert(dest_seq_index, src_res)
                                dest_seq_index += 1

                            src_seq_index += 1

    def _write_flare_log_message(self, header, comment):
        """Write Flare log message."""
        self.project.log.append(header=header, comment=comment)

    @classmethod
    def _has_caps(cls, protein):
        """Return True if the protein has been capped."""
        for res in protein.residues:
            if res is not None and cls._is_cap(res):
                return True
        return False

    def _run_protein_prep_manual_mode(self, input_protein, proteins):
        """Prepare proteins, deleting those that cannot be prepared from the project."""
        prep = flare.ProteinPrep()
        if not self._has_caps(input_protein):
            prep.cap_mode = flare.ProteinPrep.CappingMode.NoCapping
        prep.fill_small_gaps = False
        prep.remove_atoms_from_residues_with_incomplete_backbone = False

        # Collect proteins that can be prepared, and indices and errors for those that can't
        proteins_for_prep = []
        protein_indices_to_delete_and_errors = []
        for protein in proteins:
            prep.proteins = [protein]
            if prep.setup_errors():
                protein_indices_to_delete_and_errors.append(
                    (self.project.proteins.index(protein), prep.setup_errors())
                )
            else:
                proteins_for_prep.append(protein)

        prep.proteins = proteins_for_prep

        # Delete the proteins that can't be prepared from the project
        protein_indices_to_delete_and_errors.sort(reverse=True)
        for i, setup_errors in protein_indices_to_delete_and_errors:
            log_header = f"Delete {self.project.proteins[i].title} as Unable to Prepare Protein"
            log_comment = (
                f"Delete {self.project.proteins[i].title} "
                f"from the project as protein preparation cannot be performed."
            )
            log_comment += "\nSetup errors:\n"
            log_comment += "\n".join(setup_errors)
            del self.project.proteins[i]
            self._write_flare_log_message(log_header, log_comment)

        if not prep.proteins:
            QtWidgets.QMessageBox.warning(
                flare.main_window().widget(),
                "Warning",
                "No generated loop models were retained - "
                "protein preparation could not be performed for any of the models."
                "Try increasing the number of output structures and restarting the calculation. "
                "If this fails, try increasing the anchor RMSD cutoff slightly "
                "and restarting the calculation again.",
            )
            return

        # Do the preparation in parallel
        try:
            prep.start()
            prep.wait()
        except Exception:
            log_header = "Protein Preparation Failed"
            log_comment = (
                "Protein preparation failed for proteins:\n"
                + "\n".join([protein.title for protein in prep.proteins])
                + "\n"
            )
            log_comment += "\nSetup errors:\n"
            log_comment += "\n".join(prep.setup_errors())
            log_comment += "\n\nCalculation errors:\n"
            log_comment += "\n".join(prep.errors())
            self._write_flare_log_message(log_header, log_comment)

    @staticmethod
    def _show_error_dialog(text, detailed_text):
        """Show an error dialog for the given exception."""
        parent = flare.main_window().widget()
        box = QtWidgets.QMessageBox(parent)
        box.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        box.setIcon(QtWidgets.QMessageBox.Critical)
        box.setDetailedText(detailed_text)
        box.setText(text)
        box.exec_()

    def _update_progress(self, current, max, text):
        """Updates the data in the progress dialog."""
        if self.progress:
            self.progress.setMaximum(max)
            self.progress.setValue(current)
            self.progress.setLabelText(text)

    def _done(self):
        """Hide the progress dialog, set progress to None and delete temporary directory."""
        self.progress.deleteLater()
        self.progress = None
        if not os.environ.get("CRESSET_KEEP_TEMP_FILES") and os.path.exists(
            self.structure_directory
        ):
            shutil.rmtree(self.structure_directory)

        flare.main_window().create_undo_point("Loop Modeling")

    @dataclass
    class MissingLoopData:
        """Represents a gap in the protein."""

        start_residue: flare.Residue = None
        start_residue_name: str = ""
        start_residue_number: str = ""
        start_residue_chain: str = ""
        loop_sequence: str = ""

    @dataclass
    class MissingLoopProteinData:
        """Contains a list of gaps in the protein."""

        protein: flare.Protein = None
        protein_title: str = ""
        input_pdb_path: str = ""
        loops: list = field(default_factory=list)

    def _find_missing_loops(self, proteins):
        """Return all gaps in the proteins which loops will be generated for.

        A `MissingLoopProteinData` is returned for each protein with missing loops.
        MissingLoopProteinData.loops contains the start residue and sequence of each missing loop.
        """
        data = []
        for protein in proteins:
            missingLoopProteinData = self.MissingLoopProteinData()

            def createMissingLoopData(residue, loop_sequence):
                data = self.MissingLoopData()
                data.start_residue = residue
                data.start_residue_number = (
                    str(data.start_residue.seq_num) + data.start_residue.icode
                )
                data.start_residue_name = data.start_residue.name
                data.start_residue_chain = data.start_residue.chain
                data.loop_sequence = loop_sequence
                return data

            for sequence in protein.sequences:
                if sequence.type == flare.Sequence.Type.Natural and sequence:
                    loop_start = None
                    loop_sequence = ""

                    # Gaps of 1 or 2 are filled in via protein prep so there no need to
                    # find loops for them
                    MIN_LOOP_SIZE = 3

                    for residue in sequence:
                        if residue is not None and residue.atoms and not self._is_cap(residue):
                            if loop_start is not None:
                                loop_sequence_without_caps = loop_sequence.replace("X", "")
                                if len(loop_sequence_without_caps) >= MIN_LOOP_SIZE:
                                    missingLoopProteinData.loops.append(
                                        createMissingLoopData(
                                            loop_start, loop_sequence_without_caps
                                        )
                                    )
                                loop_sequence = ""
                            loop_start = residue
                        elif loop_start is not None and residue is not None:
                            loop_sequence += residue.one_letter_code()

            if missingLoopProteinData.loops:
                missingLoopProteinData.protein = protein
                missingLoopProteinData.protein_title = protein.title
                missingLoopProteinData.input_pdb_path = self._write_input_pdb_file(protein)
                data.append(missingLoopProteinData)

        return data

    def _run_pyfread_auto_mode(
        self,
        auto_data,
    ):
        """Run PyFREAD on all gaps in all proteins."""

        current_progress = 0
        max_progress = sum(len(protein_data.loops) for protein_data in auto_data)

        try:
            for protein_data in auto_data:
                log_header = f'Loop Modeling of "{protein_data.protein_title}"'
                col_width = 30
                log_comment = (
                    "Loop database path:".ljust(col_width)
                    + self.loop_database_path
                    + "\n"
                    + "Input PDB:".ljust(col_width)
                    + protein_data.protein_title
                    + "\n"
                )

                input_pdb_path = protein_data.input_pdb_path

                # Loop over each gap in the protein
                for count, loop_data in enumerate(protein_data.loops, 1):
                    flare.invoke_and_wait(
                        target=self._update_progress,
                        args=(
                            current_progress,
                            max_progress,
                            f"{protein_data.protein_title} Loop {count}",
                        ),
                    )
                    current_progress += 1

                    if self.auto_mode_canceled:
                        return

                    settings_used = (
                        "Pre-loop start residue:".ljust(col_width)
                        + loop_data.start_residue_name
                        + " "
                        + loop_data.start_residue_number
                        + "\n"
                        + "Loop sequence:".ljust(col_width)
                        + loop_data.loop_sequence
                        + "\n"
                        + "Chain:".ljust(col_width)
                        + loop_data.start_residue_chain
                    )

                    # Fill in the gap
                    messages, summary, command_line = self._run_pyfread(
                        input_pdb_path,
                        loop_data.start_residue_number,
                        loop_data.loop_sequence,
                        loop_data.start_residue_chain,
                        self.structure_directory,
                        self.anchor_rmsd_cutoff,
                        self.substitution_score_cutoff,
                        protein_data.protein_title,
                    )

                    log_comment += (
                        f"\nLoop {count}:\n"
                        + settings_used
                        + "\n"
                        + "PyFREAD arguments:\n"
                        + " ".join(command_line)
                        + "\n\n"
                        + "".join(messages)
                    )

                    if summary:
                        input_pdb_path = None
                        # Search through the loops until one is found which will not cause protein
                        # prep to fail. For example some loops may contain overlapping atoms.
                        for line in summary:
                            model_file_path, _ = self._extract_model_file_name(line)
                            if not flare.invoke_and_wait(
                                target=self._will_fail_protein_prep, args=(model_file_path,)
                            ):
                                input_pdb_path = model_file_path
                                break

                        # All loops are invalid. Pick the first one, protein prep will still fail
                        # but at least there is a result
                        if not input_pdb_path:
                            input_pdb_path, _ = self._extract_model_file_name(summary[0])

                    else:
                        log_comment += "Warning - No loop models were generated for the gap\n"

                # Write log message on main thread, one per protein
                flare.invoke_and_wait(
                    target=self._write_flare_log_message, args=(log_header, log_comment)
                )

                # Load in structures on main thread, one per protein
                flare.invoke_and_wait(
                    target=self._load_loop_model_pdb_files_into_project_auto_mode,
                    args=(protein_data, input_pdb_path),
                )

            # Run protein prep on all the results
            flare.invoke_and_wait(target=self._run_protein_prep_auto_mode)

        except Exception as e:
            flare.invoke_and_wait(
                target=self._show_error_dialog, args=(str(e), traceback.format_exc())
            )
            raise
        finally:
            flare.invoke_and_wait(target=self._done)

    def _load_loop_model_pdb_files_into_project_auto_mode(self, protein_data, pdb_path):
        """Load N best loop model PDB files generated by PyFREAD into project as proteins."""
        index = self.project.proteins.index(protein_data.protein)
        protein = self.project.proteins.insert(index + 1, next(flare.read_file(pdb_path, "pdb")))
        count = 0
        while True:
            try:
                count_str = f" ({count})" if count > 0 else ""
                count += 1
                protein.title = protein_data.protein.title + count_str + "_L"
                break
            except flare.DuplicateError:
                pass

        self._fill_in_missing_empty_residues(protein_data.protein, protein)
        self.auto_mode_result_proteins.append((protein_data.protein, protein))

    def _run_protein_prep_auto_mode(self):
        """Load N best loop model PDB files generated by PyFREAD into project as proteins."""
        for input_protein, output_protein in self.auto_mode_result_proteins:
            prep = flare.ProteinPrep()
            if not self._has_caps(input_protein):
                prep.cap_mode = flare.ProteinPrep.CappingMode.NoCapping
            prep.fill_small_gaps = True
            prep.remove_atoms_from_residues_with_incomplete_backbone = False
            prep.proteins = [output_protein]

            try:
                prep.start()
                prep.wait()
            except Exception:
                log_header = f"{output_protein.title} Protein Preparation Failed"
                log_comment = (
                    "Protein preparation failed for protein:\n" + output_protein.title + "\n"
                )
                log_comment += "\nSetup errors:\n"
                log_comment += "\n".join(prep.setup_errors())
                log_comment += "\n\nCalculation errors:\n"
                log_comment += "\n".join(prep.errors())
                self._write_flare_log_message(log_header, log_comment)
