# -*- coding: utf-8 -*-
# Copyright (C) 2023 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Provides utility functions used by other scripts."""
from cresset import flare

from PySide2 import QtCore, QtWidgets


class ProgressDialog:
    """Show a progress dialog when running from the GUI."""

    def __init__(self, text, min_value=0, max_value=100):
        """Create the progress dialog."""
        if flare.main_window() is not None:
            parent = flare.main_window().widget()
            self._progress_dialog = QtWidgets.QProgressDialog(
                text, "Cancel", min_value, max_value, parent
            )
            self._progress_dialog.setWindowModality(QtCore.Qt.WindowModal)
        else:
            self._progress_dialog = None

    def __enter__(self):
        """Show the progress dialog."""
        if self._progress_dialog is not None:
            self._progress_dialog.show()
        return self

    def __exit__(self, type, value, traceback):
        """Hide and deletes the progress dialog."""
        if self._progress_dialog is not None:
            self._progress_dialog.hide()
            self._progress_dialog.deleteLater()
        return False

    def set_text(self, text):
        """Set the text that is displayed in the progress dialog."""
        if self._progress_dialog is not None:
            self._progress_dialog.setLabelText(text)

    def set_value(self, value):
        """Set the value that is displayed, between 0 and 100."""
        if self._progress_dialog is not None:
            self._progress_dialog.setValue(value)

    def was_canceled(self):
        """Return True of the cancel button has been pressed."""
        if self._progress_dialog is not None:
            return self._progress_dialog.wasCanceled()
        return False
