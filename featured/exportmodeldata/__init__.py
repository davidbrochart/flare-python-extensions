# -*- coding: utf-8 -*-
# Copyright (C) 2023 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Adds functions for exporting QSAR model data.

Ribbon Controls:
    QSAR -> Import/Export -> QSAR Models -> Export Displayed QSAR Model Data
        Export the data used to build the displayed QSAR model.

    QSAR -> Import/Export -> QSAR Models -> Export Field Samples for Selected Ligands
        Export the field sample data for the selected ligands for use with external
        statistics packages.
"""
import os
import csv

from PySide2 import QtWidgets

from cresset import flare


@flare.extension
class ExportModelDataExtension:
    """Add a buttons to export QSAR model data."""

    def __init__(self):
        self._last_save_path = ""

    def load(self):
        """Load the extension."""
        main_window = flare.main_window()
        parent = main_window.widget()

        tab = flare.main_window().ribbon["QSAR"]

        group = tab["Model"]

        self._export_qsar_action = QtWidgets.QAction(
            "Export Displayed Field QSAR Model Data", parent
        )
        self._export_qsar_action.setStatusTip(
            "Export the data used to build the displayed Field QSAR model and the final equation "
            "to a CSV file."
        )
        self._export_qsar_action.setToolTip(self._export_qsar_action.statusTip())
        self._export_qsar_action.setEnabled(False)
        self._export_qsar_action.triggered.connect(self._write_displayed_qsar_model_data)

        self._export_field_samples_action = QtWidgets.QAction(
            "Export Field QSAR Field Samples for Selected Ligands", parent
        )
        self._export_field_samples_action.setStatusTip(
            "Export the field sample data for the selected ligands for use with external "
            "statistics packages."
        )
        self._export_field_samples_action.setToolTip(self._export_field_samples_action.statusTip())
        self._export_field_samples_action.setEnabled(False)
        self._export_field_samples_action.triggered.connect(self._write_field_sample_data)

        qsar_menu = QtWidgets.QMenu()
        qsar_menu.addAction(self._export_qsar_action)
        qsar_menu.addAction(self._export_field_samples_action)

        control = group.add_menu("Export Model Data", qsar_menu)
        control.tip_key = "X"
        control.load_icon(_package_file("Export-model.png"))
        control.tooltip = "Export data from QSAR models."

        control.set_menu_action_tip_key(self._export_qsar_action, "D")
        control.set_menu_action_tip_key(self._export_field_samples_action, "F")

        flare.callbacks.selected_ligands_changed.add(self._selected_ligands_changed)
        flare.callbacks.selected_qsar_model_changed.add(self._selected_qsar_model_changed)

        print(f"Loaded {self.__class__.__module__}.{self.__class__.__name__}")

    def load_settings(self, settings):
        """Read the last saved location to the settings."""
        self._last_save_path = settings.get("last_save_path", "")

    def save_settings(self, settings):
        """Save the last saved location to the settings."""
        settings["last_save_path"] = self._last_save_path

    def _selected_ligands_changed(self, ligands):
        main_window = flare.main_window()
        self._export_field_samples_action.setEnabled(
            len(ligands) > 0 and self._qsar_model_has_descriptor(main_window.selected_qsar_model)
        )

    def _selected_qsar_model_changed(self, qsar_model):
        main_window = flare.main_window()
        self._export_qsar_action.setEnabled(self._qsar_model_has_descriptor(qsar_model))
        self._export_field_samples_action.setEnabled(
            len(main_window.selected_ligands) > 0 and self._qsar_model_has_descriptor(qsar_model)
        )

    @staticmethod
    def _qsar_model_has_descriptor(qsar_model):
        return qsar_model is not None and qsar_model.type == flare.QSARModelType.Field

    def _write_displayed_qsar_model_data(self):
        """Ask where to write the QSAR model data to then writes the file."""
        main_window = flare.main_window()
        parent = main_window.widget()

        file_path, _ = QtWidgets.QFileDialog.getSaveFileName(
            parent,
            "Save QSAR Model Data",
            self._last_save_path,
            "Comma-separated value (*.csv);;All files (*)",
        )
        if file_path:
            self._last_save_path = file_path
            self._write_displayed_qsar_model_data_to_csv(main_window.selected_qsar_model, file_path)

    @classmethod
    def _write_displayed_qsar_model_data_to_csv(cls, qsar_model, file_path):
        """Write the QSAR model data to `file_path`."""
        with open(file_path, "w", newline="") as csvfile:
            writer = csv.writer(csvfile)

            field_descriptors_header = []
            for i, type in enumerate(qsar_model.descriptor_types, 1):
                field_descriptors_header.append(
                    cls._qsar_model_descriptor_type_to_char(type) + str(i)
                )

            descriptors_header = list(field_descriptors_header)
            descriptors_header.extend(qsar_model.column_descriptor_names)

            # Write descriptor matrix
            writer.writerow(["Type", "Molecule name", "SMILES", "Activity", *descriptors_header])
            for (ligand1, field_descriptors), (ligand2, column_descriptors) in zip(
                qsar_model.descriptors, qsar_model.column_descriptors
            ):
                assert ligand1 == ligand2
                title = ligand1.title if ligand1 is not None else ""
                smiles = ligand1.smiles() if ligand1 is not None else ""
                activity = (
                    str(ligand1.properties[qsar_model.activity_column].value)
                    if ligand1 is not None
                    else ""
                )
                xy_matrix_row = ["Data matrix", title, smiles, activity]
                for value in field_descriptors:
                    xy_matrix_row.append(str(value))
                for value in column_descriptors:
                    xy_matrix_row.append(str(value))
                writer.writerow(xy_matrix_row)

            writer.writerow([])

            # Write the coefficients if the model has components
            if qsar_model.max_components > 0:
                writer.writerow(["Equation labels", "", "", "Intercept", *field_descriptors_header])
                coefficients_row = ["Equation coefficients", "", "", qsar_model.equation_intercept]
                for value in qsar_model.equation_coefficients:
                    coefficients_row.append(str(value))
                writer.writerow(coefficients_row)
                writer.writerow([])

            # Write the coordinates of the where the descriptors field values were sampled from
            writer.writerow(["Sample co-ordinate labels", "", "", "", *field_descriptors_header])
            sample_x_row = ["Sample_X", "", "", ""]
            sample_y_row = ["Sample_Y", "", "", ""]
            sample_z_row = ["Sample_Z", "", "", ""]

            for x, y, z in qsar_model.sample_coordinates:
                sample_x_row.append(str(x))
                sample_y_row.append(str(y))
                sample_z_row.append(str(z))

            writer.writerow(sample_x_row)
            writer.writerow(sample_y_row)
            writer.writerow(sample_z_row)
            writer.writerow([])

            # Write the notes
            if qsar_model.notes:
                writer.writerow(["Notes", "", qsar_model.notes, ""])

    @staticmethod
    def _qsar_model_descriptor_type_to_char(type):
        if type == flare.QSARModel.DescriptorType.Electrostatics:
            return "E"
        if type == flare.QSARModel.DescriptorType.VanDerWaals:
            return "S"
        if type == flare.QSARModel.DescriptorType.Hydrophobics:
            return "H"
        if type == flare.QSARModel.DescriptorType.Volume:
            return "V"
        return "?"

    def _write_field_sample_data(self):
        """Write the sample data for the ligands to `file_path`."""
        main_window = flare.main_window()
        parent = main_window.widget()

        file_path, _ = QtWidgets.QFileDialog.getSaveFileName(
            parent,
            "Save Sample Data",
            self._last_save_path,
            "Comma-separated value (*.csv);;All files (*)",
        )
        if file_path:
            self._last_save_path = file_path
            self._write_field_sample_data_to_csv(
                main_window.selected_qsar_model, main_window.selected_ligands, file_path
            )

    @classmethod
    def _write_field_sample_data_to_csv(cls, qsar_model, ligands, file_path):
        """Write the QSAR model data to `file_path`."""
        with open(file_path, "w", newline="") as csvfile:
            writer = csv.writer(csvfile)

            descriptors_header = []
            for i, type in enumerate(qsar_model.descriptor_types, 1):
                descriptors_header.append(cls._qsar_model_descriptor_type_to_char(type) + str(i))

            # Write descriptor matrix
            writer.writerow(["Title", *descriptors_header])
            for ligand in ligands:
                row = [ligand.title]
                for value in qsar_model.calculate_samples(ligand):
                    row.append(str(value))
                writer.writerow(row)

            writer.writerow([])


def _package_file(file_name):
    """Return the path to the file `file_name` in this package."""
    return os.path.join(os.path.dirname(os.path.realpath(__file__)), file_name)
