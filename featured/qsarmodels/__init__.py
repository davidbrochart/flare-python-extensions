# -*- coding: utf-8 -*-
# Copyright (C) 2023 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""A collection of extensions that produce machine learning QSAR models using scikit-learn"""
try:
    from cresset import flare

    is_flare_available = True
except ImportError:
    # This file may be imported when the models are being generated.
    # `from cresset import flare` requires a license which may not be available
    # on the machine building the model but the model building should not
    # require this license. Therefore this import error is ignored
    is_flare_available = False

import qsarmodels.classificationRF as crf
import qsarmodels.classificationSVM as csvm
import qsarmodels.classificationMLP as cmlp
import qsarmodels.classificationConsensus as ccon
import qsarmodels.regressionRF as rrf
import qsarmodels.regressionSVM as rsvm
import qsarmodels.regressionGP as rgp
import qsarmodels.regressionMLP as rmlp
import qsarmodels.regressionConsensus as rcon

if is_flare_available:

    @flare.extension
    class QSARExtension:
        """Register QSAR model extensions, populating the QSAR Model Building dialog"""

        def load(self):
            """Load the extensions."""
            for model in [crf, csvm, cmlp, ccon, rrf, rsvm, rgp, rmlp, rcon]:
                flare.main_window().register_qsar_model(
                    model.model_name(),
                    model.__file__,
                    model.ParametersWidget(),
                    model.include_in_automatic(),
                    model.standardize_descriptors(),
                )
