#!/usr/bin/env pyflare
# -*- coding: utf-8 -*-
# Copyright (C) 2023 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Random Forest Regression QSAR model extension"""
from PySide2 import QtWidgets


def model_name():
    """Returns the name of this model which allows Flare to detect the QSAR
    model type. The name must contain either "Regression" or "Classification".
    """
    return "Random Forest Regression"


def include_in_automatic():
    """Returns true if the model should be included when "Automatic" is selected
    as the model type in Flare.
    """
    return True


def standardize_descriptors():
    """Returns true if descriptor values should be standardized before creating
    the model.
    """
    return True


def print_parameters():
    """Prints the values of the hyperparameters that Flare will parse and use
    to configure the model.
    """
    from cresset import flare

    qsarwidget = flare.main_window().qsar_widget(model_name())

    numtreesSB = qsarwidget.findChild(QtWidgets.QSpinBox, "numtreesSB")
    if numtreesSB is None:
        raise RuntimeError("Failed to find numtreesSB widget")
    print("n_estimators=", numtreesSB.value())

    featuresSB = qsarwidget.findChild(QtWidgets.QDoubleSpinBox, "featuresSB")
    if featuresSB is None:
        raise RuntimeError("Failed to find featuresSB widget")
    print("max_features=", featuresSB.value())

    minsamplesSB = qsarwidget.findChild(QtWidgets.QSpinBox, "minsamplesSB")
    if minsamplesSB is None:
        raise RuntimeError("Failed to find minsamplesSB widget")
    print("min_samples_leaf=", minsamplesSB.value())


def restore_parameters(parameters):
    """Restores parameter values when a saved config is loaded in Flare."""
    from cresset import flare

    qsarwidget = flare.main_window().qsar_widget(model_name())

    if "n_estimators" not in parameters:
        raise ValueError("parameters dict missing 'n_estimators' parameter")
    if "max_features" not in parameters:
        raise ValueError("parameters dict missing 'max_features' parameter")
    if "min_samples_leaf" not in parameters:
        raise ValueError("parameters dict missing 'min_samples_leaf' parameter")

    numtreesSB = qsarwidget.findChild(QtWidgets.QSpinBox, "numtreesSB")
    if numtreesSB is None:
        raise RuntimeError("Failed to find numtreesSB widget")
    numtreesSB.setValue(parameters["n_estimators"])

    featuresSB = qsarwidget.findChild(QtWidgets.QDoubleSpinBox, "featuresSB")
    if featuresSB is None:
        raise RuntimeError("Failed to find featuresSB widget")
    featuresSB.setValue(parameters["max_features"])

    minsamplesSB = qsarwidget.findChild(QtWidgets.QSpinBox, "minsamplesSB")
    if minsamplesSB is None:
        raise RuntimeError("Failed to find minsamplesSB widget")
    minsamplesSB.setValue(parameters["min_samples_leaf"])


class ParametersWidget(QtWidgets.QWidget):
    """The widget used to configure hyperparameters for this model.
    This class is required as it allows Flare to import the widget in to the
    QSAR model building dialog on start-up.
    """

    def __init__(self):
        super().__init__()

        self._trees = QtWidgets.QSpinBox(self)
        self._trees.setObjectName("numtreesSB")
        self._trees.setToolTip(
            "The number of trees in the random forest. Increasing this number will lead to a\n"
            "more robust model at the expense of longer training and prediction times."
        )
        self._trees.setRange(1, 1e6)
        self._trees.setValue(200)
        self._trees.setSingleStep(100)

        self._features = QtWidgets.QDoubleSpinBox(self)
        self._features.setObjectName("featuresSB")
        self._features.setToolTip(
            "The randomly-selected fraction of available features that are used in building a\n"
            "tree at each node."
        )
        self._features.setRange(0.01, 1)
        self._features.setValue(0.33)
        self._features.setSingleStep(0.1)

        self._min_samples = QtWidgets.QSpinBox(self)
        self._min_samples.setObjectName("minsamplesSB")
        self._min_samples.setToolTip(
            "The minimum node size, which implicitly limits the depth of a tree."
        )
        self._min_samples.setRange(1, 1e6)
        self._min_samples.setValue(2)

        self._layout = QtWidgets.QFormLayout(self)
        self._layout.addRow("Number of trees", self._trees)
        self._layout.addRow("Feature subsampling fraction", self._features)
        self._layout.addRow("Minimum samples per leaf", self._min_samples)


def leave_one_out_cv(model, X, y):
    from sklearn.model_selection import KFold

    kf = KFold(n_splits=X.shape[0], shuffle=False)
    cv_predicted = []
    for train_index, test_index in kf.split(X):
        X_train, X_test = X[train_index], X[test_index]
        y_train = y[train_index]
        model.fit(X_train, y_train)
        y_pred = model.predict(X_test)
        cv_predicted.append(y_pred[0])
    return cv_predicted


def create_model(x_train, y_train, n_estimators, min_samples_leaf, max_features, k_folds):
    """Create the model and return the best estimator.
    This is only used by the consensus regression model as GridSearchCV
    needs to be used by all the models.
    """
    from sklearn.ensemble import RandomForestRegressor
    from sklearn.model_selection import GridSearchCV

    params = {
        "n_estimators": [n_estimators],
        "min_samples_leaf": [min_samples_leaf],
        "max_features": [max_features],
    }

    model = RandomForestRegressor(random_state=0)
    model.fit(x_train, y_train)

    grid_regressor = GridSearchCV(
        model, param_grid=params, scoring="r2", n_jobs=-1, cv=k_folds, verbose=5
    )
    grid_regressor.fit(x_train, y_train)

    return grid_regressor.best_estimator_


def main():
    """Create the model using sklearn and print the results of validation"""
    # Local imports are used to reduce the extension load time as these imports
    # are only needed when building the model
    import argparse
    import numpy as np

    import scipy.stats as stats

    from sklearn.utils import shuffle
    from sklearn.ensemble import RandomForestRegressor
    from sklearn.metrics import r2_score
    from sklearn.metrics import mean_squared_error

    from sklearn.decomposition import PCA

    from skl2onnx import convert_sklearn
    from skl2onnx.common.data_types import FloatTensorType

    parser = argparse.ArgumentParser(description="Random Forest Regression Machine Learning Model")
    parser.add_argument(
        "--n_estimators",
        type=int,
        default=200,
        help="The number of trees in the forest (default: 200)",
    )
    parser.add_argument(
        "--max_features",
        type=float,
        default=0.33,
        help=(
            "The fraction of available features to consider while building the tree "
            "(default: 0.33)"
        ),
    )
    parser.add_argument(
        "--min_samples_leaf",
        type=int,
        default=2,
        help="The minimum node size which limits the depth of the tree (default: 2)",
    )

    args = parser.parse_args()

    training = np.loadtxt(open("Training.csv"), delimiter=",")
    X_train = training[:, 1:]
    y_train = training[:, 0]

    X_test = np.empty(0)
    y_test = np.empty(0)
    try:
        test = np.loadtxt(open("Test.csv"), delimiter=",")
        X_test = test[:, 1:]
        y_test = test[:, 0]
    except FileNotFoundError:
        pass

    params = {
        "n_estimators": args.n_estimators,
        "min_samples_leaf": args.min_samples_leaf,
        "max_features": args.max_features,
    }

    # Do principle component analysis before shuffling
    pca = PCA(n_components=min(10, min(X_train.shape)))
    principalComponents = pca.fit_transform(X_train)

    # Add a row to the end containing the explained variance ratios
    pcaOut = np.append(principalComponents, [pca.explained_variance_ratio_], 0)

    # Flare will read PCA.csv to populate the PCA plot
    np.savetxt("PCA.csv", pcaOut, delimiter=",")

    X_train, y_train = shuffle(X_train, y_train, random_state=0)

    model = RandomForestRegressor(random_state=0)
    model = model.set_params(**params)

    # Do cross validation first then refit the model
    cv_predicted = leave_one_out_cv(model, X_train, y_train)
    model.fit(X_train, y_train)

    cv_r2 = r2_score(y_train, cv_predicted)
    cv_rmse = mean_squared_error(y_train, cv_predicted, squared=False)
    cv_tau, p_value = stats.kendalltau(y_train, cv_predicted)

    train_y_predicted = model.predict(X_train)
    train_r2 = r2_score(y_train, train_y_predicted)
    train_rmse = mean_squared_error(y_train, train_y_predicted, squared=False)
    train_tau, p_value = stats.kendalltau(y_train, train_y_predicted)

    if len(X_test) > 0:
        test_y_predicted = model.predict(X_test)
        test_r2 = r2_score(y_test, test_y_predicted)
        test_rmse = mean_squared_error(y_test, test_y_predicted, squared=False)
        test_tau, p_value = stats.kendalltau(y_test, test_y_predicted)

    # Print log text
    print("")
    print("Random Forest (RF) Regression model parameters:")
    print("No. of trees:          ", args.n_estimators)
    print("Subsampling fraction:  ", args.max_features)
    print("Minimum node size:     ", args.min_samples_leaf)
    print("")
    print("Statistics for predictions from full model on", len(X_train), "compounds:")
    print("R^2:                    {:0.3f}".format(train_r2))
    print("RMSE:                   {:0.3f}".format(train_rmse))
    print("Kendall's tau:          {:0.3f}".format(train_tau))
    print("")
    print("Statistics for predictions from cross-validation on", len(X_train), "compounds:")
    print("Q^2:                    {:0.3f}".format(cv_r2))
    print("RMSE:                   {:0.3f}".format(cv_rmse))
    print("Kendall's tau:          {:0.3f}".format(cv_tau))
    print("")
    if len(X_test) > 0:
        print("Statistics for test set predictions from full model on", len(X_test), "compounds:")
        print("R^2:                    {:0.3f}".format(test_r2))
        print("RMSE:                   {:0.3f}".format(test_rmse))
        print("Kendall's tau:          {:0.3f}".format(test_tau))

    # Convert into ONNX format
    initial_type = [("float_input", FloatTensorType([None, X_train.shape[1]]))]
    onnx = convert_sklearn(model, initial_types=initial_type)
    with open("model.onnx", "wb") as f:
        f.write(onnx.SerializeToString())

    # Fill an array with indexes then shuffle them with the same seed as was used
    # for the training molecules to get them in the same order
    indexes = shuffle([i for i in range(0, X_train.shape[0])], random_state=0)

    predCV = np.empty([X_train.shape[0], 2])
    predCV[:, 0] = indexes
    predCV[:, 1] = cv_predicted

    # Write out the CV scores and molecule indexes so they can be plotted in Flare
    np.savetxt("CV.csv", predCV, delimiter=",")


if __name__ == "__main__":
    main()
