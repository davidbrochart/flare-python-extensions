#!/usr/bin/env pyflare
# -*- coding: utf-8 -*-
# Copyright (C) 2023 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Consensus Classification QSAR model extension"""
from PySide2 import QtWidgets


def model_name():
    """Returns the name of this model which allows Flare to detect the QSAR
    model type. The name must contain either "Regression" or "Classification".
    """
    return "Consensus Classification"


def include_in_automatic():
    """Returns true if the model should be included when "Automatic" is selected
    as the model type in Flare.
    """
    return False


def standardize_descriptors():
    """Returns true if descriptor values should be standardized before creating
    the model.
    """
    return True


def print_parameters():
    """Prints the values of the hyperparameters that Flare will parse and use
    to configure the model.
    """
    from cresset import flare

    qsarwidget = flare.main_window().qsar_widget(model_name())

    kfoldsSB = qsarwidget.findChild(QtWidgets.QSpinBox, "kfoldsSB")
    if kfoldsSB is None:
        raise RuntimeError("Failed to find kfoldsSB widget")
    print("kfolds=", kfoldsSB.value())


def restore_parameters(parameters):
    """Restores parameter values when a saved config is loaded in Flare."""
    from cresset import flare

    qsarwidget = flare.main_window().qsar_widget(model_name())

    if "kfolds" not in parameters:
        raise ValueError("parameters dict missing 'kfolds' parameter")

    kfoldsSB = qsarwidget.findChild(QtWidgets.QSpinBox, "kfoldsSB")
    if kfoldsSB is None:
        raise RuntimeError("Failed to find kfoldsSB widget")
    kfoldsSB.setValue(parameters["kfolds"])


class ParametersWidget(QtWidgets.QWidget):
    """The widget used to configure hyperparameters for this model.
    This class is required as it allows Flare to import the widget in to the
    QSAR model building dialog on start-up.
    """

    def __init__(self):
        super().__init__()

        self._kfolds = QtWidgets.QSpinBox(self)
        self._kfolds.setObjectName("kfoldsSB")
        self._kfolds.setToolTip(
            "Set the number of folds to use in k-fold cross-validation. For example, with k=5,\n"
            "the cross-validation process will build 5 models, each time leaving out 20% of the\n"
            "data set."
        )
        self._kfolds.setRange(2, 1e6)
        self._kfolds.setValue(5)

        self._layout = QtWidgets.QFormLayout(self)
        self._layout.addRow("Number of folds in k-fold cross-validation", self._kfolds)


def print_scores(conf_matrix):
    import math
    import numpy as np

    print("Confusion matrix:")
    print("\t\tPredicted")

    n_classes = len(conf_matrix[0])

    classRow = "\t"
    for col in range(0, n_classes):
        classRow += "\t{}".format(col + 1)
    print(classRow)

    tp = [0] * n_classes
    fn = [0] * n_classes
    tn = [0] * n_classes
    fp = [0] * n_classes
    total = 0
    for x in range(0, n_classes):
        columns = ""
        for y in range(0, n_classes):
            columns += "\t{}".format(conf_matrix[x][y])

            val = conf_matrix[x][y]
            total += val
            if x == y:
                tp[x] = val
            if x != y:
                fn[x] += val
                fp[y] += val
        if x == 0:
            print("Actual\t", x + 1, columns)
        else:
            print("\t", x + 1, columns)
    print("")

    class_log = []
    mean_precision = valid_precision = mean_recall = valid_recall = informedness = 0
    for x in range(0, n_classes):
        tn[x] = total - tp[x] - fp[x] - fn[x]

        precision = tp[x] / (tp[x] + fp[x])
        recall = tp[x] / (tp[x] + fn[x])
        youdens = tp[x] / (tp[x] + fn[x]) + tn[x] / (tn[x] + fp[x]) - 1

        class_log.append(
            "Class {}\t{:0.3f}\t{:0.3f}\t{:0.3f}".format(x + 1, precision, recall, youdens)
        )

        if not np.isnan(precision):
            mean_precision += precision
            valid_precision += 1
        if not np.isnan(recall):
            mean_recall += recall
            valid_recall += 1

        sum = 0
        for y in range(0, n_classes):
            ppr = conf_matrix[y][x] / total
            pr = (fn[x] + tp[x]) / total
            w = 1 / pr if (x == y) else -1 / (1 - pr)
            sum += ppr * w
        ppl = (fp[x] + tp[x]) / total
        informedness += ppl * sum

    mean_precision = mean_precision / valid_precision
    mean_recall = mean_recall / valid_recall

    f1 = 2 * (mean_precision * mean_recall) / (mean_precision + mean_recall)

    informedness_error = " (No class members)" if math.isnan(informedness) else ""

    print("Informedness (Bookmaker's):      {:0.3f}{}".format(informedness, informedness_error))
    print("F1 statistic:                    {:0.3f}".format(f1))
    print("Mean precision:                  {:0.3f}".format(mean_precision))
    print("Mean recall:                     {:0.3f}".format(mean_recall))
    print("")

    print("\tPrecision\tRecall\tYouden's J")
    for log in class_log:
        print(log)


def main():
    """Create the model using sklearn and print the results of validation"""
    # Local imports are used to reduce the extension load time as these imports
    # are only needed when building the model
    import argparse
    import numpy as np

    import qsarmodels.classificationMLP as classMLP
    import qsarmodels.classificationRF as classRF

    from sklearn.model_selection import cross_val_predict

    from sklearn.utils import shuffle
    from sklearn.ensemble import VotingClassifier
    from sklearn.metrics import confusion_matrix

    from sklearn.preprocessing import StandardScaler
    from sklearn.decomposition import PCA

    from skl2onnx import convert_sklearn
    from skl2onnx.common.data_types import FloatTensorType

    parser = argparse.ArgumentParser(description="Consensus Classification Machine Learning Model")
    parser.add_argument(
        "--kfolds",
        type=int,
        default=5,
        help="The number of folds to use in cross-validation (default: 5)",
    )

    args = parser.parse_args()

    training = np.loadtxt(open("Training.csv"), delimiter=",")
    X_train = training[:, 1:]
    y_train = training[:, 0]

    X_test = np.empty(0)
    y_test = np.empty(0)
    try:
        test = np.loadtxt(open("Test.csv"), delimiter=",")
        X_test = test[:, 1:]
        y_test = test[:, 0]
    except FileNotFoundError:
        pass

    # Do principle component analysis before shuffling
    pca = PCA(n_components=min(10, min(X_train.shape)))

    xScaled = StandardScaler().fit_transform(X_train)
    principalComponents = pca.fit_transform(xScaled)

    # Add a row to the end containing the explained variance ratios
    pcaOut = np.append(principalComponents, [pca.explained_variance_ratio_], 0)

    # Flare will read PCA.csv to populate the PCA plot
    np.savetxt("PCA.csv", pcaOut, delimiter=",")

    X_train, y_train = shuffle(X_train, y_train, random_state=0)

    rf_model = classRF.create_model(X_train, y_train, 200, 2, 0.33, args.kfolds)
    mlp_model = classMLP.create_model(X_train, y_train, "lbfgs", 200, args.kfolds)

    # flatten_transform=True is not currently supported in sklearn-onnx
    model = VotingClassifier(
        estimators=[("rf", rf_model), ("mlp", mlp_model)],
        voting="soft",
        flatten_transform=False,
    )
    model = model.fit(X_train, y_train)

    train_y_predicted = model.predict(X_train)
    train_confusion = confusion_matrix(y_train, train_y_predicted, labels=model.classes_)

    cv_predicted = cross_val_predict(model, X_train, y_train, cv=args.kfolds)
    cv_confusion = confusion_matrix(y_train, cv_predicted, labels=model.classes_)

    if len(X_test) > 0:
        test_y_predicted = model.predict(X_test)
        test_confusion = confusion_matrix(y_test, test_y_predicted, labels=model.classes_)

    # Print log text
    print("")
    print("Consensus Classification model parameters:")
    print("Number of k-folds:   ", args.kfolds)
    print("")
    print("Classification methods used:")
    print(classRF.model_name())
    print(classMLP.model_name())
    print("")
    print("Statistics for predictions from full model on", len(X_train), "compounds:")
    print_scores(train_confusion)
    print("")
    print("Statistics for predictions from cross-validation on", len(X_train), "compounds:")
    print_scores(cv_confusion)
    print("Note that the sum of the elements of the confusion matrix from k-fold cross-validation")
    print("may be less than the number of training molecules if the number of molecules in a class")
    print("is not an even multiple of the number of folds")
    print("")

    if len(X_test) > 0:
        print("Statistics for test set predictions from full model on", len(X_test), "compounds:")
        print_scores(test_confusion)

    # Convert into ONNX format
    initial_type = [("float_input", FloatTensorType([None, X_train.shape[1]]))]
    onnx = convert_sklearn(model, initial_types=initial_type)
    with open("model.onnx", "wb") as f:
        f.write(onnx.SerializeToString())

    # Fill an array with indexes then shuffle them with the same seed as was used
    # for the training molecules to get them in the same order
    indexes = shuffle([i for i in range(0, X_train.shape[0])], random_state=0)

    predCV = np.empty([X_train.shape[0], 2])
    predCV[:, 0] = indexes
    predCV[:, 1] = cv_predicted

    # Write out the CV scores and molecule indexes so they can be plotted in Flare
    np.savetxt("CV.csv", predCV, delimiter=",")


if __name__ == "__main__":
    main()
