#!/usr/bin/env pyflare
# -*- coding: utf-8 -*-
# Copyright (C) 2023 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Consensus Regression QSAR model extension"""
from PySide2 import QtWidgets


def model_name():
    """Returns the name of this model which allows Flare to detect the QSAR
    model type. The name must contain either "Regression" or "Classification".
    """
    return "Consensus Regression"


def include_in_automatic():
    """Returns true if the model should be included when "Automatic" is selected
    as the model type in Flare.
    """
    return False


def standardize_descriptors():
    """Returns true if descriptor values should be standardized before creating
    the model.
    """
    return True


def print_parameters():
    """Prints the values of the hyperparameters that Flare will parse and use
    to configure the model.
    """
    from cresset import flare

    qsarwidget = flare.main_window().qsar_widget(model_name())

    kfoldsSB = qsarwidget.findChild(QtWidgets.QSpinBox, "kfoldsSB")
    if kfoldsSB is None:
        raise RuntimeError("Failed to find kfoldsSB widget")
    print("kfolds=", kfoldsSB.value())


def restore_parameters(parameters):
    """Restores parameter values when a saved config is loaded in Flare."""
    from cresset import flare

    qsarwidget = flare.main_window().qsar_widget(model_name())

    if "kfolds" not in parameters:
        raise ValueError("parameters dict missing 'kfolds' parameter")

    kfoldsSB = qsarwidget.findChild(QtWidgets.QSpinBox, "kfoldsSB")
    if kfoldsSB is None:
        raise RuntimeError("Failed to find kfoldsSB widget")
    kfoldsSB.setValue(parameters["kfolds"])


class ParametersWidget(QtWidgets.QWidget):
    """The widget used to configure hyperparameters for this model.
    This class is required as it allows Flare to import the widget in to the
    QSAR model building dialog on start-up.
    """

    def __init__(self):
        super().__init__()

        self._kfolds = QtWidgets.QSpinBox(self)
        self._kfolds.setObjectName("kfoldsSB")
        self._kfolds.setToolTip(
            "Set the number of folds to use in k-fold cross-validation. For example, with k=5,\n"
            "the cross-validation process will build 5 models, each time leaving out 20% of the\n"
            "data set."
        )
        self._kfolds.setRange(2, 1e6)
        self._kfolds.setValue(5)

        self._layout = QtWidgets.QFormLayout(self)
        self._layout.addRow("Number of folds in k-fold cross-validation", self._kfolds)


def main():
    """Create the model using sklearn and print the results of validation"""
    # Local imports are used to reduce the extension load time as these imports
    # are only needed when building the model
    import argparse
    import numpy as np

    import scipy.stats as stats

    import qsarmodels.regressionRF as regRF
    import qsarmodels.regressionSVM as regSVM
    import qsarmodels.regressionGP as regGP
    import qsarmodels.regressionMLP as regMLP

    from sklearn.model_selection import cross_val_predict

    from sklearn.utils import shuffle
    from sklearn.ensemble import VotingRegressor
    from sklearn.metrics import r2_score
    from sklearn.metrics import mean_squared_error

    from sklearn.preprocessing import StandardScaler
    from sklearn.decomposition import PCA

    from sklearn.gaussian_process.kernels import RBF

    from skl2onnx import convert_sklearn
    from skl2onnx.common.data_types import FloatTensorType

    parser = argparse.ArgumentParser(description="Consensus Regression Machine Learning Model")
    parser.add_argument(
        "--kfolds",
        type=int,
        default=5,
        help="The number of folds to use in cross-validation (default: 5)",
    )

    args = parser.parse_args()

    training = np.loadtxt(open("Training.csv"), delimiter=",")
    X_train = training[:, 1:]
    y_train = training[:, 0]

    X_test = np.empty(0)
    y_test = np.empty(0)
    try:
        test = np.loadtxt(open("Test.csv"), delimiter=",")
        X_test = test[:, 1:]
        y_test = test[:, 0]
    except FileNotFoundError:
        pass

    # Do principle component analysis before shuffling
    pca = PCA(n_components=min(10, min(X_train.shape)))

    xScaled = StandardScaler().fit_transform(X_train)
    principalComponents = pca.fit_transform(xScaled)

    # Add a row to the end containing the explained variance ratios
    pcaOut = np.append(principalComponents, [pca.explained_variance_ratio_], 0)

    # Flare will read PCA.csv to populate the PCA plot
    np.savetxt("PCA.csv", pcaOut, delimiter=",")

    X_train, y_train = shuffle(X_train, y_train, random_state=0)

    svm_model = regSVM.create_model(X_train, y_train, "rbf", -1, args.kfolds)
    rf_model = regRF.create_model(X_train, y_train, 200, 2, 0.33, args.kfolds)
    gp_model = regGP.create_model(
        X_train, y_train, [RBF(num) for num in np.logspace(-3, 3, 10)], args.kfolds
    )
    mlp_model = regMLP.create_model(X_train, y_train, "lbfgs", 200, args.kfolds)

    model = VotingRegressor(
        estimators=[("svm", svm_model), ("rf", rf_model), ("gp", gp_model), ("mlp", mlp_model)]
    )
    model = model.fit(X_train, y_train)

    train_y_predicted = model.predict(X_train)
    train_r2 = r2_score(y_train, train_y_predicted)
    train_rmse = mean_squared_error(y_train, train_y_predicted, squared=False)
    train_tau, p_value = stats.kendalltau(y_train, train_y_predicted)

    cv_predicted = cross_val_predict(model, X_train, y_train, cv=args.kfolds)
    cv_r2 = r2_score(y_train, cv_predicted)
    cv_rmse = mean_squared_error(y_train, cv_predicted, squared=False)
    cv_tau, p_value = stats.kendalltau(y_train, cv_predicted)

    if len(X_test) > 0:
        test_y_predicted = model.predict(X_test)
        test_r2 = r2_score(y_test, test_y_predicted)
        test_rmse = mean_squared_error(y_test, test_y_predicted, squared=False)
        test_tau, p_value = stats.kendalltau(y_test, test_y_predicted)

    # Print log text
    print("")
    print("Consensus Regression model parameters:")
    print("Number of k-folds:   ", args.kfolds)
    print("")
    print("Regression methods used:")
    print(regSVM.model_name())
    print(regRF.model_name())
    print(regMLP.model_name())
    print(regGP.model_name())
    print("")
    print("Statistics for predictions from full model on", len(X_train), "compounds:")
    print("R^2:                  {:0.3f}".format(train_r2))
    print("RMSE:                 {:0.3f}".format(train_rmse))
    print("Kendall's tau:        {:0.3f}".format(train_tau))
    print("")
    print("Statistics for predictions from cross-validation on", len(X_train), "compounds:")
    print("Q^2:                  {:0.3f}".format(cv_r2))
    print("RMSE:                 {:0.3f}".format(cv_rmse))
    print("Kendall's tau:        {:0.3f}".format(cv_tau))
    print("")
    if len(X_test) > 0:
        print("Statistics for test set predictions from full model on", len(X_test), "compounds:")
        print("R^2:                  {:0.3f}".format(test_r2))
        print("RMSE:                 {:0.3f}".format(test_rmse))
        print("Kendall's tau:        {:0.3f}".format(test_tau))

    # Convert into ONNX format
    initial_type = [("float_input", FloatTensorType([None, X_train.shape[1]]))]
    onnx = convert_sklearn(model, initial_types=initial_type)
    with open("model.onnx", "wb") as f:
        f.write(onnx.SerializeToString())

    # Fill an array with indexes then shuffle them with the same seed as was used
    # for the training molecules to get them in the same order
    indexes = shuffle([i for i in range(0, X_train.shape[0])], random_state=0)

    predCV = np.empty([X_train.shape[0], 2])
    predCV[:, 0] = indexes
    predCV[:, 1] = cv_predicted

    # Write out the CV scores and molecule indexes so they can be plotted in Flare
    np.savetxt("CV.csv", predCV, delimiter=",")


if __name__ == "__main__":
    main()
