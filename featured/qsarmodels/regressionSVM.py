#!/usr/bin/env pyflare
# -*- coding: utf-8 -*-
# Copyright (C) 2023 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""SVM Regression QSAR model extension"""
from PySide2 import QtWidgets


def model_name():
    """Returns the name of this model which allows Flare to detect the QSAR
    model type. The name must contain either "Regression" or "Classification".
    """
    return "SVM Regression"


def include_in_automatic():
    """Returns true if the model should be included when "Automatic" is selected
    as the model type in Flare.
    """
    return True


def standardize_descriptors():
    """Returns true if descriptor values should be standardized before creating
    the model.
    """
    return True


def print_parameters():
    """Prints the values of the hyperparameters that Flare will parse and use
    to configure the model.
    """
    from cresset import flare

    qsarwidget = flare.main_window().qsar_widget(model_name())

    kernelCB = qsarwidget.findChild(QtWidgets.QComboBox, "kernelCB")
    if kernelCB is None:
        raise RuntimeError("Failed to find kernelCB widget")
    print("kernel=", kernelCB.currentText().lower())

    kfoldsSB = qsarwidget.findChild(QtWidgets.QSpinBox, "kfoldsSB")
    if kfoldsSB is None:
        raise RuntimeError("Failed to find kfoldsSB widget")
    print("kfolds=", kfoldsSB.value())

    maxiterSB = qsarwidget.findChild(QtWidgets.QSpinBox, "maxiterSB")
    if maxiterSB is None:
        raise RuntimeError("Failed to find maxiterSB widget")
    print("max_iter=", maxiterSB.value())


def restore_parameters(parameters):
    """Restores parameter values when a saved config is loaded in Flare."""
    from cresset import flare

    qsarwidget = flare.main_window().qsar_widget(model_name())

    if "kernel" not in parameters:
        raise ValueError("parameters dict missing 'kernel' parameter")
    if "kfolds" not in parameters:
        raise ValueError("parameters dict missing 'kfolds' parameter")
    if "max_iter" not in parameters:
        raise ValueError("parameters dict missing 'max_iter' parameter")

    kernelCB = qsarwidget.findChild(QtWidgets.QComboBox, "kernelCB")
    if kernelCB is None:
        raise RuntimeError("Failed to find kernelCB widget")
    kernel = parameters["kernel"]
    for i in range(0, kernelCB.count()):
        if kernelCB.itemText(i).lower() == kernel:
            kernelCB.setCurrentIndex(i)
            break

    kfoldsSB = qsarwidget.findChild(QtWidgets.QSpinBox, "kfoldsSB")
    if kfoldsSB is None:
        raise RuntimeError("Failed to find kfoldsSB widget")
    kfoldsSB.setValue(parameters["kfolds"])

    maxiterSB = qsarwidget.findChild(QtWidgets.QSpinBox, "maxiterSB")
    if maxiterSB is None:
        raise RuntimeError("Failed to find maxiterSB widget")
    maxiterSB.setValue(parameters["max_iter"])


class ParametersWidget(QtWidgets.QWidget):
    """The widget used to configure hyperparameters for this model.
    This class is required as it allows Flare to import the widget in to the
    QSAR model building dialog on start-up.
    """

    def __init__(self):
        super().__init__()

        self._kernel = QtWidgets.QComboBox(self)
        self._kernel.setObjectName("kernelCB")
        self._kernel.setToolTip("Choose the kernel type to use.")
        self._kernel.addItems({"Linear", "Poly", "RBF", "Sigmoid"})
        self._kernel.setCurrentText("RBF")

        self._kfolds = QtWidgets.QSpinBox(self)
        self._kfolds.setObjectName("kfoldsSB")
        self._kfolds.setToolTip(
            "Set the number of folds to use in k-fold cross-validation. For example, with k=5,\n"
            "the cross-validation process will build 5 models, each time leaving out 20% of the\n"
            "data set."
        )
        self._kfolds.setRange(2, 1e6)
        self._kfolds.setValue(5)

        self._max_iter = QtWidgets.QSpinBox(self)
        self._max_iter.setObjectName("maxiterSB")
        self._max_iter.setToolTip(
            "Some ML methods have parameters which need to be optimized to best fit the data.\n"
            "This option controls how many iterations of global optimization will be used in\n"
            "training the model. Higher values have a possibility of finding better models,\n"
            "at the expense of a longer running time."
        )
        self._max_iter.setSpecialValueText("No limit")
        self._max_iter.setRange(-1, 1e6)
        self._max_iter.setValue(-1)

        self._layout = QtWidgets.QFormLayout(self)
        self._layout.addRow("Kernel type", self._kernel)
        self._layout.addRow("Number of folds in k-fold cross-validation", self._kfolds)
        self._layout.addRow("Maximum number of optimizer iterations", self._max_iter)


def create_model(x_train, y_train, kernel, max_iter, k_folds):
    """Create the model and return the best estimator"""
    from sklearn.svm import SVR
    from sklearn.model_selection import GridSearchCV

    params = {
        "C": [0.1, 0.3, 0.5, 1.0, 3.0, 5.0, 10.0, 30.0, 50.0, 100, 300, 500, 1000],
        "kernel": [kernel],
        "gamma": [
            "scale",
            "auto",
            1e-5,
            3e-5,
            5e-5,
            1e-4,
            3e-4,
            5e-4,
            1e-3,
            3e-3,
            5e-3,
            1e-2,
            5e-2,
            3e-2,
            1e-1,
        ],
        "epsilon": [
            1e-4,
            3e-4,
            5e-4,
            1e-3,
            3e-3,
            5e-3,
            1e-2,
            3e-2,
            5e-2,
            1e-1,
            3e-1,
            5e-1,
            1,
            3,
            5,
            10,
        ],
        "max_iter": [max_iter],
    }

    model = SVR(cache_size=1000)
    model.fit(x_train, y_train)

    grid_regressor = GridSearchCV(
        model, param_grid=params, scoring="r2", n_jobs=-1, cv=k_folds, verbose=5
    )
    grid_regressor.fit(x_train, y_train)

    return grid_regressor.best_estimator_


def main():
    """Create the model using sklearn and print the results of validation"""
    # Local imports are used to reduce the extension load time as these imports
    # are only needed when building the model
    import argparse
    import numpy as np

    import scipy.stats as stats

    from sklearn.model_selection import cross_val_predict

    from sklearn.utils import shuffle
    from sklearn.metrics import r2_score
    from sklearn.metrics import mean_squared_error

    from sklearn.decomposition import PCA

    from skl2onnx import convert_sklearn
    from skl2onnx.common.data_types import FloatTensorType

    parser = argparse.ArgumentParser(description="SVM Regression Machine Learning Model")
    parser.add_argument(
        "--kernel",
        type=ascii,
        default="rbf",
        help="The kernel type to be used in the algorithm (default: 'rbf')",
    )
    parser.add_argument(
        "--kfolds",
        type=int,
        default=5,
        help="The number of folds to use in cross-validation (default: 5)",
    )
    parser.add_argument(
        "--max_iter",
        type=int,
        default=-1,
        help="The limit on solver iterations, or -1 for no limit (default: -1)",
    )

    args = parser.parse_args()

    training = np.loadtxt(open("Training.csv"), delimiter=",")
    X_train = training[:, 1:]
    y_train = training[:, 0]

    X_test = np.empty(0)
    y_test = np.empty(0)
    try:
        test = np.loadtxt(open("Test.csv"), delimiter=",")
        X_test = test[:, 1:]
        y_test = test[:, 0]
    except FileNotFoundError:
        pass

    # Do principle component analysis before shuffling
    pca = PCA(n_components=min(10, min(X_train.shape)))
    principalComponents = pca.fit_transform(X_train)

    # Add a row to the end containing the explained variance ratios
    pcaOut = np.append(principalComponents, [pca.explained_variance_ratio_], 0)

    # Flare will read PCA.csv to populate the PCA plot
    np.savetxt("PCA.csv", pcaOut, delimiter=",")

    # kernel string ends up with two sets of quotes so remove the outer one
    kernel = args.kernel[1:-1]

    X_train, y_train = shuffle(X_train, y_train, random_state=0)
    model = create_model(X_train, y_train, kernel, args.max_iter, args.kfolds)

    train_y_predicted = model.predict(X_train)
    train_r2 = r2_score(y_train, train_y_predicted)
    train_rmse = mean_squared_error(y_train, train_y_predicted, squared=False)
    train_tau, p_value = stats.kendalltau(y_train, train_y_predicted)

    cv_predicted = cross_val_predict(model, X_train, y_train, cv=args.kfolds)
    cv_r2 = r2_score(y_train, cv_predicted)
    cv_rmse = mean_squared_error(y_train, cv_predicted, squared=False)
    cv_tau, p_value = stats.kendalltau(y_train, cv_predicted)

    if len(X_test) > 0:
        test_y_predicted = model.predict(X_test)
        test_r2 = r2_score(y_test, test_y_predicted)
        test_rmse = mean_squared_error(y_test, test_y_predicted, squared=False)
        test_tau, p_value = stats.kendalltau(y_test, test_y_predicted)

    best_params = model.get_params()

    # Print log text
    print("")
    print("SVM Regression model parameters:")
    print("Kernel type:         ", kernel)
    print("Number of k-folds:   ", args.kfolds)
    print("Max iterations:      ", args.max_iter)
    print("Gamma:               ", best_params["gamma"])
    print("C:                   ", best_params["C"])
    print("Epsilon:             ", best_params["epsilon"])
    print("")
    print("Statistics for predictions from full model on", len(X_train), "compounds:")
    print("R^2:                  {:0.3f}".format(train_r2))
    print("RMSE:                 {:0.3f}".format(train_rmse))
    print("Kendall's tau:        {:0.3f}".format(train_tau))
    print("")
    print("Statistics for predictions from cross-validation on", len(X_train), "compounds:")
    print("Q^2:                  {:0.3f}".format(cv_r2))
    print("RMSE:                 {:0.3f}".format(cv_rmse))
    print("Kendall's tau:        {:0.3f}".format(cv_tau))
    print("")
    if len(X_test) > 0:
        print("Statistics for test set predictions from full model on", len(X_test), "compounds:")
        print("R^2:                  {:0.3f}".format(test_r2))
        print("RMSE:                 {:0.3f}".format(test_rmse))
        print("Kendall's tau:        {:0.3f}".format(test_tau))

    # Convert into ONNX format
    initial_type = [("float_input", FloatTensorType([None, X_train.shape[1]]))]
    onnx = convert_sklearn(model, initial_types=initial_type)
    with open("model.onnx", "wb") as f:
        f.write(onnx.SerializeToString())

    # Fill an array with indexes then shuffle them with the same seed as was used
    # for the training molecules to get them in the same order
    indexes = shuffle([i for i in range(0, X_train.shape[0])], random_state=0)

    predCV = np.empty([X_train.shape[0], 2])
    predCV[:, 0] = indexes
    predCV[:, 1] = cv_predicted

    # Write out the CV scores and molecule indexes so they can be plotted in Flare
    np.savetxt("CV.csv", predCV, delimiter=",")


if __name__ == "__main__":
    main()
