# -*- coding: utf-8 -*-
# Copyright (C) 2023 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset

from enum import Enum


class ColType(Enum):
    """The type the column is assigned to, used for color/shape/size axis"""

    NONE = 0
    NUMERIC = 1
    STRING = 2
    TAG = 3
    ROLE = 4
    RADIAL_PLOT = 5
    FAV = 6
    AM_FOCUS = 7
