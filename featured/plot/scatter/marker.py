# -*- coding: utf-8 -*-
# Copyright (C) 2023 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset

from PySide2.QtWidgets import QComboBox
from PySide2.QtCore import Signal
from matplotlib.markers import MarkerStyle


def insert_space(string: str, index: int) -> str:
    """Insert space at index in the given string"""
    return string[0:index] + " " + string[index:]


def marker_name_title(name: str) -> str:
    """Make matplotlib marker name more readable"""
    name = name.replace("_", " ").capitalize()
    if name.startswith("Caret"):
        name = insert_space(name, len("Caret"))
        if name.endswith("base"):
            return insert_space(name, len(name) - len("base"))
        return name
    elif name.startswith("Tick"):
        return insert_space(name, len("Tick"))
    elif len(name) == 5 and name.endswith("line"):
        return insert_space(name, 1)
    return name


def init_markers() -> dict:
    markers = {}
    for key, name in MarkerStyle.markers.items():
        if name != "nothing":
            markers[key] = marker_name_title(name)
    return markers


class ShapeComboBox(QComboBox):
    """Thin wrapper over combobox for use for matplotlib shapes"""

    update_shape = Signal()

    def __init__(self, init_items, parent):
        """init_items: a generated dictionary of matplotlib shapes"""
        super().__init__(parent)
        for key, name in init_items.items():
            self.addItem(name, key)
        self.setCurrentIndex(0)
        self.currentIndexChanged.connect(self.update_shape)

    def set_shape(self, shape_data: str):
        idx = self.findData(shape_data)
        if idx >= 0:
            self.setCurrentIndex(idx)

    def shape(self):
        return self.currentData()
