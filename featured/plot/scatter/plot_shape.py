# -*- coding: utf-8 -*-
# Copyright (C) 2023 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset

from PySide2 import QtCore, QtWidgets
from cresset import flare
from .col_type import ColType
from .marker import init_markers, ShapeComboBox
from ..multi_str_dialog import MultiStrSelDialog


class LigandScatterPlotAxisShapeConfig:
    """Shape axis configuration"""

    def __init__(self):
        self.column = None
        self.column_type = None
        self.str_default_shape = "o"
        self.str_custom_shapes = {}
        self.tag_default_shape = "o"
        self.tag_custom_shapes = []
        self.role_default_shape = "o"
        self.role_custom_shapes = {}
        self.fav_false_shape = "o"
        self.fav_true_shape = "x"
        self.am_focus_shape = "o"
        self.am_not_focus_shape = "x"

    def to_dict(self):
        dict = {}
        dict["Column"] = self.column
        dict["Column Type"] = self.column_type
        dict["Str Default Shape"] = self.str_default_shape
        dict["Str Custom Shapes"] = self.str_custom_shapes
        dict["Tag Default Shape"] = self.tag_default_shape
        dict["Tag Custom Shapes"] = self.tag_custom_shapes
        dict["Role Default Shape"] = self.role_default_shape
        dict["Role Custom Shapes"] = self.role_custom_shapes
        dict["Fav False Shape"] = self.fav_false_shape
        dict["Fav True Shape"] = self.fav_true_shape
        dict["AM Focus Shape"] = self.am_focus_shape
        dict["AM Not Focus Shape"] = self.am_not_focus_shape
        return dict

    def from_dict(self, dict):
        self.column = dict.get("Column", self.column)
        self.column_type = dict.get("Column Type", self.column_type)
        self.str_default_shape = dict.get("Str Default Shape", self.str_default_shape)
        self.str_custom_shapes = dict.get("Str Custom Shapes", self.str_custom_shapes)
        self.tag_default_shape = dict.get("Tag Default Shape", self.tag_default_shape)
        self.tag_custom_shapes = dict.get("Tag Custom Shapes", self.tag_custom_shapes)
        self.role_default_shape = dict.get("Role Default Shape", self.role_default_shape)
        self.role_custom_shapes = dict.get("Role Custom Shapes", self.role_custom_shapes)
        self.fav_false_shape = dict.get("Fav False Shape", self.fav_false_shape)
        self.fav_true_shape = dict.get("Fav True Shape", self.fav_true_shape)
        self.am_focus_shape = dict.get("AM Focus Shape", self.am_focus_shape)
        self.am_not_focus_shape = dict.get("AM Not Focus Shape", self.am_not_focus_shape)

    def fav_shape(self, is_fav: bool) -> str:
        return self.fav_true_shape if is_fav else self.fav_false_shape

    def am_shape(self, is_am_focus: bool) -> str:
        return self.am_focus_shape if is_am_focus else self.am_not_focus_shape

    def add_shape_data(self, shape_data, ligand):
        # Check shape by
        shape_axis_col = self.column
        if self.column_type == ColType.STRING:
            shape_value = str(ligand.properties[shape_axis_col].value)
            str_shape = self.str_default_shape
            # Sub-string matching
            for cmp_str_value, cmp_str_shape in self.str_custom_shapes.items():
                if cmp_str_value in shape_value:
                    str_shape = cmp_str_shape
                    break
            shape_data.append(str_shape)
        elif self.column_type == ColType.TAG:
            tag_shape = self.tag_default_shape
            tag_value = ligand.properties[shape_axis_col].value
            if tag_value and len(tag_value) > 0:
                # Check for exact match
                has_match = False
                for tag_data in self.tag_custom_shapes:
                    tag_count = (
                        sum(tag in tag_value for tag in tag_data["tags"]) if tag_data["tags"] else 0
                    )
                    if tag_data["tags"] and tag_count == len(tag_data["tags"]):
                        tag_shape = tag_data["shape"]
                        has_match = True
                        break

                # Otherwise check for most relative match
                if not has_match:
                    prev_tag_count = 0
                    for tag_data in self.tag_custom_shapes:
                        tag_count = (
                            sum(tag in tag_value for tag in tag_data["tags"])
                            if tag_data["tags"]
                            else 0
                        )
                        if tag_count > prev_tag_count:
                            tag_shape = tag_data["shape"]
                            prev_tag_count = tag_count
            shape_data.append(tag_shape)
        elif self.column_type == ColType.ROLE:
            role_shape = self.role_default_shape
            if ligand.role is not None:
                role_value = ligand.role.name
                if role_value in self.role_custom_shapes:
                    role_shape = self.role_custom_shapes[role_value]
            shape_data.append(role_shape)
        elif self.column_type == ColType.FAV:
            shape_data.append(self.fav_shape(ligand.properties[shape_axis_col].value))
        elif self.column_type == ColType.AM_FOCUS:
            shape_data.append(self.am_shape(ligand.properties[shape_axis_col].value))


class CustomShapeWidgets(QtCore.QObject):
    """Widgets wrapper for custom defined shapes"""

    update_shapes = QtCore.Signal()

    def __init__(self, parent, init_items, editable):
        super().__init__()
        self.parent = parent
        self._editable = editable

        self.value_combo_box = QtWidgets.QComboBox()
        self.value_combo_box.setToolTip("<span>Select a value to map a shape to.")
        self.shape_combo_box = ShapeComboBox(init_items, parent)
        self.shape_combo_box.setToolTip("<span>Select the shape to map this value to.")
        self.delete_button = QtWidgets.QPushButton("Delete")
        self.delete_button.setToolTip("<span>Delete this shape mapping.")

        self._CUSTOM_EDIT_STRING = "<Type a search string>"

        if editable:
            self.value_combo_box.addItem(self._CUSTOM_EDIT_STRING)
            self.value_combo_box.currentIndexChanged.connect(self._on_value_changed)
        else:
            self.value_combo_box.currentIndexChanged.connect(self._on_value_edit)
        self.shape_combo_box.update_shape.connect(self._on_shape_changed)
        self.delete_button.clicked.connect(self._on_delete_clicked)

    def custom_shapes(self) -> dict:
        shapes = {}
        for i in range(1 if self._editable else 0, self.value_combo_box.count()):
            shapes[self.value_combo_box.itemText(i)] = self.value_combo_box.itemData(i)
        return shapes

    def set_custom_shapes(self, custom_shapes: str):
        self.value_combo_box.setCurrentIndex(-1)
        self.value_combo_box.clear()
        if self._editable:
            self.value_combo_box.addItem(self._CUSTOM_EDIT_STRING)
        for key, shape in custom_shapes.items():
            self.value_combo_box.addItem(key, shape)

        if self.value_combo_box.count() > 0:
            self.value_combo_box.setCurrentIndex(0)
        self._on_value_edit()

    def _on_value_changed(self, idx: int):
        if self.value_combo_box.isEditable():
            self.value_combo_box.lineEdit().editingFinished.disconnect(self._on_value_edit)
        self.value_combo_box.setEditable(idx == 0)
        self._on_value_edit()
        if self.value_combo_box.isEditable():
            self.value_combo_box.lineEdit().editingFinished.connect(self._on_value_edit)

    def _on_shape_changed(self):
        self.value_combo_box.setItemData(
            self.value_combo_box.currentIndex(), self.shape_combo_box.shape()
        )
        self.update_shapes.emit()

    def _on_value_edit(self):
        if self.value_combo_box.currentText() != "":
            self.shape_combo_box.set_shape(self.value_combo_box.currentData())
            self.update_shapes.emit()
        else:
            self.shape_combo_box.setCurrentIndex(0)

    def _on_delete_clicked(self):
        idx = self.value_combo_box.currentIndex()
        if idx >= 0:
            self.value_combo_box.removeItem(idx)
            self.update_shapes.emit()


class LigandScatterPlotConfigShapeWidget:
    """Shape configuration widget"""

    def __init__(self, parent):
        self.parent = parent
        self.label = QtWidgets.QLabel("Shape by", parent)

        self.column_combo_box = QtWidgets.QComboBox(parent)
        self.column_combo_box.setToolTip(
            "<span>If set, then the shape of points in the plot will be "
            + "determined by the value of the specified column."
        )

        self.cache = {}
        self.prev_column = -1
        self._init_markers = init_markers()
        self.supported_types: list[ColType] = []

        self.stacked = QtWidgets.QStackedWidget(parent)
        self.stacked.setLineWidth(0)
        self.stacked.setFrameStyle(QtWidgets.QFrame.NoFrame)
        self.stacked.addWidget(self._init_none())
        self.stacked.addWidget(self._init_string())
        self.stacked.addWidget(self._init_tag())
        self.stacked.addWidget(self._init_role())
        self.stacked.addWidget(self._init_fav())
        self.stacked.addWidget(self._init_am_focus())

        self.column_combo_box.currentIndexChanged.connect(parent._on_setting_changed)
        self.column_combo_box.currentIndexChanged.connect(self._on_column_changed)

    def _init_none(self) -> QtWidgets.QFrame:
        self.supported_types.append(ColType.NONE)
        frame = QtWidgets.QFrame()
        frame.setLineWidth(0)
        frame.setFrameStyle(QtWidgets.QFrame.NoFrame)
        return frame

    def _init_string(self) -> QtWidgets.QFrame:
        self.supported_types.append(ColType.STRING)
        frame = QtWidgets.QFrame()
        frame.setLineWidth(0)
        frame.setFrameStyle(QtWidgets.QFrame.NoFrame)

        self.default_str_shape = ShapeComboBox(self._init_markers, self.parent)
        self.default_str_shape.setToolTip("<span>Set the default shape to use for data points.")
        self.custom_str = CustomShapeWidgets(self.parent, self._init_markers, editable=True)

        self.default_str_shape.update_shape.connect(self.parent._on_setting_changed)
        self.custom_str.update_shapes.connect(self.parent._on_setting_changed)

        layout = QtWidgets.QHBoxLayout(frame)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(QtWidgets.QLabel("Default:"))
        layout.addWidget(self.default_str_shape)
        layout.addWidget(QtWidgets.QLabel("Containing:"))
        layout.addWidget(self.custom_str.value_combo_box)
        layout.addWidget(self.custom_str.shape_combo_box)
        layout.addWidget(self.custom_str.delete_button)
        return frame

    def _init_tag(self) -> QtWidgets.QFrame:
        self.supported_types.append(ColType.TAG)
        frame = QtWidgets.QFrame()
        frame.setLineWidth(0)
        frame.setFrameStyle(QtWidgets.QFrame.NoFrame)

        self._tags_cache = set()

        self.default_tag_shape = ShapeComboBox(self._init_markers, self.parent)
        self.default_tag_shape.setToolTip("<span>Set the default shape to use for data points.")

        self.custom_tag = CustomShapeWidgets(self.parent, self._init_markers, editable=False)
        self.custom_tag_add = QtWidgets.QPushButton("Add")
        self.custom_tag_add.setToolTip("<span>Add a new mapping between a tag and a shape.")

        self.custom_tag_dlg = MultiStrSelDialog(self.parent, "Pick tags to shape")

        self.default_tag_shape.update_shape.connect(self.parent._on_setting_changed)
        self.custom_tag.update_shapes.connect(self.parent._on_setting_changed)
        self.custom_tag_add.clicked.connect(self._on_custom_tag_add)
        self.custom_tag_dlg.new_strs.connect(self._on_custom_tag_new_strs)

        layout = QtWidgets.QHBoxLayout(frame)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(QtWidgets.QLabel("Default:"))
        layout.addWidget(self.default_tag_shape)
        layout.addWidget(QtWidgets.QLabel("Custom tags:"))
        layout.addWidget(self.custom_tag.value_combo_box)
        layout.addWidget(self.custom_tag.shape_combo_box)
        layout.addWidget(self.custom_tag.delete_button)
        layout.addWidget(self.custom_tag_add)
        return frame

    def _init_role(self):
        self.supported_types.append(ColType.ROLE)
        frame = QtWidgets.QFrame()
        frame.setLineWidth(0)
        frame.setFrameStyle(QtWidgets.QFrame.NoFrame)

        self.default_role_shape = ShapeComboBox(self._init_markers, self.parent)
        self.default_role_shape.setToolTip("<span>Set the default shape to use for data points.")
        self.custom_role = CustomShapeWidgets(self.parent, self._init_markers, editable=False)
        self.custom_role.value_combo_box.setToolTip(
            '<span>Set the role to map the shape to. Click on "Add" to add a role and set a shape.'
        )
        self.custom_role_add = QtWidgets.QPushButton("Add")
        self.custom_role_add.setToolTip("<span>Add a new mapping between a role and a shape.")
        self.custom_role_dlg = MultiStrSelDialog(
            self.parent, "Pick a role to shape", checked_list=False
        )

        self.default_role_shape.update_shape.connect(self.parent._on_setting_changed)
        self.custom_role.update_shapes.connect(self.parent._on_setting_changed)
        self.custom_role_add.clicked.connect(self._on_custom_role_add)
        self.custom_role_dlg.new_strs.connect(self._on_custom_role_new_strs)

        layout = QtWidgets.QHBoxLayout(frame)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(QtWidgets.QLabel("Default:"))
        layout.addWidget(self.default_role_shape)
        layout.addWidget(QtWidgets.QLabel("Role:"))
        layout.addWidget(self.custom_role.value_combo_box)
        layout.addWidget(self.custom_role.shape_combo_box)
        layout.addWidget(self.custom_role.delete_button)
        layout.addWidget(self.custom_role_add)
        return frame

    def _init_fav(self):
        self.supported_types.append(ColType.FAV)
        frame = QtWidgets.QFrame()
        frame.setLineWidth(0)
        frame.setFrameStyle(QtWidgets.QFrame.NoFrame)

        self.fav_false = ShapeComboBox(self._init_markers, self.parent)
        self.fav_false.setToolTip("<span>The shape that will be applied to non-favorite ligands.")
        self.fav_true = ShapeComboBox(self._init_markers, self.parent)
        self.fav_true.setToolTip("<span>The shape that will be applied to favorite ligands.")

        self.fav_false.update_shape.connect(self.parent._on_setting_changed)
        self.fav_true.update_shape.connect(self.parent._on_setting_changed)

        layout = QtWidgets.QHBoxLayout(frame)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(QtWidgets.QLabel("False:"))
        layout.addWidget(self.fav_false)
        layout.addWidget(QtWidgets.QLabel("True:"))
        layout.addWidget(self.fav_true)
        return frame

    def _init_am_focus(self):
        self.supported_types.append(ColType.AM_FOCUS)
        frame = QtWidgets.QFrame()
        frame.setLineWidth(0)
        frame.setFrameStyle(QtWidgets.QFrame.NoFrame)

        self.am_focus = ShapeComboBox(self._init_markers, self.parent)
        self.am_focus.setToolTip(
            "<span>The shape that will be applied to the ligand focused by Activity Miner."
        )
        self.am_not_focus = ShapeComboBox(self._init_markers, self.parent)
        self.am_not_focus.setToolTip(
            "<span>The shape that will be applied to ligands not focused by Activity Miner."
        )

        self.am_focus.update_shape.connect(self.parent._on_setting_changed)
        self.am_not_focus.update_shape.connect(self.parent._on_setting_changed)

        layout = QtWidgets.QHBoxLayout(frame)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(QtWidgets.QLabel("Focused:"))
        layout.addWidget(self.am_focus)
        layout.addWidget(QtWidgets.QLabel("Not Focused:"))
        layout.addWidget(self.am_not_focus)
        return frame

    def _on_custom_tag_add(self):
        self.custom_tag_dlg.open_with(self._tags_cache)

    def _on_custom_tag_new_strs(self, new_strs: set):
        idx = self.custom_tag.value_combo_box.findData(new_strs, QtCore.Qt.UserRole + 1)
        if idx < 0:
            view_str = ",".join(new_strs)
            self.custom_tag.value_combo_box.addItem(view_str, "o")
            idx = self.custom_tag.value_combo_box.count() - 1
            self.custom_tag.value_combo_box.setItemData(idx, new_strs, QtCore.Qt.UserRole + 1)
        self.custom_tag.value_combo_box.setCurrentIndex(idx)

    def _on_custom_role_add(self):
        project = flare.main_window().project
        self.custom_role_dlg.open_with([role.name for role in project.roles])

    def _on_custom_role_new_strs(self, new_role: str):
        idx = self.custom_role.value_combo_box.findText(new_role)
        if idx < 0:
            self.custom_role.value_combo_box.addItem(new_role, "o")
            idx = self.custom_role.value_combo_box.count() - 1
        self.custom_role.value_combo_box.setCurrentIndex(idx)

    def tags_custom_shapes(self) -> dict:
        tags_shapes = []
        for i in range(0, self.custom_tag.value_combo_box.count()):
            tags_shapes.append(
                {
                    "tags": self.custom_tag.value_combo_box.itemData(i, QtCore.Qt.UserRole + 1),
                    "shape": self.custom_tag.value_combo_box.itemData(i),
                }
            )
        return tags_shapes

    def set_tags_custom_shapes(self, custom_shapes: dict):
        self.custom_tag.value_combo_box.clear()
        for shape_data in custom_shapes:
            tags = shape_data["tags"]
            shape = shape_data["shape"]
            tags_disp = ",".join(tags)
            self.custom_tag.value_combo_box.addItem(tags_disp, shape)
            endIdx = self.custom_tag.value_combo_box.count() - 1
            self.custom_tag.value_combo_box.setItemData(endIdx, tags, QtCore.Qt.UserRole + 1)

    def _on_column_changed(self):
        # Save into cache for prev column
        prev_type = self.column_combo_box.itemData(self.prev_column)
        prev_text = self.column_combo_box.itemText(self.prev_column)
        if prev_text != "":
            if prev_type == ColType.STRING:
                self.cache[prev_text] = {
                    "default": self.default_str_shape.shape(),
                    "custom": self.custom_str.custom_shapes(),
                }

        # Set widget from stacked to its type
        current_type = self.column_combo_box.currentData()
        self.stacked.setCurrentIndex(
            self.supported_types.index(current_type) if current_type in self.supported_types else 0
        )

        # Grab from cache/set data for current column if available
        if current_type == ColType.STRING:
            current_text = self.column_combo_box.currentText()
            if current_text not in self.cache:
                self.cache[current_text] = {
                    "default": "o",
                    "custom": {},
                }
            self.default_str_shape.set_shape(self.cache[current_text]["default"])
            self.custom_str.set_custom_shapes(self.cache[current_text]["custom"])
        elif current_type == ColType.TAG:
            self._update_tags()

        self.prev_column = self.column_combo_box.currentIndex()

    def update_property_cache(self, property_name: str):
        if property_name == "Tags":
            self._update_tags()

    def _update_tags(self):
        self._tags_cache.clear()
        project = flare.main_window().project
        for ligand in project.ligands:
            tags = ligand.properties["Tags"].value
            for tag in tags:
                self._tags_cache.add(tag)

    def set_axis_config(self, shape_config):
        column_index = 0
        if shape_config.column is not None:
            column_index = self.column_combo_box.findText(shape_config.column)
            if column_index < 0:
                column_index = 0
        self.column_combo_box.setCurrentIndex(column_index)

        if shape_config.column_type == ColType.STRING:
            self.default_str_shape.set_shape(shape_config.str_default_shape)
            self.custom_str.set_custom_shapes(shape_config.str_custom_shapes)
        elif shape_config.column_type == ColType.TAG:
            self.default_tag_shape.set_shape(shape_config.tag_default_shape)
            self.set_tags_custom_shapes(shape_config.tag_custom_shapes)
        elif shape_config.column_type == ColType.ROLE:
            self.default_role_shape.set_shape(shape_config.role_default_shape)
            self.custom_role.set_custom_shapes(shape_config.role_custom_shapes)
        elif shape_config.column_type == ColType.FAV:
            self.fav_false.set_shape(shape_config.fav_false_shape)
            self.fav_true.set_shape(shape_config.fav_true_shape)
        elif shape_config.column_type == ColType.AM_FOCUS:
            self.am_focus.set_shape(shape_config.am_focus_shape)
            self.am_not_focus.set_shape(shape_config.am_not_focus_shape)

    def axis_config(self):
        shape_config = LigandScatterPlotAxisShapeConfig()
        if self.column_combo_box.currentIndex() > 0:
            shape_config.column = self.column_combo_box.currentText()
            shape_config.column_type = self.column_combo_box.currentData()

        if shape_config.column_type == ColType.STRING:
            shape_config.str_default_shape = self.default_str_shape.shape()
            shape_config.str_custom_shapes = self.custom_str.custom_shapes()
        elif shape_config.column_type == ColType.TAG:
            shape_config.tag_default_shape = self.default_tag_shape.shape()
            shape_config.tag_custom_shapes = self.tags_custom_shapes()
        elif shape_config.column_type == ColType.ROLE:
            shape_config.role_default_shape = self.default_role_shape.shape()
            shape_config.role_custom_shapes = self.custom_role.custom_shapes()
        elif shape_config.column_type == ColType.FAV:
            shape_config.fav_false_shape = self.fav_false.shape()
            shape_config.fav_true_shape = self.fav_true.shape()
        elif shape_config.column_type == ColType.AM_FOCUS:
            shape_config.am_focus_shape = self.am_focus.shape()
            shape_config.am_not_focus_shape = self.am_not_focus.shape()
        return shape_config
