# -*- coding: utf-8 -*-
# Copyright (C) 2023 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset

from PySide2 import QtWidgets
from cresset import flare
from sys import maxsize
from .col_type import ColType


class LigandScatterPlotAxisSizeConfig:
    """Size axis configuration"""

    def __init__(self):
        self.column = None
        self.column_type = None
        self.min_size = 6
        self.min_value = 0.0
        self.max_size = 12
        self.max_value = 100.0
        self.fav_false = 6
        self.fav_true = 12
        self.am_focus = 12
        self.am_not_focus = 6

        self._ligand_column_value = None
        self._size_value_range = None
        self._size_range = None

    def to_dict(self):
        dict = {}
        dict["Column"] = self.column
        dict["Column Type"] = self.column_type
        dict["Min Size"] = self.min_size
        dict["Min Value"] = self.min_value
        dict["Max Size"] = self.max_size
        dict["Max Value"] = self.max_value
        dict["Fav False"] = self.fav_false
        dict["Fav True"] = self.fav_true
        dict["AM Focus"] = self.am_focus
        dict["AM Not Focus"] = self.am_not_focus
        return dict

    def from_dict(self, dict):
        self.column = dict.get("Column", self.column)
        self.column_type = dict.get("Column Type", self.column_type)
        self.min_size = dict.get("Min Size", self.min_size)
        self.min_value = dict.get("Min Value", self.min_value)
        self.max_size = dict.get("Max Size", self.max_size)
        self.max_value = dict.get("Max Value", self.max_value)
        self.fav_false = dict.get("Fav False", self.fav_false)
        self.fav_true = dict.get("Fav True", self.fav_true)
        self.am_focus = dict.get("AM Focus", self.am_focus)
        self.am_not_focus = dict.get("AM Not Focus", self.am_not_focus)

    def inverted_size(self) -> bool:
        return self.max_size < self.min_size

    def inverted_value(self) -> bool:
        return self.max_value < self.min_value

    def inverted(self) -> bool:
        return self.inverted_value() or self.inverted_size()

    def range_value(self) -> float:
        if self.inverted_value():
            return self.min_value - self.max_value
        return self.max_value - self.min_value

    def range_size(self) -> float:
        if self.inverted_size():
            return self.min_size - self.max_size
        return self.max_size - self.min_size

    def true_max_value(self) -> float:
        if self.inverted_value():
            return self.min_value
        return self.max_value

    def true_min_value(self) -> float:
        if self.inverted_value():
            return self.max_value
        return self.min_value

    def true_max_size(self) -> float:
        if self.inverted_size():
            return self.min_size
        return self.max_size

    def true_min_size(self) -> float:
        if self.inverted_size():
            return self.max_size
        return self.min_size

    def setup_data(self, ligand_column_value):
        self._ligand_column_value = ligand_column_value
        self._size_value_range = self.range_value()
        self._size_range = self.range_size()

    def add_size_data(self, size_data, ligand):
        size_axis_col = self.column
        if self.column_type == ColType.NUMERIC:
            size_value = self._ligand_column_value(ligand, size_axis_col)
            size_norm = 0.0
            if size_value is None:
                size_norm = 0.0
            elif size_value >= self.true_max_value():
                size_norm = 1.0
            elif size_value <= self.true_min_value():
                size_norm = 0.0
            else:
                size_norm = (size_value - self.true_min_value()) / self._size_value_range

            if self.inverted_value():
                size_norm = 1.0 - size_norm
            if self.inverted_size():
                size_norm = 1.0 - size_norm
            size_data.append((self.true_min_size() + (size_norm * self._size_range)) ** 2)
        elif self.column_type == ColType.FAV:
            size_data.append(
                (self.fav_true if ligand.properties[size_axis_col].value else self.fav_false) ** 2
            )
        elif self.column_type == ColType.AM_FOCUS:
            size_data.append(
                (self.am_focus if ligand.properties[size_axis_col].value else self.am_not_focus)
                ** 2
            )


class LigandScatterPlotConfigSizeWidget:
    """Size axis configuration widget"""

    def __init__(self, parent):
        self.parent = parent
        self.label = QtWidgets.QLabel("Size by", parent)

        self.column_combo_box = QtWidgets.QComboBox(parent)
        self.column_combo_box.setToolTip(
            "<span>Select or type a value to set the plot point size by."
        )
        self.column_combo_box.currentIndexChanged.connect(parent._on_setting_changed)
        self.column_combo_box.currentIndexChanged.connect(self._on_column_changed)

        self.prev_column = -1
        self.supported_types: list[ColType] = []
        self._DEFAULT_VALUES = LigandScatterPlotAxisSizeConfig()

        self.stacked = QtWidgets.QStackedWidget(parent)
        self.stacked.setLineWidth(0)
        self.stacked.setFrameStyle(QtWidgets.QFrame.NoFrame)
        self.stacked.addWidget(self._init_none())
        self.stacked.addWidget(self._init_numeric())
        self.stacked.addWidget(self._init_fav())
        self.stacked.addWidget(self._init_am_focus())

    def _init_size_spinbox(self, default_value: int) -> QtWidgets.QSpinBox:
        spinbox = QtWidgets.QSpinBox()
        spinbox.setMinimum(0)
        spinbox.setMaximum(1000)
        spinbox.setValue(default_value)
        spinbox.valueChanged.connect(self.parent._on_setting_changed)
        return spinbox

    def _init_value_spinbox(self) -> QtWidgets.QDoubleSpinBox:
        spinbox = QtWidgets.QDoubleSpinBox()
        spinbox.setMinimum(-10000000)
        spinbox.setMaximum(10000000)
        spinbox.valueChanged.connect(self.parent._on_setting_changed)
        return spinbox

    def _init_none(self) -> QtWidgets.QFrame:
        self.supported_types.append(ColType.NONE)
        frame = QtWidgets.QFrame()
        frame.setLineWidth(0)
        frame.setFrameStyle(QtWidgets.QFrame.NoFrame)
        return frame

    def _init_numeric(self) -> QtWidgets.QFrame:
        self.supported_types.append(ColType.NUMERIC)
        frame = QtWidgets.QFrame()
        frame.setLineWidth(0)
        frame.setFrameStyle(QtWidgets.QFrame.NoFrame)

        self.min_value_spinbox = self._init_value_spinbox()
        self.min_value_spinbox.setToolTip(
            "Set the value of the column to map to the smallest size."
        )
        self.min_size_spinbox = self._init_size_spinbox(self._DEFAULT_VALUES.min_size)
        self.min_size_spinbox.setToolTip("Set the size to use for the smallest value.")
        self.max_value_spinbox = self._init_value_spinbox()
        self.max_value_spinbox.setToolTip("Set the value of the column to map to the largest size.")
        self.max_size_spinbox = self._init_size_spinbox(self._DEFAULT_VALUES.max_size)
        self.max_size_spinbox.setToolTip("Set the size to use for the largest value.")

        layout = QtWidgets.QHBoxLayout(frame)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(QtWidgets.QLabel("Min value:"))
        layout.addWidget(self.min_value_spinbox)
        layout.addWidget(self.min_size_spinbox)
        layout.addWidget(QtWidgets.QLabel("Max value:"))
        layout.addWidget(self.max_value_spinbox)
        layout.addWidget(self.max_size_spinbox)
        return frame

    def _init_fav(self) -> QtWidgets.QFrame:
        self.supported_types.append(ColType.FAV)
        frame = QtWidgets.QFrame()
        frame.setLineWidth(0)
        frame.setFrameStyle(QtWidgets.QFrame.NoFrame)

        self.fav_false_spinbox = self._init_size_spinbox(self._DEFAULT_VALUES.fav_false)
        self.fav_false_spinbox.setToolTip("Set the size to use for non-favorite ligands.")
        self.fav_true_spinbox = self._init_size_spinbox(self._DEFAULT_VALUES.fav_true)
        self.fav_true_spinbox.setToolTip("Set the size to use for favorite ligands.")

        layout = QtWidgets.QHBoxLayout(frame)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(QtWidgets.QLabel("False:"))
        layout.addWidget(self.fav_false_spinbox)
        layout.addWidget(QtWidgets.QLabel("True:"))
        layout.addWidget(self.fav_true_spinbox)
        return frame

    def _init_am_focus(self) -> QtWidgets.QFrame:
        self.supported_types.append(ColType.AM_FOCUS)
        frame = QtWidgets.QFrame()
        frame.setLineWidth(0)
        frame.setFrameStyle(QtWidgets.QFrame.NoFrame)

        self.am_focus_spinbox = self._init_size_spinbox(self._DEFAULT_VALUES.am_focus)
        self.am_focus_spinbox.setToolTip(
            "Set the size to use for the ligand focused by Activity Miner."
        )
        self.am_not_focus_spinbox = self._init_size_spinbox(self._DEFAULT_VALUES.am_not_focus)
        self.am_not_focus_spinbox.setToolTip(
            "Set the size to use for ligands not focused by Activity Miner."
        )

        layout = QtWidgets.QHBoxLayout(frame)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(QtWidgets.QLabel("Focused:"))
        layout.addWidget(self.am_focus_spinbox)
        layout.addWidget(QtWidgets.QLabel("Not Focused:"))
        layout.addWidget(self.am_not_focus_spinbox)
        return frame

    def _on_column_changed(self):
        # Set widget from stacked to its type
        current_type = self.column_combo_box.currentData()
        self.stacked.setCurrentIndex(
            self.supported_types.index(current_type) if current_type in self.supported_types else 0
        )

        if current_type == ColType.NUMERIC:
            # Find min/max values
            project = flare.main_window().project
            colname = self.column_combo_box.currentText()
            lmin_val = maxsize
            lmax_val = -maxsize
            for ligand in project.ligands:
                value = ligand.properties[colname].value
                if isinstance(value, (int, float)) and not isinstance(value, bool):
                    if value > lmax_val:
                        lmax_val = value
                    if value < lmin_val:
                        lmin_val = value
            self.min_value_spinbox.setValue(lmin_val)
            self.max_value_spinbox.setValue(lmax_val)

    def set_axis_config(self, size_config: LigandScatterPlotAxisSizeConfig):
        column_index = 0
        if size_config.column is not None:
            column_index = self.column_combo_box.findText(size_config.column)
            if column_index < 0:
                column_index = 0
        self.column_combo_box.setCurrentIndex(column_index)

        if size_config.column_type == ColType.NUMERIC:
            self.min_size_spinbox.setValue(size_config.min_size)
            self.max_size_spinbox.setValue(size_config.max_size)
            self.min_value_spinbox.setValue(size_config.min_value)
            self.max_value_spinbox.setValue(size_config.max_value)
        elif size_config.column_type == ColType.FAV:
            self.fav_false_spinbox.setValue(size_config.fav_false)
            self.fav_true_spinbox.setValue(size_config.fav_true)
        elif size_config.column_type == ColType.AM_FOCUS:
            self.am_focus_spinbox.setValue(size_config.am_focus)
            self.am_not_focus_spinbox.setValue(size_config.am_not_focus)

    def axis_config(self) -> LigandScatterPlotAxisSizeConfig:
        size_config = LigandScatterPlotAxisSizeConfig()
        if self.column_combo_box.currentIndex() > 0:
            size_config.column = self.column_combo_box.currentText()
            size_config.column_type = self.column_combo_box.currentData()

        if size_config.column_type == ColType.NUMERIC:
            size_config.min_size = self.min_size_spinbox.value()
            size_config.max_size = self.max_size_spinbox.value()
            size_config.min_value = self.min_value_spinbox.value()
            size_config.max_value = self.max_value_spinbox.value()
        elif size_config.column_type == ColType.FAV:
            size_config.fav_false = self.fav_false_spinbox.value()
            size_config.fav_true = self.fav_true_spinbox.value()
        elif size_config.column_type == ColType.AM_FOCUS:
            size_config.am_focus = self.am_focus_spinbox.value()
            size_config.am_not_focus = self.am_not_focus_spinbox.value()
        return size_config
