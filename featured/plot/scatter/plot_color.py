# -*- coding: utf-8 -*-
# Copyright (C) 2023 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset

from PySide2 import QtCore, QtWidgets
from cresset import flare
from sys import maxsize

from matplotlib.colors import LinearSegmentedColormap

from .col_type import ColType
from ..color_button import ColorButton
from ..multi_str_dialog import MultiStrSelDialog


def radial_plot_properties_name_map(radial_plot_properties) -> dict:
    rp_names_map = {}
    for radial_plot_property in radial_plot_properties:
        rp_names_map[radial_plot_property.display_name] = radial_plot_property.name
    return rp_names_map


class LigandScatterPlotAxisColorConfig:
    """Color axis configuration"""

    def __init__(self):
        self.column = None
        self.column_type = None
        self.min_color = "#000000"
        self.min_value = 0
        self.max_color = "#FF0000"
        self.max_value = 100
        self.str_default_color = "#000000"
        self.str_custom_colors = {}
        self.tag_default_color = "#000000"
        self.tag_custom_colors = []
        self.role_default_color = "#000000"
        self.role_custom_colors = {}
        self.radial_plot_column = ""
        self.fav_false_color = "#000000"
        self.fav_true_color = "#CCCCCC"
        self.am_focus_color = "#CCCCCC"
        self.am_not_focus_color = "#000000"

        self._ligand_column_value = None
        self._color_value_range = None
        self._cmap = None

    def to_dict(self):
        dict = {}
        dict["Column"] = self.column
        dict["Column Type"] = self.column_type
        dict["Min Color"] = self.min_color
        dict["Min Value"] = self.min_value
        dict["Max Color"] = self.max_color
        dict["Max Value"] = self.max_value
        dict["Str Default Color"] = self.str_default_color
        dict["Str Custom Colors"] = self.str_custom_colors
        dict["Tag Default Color"] = self.tag_default_color
        dict["Tag Custom Colors"] = self.tag_custom_colors
        dict["Role Default Color"] = self.role_default_color
        dict["Role Custom Colors"] = self.role_custom_colors
        dict["Radial Plot Column"] = self.radial_plot_column
        dict["Fav False Color"] = self.fav_false_color
        dict["Fav True Color"] = self.fav_true_color
        dict["AM Focus Color"] = self.am_focus_color
        dict["AM Not Focus Color"] = self.am_not_focus_color
        return dict

    def from_dict(self, dict):
        self.column = dict.get("Column", self.column)
        self.column_type = dict.get("Column Type", self.column_type)
        self.min_color = dict.get("Min Color", self.min_color)
        self.min_value = dict.get("Min Value", self.min_value)
        self.max_color = dict.get("Max Color", self.max_color)
        self.max_value = dict.get("Max Value", self.max_value)
        self.str_default_color = dict.get("Str Default Color", self.str_default_color)
        self.str_custom_colors = dict.get("Str Custom Colors", self.str_custom_colors)
        self.tag_default_color = dict.get("Tag Default Color", self.tag_default_color)
        self.tag_custom_colors = dict.get("Tag Custom Colors", self.tag_custom_colors)
        self.role_default_color = dict.get("Role Default Color", self.role_default_color)
        self.role_custom_colors = dict.get("Role Custom Colors", self.role_custom_colors)
        self.radial_plot_column = dict.get("Radial Plot Column", self.radial_plot_column)
        self.fav_false_color = dict.get("Fav False Color", self.fav_false_color)
        self.fav_true_color = dict.get("Fav True Color", self.fav_true_color)
        self.am_focus_color = dict.get("AM Focus Color", self.am_focus_color)
        self.am_not_focus_color = dict.get("AM Not Focus Color", self.am_not_focus_color)

    def fav_color(self, is_fav: bool) -> str:
        return self.fav_true_color if is_fav else self.fav_false_color

    def am_color(self, is_am_focus: bool) -> str:
        return self.am_focus_color if is_am_focus else self.am_not_focus_color

    def setup_data(self, ligand_column_value):
        self._ligand_column_value = ligand_column_value
        self._color_value_range = self.max_value - self.min_value
        self._cmap = LinearSegmentedColormap.from_list("", [self.min_color, self.max_color])

    def add_color_data(self, color_data, ligand):
        color_axis_col = self.column
        if self.column_type == ColType.NUMERIC:
            color_value = self._ligand_column_value(ligand, color_axis_col)
            color_norm = 0.0
            if color_value is None:
                color_norm = 0.0
            elif color_value >= self.max_value:
                color_norm = 1.0
            elif color_value <= self.min_value:
                color_norm = 0.0
            else:
                color_norm = (color_value - self.min_value) / self._color_value_range
            color_data.append(self._cmap(color_norm))
        elif self.column_type == ColType.STRING:
            str_value = str(ligand.properties[color_axis_col].value)
            str_color = self.str_default_color
            # Sub-string matching
            for cmp_str_value, cmp_str_color in self.str_custom_colors.items():
                if cmp_str_value in str_value:
                    str_color = cmp_str_color
                    break
            color_data.append(str_color)
        elif self.column_type == ColType.TAG:
            tag_color = self.tag_default_color
            tag_value = ligand.properties[color_axis_col].value
            if tag_value and len(tag_value) > 0:
                # Check for exact match
                has_match = False
                for tag_data in self.tag_custom_colors:
                    tag_count = (
                        sum(tag in tag_value for tag in tag_data["tags"]) if tag_data["tags"] else 0
                    )
                    if tag_data["tags"] and tag_count == len(tag_data["tags"]):
                        tag_color = tag_data["color"]
                        has_match = True
                        break

                # Otherwise check for most relative match
                if not has_match:
                    prev_tag_count = 0
                    for tag_data in self.tag_custom_colors:
                        tag_count = (
                            sum(tag in tag_value for tag in tag_data["tags"])
                            if tag_data["tags"]
                            else 0
                        )
                        if tag_count > prev_tag_count:
                            tag_color = tag_data["color"]
                            prev_tag_count = tag_count
            color_data.append(tag_color)
        elif self.column_type == ColType.ROLE:
            role_color = self.role_default_color
            if ligand.role is not None:
                role_value = ligand.role.name
                if role_value in self.role_custom_colors:
                    role_color = self.role_custom_colors[role_value]
            color_data.append(role_color)
        elif self.column_type == ColType.RADIAL_PLOT:
            rp_col = self.radial_plot_column
            rp_color = "#000000"
            rp_value = 0.0
            project = flare.main_window().project
            rp_names_map = radial_plot_properties_name_map(project.radial_plot_properties)
            if rp_col in rp_names_map:
                persistent_name = rp_names_map[rp_col]
                rp_prop = project.radial_plot_properties[persistent_name]
                rp_value = self._ligand_column_value(ligand, rp_col)
                if rp_value is not None:
                    rp_color = rp_prop.color_for_value(rp_value)
                    if rp_color is None:
                        rp_color = "#000000"
            color_data.append(rp_color)
        elif self.column_type == ColType.FAV:
            color_data.append(self.fav_color(ligand.properties[color_axis_col].value))
        elif self.column_type == ColType.AM_FOCUS:
            color_data.append(self.am_color(ligand.properties[color_axis_col].value))


class CustomColorWidgets(QtCore.QObject):
    """Color custom widgets wrapper"""

    update_colors = QtCore.Signal()

    def __init__(self, parent, editable):
        super().__init__()
        self.parent = parent
        self._editable = editable

        self.value_combo_box = QtWidgets.QComboBox()
        self.value_combo_box.setToolTip("Select a value to map a color to.")
        self.color_button = ColorButton(self.parent)
        self.color_button.setToolTip("Select the color to map this value to.")
        self.delete_button = QtWidgets.QPushButton("Delete")
        self.delete_button.setToolTip("Delete this color mapping.")

        self._CUSTOM_EDIT_STRING = "<Type a search string>"

        if self._editable:
            self.value_combo_box.addItem(self._CUSTOM_EDIT_STRING)
            self.value_combo_box.setEditable(True)
            self.value_combo_box.lineEdit().editingFinished.connect(self._on_value_edit)
            self.value_combo_box.currentIndexChanged.connect(self._on_value_changed)
        else:
            self.value_combo_box.currentIndexChanged.connect(self._on_value_edit)
        self.color_button.color_changed.connect(self._on_color_changed)
        self.delete_button.clicked.connect(self._on_delete_clicked)

    def custom_colors(self) -> dict:
        colors = {}
        for i in range(1 if self._editable else 0, self.value_combo_box.count()):
            colors[self.value_combo_box.itemText(i)] = self.value_combo_box.itemData(i)
        return colors

    def set_custom_colors(self, custom_colors: dict):
        self.value_combo_box.setCurrentIndex(-1)
        self.value_combo_box.clear()
        if self._editable:
            self.value_combo_box.addItem(self._CUSTOM_EDIT_STRING)
        for key, color in custom_colors.items():
            self.value_combo_box.addItem(key, color)

        if self.value_combo_box.count() > 0:
            self.value_combo_box.setCurrentIndex(0)
        self._on_value_edit()

    def _on_value_changed(self, idx: int):
        if self.value_combo_box.isEditable():
            self.value_combo_box.lineEdit().editingFinished.disconnect(self._on_value_edit)
        self.value_combo_box.setEditable(idx == 0)
        self._on_value_edit()
        if self.value_combo_box.isEditable():
            self.value_combo_box.lineEdit().editingFinished.connect(self._on_value_edit)

    def _on_color_changed(self):
        self.value_combo_box.setItemData(
            self.value_combo_box.currentIndex(), self.color_button.color()
        )
        self.update_colors.emit()

    def _on_value_edit(self):
        if self.value_combo_box.currentText() != "":
            if self.value_combo_box.currentData() is None:
                self.value_combo_box.setItemData(self.value_combo_box.currentIndex(), "#000000")
            self.color_button.set_color(self.value_combo_box.currentData())
            self.update_colors.emit()
        else:
            self.color_button.set_color("#000000")

    def _on_delete_clicked(self):
        idx = self.value_combo_box.currentIndex()
        if idx >= 0:
            self.value_combo_box.removeItem(idx)
            self.update_colors.emit()


class LigandScatterPlotConfigColorWidget:
    """Color configuration widget"""

    def __init__(self, parent):
        self.parent = parent
        self.label = QtWidgets.QLabel("Color by", parent)

        self.column_combo_box = QtWidgets.QComboBox(parent)
        self.column_combo_box.setToolTip(
            "<span>If set, then the color of points in the plot will be "
            + "determined by the value of the specified column."
        )

        self.cache = {}
        self.prev_column = -1
        self.supported_types: list[ColType] = []

        self.stacked = QtWidgets.QStackedWidget(parent)
        self.stacked.setLineWidth(0)
        self.stacked.setFrameStyle(QtWidgets.QFrame.NoFrame)
        self.stacked.addWidget(self._init_none())
        self.stacked.addWidget(self._init_numeric())
        self.stacked.addWidget(self._init_string())
        self.stacked.addWidget(self._init_tag())
        self.stacked.addWidget(self._init_role())
        self.stacked.addWidget(self._init_radial_plot())
        self.stacked.addWidget(self._init_fav())
        self.stacked.addWidget(self._init_am_focus())

        self.column_combo_box.currentIndexChanged.connect(parent._on_setting_changed)
        self.column_combo_box.currentIndexChanged.connect(self._on_column_changed)

    def _init_none(self) -> QtWidgets.QFrame:
        self.supported_types.append(ColType.NONE)
        frame = QtWidgets.QFrame()
        frame.setLineWidth(0)
        frame.setFrameStyle(QtWidgets.QFrame.NoFrame)
        return frame

    def _init_numeric(self) -> QtWidgets.QFrame:
        self.supported_types.append(ColType.NUMERIC)
        frame = QtWidgets.QFrame()
        frame.setLineWidth(0)
        frame.setFrameStyle(QtWidgets.QFrame.NoFrame)

        self.min_value_spin_box = QtWidgets.QDoubleSpinBox()
        self.min_value_spin_box.setToolTip(
            "<span>The value to map to the minimum color. Intermediate values "
            + "will be interpolated between the two colors."
        )
        self.min_value_spin_box.setRange(-10000000, 10000000)
        self.min_color_button = ColorButton(self.parent)
        self.min_color_button.setToolTip(
            "The color to use for the minimum value of the data range."
        )

        self.min_value_spin_box.valueChanged.connect(self.parent._on_setting_changed)
        self.min_color_button.color_changed.connect(self.parent._on_setting_changed)

        self.max_value_spin_box = QtWidgets.QDoubleSpinBox()
        self.max_value_spin_box.setToolTip(
            "<span>The value to map to the maximum color. Intermediate values "
            + "will be interpolated between the two colors."
        )
        self.max_value_spin_box.setRange(-10000000, 10000000)
        self.max_color_button = ColorButton(self.parent)
        self.max_color_button.setToolTip(
            "The color to use for the maximum value of the data range."
        )

        self.max_value_spin_box.valueChanged.connect(self.parent._on_setting_changed)
        self.max_color_button.color_changed.connect(self.parent._on_setting_changed)

        layout = QtWidgets.QHBoxLayout(frame)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(QtWidgets.QLabel("Min value:"))
        layout.addWidget(self.min_value_spin_box)
        layout.addWidget(self.min_color_button)
        layout.addWidget(QtWidgets.QLabel("Max value:"))
        layout.addWidget(self.max_value_spin_box)
        layout.addWidget(self.max_color_button)
        return frame

    def _init_string(self) -> QtWidgets.QFrame:
        self.supported_types.append(ColType.STRING)
        frame = QtWidgets.QFrame()
        frame.setLineWidth(0)
        frame.setFrameStyle(QtWidgets.QFrame.NoFrame)

        self.default_str = ColorButton(self.parent)
        self.default_str.setToolTip("<span>Set the default color to use for data points.")
        self.custom_str = CustomColorWidgets(self.parent, editable=True)

        self.default_str.color_changed.connect(self.parent._on_setting_changed)
        self.custom_str.update_colors.connect(self.parent._on_setting_changed)

        layout = QtWidgets.QHBoxLayout(frame)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(QtWidgets.QLabel("Default:"))
        layout.addWidget(self.default_str)
        layout.addWidget(QtWidgets.QLabel("Containing:"))
        layout.addWidget(self.custom_str.value_combo_box)
        layout.addWidget(self.custom_str.color_button)
        layout.addWidget(self.custom_str.delete_button)
        return frame

    def _init_tag(self) -> QtWidgets.QFrame:
        self.supported_types.append(ColType.TAG)
        frame = QtWidgets.QFrame()
        frame.setLineWidth(0)
        frame.setFrameStyle(QtWidgets.QFrame.NoFrame)

        self._tags_cache = set()

        self.default_tag = ColorButton(self.parent)
        self.default_tag.setToolTip("<span>Set the default color to use for data points.")

        self.custom_tag = CustomColorWidgets(self.parent, editable=False)
        self.custom_tag_add = QtWidgets.QPushButton("Add")
        self.custom_tag_add.setToolTip("<span>Add a new mapping between a tag and a color.")
        self.custom_tag_dlg = MultiStrSelDialog(self.parent, "Pick tags to color")

        self.default_tag.color_changed.connect(self.parent._on_setting_changed)
        self.custom_tag.update_colors.connect(self.parent._on_setting_changed)
        self.custom_tag_add.clicked.connect(self._on_custom_tag_add)
        self.custom_tag_dlg.new_strs.connect(self._on_custom_tag_new_strs)

        layout = QtWidgets.QHBoxLayout(frame)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(QtWidgets.QLabel("Default:"))
        layout.addWidget(self.default_tag)
        layout.addWidget(QtWidgets.QLabel("Custom tags:"))
        layout.addWidget(self.custom_tag.value_combo_box)
        layout.addWidget(self.custom_tag.color_button)
        layout.addWidget(self.custom_tag.delete_button)
        layout.addWidget(self.custom_tag_add)
        return frame

    def _init_role(self) -> QtWidgets.QFrame:
        self.supported_types.append(ColType.ROLE)
        frame = QtWidgets.QFrame()
        frame.setLineWidth(0)
        frame.setFrameStyle(QtWidgets.QFrame.NoFrame)

        self.default_role = ColorButton(self.parent)
        self.default_role.setToolTip("<span>Set the default color to use for data points.")

        self.custom_role = CustomColorWidgets(self.parent, editable=False)
        self.custom_role.value_combo_box.setToolTip(
            '<span>Set the role to map the color to. Click on "Add" to add a role and set a color.'
        )
        self.custom_role_add = QtWidgets.QPushButton("Add")
        self.custom_role_add.setToolTip("<span>Add a new mapping between a role and a color.")
        self.custom_role_dlg = MultiStrSelDialog(
            self.parent, "Pick a role to color", checked_list=False
        )

        self.default_role.color_changed.connect(self.parent._on_setting_changed)
        self.custom_role.update_colors.connect(self.parent._on_setting_changed)
        self.custom_role_add.clicked.connect(self._on_custom_role_add)
        self.custom_role_dlg.new_strs.connect(self._on_custom_role_new_strs)

        layout = QtWidgets.QHBoxLayout(frame)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(QtWidgets.QLabel("Default:"))
        layout.addWidget(self.default_role)
        layout.addWidget(QtWidgets.QLabel("Role:"))
        layout.addWidget(self.custom_role.value_combo_box)
        layout.addWidget(self.custom_role.color_button)
        layout.addWidget(self.custom_role.delete_button)
        layout.addWidget(self.custom_role_add)
        return frame

    def _init_radial_plot(self) -> QtWidgets.QFrame:
        self.supported_types.append(ColType.RADIAL_PLOT)
        frame = QtWidgets.QFrame()
        frame.setLineWidth(0)
        frame.setFrameStyle(QtWidgets.QFrame.NoFrame)

        self.radial_plot_column_cbox = QtWidgets.QComboBox()
        self.radial_plot_column_cbox.setToolTip(
            "Specify a column: the radial plot coloring for that "
            + "column will be applied to the data points."
        )
        self.radial_plot_column_cbox.currentIndexChanged.connect(self.parent._on_setting_changed)

        layout = QtWidgets.QHBoxLayout(frame)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(QtWidgets.QLabel("Radial plot color by:"))
        layout.addWidget(self.radial_plot_column_cbox)
        return frame

    def _init_fav(self) -> QtWidgets.QFrame:
        self.supported_types.append(ColType.FAV)
        frame = QtWidgets.QFrame()
        frame.setLineWidth(0)
        frame.setFrameStyle(QtWidgets.QFrame.NoFrame)

        self.fav_false_button = ColorButton(self.parent)
        self.fav_false_button.setToolTip(
            "<span>The color that will be applied to non-favorite ligands."
        )
        self.fav_true_button = ColorButton(self.parent)
        self.fav_true_button.setToolTip("<span>The color that will be applied to favorite ligands.")

        self.fav_false_button.color_changed.connect(self.parent._on_setting_changed)
        self.fav_true_button.color_changed.connect(self.parent._on_setting_changed)

        layout = QtWidgets.QHBoxLayout(frame)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(QtWidgets.QLabel("False:"))
        layout.addWidget(self.fav_false_button)
        layout.addWidget(QtWidgets.QLabel("True:"))
        layout.addWidget(self.fav_true_button)
        return frame

    def _init_am_focus(self) -> QtWidgets.QFrame:
        self.supported_types.append(ColType.AM_FOCUS)
        frame = QtWidgets.QFrame()
        frame.setLineWidth(0)
        frame.setFrameStyle(QtWidgets.QFrame.NoFrame)

        self.am_focus_button = ColorButton(self.parent)
        self.am_focus_button.setToolTip(
            "<span>The color that will be applied to ligand focused by Activity Miner."
        )
        self.am_not_focus_button = ColorButton(self.parent)
        self.am_not_focus_button.setToolTip(
            "<span>The color that will be applied to ligands not focused by Activity Miner."
        )

        self.am_focus_button.color_changed.connect(self.parent._on_setting_changed)
        self.am_not_focus_button.color_changed.connect(self.parent._on_setting_changed)

        layout = QtWidgets.QHBoxLayout(frame)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(QtWidgets.QLabel("Focused:"))
        layout.addWidget(self.am_focus_button)
        layout.addWidget(QtWidgets.QLabel("Not Focused:"))
        layout.addWidget(self.am_not_focus_button)
        return frame

    def _on_custom_tag_add(self):
        self.custom_tag_dlg.open_with(self._tags_cache)

    def _on_custom_tag_new_strs(self, new_strs: set):
        idx = self.custom_tag.value_combo_box.findData(new_strs, QtCore.Qt.UserRole + 1)
        if idx < 0:
            view_str = ",".join(new_strs)
            self.custom_tag.value_combo_box.addItem(view_str, "#000000")
            idx = self.custom_tag.value_combo_box.count() - 1
            self.custom_tag.value_combo_box.setItemData(idx, new_strs, QtCore.Qt.UserRole + 1)
        self.custom_tag.value_combo_box.setCurrentIndex(idx)

    def _on_custom_role_add(self):
        project = flare.main_window().project
        self.custom_role_dlg.open_with([role.name for role in project.roles])

    def _on_custom_role_new_strs(self, new_role: str):
        idx = self.custom_role.value_combo_box.findText(new_role)
        if idx < 0:
            self.custom_role.value_combo_box.addItem(new_role, "#000000")
            idx = self.custom_role.value_combo_box.count() - 1
        self.custom_role.value_combo_box.setCurrentIndex(idx)

    def tags_custom_colors(self) -> dict:
        tags_colors = []
        for i in range(0, self.custom_tag.value_combo_box.count()):
            tags_colors.append(
                {
                    "tags": self.custom_tag.value_combo_box.itemData(i, QtCore.Qt.UserRole + 1),
                    "color": self.custom_tag.value_combo_box.itemData(i),
                }
            )
        return tags_colors

    def set_tags_custom_colors(self, custom_colors: dict):
        self.custom_tag.value_combo_box.clear()
        for color_data in custom_colors:
            tags = color_data["tags"]
            color = color_data["color"]
            tags_disp = ",".join(tags)
            self.custom_tag.value_combo_box.addItem(tags_disp, color)
            endIdx = self.custom_tag.value_combo_box.count() - 1
            self.custom_tag.value_combo_box.setItemData(endIdx, tags, QtCore.Qt.UserRole + 1)

    def radial_plot_column(self) -> str:
        return self.radial_plot_column_cbox.currentText()

    def set_radial_plot_column(self, rp_column: str):
        idx = self.radial_plot_column_cbox.findText(rp_column)
        if idx < 0:
            idx = -1
        self.radial_plot_column_cbox.setCurrentIndex(idx)

    def _on_column_changed(self):
        # Save into cache for prev column
        prev_type = self.column_combo_box.itemData(self.prev_column)
        prev_text = self.column_combo_box.itemText(self.prev_column)
        if prev_text != "" and prev_type == ColType.STRING:
            self.cache[prev_text] = {
                "default": self.default_str.color(),
                "custom": self.custom_str.custom_colors(),
            }

        # Set widget from stacked to its type
        current_type = self.column_combo_box.currentData()
        self.stacked.setCurrentIndex(
            self.supported_types.index(current_type) if current_type in self.supported_types else 0
        )

        # Grab from cache/set data for current column if available
        if current_type == ColType.NUMERIC:
            # Find min/max values
            project = flare.main_window().project
            colname = self.column_combo_box.currentText()
            lmin_val = maxsize
            lmax_val = -maxsize
            for ligand in project.ligands:
                value = ligand.properties[colname].value
                if isinstance(value, (int, float)) and not isinstance(value, bool):
                    if value > lmax_val:
                        lmax_val = value
                    if value < lmin_val:
                        lmin_val = value
            self.min_value_spin_box.setValue(lmin_val)
            self.max_value_spin_box.setValue(lmax_val)
        elif current_type == ColType.STRING:
            current_text = self.column_combo_box.currentText()
            if current_text not in self.cache:
                self.cache[current_text] = {
                    "default": "#000000",
                    "custom": {},
                }
            self.default_str.set_color(self.cache[current_text]["default"])
            self.custom_str.set_custom_colors(self.cache[current_text]["custom"])
        elif current_type == ColType.TAG:
            self._update_tags()
        elif current_type == ColType.RADIAL_PLOT:
            self._update_radial_plot_columns()

        self.prev_column = self.column_combo_box.currentIndex()

    def update_property_cache(self, property_name: str):
        if property_name == "Tags":
            self._update_tags()

    def _update_tags(self):
        self._tags_cache.clear()
        project = flare.main_window().project
        for ligand in project.ligands:
            tags = ligand.properties["Tags"].value
            for tag in tags:
                self._tags_cache.add(tag)

    def _update_radial_plot_columns(self):
        self.radial_plot_column_cbox.currentIndexChanged.disconnect(self.parent._on_setting_changed)
        self.radial_plot_column_cbox.clear()
        project = flare.main_window().project
        rp_names_map = radial_plot_properties_name_map(project.radial_plot_properties)
        for i in range(0, self.column_combo_box.count()):
            col_type = self.column_combo_box.itemData(i)
            col_name = self.column_combo_box.itemText(i)
            if col_type == ColType.NUMERIC and col_name in rp_names_map:
                self.radial_plot_column_cbox.addItem(col_name)
        self.radial_plot_column_cbox.currentIndexChanged.connect(self.parent._on_setting_changed)
        self.parent._on_setting_changed()

    def set_axis_config(self, color_config: LigandScatterPlotAxisColorConfig):
        column_index = 0
        if color_config.column is not None:
            column_index = self.column_combo_box.findText(color_config.column)
            if column_index < 0:
                column_index = 0
        self.column_combo_box.setCurrentIndex(column_index)

        if color_config.column_type == ColType.NUMERIC:
            self.min_color_button.set_color(color_config.min_color)
            self.max_color_button.set_color(color_config.max_color)
            self.min_value_spin_box.setValue(color_config.min_value)
            self.max_value_spin_box.setValue(color_config.max_value)
        elif color_config.column_type == ColType.STRING:
            self.default_str.set_color(color_config.str_default_color)
            self.custom_str.set_custom_colors(color_config.str_custom_colors)
        elif color_config.column_type == ColType.TAG:
            self.default_tag.set_color(color_config.tag_default_color)
            self.set_tags_custom_colors(color_config.tag_custom_colors)
        elif color_config.column_type == ColType.ROLE:
            self.default_role.set_color(color_config.role_default_color)
            self.custom_role.set_custom_colors(color_config.role_custom_colors)
        elif color_config.column_type == ColType.RADIAL_PLOT:
            self.set_radial_plot_column(color_config.radial_plot_column)
        elif color_config.column_type == ColType.FAV:
            self.fav_false_button.set_color(color_config.fav_false_color)
            self.fav_true_button.set_color(color_config.fav_true_color)
        elif color_config.column_type == ColType.AM_FOCUS:
            self.am_focus_button.set_color(color_config.am_focus_color)
            self.am_not_focus_button.set_color(color_config.am_not_focus_color)

    def axis_config(self) -> LigandScatterPlotAxisColorConfig:
        color_config = LigandScatterPlotAxisColorConfig()
        if self.column_combo_box.currentIndex() > 0:
            color_config.column = self.column_combo_box.currentText()
            color_config.column_type = self.column_combo_box.currentData()

        if color_config.column_type == ColType.NUMERIC:
            color_config.min_color = self.min_color_button.color()
            color_config.max_color = self.max_color_button.color()
            color_config.min_value = self.min_value_spin_box.value()
            color_config.max_value = self.max_value_spin_box.value()
        elif color_config.column_type == ColType.STRING:
            color_config.str_default_color = self.default_str.color()
            color_config.str_custom_colors = self.custom_str.custom_colors()
        elif color_config.column_type == ColType.TAG:
            color_config.tag_default_color = self.default_tag.color()
            color_config.tag_custom_colors = self.tags_custom_colors()
        elif color_config.column_type == ColType.ROLE:
            color_config.role_default_color = self.default_role.color()
            color_config.role_custom_colors = self.custom_role.custom_colors()
        elif color_config.column_type == ColType.RADIAL_PLOT:
            color_config.radial_plot_column = self.radial_plot_column()
        elif color_config.column_type == ColType.FAV:
            color_config.fav_false_color = self.fav_false_button.color()
            color_config.fav_true_color = self.fav_true_button.color()
        elif color_config.column_type == ColType.AM_FOCUS:
            color_config.am_focus_color = self.am_focus_button.color()
            color_config.am_not_focus_color = self.am_not_focus_button.color()
        return color_config
