# -*- coding: utf-8 -*-
# Copyright (C) 2023 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset

from PySide2 import QtCore, QtGui, QtWidgets
from .marker import init_markers, ShapeComboBox
from ..color_button import ColorButton


class LigandScatterPlotAxisConfig:
    """The plot configuration for one of the axis."""

    SCALE_TYPE_LINEAR = 0
    SCALE_TYPE_LOG = 1

    def __init__(self, x_axis=False, default_color="#000000", default_marker="o"):
        self.column = None
        self.scale_type = self.SCALE_TYPE_LINEAR
        self.auto_scale = True
        self.min_scale = 0
        self.max_scale = 100
        self.color = default_color
        self.marker = default_marker
        self.size = 6
        self.x_axis = x_axis

    def to_dict(self):
        """Stores the data to a python dict."""
        dict = {}
        dict["Column"] = self.column
        dict["Scale Type"] = self.scale_type
        dict["Auto Scale"] = self.auto_scale
        dict["Min Scale"] = self.min_scale
        dict["Max Scale"] = self.max_scale
        dict["Color"] = self.color
        dict["Marker"] = self.marker
        dict["Size"] = self.size
        dict["x_axis"] = self.x_axis
        return dict

    def from_dict(self, dict):
        """Restores the data from a python dict."""
        self.column = dict.get("Column", self.column)
        self.scale_type = dict.get("Scale Type", self.scale_type)
        self.auto_scale = dict.get("Auto Scale", self.auto_scale)
        self.min_scale = dict.get("Min Scale", self.min_scale)
        self.max_scale = dict.get("Max Scale", self.max_scale)
        self.color = dict.get("Color", self.color)
        self.marker = dict.get("Marker", self.marker)
        self.size = dict.get("Size", self.size)
        self.x_axis = dict.get("x_axis", self.x_axis)

    def scale_type_str(self) -> str:
        return "log" if self.scale_type == self.SCALE_TYPE_LOG else "linear"


class LigandScatterPlotConfigAxisWidgets:
    """The widgets used to configure one of the axis."""

    def __init__(self, name, parent, x_axis=False):
        self.label = QtWidgets.QLabel(name, parent)

        self.column = QtWidgets.QComboBox(parent)
        self.column.setToolTip("The data column to use for this axis.")
        self.column.currentIndexChanged.connect(parent._on_setting_changed)

        self.scale_type = QtWidgets.QComboBox(parent)
        self.scale_type.addItem("Linear", LigandScatterPlotAxisConfig.SCALE_TYPE_LINEAR)
        self.scale_type.addItem("Log", LigandScatterPlotAxisConfig.SCALE_TYPE_LOG)
        self.scale_type.setToolTip(
            "Specify whether to use a linear or logarithmic scaling for this axis."
        )
        self.scale_type.currentIndexChanged.connect(parent._on_setting_changed)

        self.auto_scale = QtWidgets.QCheckBox(parent)
        self.auto_scale.setToolTip(
            "<span>If checked, the minimum/maximum value for this axis will be "
            + "determined from the range of the data."
        )
        self.auto_scale.toggled.connect(parent._on_setting_changed)

        self.min_scale = QtWidgets.QLineEdit(parent)
        self.min_scale.setToolTip("<span>The minimum value for this axis.")
        self.min_scale_validator = QtGui.QDoubleValidator(parent)
        self.min_scale.setValidator(self.min_scale_validator)
        self.min_scale.textChanged.connect(parent._on_setting_changed)

        self.max_scale = QtWidgets.QLineEdit(parent)
        self.max_scale.setToolTip("<span>The maximum value for this axis.")
        self.max_scale_validator = QtGui.QDoubleValidator(parent)
        self.max_scale.setValidator(self.max_scale_validator)
        self.max_scale.textChanged.connect(parent._on_setting_changed)

        self.auto_scale.toggled.connect(self.min_scale.setDisabled)
        self.auto_scale.toggled.connect(self.max_scale.setDisabled)

        self.x_axis = x_axis
        self.color = None
        self.marker = None
        self.size = None
        if not x_axis:
            self.color = ColorButton(parent)
            self.color.setToolTip("<span>The color to use for the points plotted on this axis.")
            self.color.color_changed.connect(parent._on_setting_changed)

            self.marker = ShapeComboBox(init_markers(), parent)
            self.marker.setToolTip(
                "<span>The marker shape to use for the points plotted on this axis."
            )
            self.marker.update_shape.connect(parent._on_setting_changed)

            self.size = QtWidgets.QSpinBox(parent)
            self.size.setToolTip(
                "<span>The marker size to use for the points plotted on this axis."
            )
            self.size.setMinimum(1)
            self.size.setMaximum(100)
            self.size.valueChanged.connect(parent._on_setting_changed)

        self.r_squared = QtWidgets.QLabel(parent)
        self.r_squared.setToolTip("<span>The R^2 value for the data on this axis.")
        self.r_squared.setTextInteractionFlags(QtCore.Qt.TextSelectableByMouse)
        self.r_squared.setMinimumWidth(40)

    def set_axis_config(self, axis_config: LigandScatterPlotAxisConfig):
        """Updates the widgets  for one of the axis to show the values in the config."""
        column_index = 0
        if axis_config.column is not None:
            column_index = self.column.findText(axis_config.column)
        if column_index < 0:
            column_index = 0
        self.column.setCurrentIndex(column_index)
        scale_index = self.scale_type.findData(axis_config.scale_type)
        self.scale_type.setCurrentIndex(scale_index)
        self.auto_scale.setChecked(axis_config.auto_scale)
        self.min_scale.setText(str(axis_config.min_scale))
        self.max_scale.setText(str(axis_config.max_scale))
        if not axis_config.x_axis:
            self.color.set_color(axis_config.color)
            self.marker.set_shape(axis_config.marker)
            self.size.setValue(axis_config.size)

    def axis_config(self) -> LigandScatterPlotAxisConfig:
        """Returns the configuration of one of the axis."""
        axis_config = LigandScatterPlotAxisConfig(x_axis=self.x_axis)
        if self.column.currentIndex() <= 0:
            axis_config.column = None
        else:
            axis_config.column = self.column.currentText()
        axis_config.scale_type = self.scale_type.currentData()
        axis_config.auto_scale = self.auto_scale.isChecked()
        if not axis_config.x_axis:
            axis_config.color = self.color.color()
            axis_config.marker = self.marker.shape()
            axis_config.size = self.size.value()
        try:
            axis_config.min_scale = float(self.min_scale.text())
        except ValueError:
            pass
        try:
            axis_config.max_scale = float(self.max_scale.text())
        except ValueError:
            pass
        return axis_config
