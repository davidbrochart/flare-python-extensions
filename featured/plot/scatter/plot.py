# -*- coding: utf-8 -*-
# Copyright (C) 2023 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset

from PySide2 import QtCore, QtWidgets

from cresset import flare
from .plot_xy import LigandScatterPlotAxisConfig, LigandScatterPlotConfigAxisWidgets
from .plot_color import LigandScatterPlotAxisColorConfig, LigandScatterPlotConfigColorWidget
from .plot_shape import LigandScatterPlotAxisShapeConfig, LigandScatterPlotConfigShapeWidget
from .plot_size import LigandScatterPlotAxisSizeConfig, LigandScatterPlotConfigSizeWidget
from .col_type import ColType
from ..color_button import ColorButton


class LigandScatterPlotConfig:
    """The plot configuration"""

    def __init__(self):
        self.ligands = []
        self.show_regression_lines = True
        self.config_widget_visible = True
        self.use_theme_color = True
        self.toggle_permanent_annotations = False
        self.custom_bg_color = "#FFFFFF"
        self.custom_fg_color = "#000000"
        self.x_axis = LigandScatterPlotAxisConfig(x_axis=True)
        self.y1_axis = LigandScatterPlotAxisConfig(default_color="#ff7f0e", default_marker="o")
        self.y2_axis = LigandScatterPlotAxisConfig(default_color="#2ca02c", default_marker="s")
        self.color_axis = LigandScatterPlotAxisColorConfig()
        self.shape_axis = LigandScatterPlotAxisShapeConfig()
        self.size_axis = LigandScatterPlotAxisSizeConfig()
        self.axis_cache = {"color": {}, "shape": {}}

    def to_dict(self):
        """Stores the data to a python dict."""
        dict = {}
        dict["Ligands"] = list(self.ligands)
        dict["Show Regression Lines"] = self.show_regression_lines
        dict["Config Widget Visible"] = self.config_widget_visible
        dict["Use Theme Color"] = self.use_theme_color
        dict["Permanent Annotations"] = self.toggle_permanent_annotations
        dict["Custom BG Color"] = self.custom_bg_color
        dict["Custom FG Color"] = self.custom_fg_color
        dict["X-Axis"] = self.x_axis.to_dict()
        dict["Y1-Axis"] = self.y1_axis.to_dict()
        dict["Y2-Axis"] = self.y2_axis.to_dict()
        dict["Color-Axis"] = self.color_axis.to_dict()
        dict["Shape-Axis"] = self.shape_axis.to_dict()
        dict["Size-Axis"] = self.size_axis.to_dict()
        dict["Axis-Cache"] = self.axis_cache
        return dict

    def from_dict(self, dict):
        """Restores the data from a python dict."""
        self.ligands = dict.get("Ligands", self.ligands)
        self.show_regression_lines = dict.get("Show Regression Lines", self.show_regression_lines)
        self.config_widget_visible = dict.get("Config Widget Visible", self.config_widget_visible)
        self.use_theme_color = dict.get("Use Theme Color", self.use_theme_color)
        self.toggle_permanent_annotations = dict.get(
            "Permanent Annotations", self.toggle_permanent_annotations
        )
        self.custom_bg_color = dict.get("Custom BG Color", self.custom_bg_color)
        self.custom_fg_color = dict.get("Custom FG Color", self.custom_fg_color)
        self.x_axis.from_dict(dict.get("X-Axis", {}))
        self.y1_axis.from_dict(dict.get("Y1-Axis", {}))
        self.y2_axis.from_dict(dict.get("Y2-Axis", {}))
        self.color_axis.from_dict(dict.get("Color-Axis", {}))
        self.shape_axis.from_dict(dict.get("Shape-Axis", {}))
        self.size_axis.from_dict(dict.get("Size-Axis", {}))
        self.axis_cache = dict.get("Axis-Cache", {"color": {}, "shape": {}})


class LigandScatterPlotConfigWidget(QtWidgets.QWidget):
    """Widget to configure the plot.

    When the configuration is changed `config_changed` is emitted with the new config.
    """

    config_changed = QtCore.Signal(LigandScatterPlotConfig)
    """Emitted when the configuration is changed"""

    def __init__(self, parent=None):
        super().__init__(parent)

        self._ligands = []
        self._config_widget_visible = None

        self._show_regression_lines = QtWidgets.QCheckBox("Show Regression Lines", self)
        self._show_regression_lines.setToolTip(
            "<span>If checked, draw a linear regression line for each data series."
        )

        self._x_axis_widgets = LigandScatterPlotConfigAxisWidgets("X", self, x_axis=True)
        self._y1_axis_widgets = LigandScatterPlotConfigAxisWidgets("Y Series 1", self)
        self._y2_axis_widgets = LigandScatterPlotConfigAxisWidgets("Y Series 2", self)
        self._color_axis_widgets = LigandScatterPlotConfigColorWidget(self)
        self._shape_axis_widgets = LigandScatterPlotConfigShapeWidget(self)
        self._size_axis_widgets = LigandScatterPlotConfigSizeWidget(self)

        self._show_all_ligands = QtWidgets.QPushButton("Plot All Ligands", self)
        self._show_all_ligands.setToolTip("Redraw the plot so it contains all ligands")

        self._show_selected_ligands = QtWidgets.QPushButton("Plot Selected Ligands", self)
        self._show_selected_ligands.setToolTip(
            "Redraw the plot so it only contains the selected ligands"
        )

        self._use_theme_color = QtWidgets.QCheckBox("Use theme color", self)
        self._use_theme_color.setToolTip(
            "Set background and foreground color from the current GUI theme"
        )
        self._bg_color = ColorButton(parent)
        self._fg_color = ColorButton(parent)

        self._toggle_permanent_annotations = QtWidgets.QCheckBox("Permanent Annotations", self)
        self._toggle_permanent_annotations.setToolTip(
            "<span>Set the annotation to stay on permanently when the data point is clicked. "
            + "Click the data point again to remove the annotation."
        )

        self._show_all_ligands.clicked.connect(self._set_all_ligands)
        self._show_selected_ligands.clicked.connect(self._set_selected_ligands)
        self._show_regression_lines.toggled.connect(self._on_setting_changed)
        self._use_theme_color.toggled.connect(self._on_use_theme_color)
        self._bg_color.color_changed.connect(self._on_setting_changed)
        self._fg_color.color_changed.connect(self._on_setting_changed)
        self._toggle_permanent_annotations.toggled.connect(self._on_setting_changed)

        self._custom_bgfgcolor = QtWidgets.QFrame()
        self._custom_bgfgcolor.setLineWidth(0)
        self._custom_bgfgcolor.setFrameStyle(QtWidgets.QFrame.NoFrame)

        bg_label = QtWidgets.QLabel("Background:")
        fg_label = QtWidgets.QLabel("Foreground:")
        bg_label.setToolTip("Set background color of the plot")
        fg_label.setToolTip("Set foreground/text color of the plot")

        bgfgcolor_layout = QtWidgets.QHBoxLayout(self._custom_bgfgcolor)
        bgfgcolor_layout.setContentsMargins(0, 0, 0, 0)
        bgfgcolor_layout.addWidget(bg_label)
        bgfgcolor_layout.addWidget(self._bg_color)
        bgfgcolor_layout.addWidget(fg_label)
        bgfgcolor_layout.addWidget(self._fg_color)

        self._general_settings_layout = QtWidgets.QHBoxLayout()
        self._general_settings_layout.setContentsMargins(0, 0, 0, 0)
        self._general_settings_layout.addWidget(self._show_regression_lines)
        self._general_settings_layout.addWidget(self._use_theme_color)
        self._general_settings_layout.addWidget(self._custom_bgfgcolor)
        self._general_settings_layout.addWidget(self._toggle_permanent_annotations)
        self._general_settings_layout.addStretch(1)
        self._general_settings_layout.addWidget(self._show_all_ligands)
        self._general_settings_layout.addWidget(self._show_selected_ligands)

        self._axis_layout = QtWidgets.QGridLayout()
        self._axis_layout.setContentsMargins(0, 0, 0, 0)

        self._axis_layout.addWidget(QtWidgets.QLabel("Axis", self), 0, 0)
        self._axis_layout.addWidget(QtWidgets.QLabel("Column", self), 0, 1)
        self._axis_layout.addWidget(QtWidgets.QLabel("Scale Type", self), 0, 2)
        self._axis_layout.addWidget(QtWidgets.QLabel("Auto Scale", self), 0, 3)
        self._axis_layout.addWidget(QtWidgets.QLabel("Min Scale", self), 0, 4)
        self._axis_layout.addWidget(QtWidgets.QLabel("Max Scale", self), 0, 5)
        self._axis_layout.addWidget(QtWidgets.QLabel("Color", self), 0, 6)
        self._axis_layout.addWidget(QtWidgets.QLabel("Marker", self), 0, 7)
        self._axis_layout.addWidget(QtWidgets.QLabel("Size", self), 0, 8)
        self._axis_layout.addWidget(QtWidgets.QLabel("Pearson's R²", self), 0, 9)

        for row, axis in enumerate(
            (self._x_axis_widgets, self._y1_axis_widgets, self._y2_axis_widgets), 1
        ):
            self._axis_layout.addWidget(axis.label, row, 0)
            self._axis_layout.addWidget(axis.column, row, 1)
            self._axis_layout.addWidget(axis.scale_type, row, 2)
            self._axis_layout.addWidget(axis.auto_scale, row, 3)
            self._axis_layout.addWidget(axis.min_scale, row, 4)
            self._axis_layout.addWidget(axis.max_scale, row, 5)
            if not axis.x_axis:
                self._axis_layout.addWidget(axis.color, row, 6)
                self._axis_layout.addWidget(axis.marker, row, 7)
                self._axis_layout.addWidget(axis.size, row, 8)
            self._axis_layout.addWidget(axis.r_squared, row, 9)

        # Color widgets
        self._axis_layout.addWidget(self._color_axis_widgets.label, 4, 0)
        self._axis_layout.addWidget(self._color_axis_widgets.column_combo_box, 4, 1)
        self._axis_layout.addWidget(self._color_axis_widgets.stacked, 4, 2, 1, 8)

        # Shape widgets
        self._axis_layout.addWidget(self._shape_axis_widgets.label, 5, 0)
        self._axis_layout.addWidget(self._shape_axis_widgets.column_combo_box, 5, 1)
        self._axis_layout.addWidget(self._shape_axis_widgets.stacked, 5, 2, 1, 8)

        # Size widgets
        self._axis_layout.addWidget(self._size_axis_widgets.label, 6, 0)
        self._axis_layout.addWidget(self._size_axis_widgets.column_combo_box, 6, 1)
        self._axis_layout.addWidget(self._size_axis_widgets.stacked, 6, 2, 1, 8)

        self._update_column_names()

        self._vbl = QtWidgets.QVBoxLayout(self)
        self._vbl.addLayout(self._general_settings_layout)
        self._vbl.addLayout(self._axis_layout)
        self._vbl.addStretch(1)

        flare.callbacks.ligand_table_columns_changed.add(self._update_column_names)
        flare.callbacks.ligand_property_changed.add(self._on_ligand_property_changed)

    def shutdown(self):
        """Remove the callbacks when the dialog is closed."""
        flare.callbacks.ligand_table_columns_changed.remove(self._update_column_names)
        flare.callbacks.ligand_property_changed.remove(self._on_ligand_property_changed)

    def set_config(self, config):
        """Updates the widgets to show the values in the config."""
        self._ligands = config.ligands
        self._config_widget_visible = config.config_widget_visible
        self._show_regression_lines.setChecked(config.show_regression_lines)
        self._use_theme_color.setChecked(config.use_theme_color)
        self._toggle_permanent_annotations.setChecked(config.toggle_permanent_annotations)
        self._bg_color.set_color(config.custom_bg_color)
        self._fg_color.set_color(config.custom_fg_color)

        self._color_axis_widgets.cache = config.axis_cache["color"]
        self._shape_axis_widgets.cache = config.axis_cache["shape"]

        self._x_axis_widgets.set_axis_config(config.x_axis)
        self._y1_axis_widgets.set_axis_config(config.y1_axis)
        self._y2_axis_widgets.set_axis_config(config.y2_axis)
        self._color_axis_widgets.set_axis_config(config.color_axis)
        self._shape_axis_widgets.set_axis_config(config.shape_axis)
        self._size_axis_widgets.set_axis_config(config.size_axis)

        self.config_changed.emit(config)

    def config(self):
        """Return the plot configuration."""
        config = LigandScatterPlotConfig()
        config.ligands = self._ligands
        config.config_widget_visible = self._config_widget_visible
        config.show_regression_lines = self._show_regression_lines.isChecked()
        config.use_theme_color = self._use_theme_color.isChecked()
        config.toggle_permanent_annotations = self._toggle_permanent_annotations.isChecked()
        config.custom_bg_color = self._bg_color.color()
        config.custom_fg_color = self._fg_color.color()

        config.x_axis = self._x_axis_widgets.axis_config()
        config.y1_axis = self._y1_axis_widgets.axis_config()
        config.y2_axis = self._y2_axis_widgets.axis_config()
        config.color_axis = self._color_axis_widgets.axis_config()
        config.shape_axis = self._shape_axis_widgets.axis_config()
        config.size_axis = self._size_axis_widgets.axis_config()

        config.axis_cache["color"] = self._color_axis_widgets.cache
        config.axis_cache["shape"] = self._shape_axis_widgets.cache

        return config

    def _on_use_theme_color(self, checked: bool):
        self._custom_bgfgcolor.setVisible(not checked)
        self._on_setting_changed()

    def set_r_squared(self, y1, y2):
        """Set the r2 values which are displayed next to the axis configuration."""
        self._y1_axis_widgets.r_squared.setText(y1)
        self._y2_axis_widgets.r_squared.setText(y2)

    def _update_column_names(self):
        """Update the values in the columns combo boxes"""
        names = self._any_columns()

        for axis in (self._x_axis_widgets, self._y1_axis_widgets, self._y2_axis_widgets):
            self._append_columns(axis.column, names, {ColType.NONE, ColType.NUMERIC})

        self._append_columns(
            self._color_axis_widgets.column_combo_box,
            names,
            set(self._color_axis_widgets.supported_types),
        )
        self._append_columns(
            self._shape_axis_widgets.column_combo_box,
            names,
            set(self._shape_axis_widgets.supported_types),
        )
        self._append_columns(
            self._size_axis_widgets.column_combo_box,
            names,
            set(self._size_axis_widgets.supported_types),
        )

    def _on_ligand_property_changed(self, ligand, property_name, property_value):
        self._color_axis_widgets.update_property_cache(property_name)
        self._shape_axis_widgets.update_property_cache(property_name)

    def _append_columns(self, column_combo_box, names, restrict_by: set[ColType]):
        current = column_combo_box.currentText()
        column_combo_box.clear()
        for name in filter(lambda name: name[1] in restrict_by, names):
            column_combo_box.addItem(name[0], name[1])
        index = column_combo_box.findText(current)
        index = max(0, index)
        column_combo_box.setCurrentIndex(index)

    def _any_columns(self):
        """Return list of columns in the project and categories them by value type"""
        project = flare.main_window().project
        any_columns = []
        any_columns.append(("<None>", ColType.NONE))
        any_columns.append(("<Role>", ColType.ROLE))
        any_columns.append(("<Radial Plot>", ColType.RADIAL_PLOT))
        columns = project.ligands.columns.keys()
        for column in columns:
            # Ignore structure and visibile columns
            if column == "Structure" or column == "Visible":
                continue
            elif column == "Tags":
                any_columns.append((column, ColType.TAG))
            elif column == "Title":
                any_columns.append((column, ColType.STRING))
            elif column == "Fav":
                any_columns.append((column, ColType.FAV))
            elif column == "Activity Miner Focus":
                any_columns.append((column, ColType.AM_FOCUS))
            else:
                # Check if numeric
                valid_count = 0
                total_count = 0

                for ligand in project.ligands:
                    value = ligand.properties[column].value
                    if value is not None and value != "":
                        total_count += 1
                        if isinstance(value, (int, float)) and not isinstance(value, bool):
                            valid_count += 1
                        # Limit the number of molecules to search for performance reasons
                        if total_count > 20:
                            break

                # Class the column as numerical if 20% of its non blank values are a number.
                if total_count > 0 and (valid_count / total_count) > 0.2:
                    any_columns.append((column, ColType.NUMERIC))
                else:
                    any_columns.append((column, ColType.STRING))
        return any_columns

    def _on_setting_changed(self):
        """Emits `config_changed` with the current configuration."""
        self.config_changed.emit(self.config())

    def _set_given_ligands(self, new_ligands):
        self._ligands = new_ligands
        self._on_setting_changed()

    def _set_selected_ligands(self):
        """Set the ligands to show in the plot."""
        self._set_given_ligands(flare.main_window().selected_ligands)

    def _set_all_ligands(self):
        """Set the ligands to show in the plot."""
        self._set_given_ligands(list(flare.main_window().project.ligands))
