# -*- coding: utf-8 -*-
# Copyright (C) 2023 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Provides the Ramachandran Plot Dialog and functions for calculating the phi and psi values."""
import collections

from cresset import flare
from enum import IntEnum
from PySide2 import QtCore, QtGui, QtWidgets

import numpy as np

import matplotlib
import matplotlib.colors
from matplotlib.widgets import LassoSelector
from matplotlib.path import Path
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure


class Atoms:
    """The atoms for a residue which phi and psi will be calculated on."""

    #                 O
    #                 |
    #      N (prev)   C (next)
    #     /  \       /  \       /
    #    /    \phi  /psi \omega
    #   /      \   /      \   /
    #  C (prev) Ca          N (next)
    #

    def __init__(self):
        """Create the object with all atoms None."""
        self.prev_carbon = None
        self.prev_nitrogen = None
        self.alpha = None
        self.next_carbon = None
        self.next_nitrogen = None

    def is_valid(self):
        """Return true if phi and psi can be calculated on the residue."""
        return (
            self.prev_carbon is not None
            and self.prev_nitrogen is not None
            and self.alpha is not None
            and self.next_carbon is not None
            and self.next_nitrogen is not None
        )


def _find_neighbour(bond_map, atom, atom_type):
    """Return the atom with a bond to `atom` and with the `atom_type`."""
    for neighbour_atom in bond_map[atom]:
        if neighbour_atom.type == atom_type:
            return neighbour_atom
    return None


def _residues_atoms(molecule):
    """Return a dict of residues to the atoms used for calculating its phi, psi."""
    residue_atoms = collections.defaultdict(Atoms)
    bond_map = collections.defaultdict(list)

    for bond in molecule.bonds:
        try:
            bond_map[bond.atoms[0]].append(bond.atoms[1])
            bond_map[bond.atoms[1]].append(bond.atoms[0])
        except flare.ObjectDeletedError:
            pass

    for atom in molecule.atoms:
        try:
            # For each alpha carbon find the atoms next to it which will be used
            # to calculate phi and psi
            if atom.is_alpha_carbon():
                residue_atoms[atom.residue].alpha = atom
                residue_atoms[atom.residue].next_carbon = _find_neighbour(
                    bond_map, atom, flare.Atom.XedType.Csp2
                )
                residue_atoms[atom.residue].next_nitrogen = _find_neighbour(
                    bond_map, residue_atoms[atom.residue].next_carbon, flare.Atom.XedType.Ntri
                )
                residue_atoms[atom.residue].prev_nitrogen = _find_neighbour(
                    bond_map, atom, flare.Atom.XedType.Ntri
                )
                residue_atoms[atom.residue].prev_carbon = _find_neighbour(
                    bond_map, residue_atoms[atom.residue].prev_nitrogen, flare.Atom.XedType.Csp2
                )
        except flare.ObjectDeletedError:
            pass

    return residue_atoms


class Angles:
    """The residue phi and psi values."""

    def __init__(self):
        """Create the object with phi and psi set to None."""
        self.phi = None
        self.psi = None


def _torsion(atom1, atom2, atom3, atom4):
    """Return the _torsion of the 4 atoms."""
    measurement = flare.Measurement(atom1, atom2, atom3, atom4)
    return measurement.value


def _calculate_phi_psi_from_atoms(residue_atoms):
    """Return a map of residues to phi, psi values."""
    residue_angles = collections.defaultdict(Angles)
    for residue, atoms in residue_atoms.items():
        if atoms.is_valid():
            residue_angles[residue].phi = _torsion(
                atoms.prev_carbon, atoms.prev_nitrogen, atoms.alpha, atoms.next_carbon
            )

            residue_angles[residue].psi = _torsion(
                atoms.prev_nitrogen, atoms.alpha, atoms.next_carbon, atoms.next_nitrogen
            )
    return residue_angles


def _calculate_phi_psi(protein):
    """Return the residues plus their phi, psi values.

    Returns
    -------
    list(cresset.flare.Residue)
        List of residues which phi and psi values have been calculated for.
    list(float)
        List phi values, the list is the same size and order as the residue list.
    list(float)
        List psi values, the list is the same size and order as the residue list.
    """
    residues_atoms = _residues_atoms(protein)
    angles = _calculate_phi_psi_from_atoms(residues_atoms)

    residues = []
    phi = []
    psi = []

    for res, ang in angles.items():
        residues.append(res)
        phi.append(ang.phi)
        psi.append(ang.psi)

    return residues, phi, psi


class RamachandranPlotDialog(QtWidgets.QDialog):
    """Dialog that contains the Ramachandran plot.

    Items in the plot can be selected by clicking them or drawing a lasso around them.
    This will pick the atoms in the residues the items represent.

    Matplotlib is used to draw the Ramachandran plot which is embedded into
    this dialog.

    Callbacks are added to flare so that when the protein or atom pick is updated the
    dialog is updated accordingly.
    """

    def __init__(self, protein, parent=None):
        super().__init__(parent)
        # If `parent` was set then the dialog will not be deleted until the parent is
        # deleted. The WA_DeleteOnClose flag changes this so this dialog is deleted
        # when it is closed
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)

        self._plot_widget = _RamachandranPlotWidget(protein, self)

        # Add the Matplotlib plot to this dialog
        self._vbl = QtWidgets.QVBoxLayout(self)
        self._vbl.setContentsMargins(0, 0, 0, 0)
        self._vbl.addWidget(self._plot_widget)

        flare.callbacks.main_window_project_changed.add(self._on_project_changed)
        flare.callbacks.proteins_removed.add(self._on_proteins_removed)

    def done(self, result):
        """Remove the callbacks when the dialog is closed."""
        flare.callbacks.main_window_project_changed.remove(self._on_project_changed)
        flare.callbacks.proteins_removed.remove(self._on_proteins_removed)
        self._plot_widget._done()
        super().done(result)

    def _on_proteins_removed(self, proteins):
        """Close the dialog when its protein is deleted."""
        if self._protein in proteins:
            self.hide()

    def _on_project_changed(self, project):
        """Close the dialog when the project is changed."""
        self.hide()


class _RamachandranPlotWidget(QtWidgets.QWidget):
    def __init__(self, protein, parent=None):
        """Create the ramachandran plot.

        Parameters
        ----------
        protein : cresset.flare.Protein
            The protein the plot is for.
        """
        super().__init__(parent)

        # Create the matplotlib plot
        self._fig = Figure()
        self._ax = self._fig.add_subplot(111)
        self._canvas = FigureCanvas(self._fig)
        self._sc = None

        # matplotlib raises exceptions if it gets too small
        self._canvas.setMinimumSize(100, 100)

        self._configure_annotation()
        self._configure_lasso_selector()

        # Connect the pick event so when a point in the plot is clicked _on_pick is called
        self._canvas.mpl_connect("pick_event", self._on_pick)

        self._protein = protein

        self._configure_plot_axis()

        self._add_allowed_regions()

        # Add the data to the plot
        self._update_plot()

        # Add callbacks which update the plot when the protein or atom pick is changed
        flare.callbacks.picked_atoms_changed.add(self._on_picked_atoms_changed)
        flare.callbacks.protein_structure_changed.add(self._on_protein_structure_changed)

        # Add the Matplotlib plot to this dialog
        self._vbl = QtWidgets.QVBoxLayout(self)
        self._vbl.setContentsMargins(0, 0, 0, 0)
        self._vbl.addWidget(self._canvas)

    def _done(self):
        """Remove the callbacks when the dialog is closed."""
        flare.callbacks.picked_atoms_changed.remove(self._on_picked_atoms_changed)
        flare.callbacks.protein_structure_changed.remove(self._on_protein_structure_changed)

    def _configure_plot_axis(self):
        """Configure the axis scales and labels."""
        self._ax.set_xlim([-180, 180])
        self._ax.set_ylim([-180, 180])
        self._ax.plot([-180, 180], [0, 0], color="black")
        self._ax.plot([0, 0], [-180, 180], color="black")
        major = list(range(-180, 181, 90))
        self._ax.set_xticks(major)
        self._ax.set_xticklabels(major)
        self._ax.set_yticks(major)
        self._ax.set_yticklabels(major)
        self._ax.set_xlabel(r"$\phi$")
        self._ax.set_ylabel(r"$\psi$")
        self._ax.grid()

    def _update_plot(self):
        """Calculate the protein phi and psi values and updates the plot."""
        # Update the plot title in case it changed
        self._ax.set_title(f"Ramachandran Plot for {self._protein}")

        # Calculate the phi and psi values
        self._residues, self._phi, self._psi = _calculate_phi_psi(self._protein)

        # Create or update scatter plot
        self._plot_x_y_data(self._phi, self._psi)

        # Update the colors of the scatter points for the picked residues
        self._on_picked_atoms_changed(flare.main_window().picked_atoms)

        self._canvas.draw()

    def _plot_x_y_data(self, x_data, y_data):
        """Update the scatter plot with the x,y data."""
        if self._sc is None:
            # First time showing the plot, create a new collection
            self._sc = self._ax.scatter(x_data, y_data, picker=True)
        else:
            # Update the collection with the new data
            data = list(zip(x_data, y_data))
            self._sc.set_offsets(data)

        # Set each point in the scatter plot to have its own color.
        face_colors = self._sc.get_facecolors()

        if x_data and y_data:
            if not face_colors.all():
                raise ValueError("Collection must have a facecolor")
            elif len(face_colors) == 1:  # First time showing the plot
                face_colors = np.tile(face_colors, len(x_data)).reshape(len(x_data), -1)
            elif len(face_colors) != len(x_data):  # Updated the collection with the new data
                face_colors = np.tile(face_colors[0], len(x_data)).reshape(len(x_data), -1)

            self._sc.set_facecolors(face_colors)

    def _hover(self, event):
        """Show or hide the annotation if needed when the mouse is moved over the plot."""
        # Dont show annotions when drawning the lasso
        if not self._lasso._selection_artist.get_visible() and self._sc:
            vis = self._annot.get_visible()
            if event.inaxes == self._ax:
                cont, ind = self._sc.contains(event)
                if cont:
                    self._update_annotation(ind)
                    self._annot.set_visible(True)
                elif vis:
                    self._annot.set_visible(False)

            self._canvas.draw()

    def _configure_annotation(self):
        """Configure the annotation which is shown when hovering over a scatter point.

        The text which is displayed for each point is returned by _annotation_text().
        """
        self._annot = self._ax.annotate(
            "",
            xy=(0, 0),
            xytext=(10, 10),
            textcoords="offset points",
            bbox=dict(boxstyle="round", fc="w"),
            arrowprops=dict(arrowstyle="->"),
        )
        self._annot.set_visible(False)
        self._canvas.mpl_connect("motion_notify_event", self._hover)

    def _update_annotation(self, ind):
        """Update the annotation to show the residues names for the `ind`.

        Parameters
        ----------
        ind : dict(str,list(int))
            The indexes which the mouse is hovering over.
        """
        self._annot.xy = self._sc.get_offsets()[ind["ind"][0]]
        self._annot.set_text(self._annotation_text(ind["ind"]))
        self._annot.get_bbox_patch().set_alpha(0.4)

    def _annotation_text(self, indexes):
        """Return the names of the residues at the indexes.

        This text will be displayed in the annotation which appears when hovering over a point.

        Parameters
        ----------
        indexes : list(int)
            The indexes of the items in the scatter plot which the mouse pointer is over.
        """
        residues = [self._residues[n] for n in indexes]
        names = [str(residue) for residue in residues]

        max_names = 5
        if len(names) > max_names:
            names = names[0:max_names]
            names.append("...")

        text = ",\n".join(names)
        return text

    def _configure_lasso_selector(self):
        """Configure the lasso selector which is used to pick items in the plot."""
        self._lasso = LassoSelector(self._ax, onselect=self._on_lasso_select, button=1)

    def _on_lasso_select(self, verts):
        """Call when the lasso selects items in the plot.

        _select_indexes() will be called with the indexes of the selected items.
        """
        path = Path(verts)
        if len(path) > 2:
            xys = self._sc.get_offsets()
            ind = np.nonzero([path.contains_point(xy) for xy in xys])[0]

            self._select_indexes(ind)
        self._canvas.draw()

    def _select_indexes(self, indexes):
        """Pick the atoms in selected residues.

        This is called when a point is clicked or a lasso is drawn.

        Parameters
        ----------
        indexes : list(int)
            The indexes of the items in the scatter plot which the mouse pointer is over.
        """
        residues = [self._residues[n] for n in indexes]
        atoms = [atom for atom in self._protein.atoms if atom.residue in residues]
        flare.main_window().picked_atoms = atoms

    def _on_pick(self, event):
        """Select the items associated with the point clicked."""
        self._select_indexes(event.ind)

    def _on_picked_atoms_changed(self, atoms):
        """Update the scatter plot coloring then the picked atoms changes."""
        picked_residues = set([atom.residue for atom in atoms])

        color = matplotlib.colors.to_rgba(matplotlib.rcParams["lines.color"], 0.3)
        picked_color = matplotlib.colors.to_rgba("C3", 1)

        face_colors = self._sc.get_facecolors()

        # Change the alpha depending on if a residue is picked
        for i, residue in enumerate(self._residues):
            face_colors[i] = picked_color if residue in picked_residues else color

        self._sc.set_facecolors(face_colors)
        self._canvas.draw()

    def _on_protein_structure_changed(self, protein):
        """Update the plot when the structure of the protein is changed."""
        if self._protein == protein:
            self._update_plot()

    # The favored and allowed regions of the plot. The plot extends from -180 to 180 in both
    # axis. If a region goes outside of that range, then it is assuming the region wraps round
    # the plot.
    FAVORED_REGIONS = (
        # Region 1, bottom left, connects to region 3
        (
            (-177.5, -180.1),
            (-177.5, -177.5),
            (-172.5, -177.5),
            (-172.5, -172.5),
            (-167.5, -172.5),
            (-167.5, -167.5),
            (-127.5, -167.5),
            (-127.5, -172.5),
            (-97.5, -172.5),
            (-97.5, -167.5),
            (-77.5, -167.5),
            (-77.5, -172.5),
            (-72.5, -172.5),
            (-72.5, -177.5),
            (-67.5, -177.5),
            (-67.5, -180.1),
        ),
        # Region 2, center right
        (
            (57.5, 67.5),
            (57.5, 62.5),
            (62.5, 62.5),
            (62.5, 57.5),
            (67.5, 57.5),
            (67.5, 47.5),
            (72.5, 47.5),
            (77.5, 32.5),
            (77.5, 2.5),
            (62.5, 2.5),
            (62.5, 7.5),
            (57.5, 7.5),
            (57.5, 12.5),
            (52.5, 12.5),
            (52.5, 22.5),
            (47.5, 22.5),
            (47.5, 27.5),
            (42.5, 27.5),
            (42.5, 37.5),
            (37.5, 37.5),
            (37.5, 62.5),
            (42.5, 62.5),
            (42.5, 67.5),
            (57.5, 67.5),
        ),
        # Region 3, top left, connects to region 1
        (
            (-62.5, 180.1),
            (-62.5, 172.5),
            (-57.5, 172.5),
            (-57.5, 167.5),
            (-52.5, 167.5),
            (-52.5, 157.5),
            (-47.5, 157.5),
            (-47.5, 147.5),
            (-42.5, 147.5),
            (-42.5, 137.5),
            (-37.5, 137.5),
            (-37.5, 122.5),
            (-42.5, 122.5),
            (-42.5, 117.5),
            (-47.5, 117.5),
            (-47.5, 112.5),
            (-57.5, 112.5),
            (-57.5, 107.5),
            (-62.5, 107.5),
            (-62.5, 102.5),
            (-67.5, 102.5),
            (-67.5, 97.5),
            (-72.5, 97.5),
            (-72.5, 62.5),
            (-77.5, 62.5),
            (-77.5, 52.5),
            (-87.5, 52.5),
            (-87.5, 47.5),
            (-92.5, 47.5),
            (-92.5, 52.5),
            (-97.5, 52.5),
            (-97.5, 67.5),
            (-102.5, 67.5),
            (-102.5, 77.5),
            (-107.5, 77.5),
            (-107.5, 82.5),
            (-112.5, 82.5),
            (-112.5, 72.5),
            (-117.5, 72.5),
            (-117.5, 62.5),
            (-122.5, 62.5),
            (-122.5, 52.5),
            (-127.5, 52.5),
            (-127.5, 47.5),
            (-112.5, 47.5),
            (-112.5, 42.5),
            (-102.5, 42.5),
            (-102.5, 37.5),
            (-92.5, 37.5),
            (-92.5, 32.5),
            (-87.5, 32.5),
            (-87.5, 22.5),
            (-82.5, 22.5),
            (-82.5, 17.5),
            (-77.5, 17.5),
            (-77.5, 12.5),
            (-67.5, 12.5),
            (-67.5, 7.5),
            (-62.5, 7.5),
            (-62.5, 2.5),
            (-57.5, 2.5),
            (-57.5, -7.5),
            (-52.5, -7.5),
            (-52.5, -12.5),
            (-47.5, -12.5),
            (-47.5, -22.5),
            (-42.5, -22.5),
            (-42.5, -32.5),
            (-37.5, -32.5),
            (-37.5, -62.5),
            (-42.5, -62.5),
            (-42.5, -67.5),
            (-77.5, -67.5),
            (-77.5, -62.5),
            (-117.5, -62.5),
            (-117.5, -57.5),
            (-122.5, -57.5),
            (-122.5, -47.5),
            (-127.5, -47.5),
            (-127.5, -37.5),
            (-132.5, -37.5),
            (-132.5, -17.5),
            (-137.5, -17.5),
            (-137.5, 2.5),
            (-142.5, 2.5),
            (-142.5, 32.5),
            (-137.5, 32.5),
            (-137.5, 52.5),
            (-142.5, 52.5),
            (-142.5, 57.5),
            (-147.5, 57.5),
            (-147.5, 67.5),
            (-152.5, 67.5),
            (-152.5, 77.5),
            (-147.5, 77.5),
            (-147.5, 87.5),
            (-152.5, 87.5),
            (-152.5, 97.5),
            (-157.5, 97.5),
            (-157.5, 112.5),
            (-162.5, 112.5),
            (-162.5, 122.5),
            (-167.5, 122.5),
            (-167.5, 132.5),
            (-172.5, 132.5),
            (-172.5, 142.5),
            (-180.0, 142.5),
            (-180.0, 180.1),
        ),
    )
    ALLOWED_REGIONS = (
        # Region 1, left side, connects to regions 5 and 6
        (
            (-180.1, -147.5),  # connects to region 6
            (-177.5, -147.5),
            (-167.5, -147.5),
            (-167.5, -142.5),
            (-157.5, -142.5),
            (-157.5, -137.5),
            (-147.5, -137.5),
            (-147.5, -132.5),
            (-142.5, -132.5),
            (-142.5, -127.5),
            (-147.5, -127.5),
            (-147.5, -97.5),
            (-152.5, -97.5),
            (-152.5, -92.5),
            (-157.5, -92.5),
            (-157.5, -82.5),
            (-162.5, -82.5),
            (-162.5, -52.5),
            (-157.5, -52.5),
            (-157.5, -37.5),
            (-162.5, -37.5),
            (-162.5, -7.5),
            (-167.5, -7.5),
            (-167.5, 32.5),
            (-172.5, 32.5),
            (-172.5, 52.5),
            (-177.5, 52.5),
            (-177.5, 77.5),
            (-180.1, 77.5),  # connects to region 5
            (-180.1, 180.1),
            (-42.5, 180.1),  # connects to region 1
            (-42.5, 172.5),
            (-42.5, 172.5),
            (-37.5, 172.5),
            (-37.5, 167.5),
            (-32.5, 167.5),
            (-32.5, 157.5),
            (-27.5, 157.5),
            (-27.5, 147.5),
            (-22.5, 147.5),
            (-22.5, 127.5),
            (-17.5, 127.5),
            (-17.5, 112.5),
            (-22.5, 112.5),
            (-22.5, 107.5),
            (-27.5, 107.5),
            (-27.5, 102.5),
            (-32.5, 102.5),
            (-32.5, 97.5),
            (-47.5, 97.5),
            (-47.5, 92.5),
            (-52.5, 92.5),
            (-52.5, 72.5),
            (-57.5, 72.5),
            (-57.5, 42.5),
            (-62.5, 42.5),
            (-62.5, 27.5),
            (-57.5, 27.5),
            (-57.5, 22.5),
            (-52.5, 22.5),
            (-52.5, 12.5),
            (-47.5, 12.5),
            (-47.5, 7.5),
            (-42.5, 7.5),
            (-42.5, 2.5),
            (-37.5, 2.5),
            (-37.5, -7.5),
            (-32.5, -7.5),
            (-32.5, -12.5),
            (-27.5, -12.5),
            (-27.5, -27.5),
            (-22.5, -27.5),
            (-22.5, -47.5),
            (-17.5, -47.5),
            (-17.5, -67.5),
            (-22.5, -67.5),
            (-22.5, -77.5),
            (-27.5, -77.5),
            (-27.5, -82.5),
            (-47.5, -82.5),
            (-47.5, -87.5),
            (-77.5, -87.5),
            (-77.5, -92.5),
            (-87.5, -92.5),
            (-87.5, -112.5),
            (-92.5, -112.5),
            (-92.5, -122.5),
            (-97.5, -122.5),
            (-97.5, -137.5),
            (-92.5, -137.5),
            (-92.5, -142.5),
            (-82.5, -142.5),
            (-82.5, -147.5),
            (-72.5, -147.5),
            (-72.5, -152.5),
            (-67.5, -152.5),
            (-67.5, -157.5),
            (-62.5, -157.5),
            (-62.5, -162.5),
            (-57.5, -162.5),
            (-57.5, -167.5),
            (-52.5, -167.5),
            (-52.5, -172.5),
            (-47.5, -172.5),
            (-47.5, -177.5),
            (-42.5, -177.5),
            (-42.5, -180.1),  # connects to region 1
            (-180.1, -180.1),
        ),
        # Region 2, center right
        (
            (82.5, 57.5),
            (87.5, 57.5),
            (87.5, 42.5),
            (92.5, 42.5),
            (92.5, 22.5),
            (97.5, 22.5),
            (97.5, -17.5),
            (92.5, -17.5),
            (92.5, -22.5),
            (87.5, -22.5),
            (87.5, -27.5),
            (82.5, -27.5),
            (82.5, -37.5),
            (87.5, -37.5),
            (87.5, -47.5),
            (92.5, -47.5),
            (92.5, -57.5),
            (87.5, -57.5),
            (87.5, -67.5),
            (82.5, -67.5),
            (82.5, -72.5),
            (77.5, -72.5),
            (77.5, -77.5),
            (62.5, -77.5),
            (62.5, -72.5),
            (57.5, -72.5),
            (57.5, -67.5),
            (52.5, -67.5),
            (52.5, -37.5),
            (57.5, -37.5),
            (57.5, -27.5),
            (62.5, -27.5),
            (62.5, -22.5),
            (57.5, -22.5),
            (57.5, -12.5),
            (52.5, -12.5),
            (52.5, -7.5),
            (47.5, -7.5),
            (47.5, -2.5),
            (42.5, -2.5),
            (42.5, 2.5),
            (37.5, 2.5),
            (37.5, 12.5),
            (32.5, 12.5),
            (32.5, 22.5),
            (27.5, 22.5),
            (27.5, 32.5),
            (22.5, 32.5),
            (22.5, 47.5),
            (17.5, 47.5),
            (17.5, 67.5),
            (22.5, 67.5),
            (22.5, 77.5),
            (27.5, 77.5),
            (27.5, 82.5),
            (32.5, 82.5),
            (32.5, 87.5),
            (47.5, 87.5),
            (47.5, 92.5),
            (67.5, 92.5),
            (67.5, 87.5),
            (72.5, 87.5),
            (72.5, 82.5),
            (77.5, 82.5),
            (77.5, 77.5),
            (82.5, 77.5),
            (82.5, 57.5),
        ),
        # Region 3, bottom center, connects to region 4
        (
            (72.5, -102.5),
            (72.5, -112.5),
            (77.5, -112.5),
            (77.5, -157.5),
            (72.5, -157.5),
            (72.5, -180.1),  # connects to region 4
            (57.5, -180.1),  # connects to region 4
            (57.5, -167.5),
            (52.5, -167.5),
            (52.5, -162.5),
            (47.5, -162.5),
            (47.5, -157.5),
            (42.5, -157.5),
            (42.5, -152.5),
            (37.5, -152.5),
            (37.5, -142.5),
            (32.5, -142.5),
            (32.5, -107.5),
            (37.5, -107.5),
            (37.5, -102.5),
            (42.5, -102.5),
            (42.5, -97.5),
            (52.5, -97.5),
            (52.5, -92.5),
            (62.5, -92.5),
            (62.5, -97.5),
            (67.5, -97.5),
            (67.5, -102.5),
            (72.5, -102.5),
        ),
        # Region 4, top center, connects to region 3
        (
            (72.5, 180.1),  # connects to region 3
            (77.5, 180.0),
            (77.5, 162.5),
            (82.5, 162.5),
            (82.5, 147.5),
            (72.5, 147.5),
            (72.5, 157.5),
            (67.5, 157.5),
            (67.5, 167.5),
            (62.5, 167.5),
            (62.5, 180.0),
            (57.5, 180.1),  # connects to region 3
        ),
        # Region 5, top right, connects to regions 1 and 6
        (
            (162.5, 180.1),  # connects to region 6
            (162.5, 147.5),
            (167.5, 147.5),
            (167.5, 132.5),
            (172.5, 132.5),
            (172.5, 117.5),
            (177.5, 117.5),
            (177.5, 77.5),
            (180.1, 77.5),  # connects to region 1
            (180.1, 180.1),
        ),
        # Region 6, bottom right, connects to regions 1 and 5
        (
            (162.5, -180.1),  # connects to region 5
            (162.5, -177.5),
            (167.5, -177.5),
            (167.5, -167.5),
            (172.5, -167.5),
            (172.5, -157.5),
            (177.5, -157.5),
            (177.5, -147.5),
            (180.1, -147.5),  # connects to region 1
            (180.1, -180.1),
        ),
    )

    def _add_allowed_regions(self):
        """Add the allowed regions to the plot."""
        for region in self.FAVORED_REGIONS:
            self._add_allowed_region(region, c="r", alpha=0.25, zorder=-1)

        for region in self.ALLOWED_REGIONS:
            self._add_allowed_region(region, c="b", alpha=0.1, zorder=-2)

    def _add_allowed_region(self, region, *args, **kwargs):
        """Add a allowed regions to the plot."""
        new_args = (*zip(*region), *args)
        self._ax.fill(*new_args, **kwargs)


class _RegionType(IntEnum):
    """Describes the type of region a residue is in."""

    FAVORED = 3
    ALLOWED = 2
    POOR = 1


class _Axis(IntEnum):
    """Describes a axis in the plot."""

    PHI = 0
    PSI = 1


def color_residues_by_ramachandran(residues, color_ribbon):
    """Color the atoms in the residues by where they are located in the ramachandran plot.

    For each residue the script calculates if its in a favored, allowed or poor region and a score.
    The score is based on how close a residue is to an edge of a region and if that other region
    is better or worst then the region the residue is in. The residue is then colored based on
    the region and the score.
    """
    residues = set(residues)
    proteins = set()
    for residue in residues:
        proteins.add(residue.molecule)

    phi_edges = _regions_edges(_Axis.PHI)
    psi_edges = _regions_edges(_Axis.PSI)

    for protein in proteins:
        # Calculate the phi and psi values
        prot_residues, phi_list, psi_list = _calculate_phi_psi(protein)
        residues_regions = _calculate_region_and_score(
            residues, phi_edges, psi_edges, prot_residues, phi_list, psi_list
        )
        residue_colors = _region_and_score_to_colors(residues_regions)
        _color_protein_by_region_and_score(protein, residue_colors, color_ribbon)

    type = "ribbon" if color_ribbon else "atoms"
    flare.main_window().create_undo_point(f"Color {type} by Ramachandran")


def _regions_edges(axis):
    """Return the edges for all regions sorted by the axis.

    Returns:
    list(((edge_p1, edge_p2), _RegionType))
        The edges.
    """
    edges = []

    for region in _RamachandranPlotWidget.FAVORED_REGIONS:
        for edge in _points_to_lines(region):
            edges.append((edge, _RegionType.FAVORED))

    for region in _RamachandranPlotWidget.ALLOWED_REGIONS:
        for edge in _points_to_lines(region):
            edges.append((edge, _RegionType.ALLOWED))

    edges.sort(key=lambda edge: edge[0][0][axis])
    return edges


def _points_to_lines(points):
    """Convert a list of points to lines."""
    it = iter(points)
    first = next(it)
    point = first
    try:
        while True:
            next_point = next(it)
            yield (point, next_point)
            point = next_point
    except StopIteration:
        # If the region is not close then add a line to close it
        if first != point:
            yield (point, first)


def _calculate_region_and_score(
    allowed_residues, phi_edges, psi_edges, prot_residues, phi_list, psi_list
):
    """For each residue in calculate the region its in and the score.

    Parameters
    ----------
    allowed_residues: list(cresset.flare.Residue)
        The residues which the region and score will be calculated for.
    phi_edges: list(((edge_p1, edge_p2), _RegionType))
        The edges of all regions sorted on the phi axis.
    psi_edges: list(((edge_p1, edge_p2), _RegionType))
        The edges of all regions sorted on the psi axis.
    prot_residues: list(cresset.flare.Residue)
        All residues in the protein ehich have a phi and psi value.
        The prot_residues, phi_list and psi_list have the same size and are in the same order.
    phi_list: list(float)
        The phi value for each residue in `prot_residues`.
    psi_list: list(float)
        The psi value for each residue in `prot_residues`.

    Returns
    -------
    dict(cresset.flare.Residue, (_RegionType, score))
        The region type and score for each residue
    """
    residues_regions = {}
    for residue, phi, psi in list(zip(prot_residues, phi_list, psi_list)):
        if residue in allowed_residues:
            phi_region, phi_score = _region_and_score_for_axis(_Axis.PHI, phi_edges, (phi, psi))
            psi_region, psi_score = _region_and_score_for_axis(_Axis.PSI, psi_edges, (phi, psi))

            region = max(phi_region, psi_region)
            score = (phi_score + psi_score) / 2

            residues_regions[residue] = (region, score)
    return residues_regions


def _region_and_score_for_axis(axis, edges, phi_psi):
    """Calculate the region and score of point `phi_psi` along the `axis`.

    Returns:
    (_RegionType, score)
        The type of region `phi_psi` is in and its score.
        The score is between 1.0 (high) and 0.0 (low).
    """
    # Draw a line left to right (or bottom to top) and move along it using the
    # even-odd rule algorithm to find out what region `phi_psi` is in
    # and if `phi_psi` is closer to a better region or a poorer region (the score)

    master_line = _line_along_axis(axis, phi_psi)

    # The region phi_psi is in
    region = _RegionType.POOR
    # The region to the left (or below) of phi_psi and the distance to it
    nearest_before_dist = -360
    nearest_before_region = _RegionType.POOR
    # The region to the right (or above) of phi_psi and the distance to it
    nearest_after_dist = 360
    nearest_after_region = _RegionType.POOR

    # The region that we are currently in as we move along `master_line`.
    inside_region = set([_RegionType.POOR])

    # Note edges are sorted left to right or bottom to top
    for edge, edge_type in edges:
        intersect_point = _closed_segment_intersect(
            edge[0], edge[1], master_line[0], master_line[1]
        )
        if intersect_point:
            before_region = max(inside_region)
            # Leaving a region
            if edge_type in inside_region:
                inside_region.remove(edge_type)
            else:  # Entering a a region
                inside_region.add(edge_type)
            after_region = max(inside_region)

            # Ignore edges outside of the plot, these indicate the region wraps around the
            # boundaries of the plot. They are used for drawing the region on the plot but as they
            # artificial they should not be used for calculating the score
            if (abs(edge[0][0]) <= 180 and abs(edge[0][1]) <= 180) or (
                abs(edge[1][0]) <= 180 and abs(edge[1][1]) <= 180
            ):
                # The distance from phi_psi to the edge a negativity distance
                # shows the edge is left (or below) `phi_psi`.
                dist = intersect_point[axis] - phi_psi[axis]

                # Since the plot wraps on the boundaries if the distance is too far then
                # look at the distance from the other direction
                if dist >= 180:
                    dist = dist - 360
                if dist <= -180:
                    dist = dist + 360

                # If this edge is closer than any other edge seen on the same side then
                # keep track of it
                if dist <= 0 and dist > nearest_before_dist:
                    region = after_region
                    nearest_before_region = before_region
                    nearest_before_dist = dist

                if dist >= 0 and dist < nearest_after_dist:
                    region = before_region
                    nearest_after_region = after_region
                    nearest_after_dist = dist

    # Find where about `phi_psi` sits between the nearest edges on each side
    total_dist = abs(nearest_before_dist) + abs(nearest_after_dist)
    ratio = abs(nearest_before_dist) / (total_dist) if total_dist > 0 else 0

    # If the nearest edges on each side are from the same region take the center
    # point between the 2 edges as the best (_RegionType.FAVORED) or worst (_RegionType.POOR)
    # point.
    # Otherwise take the edge with the best region type as best point and the other edge with
    # as the worst point.
    # Then use `phi_psi` and the best and worst points to calculate the score.
    if nearest_before_region == nearest_after_region:
        ratio = abs((ratio * 2) - 1.0)
        if nearest_before_region < region:
            ratio = 1.0 - ratio
    elif nearest_before_region > nearest_after_region:
        ratio = 1.0 - ratio

    return region, ratio


def _line_along_axis(axis, phi_psi):
    """Return a line which goes through `phi_psi` and is parallel to the axis."""
    inv_axis = (axis + 1) % 2
    if inv_axis == _Axis.PHI:
        return ((phi_psi[inv_axis], -360), (phi_psi[inv_axis], 360))
    else:
        return ((-360, phi_psi[inv_axis]), (360, phi_psi[inv_axis]))


def _closed_segment_intersect(a1, a2, b1, b2):
    """Return the point line a1,a2 segment intersects with b1,b2."""
    point = None
    if _rect_intersect(a1, a2, b1, b2):
        point = _intersect(a1, a2, b1, b2)
        if point and not _inside_rect(point, a1, a2):
            point = None
    return point


def _rect_intersect(a1, a2, b1, b2):
    """Return if rect a1, a2 intersects with rect b1, b2."""
    a_min, a_max = _corners(a1, a2)
    b_min, b_max = _corners(b1, b2)
    return not (
        a_max[0] < b_min[0] or b_max[0] < a_min[0] or a_max[1] < b_min[1] or b_max[1] < a_min[1]
    )


def _corners(a1, a2):
    """Return the min and max corners of the rect refined by `a1` and `a2`."""
    min_corner = (min(a1[0], a2[0]), min(a1[1], a2[1]))
    max_corner = (max(a1[0], a2[0]), max(a1[1], a2[1]))
    return min_corner, max_corner


def _intersect(a1, a2, b1, b2):
    """Return the point line a1, a2 intersects with b1, b2."""
    s = np.vstack([a1, a2, b1, b2])  # s for stacked
    h = np.hstack((s, np.ones((4, 1))))  # h for homogeneous
    l1 = np.cross(h[0], h[1])  # get first line
    l2 = np.cross(h[2], h[3])  # get second line
    x, y, z = np.cross(l1, l2)  # point of intersection
    if z == 0:  # lines are parallel
        return None
    return (round(x / z, 3), round(y / z, 3))


def _inside_rect(point, a1, a2):
    """Return if `point` is inside rect a1, a2."""
    return _rect_intersect(point, point, a1, a2)


def _region_and_score_to_colors(residues_regions):
    """For each residue convert the region type and score to a color.

    Parameters
    ----------
    residues_regions: dict(cresset.flare.Residue, (_RegionType, score))
        The region type and score for each residue

    Returns
    -------
    dict(cresset.flare.Residue, (r,g,b))
        The colors for each residue.
    """
    residue_colors = {}
    for residue, (region, score) in residues_regions.items():
        h = None
        if region == _RegionType.FAVORED:
            h = (50 * (score)) + 70  # Green
        elif region == _RegionType.ALLOWED:
            h = (30 * (score)) + 40  # Yellow
        elif region == _RegionType.POOR:
            h = 40 * (score)  # Red

        if h:
            color = QtGui.QColor.fromHsv(h, 225, 204)
            residue_colors[residue] = (color.redF(), color.greenF(), color.blueF())
    return residue_colors


def _color_protein_by_region_and_score(protein, residue_colors, color_ribbon):
    """Color the protein atoms by the colors in `residue_colors`.

    Parameters
    ----------
    protein : cresset.flare.Protein
        The protein who atoms to color.
    residue_colors : dict(cresset.flare.Residue, (r,g,b))
        The colors for each residue.
    """
    if color_ribbon:
        if protein.style.ribbon_style == flare.RibbonStyle.Cartoon:
            protein.style.ribbon_style = flare.RibbonStyle.Tube
    else:
        protein.style.color = (0.5, 0.5, 0.5)

    for atom in protein.atoms:
        try:
            if color_ribbon:
                atom.style.ribbon_color = residue_colors[atom.residue]
            else:
                atom.style.color = residue_colors[atom.residue]
        except KeyError:
            pass
