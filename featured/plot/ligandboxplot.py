# -*- coding: utf-8 -*-
# Copyright (C) 2023 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset

from matplotlib.figure import Figure

from PySide2.QtWidgets import (
    QWidget,
    QVBoxLayout,
    QHBoxLayout,
    QGridLayout,
    QLabel,
    QPushButton,
    QCheckBox,
    QMenu,
    QAction,
    QFileDialog,
    QFrame,
)
from PySide2.QtCore import Signal, QEvent, Qt, QFileInfo, QTimer
from PySide2.QtGui import QPalette, QColor

from .multi_str_dialog import MultiStrSelDialog
from .color_button import ColorButton
from .figure_canvas import FigureCanvas

from uuid import uuid1
import os.path

from cresset import flare


class LigandBoxPlotConfig:
    def __init__(self):
        self.columns = {}
        self.ligands = []
        self.horizontal = False
        self.config_widget_visible = True
        self.use_theme_color = True
        self.custom_bg_color = "#FFFFFF"
        self.custom_fg_color = "#000000"

    def to_dict(self):
        dict = {}
        dict["Columns"] = self.columns
        dict["Ligands"] = list(self.ligands)
        dict["Horizontal"] = self.horizontal
        dict["Config Widget Visible"] = self.config_widget_visible
        dict["Use Theme Color"] = self.use_theme_color
        dict["Custom BG Color"] = self.custom_bg_color
        dict["Custom FG Color"] = self.custom_fg_color
        return dict

    def from_dict(self, dict):
        self.columns = dict.get("Columns", self.columns)
        self.ligands = dict.get("Ligands", self.ligands)
        self.horizontal = dict.get("Horizontal", self.horizontal)
        self.config_widget_visible = dict.get("Config Widget Visible", self.config_widget_visible)
        self.use_theme_color = dict.get("Use Theme Color", self.use_theme_color)
        self.custom_bg_color = dict.get("Custom BG Color", self.custom_bg_color)
        self.custom_fg_color = dict.get("Custom FG Color", self.custom_fg_color)


class LigandBoxPlotWidget(QWidget):
    config_changed = Signal(QWidget)
    closed = Signal(QWidget)

    def __init__(self, parent):
        super().__init__(parent)

        self.setMinimumSize(100, 100)
        self.setWindowTitle("Boxplot")

        self.uuid = uuid1()
        self._project = flare.main_window().project
        self._ligands = []

        parent.installEventFilter(self)

        self._fig = Figure()
        self._subplot = self._fig.add_subplot(111)
        self._canvas = FigureCanvas(self._fig)
        self._canvas.setContextMenuPolicy(Qt.CustomContextMenu)
        self._canvas.customContextMenuRequested.connect(self._on_plot_menu)

        self._cfg_widget = LigandBoxPlotConfigWidget(self)
        self._cfg_widget.config_changed.connect(self._on_config_changed)

        self._vbl = QVBoxLayout(self)
        self._vbl.setContentsMargins(0, 0, 0, 0)
        self._vbl.setSpacing(0)
        self._vbl.addWidget(self._canvas, 1)
        self._vbl.addWidget(self._cfg_widget)

        self._dir = os.path.expanduser("~")

        # Initial config
        config = LigandBoxPlotConfig()
        config.ligands = flare.main_window().selected_ligands
        if len(config.ligands) < 2:
            config.ligands = list(flare.main_window().project.ligands)
        self._cfg_widget.set_config(config)

        # Timer which rate limits how often the plot is updated
        # when ligands are being changed
        self._update_plot_timer = QTimer(self)
        self._update_plot_timer.setSingleShot(True)
        self._update_plot_timer.setInterval(1000)
        self._update_plot_timer.timeout.connect(self._update_plot)

        flare.callbacks.main_window_project_changed.add(self._on_project_changed)
        flare.callbacks.ligand_property_changed.add(self._on_ligand_property_changed)
        flare.callbacks.ligands_removed.add(self._on_ligands_removed)

    def shutdown(self):
        flare.callbacks.main_window_project_changed.remove(self._on_project_changed)
        flare.callbacks.ligand_property_changed.remove(self._on_ligand_property_changed)
        flare.callbacks.ligands_removed.remove(self._on_ligands_removed)
        self.closed.emit(self)

    # override QObject.eventFilter
    def eventFilter(self, watched, event):
        if event.type() == QEvent.Close:
            self.shutdown()
        return super().eventFilter(watched, event)

    @property
    def config(self):
        return self._cfg_widget.config()

    @config.setter
    def config(self, config):
        self._cfg_widget.set_config(config)

    def _on_plot_menu(self, pos):
        """Show the context menu."""
        menu = QMenu(self)

        save = QAction("Save Image")
        save.triggered.connect(self._save_image)
        menu.addAction(save)

        show_config = QAction("Show Config")
        show_config.setCheckable(True)
        show_config.setChecked(self.config.config_widget_visible)
        show_config.triggered.connect(self._set_config_widget_visible)
        menu.addAction(show_config)

        menu.exec_(self.mapToGlobal(pos))

    def _save_image(self):
        """Save the plot as an image."""
        path, _filter = QFileDialog.getSaveFileName(self, "Save Image", self._dir, "*.png")
        if path:
            info = QFileInfo(path)
            self._dir = info.dir().absolutePath()
            self._fig.savefig(path)

    def _set_config_widget_visible(self, checked):
        config = self.config
        config.config_widget_visible = checked
        self.config = config

    def _on_project_changed(self, project):
        if project != self._project:
            self.hide()

    def _on_ligand_property_changed(self, _ligand, _property_name, _property_value):
        """Schedules an update of the plot."""
        self._update_plot_timer.start()

    def _on_config_changed(self, config):
        self._cfg_widget.setVisible(config.config_widget_visible)
        self._update_plot()
        self.config_changed.emit(self)

    def _on_ligands_removed(self, ligands):
        """Remove the ligands from the plot."""
        config = self.config
        for ligand in ligands:
            if ligand in config.ligands:
                config.ligands.remove(ligand)
        self.config = config

    def _update_plot(self):
        col_values_dict = {}
        config = self._cfg_widget.config()
        for ligand in config.ligands:
            for column in self._project.ligands.columns:
                if column in config.columns:
                    value = ligand.properties[column].value
                    if not isinstance(value, bool) and (
                        isinstance(value, int) or isinstance(value, float)
                    ):
                        if column not in col_values_dict:
                            col_values_dict[column] = []
                        col_values_dict[column].append(value)

        box_plot_data = []
        x_ticks = []
        for x_axis, val in col_values_dict.items():
            x_ticks.append(x_axis)
            box_plot_data.append(val)

        bg_color = config.custom_bg_color
        fg_color = config.custom_fg_color
        if config.use_theme_color:
            palette = QPalette()
            bg_color = palette.color(QPalette.Window).name(QColor.HexRgb)
            fg_color = palette.color(QPalette.WindowText).name(QColor.HexRgb)
        self._fig.patch.set_facecolor(bg_color)
        self._subplot.set_facecolor(bg_color)
        self._subplot.spines["bottom"].set_color(fg_color)
        self._subplot.spines["top"].set_color(fg_color)
        self._subplot.spines["left"].set_color(fg_color)
        self._subplot.spines["right"].set_color(fg_color)
        self._subplot.tick_params(axis="x", colors=fg_color)
        self._subplot.tick_params(axis="y", colors=fg_color)
        self._subplot.xaxis.label.set_color(fg_color)
        self._subplot.yaxis.label.set_color(fg_color)

        self._subplot.clear()
        self._subplot.boxplot(
            box_plot_data,
            vert=0 if config.horizontal else 1,
            boxprops=dict(color=fg_color),
            whiskerprops=dict(color=fg_color),
            capprops=dict(color=fg_color),
        )

        if config.horizontal:
            self._subplot.set_yticklabels(x_ticks)
        else:
            self._subplot.set_xticklabels(x_ticks)
        self._fig.tight_layout()
        self._canvas.draw()


class LigandBoxPlotConfigWidget(QWidget):
    config_changed = Signal(LigandBoxPlotConfig)

    def __init__(self, parent=None):
        super().__init__(parent)
        self._columns_list = []
        self._config_widget_visible = True

        self._select_columns_button = QPushButton("Change Shown Columns")
        self._select_columns_button.setToolTip(
            "<span>Specify which result table columns to show in the box plot."
        )
        self._select_columns_button.clicked.connect(self._on_select_columns_clicked)

        self._multi_str_dlg = MultiStrSelDialog(self, "Pick columns to show")
        self._multi_str_dlg.new_strs.connect(self._on_new_selected_columns)

        self._plot_ligands_all_btn = QPushButton("Plot All Ligands")
        self._plot_ligands_all_btn.setToolTip("Redraw the plot so it contains all ligands")
        self._plot_ligands_all_btn.clicked.connect(self._set_all_ligands)

        self._plot_ligands_selected_btn = QPushButton("Plot Selected Ligands")
        self._plot_ligands_selected_btn.setToolTip(
            "Redraw the plot so it only contains the selected ligands"
        )
        self._plot_ligands_selected_btn.clicked.connect(self._set_selected_ligands)

        self._horizontal_checkbox = QCheckBox("Show horizontal")
        self._horizontal_checkbox.setToolTip(
            "<span>If checked, show a horizontal rather than a vertical boxplot."
        )
        self._horizontal_checkbox.toggled.connect(lambda _checked: self._on_setting_changed())

        self._use_theme_color_checkbox = QCheckBox("Use theme color")
        self._use_theme_color_checkbox.toggled.connect(self._on_use_theme_color_checkbox_changed)

        self._bg_color_button = ColorButton(parent)
        self._fg_color_button = ColorButton(parent)

        self._bg_color_button.color_changed.connect(lambda _color: self._on_setting_changed())
        self._fg_color_button.color_changed.connect(lambda _color: self._on_setting_changed())

        self._custom_bgfgcolor_frame = QFrame()
        self._custom_bgfgcolor_frame.setLineWidth(0)
        self._custom_bgfgcolor_frame.setFrameStyle(QFrame.NoFrame)

        bg_label = QLabel("Background:")
        fg_label = QLabel("Foreground:")
        bg_label.setToolTip("Set background color of the plot")
        fg_label.setToolTip("Set foreground/text color of the plot")

        bgfgcolor_layout = QHBoxLayout(self._custom_bgfgcolor_frame)
        bgfgcolor_layout.setContentsMargins(0, 0, 0, 0)
        bgfgcolor_layout.addWidget(bg_label)
        bgfgcolor_layout.addWidget(self._bg_color_button)
        bgfgcolor_layout.addWidget(fg_label)
        bgfgcolor_layout.addWidget(self._fg_color_button)

        self._main_layout = QGridLayout(self)
        self._main_layout.setContentsMargins(0, 0, 0, 0)
        self._main_layout.addWidget(self._plot_ligands_all_btn, 0, 0)
        self._main_layout.addWidget(self._select_columns_button, 0, 1)
        self._main_layout.addWidget(self._horizontal_checkbox, 0, 2)
        self._main_layout.addWidget(self._plot_ligands_selected_btn, 1, 0)
        self._main_layout.addWidget(self._use_theme_color_checkbox, 1, 1)
        self._main_layout.addWidget(self._custom_bgfgcolor_frame, 1, 2)

    def _on_setting_changed(self):
        self.config_changed.emit(self.config())

    def _on_use_theme_color_checkbox_changed(self, checked):
        self._custom_bgfgcolor_frame.setHidden(checked)
        self._on_setting_changed()

    def _available_columns(self) -> list:
        ligands = self._ligands
        if len(ligands) == 0:
            return []

        valid_columns = {}
        ligands_check_total = min(len(ligands), 100)
        for i in range(0, ligands_check_total):
            ligand = ligands[i]
            for column in flare.main_window().project.ligands.columns:
                value = ligand.properties[column].value
                if not isinstance(value, bool) and (
                    isinstance(value, int) or isinstance(value, float)
                ):
                    if column in valid_columns:
                        valid_columns[column] += 1
                    else:
                        valid_columns[column] = 1

        av_columns = []
        for column, valid_total in valid_columns.items():
            valid_percentage = valid_total / ligands_check_total
            if valid_percentage >= 0.8:
                av_columns.append(column)
        return av_columns

    def _on_select_columns_clicked(self):
        self._multi_str_dlg.open_with(self._available_columns(), set(self._columns_list))

    def _on_new_selected_columns(self, selected_columns: set):
        self._columns_list = selected_columns
        self._on_setting_changed()

    def _set_all_ligands(self):
        self._ligands = list(flare.main_window().project.ligands)
        self._on_setting_changed()

    def _set_selected_ligands(self):
        self._ligands = flare.main_window().selected_ligands
        self._on_setting_changed()

    def set_config(self, config: LigandBoxPlotConfig):
        self._columns_list = config.columns
        self._ligands = config.ligands
        self._horizontal_checkbox.setChecked(config.horizontal)
        self._config_widget_visible = config.config_widget_visible
        self._use_theme_color_checkbox.setChecked(config.use_theme_color)
        self._bg_color_button.set_color(config.custom_bg_color)
        self._fg_color_button.set_color(config.custom_fg_color)
        self.config_changed.emit(config)

    def config(self):
        config = LigandBoxPlotConfig()
        config.columns = self._columns_list
        config.ligands = self._ligands
        config.horizontal = self._horizontal_checkbox.isChecked()
        config.config_widget_visible = self._config_widget_visible
        config.use_theme_color = self._use_theme_color_checkbox.isChecked()
        config.custom_bg_color = self._bg_color_button.color()
        config.custom_fg_color = self._fg_color_button.color()
        return config
