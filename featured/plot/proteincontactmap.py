# -*- coding: utf-8 -*-
# Copyright (C) 2023 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Provides protein contact map dialog."""
from cresset import flare
from PySide2 import QtCore, QtWidgets

import numpy as np

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure


class ProteinContactMapDialog(QtWidgets.QDialog):
    """Dialog that contains the protein contact map.

    clicking on a point in the plot picks the 2 residues associated with that point and
    shows the 2 residues in the 3D window.

    Matplotlib is used to draw the protein contact map plot which is embedded into
    this dialog.

    Callbacks are added to flare so that when the protein is updated the
    dialog is updated accordingly.
    """

    def __init__(self, protein, parent=None):
        """Create the protein contact map.

        Parameters
        ----------
        protein : cresset.flare.Protein
            The protein the plot is for.
        """
        super().__init__(parent)

        self._protein = protein

        # If `parent` was set then the dialog will not be deleted until the parent is
        # deleted. The WA_DeleteOnClose flag changes this so this dialog is deleted
        # when it is closed
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)

        # Spinboxes to control the threshold which defines when items
        # in the heat map turn black or white.
        self._lower_bound = QtWidgets.QDoubleSpinBox(self)
        self._lower_bound.setRange(0, 99999)
        self._lower_bound.setValue(6)

        self._upper_bound = QtWidgets.QDoubleSpinBox(self)
        self._upper_bound.setRange(0, 99999)
        self._upper_bound.setValue(12)

        self._update = QtWidgets.QPushButton("Update", self)
        self._update.clicked.connect(self._update_plot)

        # Create the matplotlib plot
        self._fig = Figure()
        self._ax = self._fig.add_subplot(111)
        self._canvas = FigureCanvas(self._fig)
        self._sc = None

        # matplotlib raises exceptions if it gets too small
        self._canvas.setMinimumSize(100, 100)

        # Configure the annotate which is shown when moving the mouse over the plot
        self._annot = self._ax.annotate(
            "",
            xy=(0, 0),
            xytext=(10, 10),
            textcoords="offset points",
            bbox=dict(boxstyle="round", fc="w"),
        )
        self._annot.get_bbox_patch().set_alpha(0.4)

        self._annot.set_visible(False)
        self._canvas.mpl_connect("motion_notify_event", self._update_annotation)

        # Connect the pick event so when a point in the plot is clicked _on_pick is called
        self._canvas.mpl_connect("pick_event", self._on_pick)

        # Add the data to the plot
        self._update_plot()

        # Add callbacks which update the plot when the protein or atom pick is changed
        flare.callbacks.protein_structure_changed.add(self._on_protein_structure_changed)
        flare.callbacks.proteins_removed.add(self._on_proteins_removed)
        flare.callbacks.main_window_project_changed.add(self._on_project_changed)

        # Add the threshold spin boxes to the bottom of the dialog
        bottom_bar = QtWidgets.QHBoxLayout()
        bottom_bar.addSpacing(11)
        bottom_bar.addWidget(QtWidgets.QLabel("Lower threshold: ", self))
        bottom_bar.addWidget(self._lower_bound)
        bottom_bar.addSpacing(11)
        bottom_bar.addWidget(QtWidgets.QLabel("Upper threshold: ", self))
        bottom_bar.addWidget(self._upper_bound)
        bottom_bar.addWidget(self._update)
        bottom_bar.addStretch(1)

        # Add the Matplotlib plot to this dialog
        self._vbl = QtWidgets.QVBoxLayout(self)
        self._vbl.setContentsMargins(0, 0, 0, 0)
        self._vbl.addWidget(self._canvas, 1)
        self._vbl.addLayout(bottom_bar)

    def done(self, result):
        """Remove the callbacks when the dialog is closed."""
        flare.callbacks.protein_structure_changed.remove(self._on_protein_structure_changed)
        flare.callbacks.proteins_removed.remove(self._on_proteins_removed)
        flare.callbacks.main_window_project_changed.remove(self._on_project_changed)
        super().done(result)

    def _update_annotation(self, event):
        """Show/hide and update the annotation when the mouse is moved over the plot."""
        if event.xdata is not None and event.ydata is not None:
            self._annot.xy = (event.xdata, event.ydata)
            x_residue = self._amino_acids[int(event.xdata)]
            y_residue = self._amino_acids[int(event.ydata)]
            distance = self._distance[(x_residue, y_residue)]
            self._annot.set_text(f"{str(x_residue)}\n{str(y_residue)}\nDistance={distance}A")
            self._annot.set_visible(True)
        else:
            self._annot.set_visible(False)

        self._canvas.draw()

    def _update_plot(self):
        """Calculate the distance between each residue and update the plot."""
        # Update the plot title in case it changed
        self._ax.set_title(
            f"Contact map Plot for {self._protein}\n"
            + "Click on the plot to center on its residues."
        )

        # Map of residues to their alpha carbon
        residue_alpha_carbon_dict = {}
        for atom in self._protein.atoms:
            if atom.is_alpha_carbon():
                residue_alpha_carbon_dict[atom.residue] = atom

        # Find all amino acid residues in the protein which have a alpha carbon.
        # The residues are shown in the order they appear in the sequence view
        self._amino_acids = []
        for sequence in self._protein.sequences:
            if sequence.type == flare.Sequence.Type.Natural:
                for residue in sequence:
                    if residue in residue_alpha_carbon_dict:
                        self._amino_acids.append(residue)

        # Calculate the a value between 0 and 1 for the distance between the residues.
        # 1 means the residues are far apart, 0 means they are close to each other
        # The distance between residues is stored in self._distance so it can be shown in
        # the annotation
        data = np.empty([len(self._amino_acids), len(self._amino_acids)])
        self._distance = {}

        map_from_range = [self._lower_bound.value(), self._upper_bound.value()]
        map_to_range = [0, 1]

        for i, res_i in enumerate(self._amino_acids):
            if res_i in residue_alpha_carbon_dict:
                pos_i = np.array(residue_alpha_carbon_dict[res_i].pos)
                for j, res_j in enumerate(self._amino_acids[i:]):
                    if res_j in residue_alpha_carbon_dict:
                        pos_j = np.array(residue_alpha_carbon_dict[res_j].pos)
                        # Distance between the residues alpha carbons
                        dist = abs(np.linalg.norm(pos_i - pos_j))
                        # Map the distance into the range 0 to 1.
                        value = np.interp(dist, map_from_range, map_to_range)

                        data[i, j + i] = value
                        data[j + i, i] = value
                        dist_round = round(dist, 1)
                        self._distance[(res_i, res_j)] = dist_round
                        self._distance[(res_j, res_i)] = dist_round

        # Remove the heat map (if any) from the plot
        if self._sc is not None:
            self._sc.remove()

        # Plot the heat map
        self._sc = self._ax.imshow(data, cmap="gray", origin="lower", picker=True)

        # Hide the axis as on medium size proteins there too much data to show
        self._ax.set_xticks([])
        self._ax.set_yticks([])
        self._ax.set_xticklabels([])
        self._ax.set_yticklabels([])
        self._fig.tight_layout()

        self._canvas.draw()

    def _on_pick(self, event):
        """Pick and zoom to the residues atoms."""
        xdata = event.mouseevent.xdata
        ydata = event.mouseevent.ydata

        if xdata is not None and ydata is not None:
            x_residue = self._amino_acids[int(xdata)]
            y_residue = self._amino_acids[int(ydata)]

            flare.main_window().picked_atoms = [*x_residue.atoms, *y_residue.atoms]
            flare.main_window().camera.fit_to_window([x_residue, y_residue])

    def _on_protein_structure_changed(self, protein):
        """Update the plot when the structure of the protein is changed."""
        if self._protein == protein:
            self._update_plot()

    def _on_proteins_removed(self, proteins):
        """Close the dialog when its protein is deleted."""
        if self._protein in proteins:
            self.hide()

    def _on_project_changed(self, project):
        """Close the dialog when the project is changed."""
        self.hide()
