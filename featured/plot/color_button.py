# -*- coding: utf-8 -*-
# Copyright (C) 2023 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset

from PySide2.QtWidgets import QColorDialog, QPushButton
from PySide2.QtGui import QColor, QPainter, QPen, QBrush, QPalette
from PySide2.QtCore import Signal


class ColorButton(QPushButton):
    """Thin wrapper class which combines QPushButton and QColorDialog"""

    color_changed = Signal(str)

    def __init__(self, parent=None):
        super().__init__()
        self.color_dialog = QColorDialog(parent)
        self.color_dialog.setOptions(QColorDialog.NoButtons | QColorDialog.DontUseNativeDialog)
        self.color_dialog.currentColorChanged.connect(self._on_color_changed)
        self.color_dialog.setCurrentColor("#000000")
        self.clicked.connect(self.color_dialog.show)

    def _on_color_changed(self):
        self.repaint()
        self.color_changed.emit(self.color())

    def set_color(self, new_color: str):
        self.color_dialog.setCurrentColor(new_color)

    def color(self) -> str:
        return self.color_dialog.currentColor().name(QColor.HexRgb)

    def paintEvent(self, event):
        rect = event.rect()
        rect.setWidth(rect.width() - 1)
        rect.setHeight(rect.height() - 1)

        palette = QPalette()

        if self.underMouse():
            border_color = palette.color(QPalette.BrightText)
        else:
            border_color = palette.color(QPalette.Mid)

        painter = QPainter(self)
        painter.setPen(QPen(border_color, 1))
        painter.setBrush(QBrush(self.color_dialog.currentColor()))
        painter.drawRect(rect)
