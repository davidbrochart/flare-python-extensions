# -*- coding: utf-8 -*-
# Copyright (C) 2023 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Adds a ligand scatter plot, Ramachandran plot and protein contact map plot to Flare.

Ribbon Controls:
    Ligand -> Table -> Plot
        Open a scatter plot for 2 or 3 columns in the ligand table.
        The plot shows which ligands are selected and allows ligands in the plot
        to be selected by clicking them or drawing a lasso around them.

    Ligand -> Table -> Histogram
        Open a histogram plot for a column in the ligand table.

    Ligand -> Table -> Box Plot
        Open a boxplot for columns in the ligand table.

    Protein -> Structure -> Ramachandran
        Open a Ramachandran plot for the selected proteins.
        The plot shows which residues are picked and allows residues in the plot
        to be picked by clicking them or drawing a lasso around them.

    Protein -> Structure -> Contact Map
        Open a Protein Contact Map which shows the distance between the protein's alpha carbons.

    Home -> Appearance -> Color -> Color by Ramachandran
        Color the protein residues accordingly to which region they fall in the Ramachandran plot.

    Home -> Appearance -> Ribbon Color -> Color Ribbon by Ramachandran
        Color the ribbon accordingly to which region the residues fall in the the Ramachandran plot.
"""
import os
from cresset import flare

from PySide2 import QtCore, QtWidgets


@flare.extension
class PlotExtension:
    """Add buttons to the ribbon to show the various plots."""

    def load(self):
        """Load the extension."""
        tab = flare.main_window().ribbon["Ligand"]
        group = tab["Table"]
        control = group.add_button("Histogram", self._ligand_histogram_plot)
        control.size = flare.UIRibbonControl.Size.Large
        control.tooltip = (
            "Open a histogram plot for a column in the ligand table. "
            + "If 2 or more ligands are selected then only those ligands are shown, "
            + "otherwise all ligands are shown."
        )
        control.tip_key = "H"
        control.load_icon(_package_file("Histogram.png"))

        control = group.add_button("Plot", self._ligand_scatter_plot)
        control.size = flare.UIRibbonControl.Size.Large
        control.tooltip = (
            "Open a scatter plot for 2 or 3 columns in the ligand table. "
            + "If 2 or more ligands are selected then only those ligands are shown, "
            + "otherwise all ligands are shown."
        )
        control.tip_key = "P"
        control.load_icon(_package_file("scatter-plot.png"))

        control = group.add_button("Boxplot", self._ligand_box_plot)
        control.size = flare.UIRibbonControl.Size.Large
        control.tooltip = "Open a box plot for 1 or more columns in the ligand table."
        control.tip_key = "B"
        control.load_icon(_package_file("Boxplot.png"))

        tab = flare.main_window().ribbon["Protein"]
        group = tab["Structure"]
        control = group.add_button("Ramachandran", self._ramachandran_plot)
        control.size = flare.UIRibbonControl.Size.Small
        control.tooltip = "Open the Ramachandran plot for the selected proteins."
        control.tip_key = "AR"
        control.load_icon(_package_file("Ramachandran-plot.png"))

        control = group.add_button("Contact Map", self._protein_contact_map)
        control.size = flare.UIRibbonControl.Size.Small
        control.tooltip = "Open the protein contact map for the selected proteins."
        control.tip_key = "AC"
        control.load_icon(_package_file("Protein-contact-map.png"))

        control = flare.main_window().ribbon["Home"]["Appearance"]["Color"]
        self._add_menu_item(
            control,
            "Color by Ramachandran",
            "Color the residues accordingly to which region they fall in the Ramachandran plot.",
            -2,
            self._color_by_ramachandran,
        )

        control = flare.main_window().ribbon["Home"]["Appearance"]["Ribbon Color"]
        self._add_menu_item(
            control,
            "Color Ribbon by Ramachandran",
            "Color the ribbon accordingly to which region the residues fall in the "
            "Ramachandran plot.",
            None,
            self._color_ribbon_by_ramachandran,
        )

        flare.callbacks.main_window_project_changed.add(self._on_project_changed)

        print(f"Loaded {self.__class__.__module__}.{self.__class__.__name__}")

    def _ligand_box_plot(self):
        self._create_ligand_box_plot()

    def _create_ligand_box_plot(self):
        from . import ligandboxplot

        parent = flare.main_window().widget()

        # Create the dialog to show the plot
        dock = QtWidgets.QDockWidget(parent)

        # If `parent` was set then the dock will not be deleted until the parent is
        # deleted. The WA_DeleteOnClose flag changes this so this dialog is deleted
        # when it is closed
        dock.setAttribute(QtCore.Qt.WA_DeleteOnClose)

        widget = ligandboxplot.LigandBoxPlotWidget(dock)
        widget.config_changed.connect(self._box_plot_config_changed)
        widget.closed.connect(self._box_plot_closed)
        dock.setWindowTitle(widget.windowTitle())
        widget.windowTitleChanged.connect(dock.setWindowTitle)

        dock.setWidget(widget)
        parent.addDockWidget(QtCore.Qt.RightDockWidgetArea, dock)
        dock.setFloating(True)

        # Show the dialog
        dock.show()
        self._center_on_window(dock, parent)

        return widget

    def _box_plot_config_changed(self, plot):
        """Save the box plot config to the project."""
        project = flare.main_window().project
        data = None
        if "LigandBoxPlot" in project.user_data:
            data = project.user_data["LigandBoxPlot"]
        else:
            data = {"Dialogs": {}}

        if plot.uuid in data["Dialogs"]:
            data["Dialogs"][plot.uuid]["Config"] = plot.config.to_dict()
        else:
            data["Dialogs"][plot.uuid] = {"Config": plot.config.to_dict()}

        project.user_data["LigandBoxPlot"] = data

    def _box_plot_closed(self, plot):
        """Remove the box plot config from the project."""
        project = flare.main_window().project
        data = None
        if "LigandBoxPlot" in project.user_data:
            data = project.user_data["LigandBoxPlot"]
        else:
            data = {"Dialogs": {}}

        if plot.uuid in data["Dialogs"]:
            del data["Dialogs"][plot.uuid]

        project.user_data["LigandBoxPlot"] = data

    def _ligand_scatter_plot(self):
        """Show the scatter plot."""
        self._create_ligand_scatter_plot()

    def _create_ligand_scatter_plot(self):
        """Show and return the scatter plot."""
        from . import ligandscatter

        parent = flare.main_window().widget()

        # Create the dialog to show the plot
        dock = QtWidgets.QDockWidget(parent)

        # If `parent` was set then the dock will not be deleted until the parent is
        # deleted. The WA_DeleteOnClose flag changes this so this dialog is deleted
        # when it is closed
        dock.setAttribute(QtCore.Qt.WA_DeleteOnClose)

        widget = ligandscatter.LigandScatterPlotWidget(dock)
        widget.config_changed.connect(self._scatter_plot_config_changed)
        widget.closed.connect(self._scatter_plot_closed)
        dock.setWindowTitle(widget.windowTitle())
        widget.windowTitleChanged.connect(dock.setWindowTitle)

        dock.setWidget(widget)
        parent.addDockWidget(QtCore.Qt.RightDockWidgetArea, dock)
        dock.setFloating(True)

        # Show the dialog
        dock.show()
        self._center_on_window(dock, parent)

        return widget

    def _scatter_plot_config_changed(self, plot):
        """Save the scatter plot config to the project."""
        project = flare.main_window().project
        data = None
        if "LigandScatterPlot" in project.user_data:
            data = project.user_data["LigandScatterPlot"]
        else:
            data = {"Dialogs": {}}

        if plot.uuid in data["Dialogs"]:
            data["Dialogs"][plot.uuid]["Config"] = plot.config.to_dict()
        else:
            data["Dialogs"][plot.uuid] = {"Config": plot.config.to_dict()}

        project.user_data["LigandScatterPlot"] = data

    def _scatter_plot_closed(self, plot):
        """Remove the scatter plot config from the project."""
        project = flare.main_window().project
        data = None
        if "LigandScatterPlot" in project.user_data:
            data = project.user_data["LigandScatterPlot"]
        else:
            data = {"Dialogs": {}}

        if plot.uuid in data["Dialogs"]:
            del data["Dialogs"][plot.uuid]

        project.user_data["LigandScatterPlot"] = data

    def _ligand_histogram_plot(self):
        """Show the histogram plot."""
        self._create_ligand_histogram_plot()

    def _create_ligand_histogram_plot(self):
        """Show and return the _create_ligand_histogram_plot plot."""
        from . import ligandhistogram

        parent = flare.main_window().widget()

        # Create the dialog to show the plot
        dock = QtWidgets.QDockWidget(parent)

        # If `parent` was set then the dock will not be deleted until the parent is
        # deleted. The WA_DeleteOnClose flag changes this so this dialog is deleted
        # when it is closed
        dock.setAttribute(QtCore.Qt.WA_DeleteOnClose)

        widget = ligandhistogram.LigandHistogramPlotWidget(dock)
        widget.config_changed.connect(self._histogram_plot_config_changed)
        widget.closed.connect(self._histogram_plot_closed)
        dock.setWindowTitle(widget.windowTitle())
        widget.windowTitleChanged.connect(dock.setWindowTitle)

        dock.setWidget(widget)
        parent.addDockWidget(QtCore.Qt.RightDockWidgetArea, dock)
        dock.setFloating(True)

        # Show the dialog
        dock.show()
        self._center_on_window(dock, parent)

        return widget

    def _histogram_plot_config_changed(self, plot):
        """Save the histogram plot config to the project."""
        project = flare.main_window().project
        data = None
        if "LigandHistogramPlot" in project.user_data:
            data = project.user_data["LigandHistogramPlot"]
        else:
            data = {"Dialogs": {}}

        if plot.uuid in data["Dialogs"]:
            data["Dialogs"][plot.uuid]["Config"] = plot.config.to_dict()
        else:
            data["Dialogs"][plot.uuid] = {"Config": plot.config.to_dict()}

        project.user_data["LigandHistogramPlot"] = data

    def _histogram_plot_closed(self, plot):
        """Remove the histogram plot config from the project."""
        project = flare.main_window().project
        data = None
        if "LigandHistogramPlot" in project.user_data:
            data = project.user_data["LigandHistogramPlot"]
        else:
            data = {"Dialogs": {}}

        if plot.uuid in data["Dialogs"]:
            del data["Dialogs"][plot.uuid]

        project.user_data["LigandHistogramPlot"] = data

    def _on_project_changed(self, project):
        """Loads the boxplot, scatter, and histogram plots from the project and shows them."""
        boxplot_dialogs = None
        try:
            boxplot_dialogs = project.user_data["LigandBoxPlot"]["Dialogs"]
        except KeyError:
            return

        for uuid, data in boxplot_dialogs.items():
            from . import ligandboxplot

            # Create the dialog to show the plot
            widget = self._create_ligand_box_plot()
            config = ligandboxplot.LigandBoxPlotConfig()
            config.from_dict(data["Config"])
            widget.uuid = uuid
            widget.config = config

        scatter_dialogs = None
        try:
            scatter_dialogs = project.user_data["LigandScatterPlot"]["Dialogs"]
        except KeyError:
            return

        for uuid, data in scatter_dialogs.items():
            from . import ligandscatter

            # Create the dialog to show the plot
            widget = self._create_ligand_scatter_plot()
            config = ligandscatter.LigandScatterPlotConfig()
            config.from_dict(data["Config"])
            widget.uuid = uuid
            widget.config = config

        histogram_dialogs = None
        try:
            histogram_dialogs = project.user_data["LigandHistogramPlot"]["Dialogs"]
        except KeyError:
            return

        for uuid, data in histogram_dialogs.items():
            from . import ligandhistogram

            # Create the dialog to show the plot
            widget = self._create_ligand_histogram_plot()
            config = ligandhistogram.LigandHistogramPlotConfig()
            config.from_dict(data["Config"])
            widget.uuid = uuid
            widget.config = config

    def _center_on_window(self, widget, window):
        """Center the widget on the window."""
        parent_window = window.window()
        parent_window_centre = parent_window.frameGeometry().center()
        child_size = widget.frameGeometry().size()
        child_center = QtCore.QPoint(child_size.width() / 2, child_size.height() / 2)
        available_geometry = QtWidgets.QApplication.instance().desktop().availableGeometry(widget)
        new_center = parent_window_centre - child_center
        new_center.setY(
            min(
                max(available_geometry.top(), new_center.y()),
                available_geometry.bottom() - child_size.height(),
            )
        )
        new_center.setX(
            min(
                max(available_geometry.left(), new_center.x()),
                available_geometry.right() - child_size.width(),
            )
        )
        widget.move(new_center)

    @staticmethod
    def _ramachandran_plot():
        """Show the Ramachandran plot for the selected proteins."""
        from . import ramachandran

        parent = flare.main_window().widget()

        for protein in flare.main_window().selected_proteins:
            dialog = ramachandran.RamachandranPlotDialog(protein, parent)
            dialog.show()

    @staticmethod
    def _protein_contact_map():
        """Show the protein contact map for the selected proteins."""
        from . import proteincontactmap

        parent = flare.main_window().widget()
        proteins_list = flare.main_window().selected_proteins
        total_proteins = len(proteins_list)
        load_proteins = True
        if total_proteins > 10:
            msgbox_ret = QtWidgets.QMessageBox.warning(
                parent,
                "Contact Map Warning",
                f"{total_proteins} proteins selected, this will take a while to load. Do you want"
                + " to continue?",
                QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No,
            )
            load_proteins = msgbox_ret == QtWidgets.QMessageBox.Yes

        if load_proteins:
            for protein in proteins_list:
                dialog = proteincontactmap.ProteinContactMapDialog(protein, parent)
                dialog.show()

    @staticmethod
    def _color_by_ramachandran():
        """Color the proteins by the ramachandran plot."""
        from . import ramachandran

        picked_residues = flare.main_window().picked_residues
        if not picked_residues:
            for protein in flare.main_window().selected_proteins:
                picked_residues.extend(protein.residues)
        ramachandran.color_residues_by_ramachandran(picked_residues, False)

    @staticmethod
    def _color_ribbon_by_ramachandran():
        """Color the proteins by the ramachandran plot."""
        from . import ramachandran

        picked_residues = flare.main_window().picked_residues
        if not picked_residues:
            for protein in flare.main_window().selected_proteins:
                picked_residues.extend(protein.residues)
        ramachandran.color_residues_by_ramachandran(picked_residues, True)

    @classmethod
    def _add_menu_item(cls, control, title, status_tip, insert_index, func):
        """Add a menu item to control menu which runs `func` when pressed."""
        menu = control.menu()
        actions = menu.actions()
        parent = flare.main_window().widget()
        action = QtWidgets.QAction(title, parent)
        action.setToolTip(status_tip)
        action.setStatusTip(status_tip)
        action.triggered.connect(func)
        if insert_index is None:
            menu.addAction(action)
        else:
            insert_before_action = actions[insert_index]
            menu.insertAction(insert_before_action, action)


def _package_file(file_name):
    """Return the path to the file `file_name` in this package."""
    return os.path.join(os.path.dirname(os.path.realpath(__file__)), file_name)
