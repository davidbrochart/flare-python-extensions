# -*- coding: utf-8 -*-
# Copyright (C) 2023 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Provides the ligand scatter plot."""
from scipy import stats

from PySide2 import QtCore, QtGui, QtWidgets

import numpy as np
import uuid

import os.path

from matplotlib.widgets import LassoSelector
from matplotlib.path import Path
from matplotlib.figure import Figure
from matplotlib.markers import MarkerStyle

from cresset import flare
from .scatter.plot import LigandScatterPlotConfig, LigandScatterPlotConfigWidget
from .figure_canvas import FigureCanvas


class LigandScatterPlotWidget(QtWidgets.QWidget):
    """A widget which displays a scatter plot for 2 columns in the ligand table.

    Selecting a point in the plot either by clicking it or dragging a lasso
    around it will select the point ligands.

    Hovering the mouse cursor over a point will show the name of the ligand that point represents.

    There's also a permanent annotations toggle which allows for the name of the ligand of that
    point to stay on, toggled by mouse click of a point.
    """

    config_changed = QtCore.Signal(QtWidgets.QWidget)
    """Emitted when the configuration is changed"""

    closed = QtCore.Signal(QtWidgets.QWidget)
    """Emitted when the plot is closed"""

    def __init__(self, parent):
        super().__init__(parent)

        self.uuid = uuid.uuid1()
        self._project = flare.main_window().project

        parent.installEventFilter(self)

        self._plot_widget = _LigandScatterPlotWidget(self)
        self._configure_axis = LigandScatterPlotConfigWidget(self)

        self.setPalette(QtWidgets.QDialog().palette())

        # Add the Matplotlib plot to this dialog
        self._vbl = QtWidgets.QVBoxLayout(self)
        self._vbl.setContentsMargins(0, 0, 0, 0)
        self._vbl.setSpacing(0)
        self._vbl.addWidget(self._plot_widget, 1)
        self._vbl.addWidget(self._configure_axis)

        flare.callbacks.main_window_project_changed.add(self._on_project_changed)
        self._plot_widget.r_squared_changed.connect(self._configure_axis.set_r_squared)
        self._plot_widget.plot_title_changed.connect(self.setWindowTitle)
        self._configure_axis.config_changed.connect(self._plot_widget.set_config)
        self._configure_axis.config_changed.connect(self._on_config_changed)

        self._plot_widget.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self._plot_widget.customContextMenuRequested.connect(self._on_plot_menu)

        # Set the inital config to use in the plot
        config = LigandScatterPlotConfig()
        config.ligands = flare.main_window().selected_ligands
        # if there are less than 2 selected ligands, use all ligands
        if len(config.ligands) < 2:
            config.ligands = list(self._project.ligands)
        self._configure_axis.set_config(config)

        self._dir = os.path.expanduser("~")

    def shutdown(self):
        """Remove the callbacks when the dialog is closed."""
        flare.callbacks.main_window_project_changed.remove(self._on_project_changed)
        self._plot_widget.shutdown()
        self._configure_axis.shutdown()
        self.closed.emit(self)

    def _on_project_changed(self, project):
        """Close the dialog when the project is changed."""
        if project != self._project:
            self.hide()

    def _on_config_changed(self, config):
        """Emits config_changed."""
        self._configure_axis.setVisible(config.config_widget_visible)
        self.config_changed.emit(self)

    @property
    def config(self):
        """Save the plot configuration to the project."""
        return self._configure_axis.config()

    @config.setter
    def config(self, config):
        """Save the plot configuration to the project."""
        self._configure_axis.set_config(config)

    def _on_plot_menu(self, pos):
        """Show the context menu."""
        menu = QtWidgets.QMenu(self)

        save = QtWidgets.QAction("Save Image")
        save.triggered.connect(self._save_image)
        menu.addAction(save)

        show_config = QtWidgets.QAction("Show Config")
        show_config.setCheckable(True)
        show_config.setChecked(self.config.config_widget_visible)
        show_config.triggered.connect(self._set_config_widget_visible)
        menu.addAction(show_config)

        menu.exec_(self.mapToGlobal(pos))

    def _save_image(self):
        """Save the plot as an image."""
        path, filter = QtWidgets.QFileDialog.getSaveFileName(self, "Save Image", self._dir, "*.png")
        info = QtCore.QFileInfo(path)
        self._dir = info.dir().absolutePath()
        self._plot_widget._fig.savefig(path)

    def _set_config_widget_visible(self, checked):
        """Hide or show the config widget."""
        conf = self.config
        conf.config_widget_visible = checked
        self.config = conf

    # override QObject.eventFilter
    def eventFilter(self, watched, event):
        if event.type() == QtCore.QEvent.Close:
            self.shutdown()
        return super().eventFilter(watched, event)


class _LigandScatterPlotSeries:
    """The data for one of the series in the plot.

    The plot can contain 2 series, one for x vs y1 and a second
    for x vs y2.

    This also holds the annoations, both hover and permanent.
    Hover annoation appears on mouse hover over a point.
    Permanent annotations can be toggled when the user clicks
    on a point while the permanent annotations option is turned
    on. Those just simply stays on regardless of mouse position
    until the ligand gets removed or selected ligands changed
    in the plot.
    """

    def __init__(self, axis):
        self.axis = axis
        self.path = None
        self.regression_line = None
        self.ligands = []
        self.permanent_annotations = {}

        self.hover_annotation = self.axis.annotate(
            "",
            xy=(0, 0),
            xytext=(10, 10),
            textcoords="offset points",
            bbox=dict(boxstyle="round", fc="w"),
            arrowprops=dict(arrowstyle="->"),
        )
        self.hover_annotation.set_visible(False)


class _LigandScatterPlotWidget(QtWidgets.QWidget):
    """Widget showing the plot."""

    r_squared_changed = QtCore.Signal(str, str)
    """Emitted when the r2 values for each series may have changed."""
    plot_title_changed = QtCore.Signal(str)
    """Emitted when the plot title may have changed."""

    def __init__(self, parent=None):
        super().__init__(parent)

        # matplotlib raises exceptions if it gets too small
        self.setMinimumSize(100, 100)

        # Create the matplotlib scatter plot
        self._fig = Figure()
        self._canvas = FigureCanvas(self._fig)
        self._series1 = _LigandScatterPlotSeries(self._fig.add_subplot(111))
        self._series2 = _LigandScatterPlotSeries(self._series1.axis.twinx())
        self._cur_shape_data = {}

        # Connect the pick event so when a point in the plot is clicked _on_pick is called
        # Note "pick_event" does not work due to the twinx() function being called
        self._canvas.mpl_connect("button_release_event", self._on_pick)

        # Lasso only works if added to the axis created last, howerever it selects items in all axis
        # Due to the order of these connects when the mouse button is released after drawning a
        # lasso it will first call self._on_pick then call self._on_lasso_select
        self._lasso = LassoSelector(self._series2.axis, onselect=self._on_lasso_select, button=1)

        # Show annotation when the mouse is over a point
        self._canvas.mpl_connect("motion_notify_event", self._hover)

        # Add the Matplotlib plot to this dialog
        self._vbl = QtWidgets.QVBoxLayout(self)
        self._vbl.setContentsMargins(0, 0, 0, 0)
        self._vbl.addWidget(self._canvas)

        # Set the default config
        self._config = LigandScatterPlotConfig()

        # Timer which rate limits how often the plot is updated
        # when ligands are being changed
        self._update_plot_timer = QtCore.QTimer(self)
        self._update_plot_timer.setSingleShot(True)
        self._update_plot_timer.setInterval(1000)
        self._update_plot_timer.timeout.connect(self._update_plot)

        # Add the data to the plot
        self._update_plot()

        # Add callbacks which update the plot when the protein or atom pick is changed
        flare.callbacks.selected_ligands_changed.add(self._on_selected_ligands_changed)
        flare.callbacks.ligands_removed.add(self._on_ligands_removed)
        flare.callbacks.ligand_property_changed.add(self._on_ligand_property_changed)
        flare.callbacks.roles_added.add(self._on_roles_changed)
        flare.callbacks.roles_changed.add(self._on_roles_changed)
        flare.callbacks.roles_removed.add(self._on_roles_changed)
        flare.callbacks.ligands_role_changed.add(self._on_ligands_role_changed)

    def shutdown(self):
        """Remove the callbacks when the dialog is closed."""
        flare.callbacks.selected_ligands_changed.remove(self._on_selected_ligands_changed)
        flare.callbacks.ligands_removed.remove(self._on_ligands_removed)
        flare.callbacks.ligand_property_changed.remove(self._on_ligand_property_changed)
        flare.callbacks.roles_added.remove(self._on_roles_changed)
        flare.callbacks.roles_changed.remove(self._on_roles_changed)
        flare.callbacks.roles_removed.remove(self._on_roles_changed)
        flare.callbacks.ligands_role_changed.remove(self._on_ligands_role_changed)

    def set_config(self, config):
        """Redraws the plot for the given configuration."""
        self._config = config
        self._update_plot()

    # override QWidget.sizeHint
    def sizeHint(self):
        """Return the default size of the plot."""
        return QtCore.QSize(500, 500)

    # override QWidget.resizeEvent
    def resizeEvent(self, event):
        """Redraws the plot."""
        self._fig.tight_layout()
        self._canvas.draw()
        super().resizeEvent(event)

    def _update_plot(self):
        """Update the data displayed in the plot."""

        # Get the data out of the ligand table
        x1_data = []
        x2_data = []
        y1_data = []
        y2_data = []
        color_data = []
        shape_data = []
        size_data = []

        self._series1.ligands = []
        self._series2.ligands = []

        self._series1.r_squared = ""
        self._series2.r_squared = ""

        self._config.color_axis.setup_data(self._ligand_column_value)
        self._config.size_axis.setup_data(self._ligand_column_value)

        for ligand in self._config.ligands:
            x_value = self._ligand_column_value(ligand, self._config.x_axis.column)
            y1_value = self._ligand_column_value(ligand, self._config.y1_axis.column)
            y2_value = self._ligand_column_value(ligand, self._config.y2_axis.column)

            # skip ligands for which a float is not available for both properties
            if x_value is not None:
                if y1_value is not None:
                    x1_data.append(x_value)
                    y1_data.append(y1_value)
                    self._series1.ligands.append(ligand)

                if y2_value is not None:
                    x2_data.append(x_value)
                    y2_data.append(y2_value)
                    self._series2.ligands.append(ligand)

                if y1_value is not None or y2_value is not None:
                    self._config.color_axis.add_color_data(color_data, ligand)
                    self._config.shape_axis.add_shape_data(shape_data, ligand)
                    self._config.size_axis.add_size_data(size_data, ligand)

        # Set back/foreground color
        bg_color, fg_color = self._bgfg_colors()
        self._fig.patch.set_facecolor(bg_color)
        self._series1.axis.set_facecolor(bg_color)
        self._series2.axis.set_facecolor(bg_color)
        self._series1.axis.spines["bottom"].set_color(fg_color)
        self._series1.axis.spines["top"].set_color(fg_color)
        self._series1.axis.spines["left"].set_color(fg_color)
        self._series1.axis.spines["right"].set_color(fg_color)
        self._series2.axis.spines["bottom"].set_color(fg_color)
        self._series2.axis.spines["top"].set_color(fg_color)
        self._series2.axis.spines["left"].set_color(fg_color)
        self._series2.axis.spines["right"].set_color(fg_color)
        self._series1.axis.tick_params(axis="x", colors=fg_color, which="both")
        self._series1.axis.tick_params(axis="y", colors=fg_color, which="both")
        self._series2.axis.tick_params(axis="y", colors=fg_color, which="both")
        self._series1.axis.xaxis.label.set_color(fg_color)
        self._series1.axis.yaxis.label.set_color(fg_color)
        self._series2.axis.yaxis.label.set_color(fg_color)
        for series in (self._series1, self._series2):
            series.hover_annotation.set_color(fg_color)
            for _, annotation in series.permanent_annotations.items():
                annotation.set_color(fg_color)

        self._plot_x_y_data(
            x1_data,
            y1_data,
            self._series1,
            self._config.y1_axis.marker,
            self._config.y1_axis.color,
            self._config.y1_axis.size,
            color_data,
            shape_data,
            size_data,
            "xy1",
        )
        self._plot_x_y_data(
            x2_data,
            y2_data,
            self._series2,
            self._config.y2_axis.marker,
            self._config.y2_axis.color,
            self._config.y2_axis.size,
            color_data,
            shape_data,
            size_data,
            "xy2",
        )

        self._series1.axis.set_xlabel(self._config.x_axis.column)
        self._series1.axis.set_ylabel(self._config.y1_axis.column)
        self._series2.axis.set_ylabel(self._config.y2_axis.column)

        self._series1.axis.set_xscale(self._config.x_axis.scale_type_str())
        self._series1.axis.set_yscale(self._config.y1_axis.scale_type_str())
        self._series2.axis.set_yscale(self._config.y2_axis.scale_type_str())

        # self._series1.axis.set_autoscale_on is broken for scatter plots so the auto scaling
        # is done manually
        lower, upper = self._get_axis_scale((*x1_data, *x2_data), self._config.x_axis)
        self._series1.axis.set_xbound(lower, upper)

        lower, upper = self._get_axis_scale(y1_data, self._config.y1_axis)
        self._series1.axis.set_ybound(lower, upper)

        lower, upper = self._get_axis_scale(y2_data, self._config.y2_axis)
        self._series2.axis.set_ybound(lower, upper)

        columns = [
            axis_config.column
            for axis_config in (self._config.x_axis, self._config.y1_axis, self._config.y2_axis)
            if axis_config.column is not None
        ]
        if columns:
            self.plot_title_changed.emit(" - ".join(columns))
        else:
            self.plot_title_changed.emit("Plot")

        # Update the colors of the scatter points for the selected ligands
        self._on_selected_ligands_changed(flare.main_window().selected_ligands)
        self._update_permanent_annotations()

        self._fig.tight_layout()
        self._canvas.draw()
        self.r_squared_changed.emit(self._series1.r_squared, self._series2.r_squared)

    def _plot_x_y_data(
        self,
        x_data,
        y_data,
        series,
        marker,
        y_default_color,
        size,
        color_data,
        shape_data,
        size_data,
        plot_id,
    ):
        """Update the scatter plot with the x,y data."""

        if series.regression_line is not None:
            series.regression_line.set_visible(False)

        # matplotlib does not have a simple way to remove a series and setting the
        # series data to an empty array causes matplotlib to crash.
        # Instead hide the series but leave it with the old data
        if not x_data or not y_data:
            if series.path:
                series.path.set_visible(False)
                series.axis.get_yaxis().set_visible(False)
                return

        # Take default color if color_data is empty
        if color_data == []:
            color_data = y_default_color

        # Take default shape if shape_data is empty
        if shape_data == []:
            shape_data = [marker] * len(x_data)

        # Take default size if size_data is empty
        if size_data == []:
            size_data = [size**2] * len(x_data)

        # Matplotlib cannot just simply update the shapes/markers, so need to
        # remove the scatter and inserting it again to refresh shapes/markers
        if series.path and (
            plot_id in self._cur_shape_data and self._cur_shape_data[plot_id] != shape_data
        ):
            series.path.set_visible(False)
            series.axis.get_yaxis().set_visible(False)
            del series.path
            series.path = None

        if series.path is None:
            # Matplotlib does not support list of markers, will have to plot them ourselves
            # Also just setting a single marker is broken also, so just do this anyway
            series.path = series.axis.scatter(x_data, y_data, s=size_data, color=color_data)
            paths = []
            for marker in shape_data:
                if isinstance(marker, MarkerStyle):
                    marker_obj = marker
                else:
                    marker_obj = MarkerStyle(marker)
                paths.append(marker_obj.get_path().transformed(marker_obj.get_transform()))
            series.path.set_paths(paths)
        else:
            # Update the collection with the new data
            data = list(zip(x_data, y_data))
            series.path.set_offsets(data)
            series.path.set_color(color_data)
            series.path.set_sizes(size_data)

        series.path.set_visible(True)
        series.axis.get_yaxis().set_visible(True)

        # Set each point in the scatter plot to have its own color.
        face_colors = series.path.get_facecolors()

        if x_data and y_data:
            if len(face_colors) == 1:  # First time showing the plot
                face_colors = np.tile(face_colors, len(x_data)).reshape(len(x_data), -1)
            elif len(face_colors) != len(x_data):  # Updated the collection with the new data
                face_colors = np.tile(face_colors[0], len(x_data)).reshape(len(x_data), -1)

            series.path.set_facecolors(face_colors)

        # Calculate the regression line and r2 values
        if len(x_data) > 1 and len(y_data) > 1:
            slope, intercept, r_value, _, _ = stats.linregress(x_data, y_data)
            regression_x_data = [min(x_data), max(x_data)]
            regression_y_data = [slope * x + intercept for x in regression_x_data]
            series.r_squared = str(round(r_value**2, 3))

            if series.regression_line is None:
                series.regression_line = series.axis.plot(
                    regression_x_data, regression_y_data, color=y_default_color, antialiased=True
                )[0]
                series.regression_line.zorder = -1
            else:
                series.regression_line.set_xdata(regression_x_data)
                series.regression_line.set_ydata(regression_y_data)
                series.regression_line.set_color(y_default_color)

            series.regression_line.set_visible(self._config.show_regression_lines)

        self._cur_shape_data[plot_id] = shape_data

    def _get_axis_scale(self, data, axis_config):
        """Return the lower and upper bound of the axis scale"""
        lower_bound = axis_config.min_scale
        upper_bound = axis_config.max_scale
        if axis_config.auto_scale:
            lower = min(data, default=lower_bound)
            upper = max(data, default=upper_bound)
            buffer = (upper - lower) * 0.05
            lower_bound = lower - buffer
            upper_bound = upper + buffer

        if lower_bound >= upper_bound:
            upper_bound = lower_bound + 1

        return (lower_bound, upper_bound)

    @staticmethod
    def _ligand_column_value(ligand, column):
        """Returns the value in the column for the ligand

        Returns None if the value in the column is not a number.
        """
        if column is None:
            return None
        try:
            return float(ligand.properties[column].value)
        except (ValueError, TypeError):
            pass
        return None

    def _on_selected_ligands_changed(self, ligands):
        """Colors the points on the plot differently for selected and unselected ligands."""

        for series in (self._series1, self._series2):
            if series.path is not None and series.path.get_visible():
                face_colors = series.path.get_facecolors()

                # Change the alpha depending on if a ligands is selected
                for i, ligand in enumerate(series.ligands):
                    alpha = 1.0 if ligand in ligands else 0
                    if i < len(face_colors):
                        face_colors[i, -1] = alpha

                series.path.set_facecolors(face_colors)
        self._canvas.draw()

    def _update_permanent_annotations(self):
        for series in (self._series1, self._series2):
            for ligands_set in list(series.permanent_annotations.keys()):
                delete_annotation = True
                if self._config.toggle_permanent_annotations:
                    annotation = series.permanent_annotations[ligands_set]
                    ligand = list(ligands_set)[0]
                    if ligand in series.ligands:
                        idx = series.ligands.index(ligand)
                        new_xy = series.path.get_offsets()[idx]
                        if not np.array_equal(new_xy, annotation.xy):
                            annotation.xy = new_xy
                        delete_annotation = False

                if delete_annotation:
                    series.permanent_annotations[ligands_set].remove()
                    del series.permanent_annotations[ligands_set]

    def _fix_permanent_annotations(self, rm_ligands):
        for series_idx, series in enumerate((self._series1, self._series2)):
            if len(series.permanent_annotations) > 0:
                for ligands_set in list(series.permanent_annotations.keys()):
                    if len(ligands_set.intersection(rm_ligands[series_idx])) > 0:
                        series.permanent_annotations[ligands_set].remove()
                        del series.permanent_annotations[ligands_set]

    def _on_ligands_removed(self, ligands):
        """Remove the ligands from the plot."""
        rm_ligands = {0: set(), 1: set()}
        for ligand in ligands:
            for series_idx, series in enumerate((self._series1, self._series2)):
                if ligand in series.ligands:
                    rm_ligands[series_idx].add(ligand)
            if ligand in self._config.ligands:
                self._config.ligands.remove(ligand)

        self._fix_permanent_annotations(rm_ligands)
        self._update_plot()

        for ligand in ligands:
            for series in (self._series1, self._series2):
                if ligand in series.ligands:
                    series.ligands.remove(ligand)

    def _on_ligand_property_changed(self, ligand, property_name, property_value):
        """Schedules an update of the plot. Also update permanent annotation title if applicable."""
        if property_name == "Title":
            annotations_has_changed = False
            for series in (self._series1, self._series2):
                for ligands_set, annotation in series.permanent_annotations.items():
                    if ligand in ligands_set:
                        annotation.set_text(self._annotation_text(series, ligands_set))
                        annotations_has_changed = True
                        break
            if annotations_has_changed:
                self._canvas.draw()
        self._update_plot_timer.start()

    def _on_roles_changed(self, _role_list):
        self._update_plot()

    def _on_ligands_role_changed(self, _ligands_list, _role):
        self._update_plot()

    def _hover(self, event):
        """Show or hide the annotation if needed when the mouse is moved over the plot."""
        # Dont show annotions when drawning the lasso
        if self._lasso._selection_artist.get_visible():
            return

        for series in (self._series1, self._series2):
            if series.path is not None and series.path.get_visible():
                vis = series.hover_annotation.get_visible()
                cont, ind = series.path.contains(event)
                indexes, ligands_set, _ = self._ligands_set(series, ind["ind"])
                if cont:
                    series.hover_annotation.xy = series.path.get_offsets()[indexes[0]]
                    series.hover_annotation.set_text(self._annotation_text(series, ligands_set))
                    series.hover_annotation.get_bbox_patch().set_alpha(0)
                    series.hover_annotation.set_visible(True)
                elif vis:
                    series.hover_annotation.set_visible(False)

        self._canvas.draw()

    def _annotation_text(self, series, ligands_set):
        """Return the names of the ligands.

        This text will be displayed in the annotation which appears when hovering over a point.

        Parameters
        ----------
        ligands_set : set(flare.Ligand)
            The ligands of the items in the scatter plot which the mouse pointer is over.
        """
        names = []
        for ligand in ligands_set:
            if ligand in series.ligands:
                names.append(str(ligand.title))

        MAX_NAMES = 5
        if len(names) > MAX_NAMES:
            names = names[0:MAX_NAMES]
            names.append("...")

        text = ",\n".join(names)
        return text

    def _on_lasso_select(self, verts):
        """Call when the lasso selects items in the plot.

        _select_indexes() will be called with the indexes of the selected items.
        """
        if len(verts) > 2:
            # verts is in series2 axis coordinate space, convert it to display coordinates
            # then back into axis coordinate space for each series
            points_data = self._series2.axis.transData.transform(verts)

            ligands = []
            for series in (self._series1, self._series2):
                if series.path is not None and series.path.get_visible():
                    axis_verts = series.axis.transData.inverted().transform(points_data)
                    path = Path(axis_verts)
                    xys = series.path.get_offsets()
                    ind = np.nonzero([path.contains_point(xy) for xy in xys])[0]
                    ligands.extend([series.ligands[n] for n in ind])

            flare.main_window().selected_ligands = ligands

        self._canvas.draw()

    def _create_annotation(self, series, text: str, xy_coord: list[float] = [0.0, 0.0]):
        annotation = series.axis.annotate(
            text,
            xy=xy_coord,
            xytext=(10, 10),
            textcoords="offset points",
            bbox=dict(boxstyle="round", fc="w"),
            arrowprops=dict(arrowstyle="->"),
        )
        annotation.get_bbox_patch().set_alpha(0)
        annotation.set_visible(True)
        _, fg_color = self._bgfg_colors()
        annotation.set_color(fg_color)
        return annotation

    def _ligands_set(self, series, indexes):
        ligands = [series.ligands[n] for n in indexes]
        return indexes, frozenset(ligands), ligands

    def _on_pick(self, event):
        """Select the items associated with the point clicked."""
        ligands = []
        for series in (self._series1, self._series2):
            if series.path is not None and series.path.get_visible():
                cont, ind = series.path.contains(event)
                if cont:
                    indexes, ligands_set, ligands_list = self._ligands_set(series, ind["ind"])
                    ligands.extend(ligands_list)
                    if (
                        self._config.toggle_permanent_annotations
                        and series.hover_annotation.get_visible()
                    ):
                        if ligands_set in series.permanent_annotations:
                            # Removal
                            series.permanent_annotations[ligands_set].remove()
                            del series.permanent_annotations[ligands_set]
                        else:
                            # Inserting
                            series.permanent_annotations[ligands_set] = self._create_annotation(
                                series,
                                self._annotation_text(series, ligands_set),
                                series.path.get_offsets()[indexes[0]],
                            )

        flare.main_window().selected_ligands = ligands
        self._canvas.draw()

    def _bgfg_colors(self) -> (tuple, tuple):
        if self._config.use_theme_color:
            palette = QtGui.QPalette()
            color = palette.color(QtGui.QPalette.Window)
            bg_color = (color.redF(), color.greenF(), color.blueF())
            color = palette.color(QtGui.QPalette.WindowText)
            fg_color = (color.redF(), color.greenF(), color.blueF())
            return (bg_color, fg_color)
        else:
            return (self._config.custom_bg_color, self._config.custom_fg_color)
