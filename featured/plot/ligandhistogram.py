# -*- coding: utf-8 -*-
# Copyright (C) 2023 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Provides the ligand histogram plot."""

from PySide2 import QtCore, QtWidgets, QtGui

import uuid
import os.path

from matplotlib.figure import Figure

from .figure_canvas import FigureCanvas
from .color_button import ColorButton

from cresset import flare


class LigandHistogramPlotWidget(QtWidgets.QWidget):
    """A widget which displays a histogram plot for a column in the ligand table."""

    config_changed = QtCore.Signal(QtWidgets.QWidget)
    """Emitted when the configuration is changed"""

    closed = QtCore.Signal(QtWidgets.QWidget)
    """Emitted when the plot is closed"""

    def __init__(self, parent):
        super().__init__(parent)

        self.uuid = uuid.uuid1()
        self._project = flare.main_window().project

        parent.installEventFilter(self)

        self._plot_widget = _LigandHistogramPlotWidget(self)
        self._configure_axis = LigandHistogramPlotConfigWidget(self)

        self.setPalette(QtWidgets.QDialog().palette())

        # Add the Matplotlib plot to this dialog
        self._vbl = QtWidgets.QVBoxLayout(self)
        self._vbl.setContentsMargins(0, 0, 0, 0)
        self._vbl.setSpacing(0)
        self._vbl.addWidget(self._plot_widget, 1)
        self._vbl.addWidget(self._configure_axis)

        flare.callbacks.main_window_project_changed.add(self._on_project_changed)
        self._plot_widget.plot_title_changed.connect(self.setWindowTitle)
        self._configure_axis.config_changed.connect(self._plot_widget.set_config)
        self._configure_axis.config_changed.connect(self._on_config_changed)

        self._plot_widget.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self._plot_widget.customContextMenuRequested.connect(self._on_plot_menu)

        # Set the inital config to use in the plot
        config = LigandHistogramPlotConfig()
        config.ligands = flare.main_window().selected_ligands
        # if there are less than 2 selected ligands, use all ligands
        if len(config.ligands) < 2:
            config.ligands = list(self._project.ligands)
        self._configure_axis.set_config(config)

        self._dir = os.path.expanduser("~")

    def shutdown(self):
        """Remove the callbacks when the dialog is closed."""
        flare.callbacks.main_window_project_changed.remove(self._on_project_changed)
        self._plot_widget.shutdown()
        self._configure_axis.shutdown()
        self.closed.emit(self)

    def _on_project_changed(self, project):
        """Close the dialog when the project is changed."""
        if project != self._project:
            self.hide()

    def _on_config_changed(self, config):
        """Emits config_changed."""
        self._configure_axis.setVisible(config.config_widget_visible)
        self.config_changed.emit(self)

    @property
    def config(self):
        """Save the plot configuration to the project."""
        return self._configure_axis.config()

    @config.setter
    def config(self, config):
        """Save the plot configuration to the project."""
        self._configure_axis.set_config(config)

    def _on_plot_menu(self, pos):
        """Show the context menu."""
        menu = QtWidgets.QMenu(self)

        save = QtWidgets.QAction("Save Image")
        save.triggered.connect(self._save_image)
        menu.addAction(save)

        show_config = QtWidgets.QAction("Show Config")
        show_config.setCheckable(True)
        show_config.setChecked(self.config.config_widget_visible)
        show_config.triggered.connect(self._set_config_widget_visible)
        menu.addAction(show_config)

        menu.exec_(self.mapToGlobal(pos))

    def _save_image(self):
        """Save the plot as an image."""
        path, _filter = QtWidgets.QFileDialog.getSaveFileName(
            self, "Save Image", self._dir, "*.png"
        )
        if path:
            info = QtCore.QFileInfo(path)
            self._dir = info.dir().absolutePath()
            self._plot_widget._fig.savefig(path)

    def _set_config_widget_visible(self, checked):
        """Hide or show the config widget."""
        conf = self.config
        conf.config_widget_visible = checked
        self.config = conf

    # override QObject.eventFilter
    def eventFilter(self, watched, event):
        if event.type() == QtCore.QEvent.Close:
            self.shutdown()
        return super().eventFilter(watched, event)


class LigandHistogramPlotConfig:
    """The plot configuration"""

    def __init__(self):
        self.ligands = []
        self.column = None
        self.num_buckets = 10
        self.config_widget_visible = True
        self.use_theme_color = True
        self.custom_bg_color = "#FFFFFF"
        self.custom_fg_color = "#000000"
        self.selected_color = "#ff7f0e"
        self.non_selected_color = "#2ca02c"

    def to_dict(self):
        """Stores the data to a python dict."""
        dict = {}
        dict["Ligands"] = list(self.ligands)
        dict["Column"] = self.column
        dict["Buckets"] = self.num_buckets
        dict["Config Widget Visible"] = self.config_widget_visible
        dict["Use Theme Color"] = self.use_theme_color
        dict["Custom BG Color"] = self.custom_bg_color
        dict["Custom FG Color"] = self.custom_fg_color
        dict["Selected Color"] = self.selected_color
        dict["Non Selected Color"] = self.non_selected_color
        return dict

    def from_dict(self, dict):
        """Restores the data from a python dict."""
        self.ligands = dict.get("Ligands", self.ligands)
        self.column = dict.get("Column", self.column)
        self.num_buckets = dict.get("Buckets", self.num_buckets)
        self.config_widget_visible = dict.get("Config Widget Visible", self.config_widget_visible)
        self.use_theme_color = dict.get("Use Theme Color", self.use_theme_color)
        self.custom_bg_color = dict.get("Custom BG Color", self.custom_bg_color)
        self.custom_fg_color = dict.get("Custom FG Color", self.custom_fg_color)
        self.selected_color = dict.get("Selected Color", self.selected_color)
        self.non_selected_color = dict.get("Non Selected Color", self.non_selected_color)


class LigandHistogramPlotConfigWidget(QtWidgets.QWidget):
    """Widget to configure the plot.

    When the configuration is changed `config_changed` is emitted with the new config.
    """

    config_changed = QtCore.Signal(LigandHistogramPlotConfig)
    """Emitted when the configuration is changed"""

    def __init__(self, parent=None):
        super().__init__(parent)

        self._ligands = []
        self._config_widget_visible = None

        self._column = QtWidgets.QComboBox(parent)
        self._column.setToolTip("Choose which column from the ligand table should be plotted.")
        self._column.currentIndexChanged.connect(self._on_setting_changed)

        self._num_buckets = QtWidgets.QSpinBox(parent)
        self._num_buckets.setToolTip("Specify how many buckets to use in the histogram.")
        self._num_buckets.setMinimum(1)
        self._num_buckets.setMaximum(100)
        self._num_buckets.setSingleStep(1)
        self._num_buckets.setValue(10)
        self._num_buckets.valueChanged.connect(self._on_setting_changed)

        self._show_all_ligands = QtWidgets.QPushButton("Plot All Ligands", self)
        self._show_all_ligands.setToolTip("Redraw the plot so it contains all ligands.")
        self._show_all_ligands.clicked.connect(self._set_all_ligands)

        self._show_selected_ligands = QtWidgets.QPushButton("Plot Selected Ligands", self)
        self._show_selected_ligands.setToolTip(
            "Redraw the plot so it only contains the selected ligands"
        )
        self._show_selected_ligands.clicked.connect(self._set_selected_ligands)

        self._use_theme_color_checkbox = QtWidgets.QCheckBox("Use theme color")
        self._use_theme_color_checkbox.setToolTip(
            "If checked, the background and foreground colors "
            + "are obtained from the main Flare display theme."
        )
        self._use_theme_color_checkbox.toggled.connect(self._on_use_theme_color_checkbox_changed)

        self._bg_color_button = ColorButton(parent)
        self._fg_color_button = ColorButton(parent)

        self._bg_color_button.color_changed.connect(
            lambda _color: self.config_changed.emit(self.config())
        )
        self._fg_color_button.color_changed.connect(
            lambda _color: self.config_changed.emit(self.config())
        )

        self._custom_bgfgcolor_frame = QtWidgets.QFrame()
        self._custom_bgfgcolor_frame.setLineWidth(0)
        self._custom_bgfgcolor_frame.setFrameStyle(QtWidgets.QFrame.NoFrame)

        bg_label = QtWidgets.QLabel("Background:")
        fg_label = QtWidgets.QLabel("Foreground:")
        bg_label.setToolTip("Set background color of the histogram.")
        self._bg_color_button.setToolTip(bg_label.toolTip())
        fg_label.setToolTip("Set foreground/text color of the histogram.")
        self._fg_color_button.setToolTip(fg_label.toolTip())

        bgfgcolor_layout = QtWidgets.QHBoxLayout(self._custom_bgfgcolor_frame)
        bgfgcolor_layout.setContentsMargins(0, 0, 0, 0)
        bgfgcolor_layout.addWidget(bg_label)
        bgfgcolor_layout.addWidget(self._bg_color_button)
        bgfgcolor_layout.addWidget(fg_label)
        bgfgcolor_layout.addWidget(self._fg_color_button)

        self._selected_color_button = ColorButton(parent)
        self._non_selected_color_button = ColorButton(parent)

        self._selected_color_button.color_changed.connect(
            lambda _color: self.config_changed.emit(self.config())
        )
        self._non_selected_color_button.color_changed.connect(
            lambda _color: self.config_changed.emit(self.config())
        )

        sel_label = QtWidgets.QLabel("Selected:")
        sel_label.setToolTip("Set the color of the selected bar.")
        self._selected_color_button.setToolTip(sel_label.toolTip())
        nonsel_label = QtWidgets.QLabel("Non-selected:")
        nonsel_label.setToolTip("Set the color of the non-selected bars.")
        self._non_selected_color_button.setToolTip(nonsel_label.toolTip())

        selcolor_frame = QtWidgets.QFrame()
        selcolor_frame.setLineWidth(0)
        selcolor_frame.setFrameStyle(QtWidgets.QFrame.NoFrame)
        selcolor_layout = QtWidgets.QHBoxLayout(selcolor_frame)
        selcolor_layout.setContentsMargins(0, 0, 0, 0)
        selcolor_layout.addWidget(sel_label)
        selcolor_layout.addWidget(self._selected_color_button)

        nonselcolor_frame = QtWidgets.QFrame()
        nonselcolor_frame.setLineWidth(0)
        nonselcolor_frame.setFrameStyle(QtWidgets.QFrame.NoFrame)
        nonselcolor_layout = QtWidgets.QHBoxLayout(nonselcolor_frame)
        nonselcolor_layout.setContentsMargins(0, 0, 0, 0)
        nonselcolor_layout.addWidget(nonsel_label)
        nonselcolor_layout.addWidget(self._non_selected_color_button)

        self._general_settings_layout = QtWidgets.QGridLayout()
        self._general_settings_layout.setContentsMargins(0, 0, 0, 0)
        self._general_settings_layout.addWidget(self._show_all_ligands, 0, 0)
        self._general_settings_layout.addWidget(QtWidgets.QLabel("Column: ", self), 0, 1)
        self._general_settings_layout.addWidget(self._column, 0, 2)
        self._general_settings_layout.addWidget(QtWidgets.QLabel("Buckets: ", self), 0, 3)
        self._general_settings_layout.addWidget(self._num_buckets, 0, 4)
        self._general_settings_layout.addWidget(self._show_selected_ligands, 1, 0)
        self._general_settings_layout.addWidget(self._use_theme_color_checkbox, 1, 1)
        self._general_settings_layout.addWidget(self._custom_bgfgcolor_frame, 1, 2)
        self._general_settings_layout.addWidget(selcolor_frame, 1, 3)
        self._general_settings_layout.addWidget(nonselcolor_frame, 1, 4)

        self._update_column_names()

        self._vbl = QtWidgets.QVBoxLayout(self)
        self._vbl.setContentsMargins(11, 0, 0, 2)
        self._vbl.addLayout(self._general_settings_layout)
        self._vbl.addStretch(1)

        flare.callbacks.ligand_table_columns_changed.add(self._update_column_names)

    def _on_use_theme_color_checkbox_changed(self, checked):
        self._custom_bgfgcolor_frame.setHidden(checked)
        self.config_changed.emit(self.config())

    def shutdown(self):
        """Remove the callbacks when the dialog is closed."""
        flare.callbacks.ligand_table_columns_changed.remove(self._update_column_names)

    def set_config(self, config):
        """Updates the widgets to show the values in the config."""
        self._ligands = config.ligands
        self._config_widget_visible = config.config_widget_visible

        column_index = 0
        if config.column is not None:
            column_index = self._column.findText(config.column)
        if column_index < 0:
            column_index = 0
        self._column.setCurrentIndex(column_index)

        self._num_buckets.setValue(config.num_buckets)
        self._use_theme_color_checkbox.setChecked(config.use_theme_color)
        self._bg_color_button.set_color(config.custom_bg_color)
        self._fg_color_button.set_color(config.custom_fg_color)
        self._selected_color_button.set_color(config.selected_color)
        self._non_selected_color_button.set_color(config.non_selected_color)

        self.config_changed.emit(config)

    def config(self):
        """Return the plot configuration."""
        config = LigandHistogramPlotConfig()
        config.ligands = self._ligands
        if self._column.currentIndex() <= 0:
            config.column = None
        else:
            config.column = self._column.currentText()
        config.num_buckets = self._num_buckets.value()
        config.config_widget_visible = self._config_widget_visible
        config.use_theme_color = self._use_theme_color_checkbox.isChecked()
        config.custom_bg_color = self._bg_color_button.color()
        config.custom_fg_color = self._fg_color_button.color()
        config.selected_color = self._selected_color_button.color()
        config.non_selected_color = self._non_selected_color_button.color()
        return config

    def _update_column_names(self):
        """Update the values in the columns combo boxes"""
        names = self._number_columns()

        current = self._column.currentText()
        self._column.clear()
        self._column.addItems(names)
        index = self._column.findText(current)
        index = max(0, index)
        self._column.setCurrentIndex(index)

    def _number_columns(self):
        """Return a list of columns in the project which contain numerical data."""
        project = flare.main_window().project
        number_columns = []

        columns = project.ligands.columns.keys()
        columns.remove("Index")

        for column in columns:
            valid_count = 0
            total_count = 0

            for ligand in project.ligands:
                value = ligand.properties[column].value
                if value is not None and value != "":
                    total_count += 1
                    if isinstance(value, (int, float)) and not isinstance(value, bool):
                        valid_count += 1
                    # Limit the number of molecules to search for performance reasons
                    if total_count > 20:
                        break

            # Class the column as numerical if 20% of its non blank values are a number.
            if total_count > 0 and (valid_count / total_count) > 0.2:
                number_columns.append(column)

        number_columns.insert(0, "<None>")
        return number_columns

    def _on_setting_changed(self):
        """Emits `config_changed` with the current configuration."""
        self.config_changed.emit(self.config())

    def _set_selected_ligands(self):
        """Set the ligands to show in the plot."""
        self._ligands = flare.main_window().selected_ligands
        self._on_setting_changed()

    def _set_all_ligands(self):
        """Set all the ligands to show in the plot."""
        self._ligands = list(flare.main_window().project.ligands)
        self._on_setting_changed()


class _LigandHistogramPlotWidget(QtWidgets.QWidget):
    """Widget showing the plot."""

    plot_title_changed = QtCore.Signal(str)
    """Emitted when the plot title may have changed."""

    def __init__(self, parent=None):
        super().__init__(parent)

        # matplotlib raises exceptions if it gets too small
        self.setMinimumSize(100, 100)

        # Create the matplotlib histogram plot
        self._fig = Figure()
        self._canvas = FigureCanvas(self._fig)
        self._axis = self._fig.subplots(1, 1)
        self._buckets_edges = None

        # Connect the pick event so when a point in the plot is clicked _on_pick is called
        # Note "pick_event" does not work due to the twinx() function being called
        self._canvas.mpl_connect("button_release_event", self._on_pick)

        # Add the Matplotlib plot to this dialog
        self._vbl = QtWidgets.QVBoxLayout(self)
        self._vbl.setContentsMargins(0, 0, 0, 0)
        self._vbl.addWidget(self._canvas)

        # Set the default config
        self._config = LigandHistogramPlotConfig()

        # Timer which rate limits how often the plot is updated
        # when ligands are being changed
        self._update_plot_timer = QtCore.QTimer(self)
        self._update_plot_timer.setSingleShot(True)
        self._update_plot_timer.setInterval(1000)
        self._update_plot_timer.timeout.connect(self._update_plot)

        # Add the data to the plot
        self._update_plot()

        # Add callbacks which update the plot when the protein or atom pick is changed
        flare.callbacks.selected_ligands_changed.add(self._on_selected_ligands_changed)
        flare.callbacks.ligands_removed.add(self._on_ligands_removed)
        flare.callbacks.ligand_property_changed.add(self._on_ligand_property_changed)

    def shutdown(self):
        """Remove the callbacks when the dialog is closed."""
        flare.callbacks.selected_ligands_changed.remove(self._on_selected_ligands_changed)
        flare.callbacks.ligands_removed.remove(self._on_ligands_removed)
        flare.callbacks.ligand_property_changed.remove(self._on_ligand_property_changed)

    def set_config(self, config):
        """Redraws the plot for the given configuration."""
        self._config = config
        self._update_plot()

    # override QWidget.sizeHint
    def sizeHint(self):
        """Return the default size of the plot."""
        return QtCore.QSize(500, 500)

    # override QWidget.resizeEvent
    def resizeEvent(self, event):
        """Redraws the plot."""
        self._fig.tight_layout()
        self._canvas.draw()
        super().resizeEvent(event)

    def _update_plot(self):
        """Update the data displayed in the plot."""

        selected_ligands = set(flare.main_window().selected_ligands)

        # Get the data out of the ligand table
        not_selected = []
        selected = []

        for ligand in self._config.ligands:
            value = self._ligand_column_value(ligand, self._config.column)

            if value is not None:
                if ligand in selected_ligands:
                    selected.append(value)
                else:
                    not_selected.append(value)

        config = self._config
        bg_color = config.custom_bg_color
        fg_color = config.custom_fg_color
        if config.use_theme_color:
            palette = QtGui.QPalette()
            bg_color = palette.color(QtGui.QPalette.Window).name(QtGui.QColor.HexRgb)
            fg_color = palette.color(QtGui.QPalette.WindowText).name(QtGui.QColor.HexRgb)
        self._fig.patch.set_facecolor(bg_color)
        self._axis.set_facecolor(bg_color)
        self._axis.spines["bottom"].set_color(fg_color)
        self._axis.spines["top"].set_color(fg_color)
        self._axis.spines["left"].set_color(fg_color)
        self._axis.spines["right"].set_color(fg_color)
        self._axis.tick_params(axis="x", colors=fg_color)
        self._axis.tick_params(axis="y", colors=fg_color)
        self._axis.xaxis.label.set_color(fg_color)
        self._axis.yaxis.label.set_color(fg_color)

        self._axis.cla()
        _, self._buckets_edges, _ = self._axis.hist(
            [selected, not_selected],
            bins=self._config.num_buckets,
            align="mid",
            color=[config.selected_color, config.non_selected_color],
            histtype="barstacked",
            stacked=True,
        )
        self._axis.set_xlabel(self._config.column)

        if self._config.column:
            self.plot_title_changed.emit(self._config.column)
        else:
            self.plot_title_changed.emit("Histogram")

        self._fig.tight_layout()
        self._canvas.draw()

    def _ligand_column_value(self, ligand, column):
        """Returns the value in the column for the ligand

        Returns None if the value in the column is not a number.
        """
        if column is None:
            return None
        try:
            return float(ligand.properties[column].value)
        except (ValueError, TypeError):
            pass
        return None

    def _on_selected_ligands_changed(self, ligands):
        """Colors the bars on the plot differently for selected and unselected ligands."""
        self._update_plot()

    def _on_ligands_removed(self, ligands):
        """Remove the ligands from the plot."""
        for ligand in ligands:
            if ligand in self._config.ligands:
                self._config.ligands.remove(ligand)

        self._update_plot()

    def _on_ligand_property_changed(self, ligand, property_name, property_value):
        """Schedules an update of the plot."""
        self._update_plot_timer.start()

    def _on_pick(self, event):
        """Select the items associated with the point clicked."""
        if event.xdata is None:
            return

        # Find which bucket was clicked
        bucket = None
        for i in range(self._config.num_buckets):
            if self._buckets_edges[i] <= event.xdata < self._buckets_edges[i + 1]:
                bucket = i
                break

        # Find the ligands in the bucket
        if bucket is not None:
            ligands = []
            for ligand in self._config.ligands:
                value = self._ligand_column_value(ligand, self._config.column)

                # When a value matches the edge between buckets the value falls into the
                # bucket on the right. Except for edge after the last bucket where it falls
                # into the bucket on the left.
                if value is not None and (
                    (self._buckets_edges[bucket] <= value < self._buckets_edges[bucket + 1])
                    or (
                        bucket + 1 == self._config.num_buckets
                        and self._buckets_edges[bucket] <= value
                    )
                ):
                    ligands.append(ligand)
            flare.main_window().selected_ligands = ligands
