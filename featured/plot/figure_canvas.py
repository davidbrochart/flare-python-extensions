# -*- coding: utf-8 -*-
# Copyright (C) 2023 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg


class FigureCanvas(FigureCanvasQTAgg):
    def showEvent(self, event):
        try:
            FigureCanvasQTAgg.showEvent(self, event)
        except RuntimeError as e:
            # This error happens on docking widget, just ignore it
            if str(e) != "Internal C++ object (PySide2.QtGui.QWindow) already deleted.":
                raise e
