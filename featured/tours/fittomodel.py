# -*- coding: utf-8 -*-
# Copyright (C) 2023 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
from cresset import flare


class FitToModelTour:
    """A tour on how to fit ligands to a QSAR model."""

    def __init__(self):
        self._fit_to_model()

    def _fit_to_model(self):
        """Start the tour"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("File")
            tour_widget.text = (
                "<b>Fit molecules to a QSAR model</b><br>&nbsp;<br>"
                + "Predict activity or novelty for new compounds using an existing "
                + "quantitative or qualitative SAR model.<br>&nbsp;<br>"
                + "You must have a Flare project with at least one pre-calculated "
                + "QSAR model to use this tour.<br>"
                + "If the model was built using Cresset 3D descriptors/similarity "
                + "the project must also contain the reference molecule(s) used to "
                + "align the training set compounds.<br>&nbsp;<br>"
                + '<a href="link_project_ligandsloaded">'
                + "The Flare project is open, and the ligands to score are already in the project"
                + "</a><br></a><br>"
                + '<a href="link_project_is_open">'
                + "The Flare project is open, and you want to load ligands to score"
                + "</a><br></a><br>"
                + '<a href="link_project_not_open">'
                + "Open a Flare project with a calculated QSAR model, and then import the ligands "
                + "to score.</a><br></a><br>"
            )

            tour_widget.add_link_callback("link_project_is_open", self._fit_to_model_projectopen)
            tour_widget.add_link_callback(
                "link_project_not_open", self._fit_to_model_projectnotopen
            )
            tour_widget.add_link_callback(
                "link_project_ligandsloaded", self._fit_to_model_ligandsloaded
            )
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    # Option 1: Project is open, ligands to predict are already in the project

    def _fit_to_model_ligandsloaded(self):
        """Ask the user whether they are using a 3D QSAR model"""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = flare.tour.find_widget("QSAR")
        tour_widget.text = (
            "Are you making prediction for a 3D or 2D QSAR model?<br>&nbsp;<br>"
            + '<a href="ligandsloaded3D">'
            + "My model(s) were built using Cresset 3D descriptors/similarity"
            + "</a><br></a><br>"
            + '<a href="ligandsloaded2D">'
            + "My model(s) were built using 2D similarity/imported descriptors"
            + "</a><br></a><br>"
        )
        tour_widget.add_link_callback("ligandsloaded3D", self._ligandsloaded3D)
        tour_widget.add_link_callback("ligandsloaded2D", self._ligandsloaded2D)

        # Model was built using 2D fingerprints or custom descriptors

    def _ligandsloaded2D(self):
        """Tell the user to find the ligands for which they want to make predictions."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("Ligands")
            tour_widget.text = (
                "Find the ligands you want to predict. <br>"
                + "Scroll towards the right of the Ligands table to find the "
                + "columns with the QSAR model predictions.<br>&nbsp;<br>"
            )
            tour_widget.add_next_callback(self._ligandsloaded2D_done)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _ligandsloaded2D_done(self):
        """Tell the user to check the predictions in the Ligands table."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("Ligands")
            tour_widget.text = (
                "Predictions for each 2D QSAR model in the project are made"
                + " as soon as the ligands to predict are added "
                + "to the Ligands table.<br>&nbsp;<br>"
                + "If predictions are missing for one or more of your ligands, "
                + "check whether there are any missing values "
                + "in the descriptors used by the model. <br>&nbsp;<br>"
                + "<b>This tour is now complete: "
                + "press Stop to close this widget.</b>"
            )
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

        # Model was built using 3D descriptors

    def _ligandsloaded3D(self):
        """Tell the user to select the ligands for which they want to make predictions."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("Ligands")
            tour_widget.text = "Select all the the ligands you want to predict."
            tour_widget.add_next_callback(self._fit_to_model_ligandsloaded_ligandtab)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _fit_to_model_ligandsloaded_ligandtab(self):
        """Tell the user to press the Conf Hunt and Align button."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("Conf Hunt and Align")
            tour_widget.text = "Press the <b>Conf Hunt and Align</b> button.<br>"
            tour_widget.add_named_widget_shown_callback(
                ["Dialog", "Conf Hunt and Align"], self._fit_to_model_confhuntalign_step1
            )
        except ValueError:
            tour_widget.text = "Go to the <b>3D Pose tab</b>. "
            tour_widget.add_named_widget_shown_callback(
                ["3D Pose", "Conf Hunt and Align"], self._fit_to_model_confhuntalign
            )
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    # Option 2: Project is open, but ligands must be loaded

    def _fit_to_model_projectopen(self):
        """Tell the user to open the file menu."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("File")
            tour_widget.text = "Press the <b>File</b> button to load the ligands you want to score."
            tour_widget.add_named_widget_shown_callback(
                "Open File", self._fit_to_model_projectopen_open_file_menu
            )
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _fit_to_model_projectopen_open_file_menu(self, widget):
        """Tell the user to press Open File in the File menu."""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = widget
        tour_widget.text = "Press <b>Open File</b>."
        tour_widget.add_named_widget_shown_callback(
            "Choose a file", self._fit_to_model_projectopen_choose_file
        )

    def _fit_to_model_projectopen_choose_file(self, widget):
        """Tell the user to choose a file."""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = widget
        tour_widget.text = "Browse to find the ligands file and press <b>Open</b>."

        tour_widget.add_named_widget_shown_callback(
            "Open Molecules", self._fit_to_model_projectopen_ligands_open_file_options
        )
        tour_widget.add_named_widget_shown_callback(
            "CSV Import", self._fit_to_model_projectopen_ligands_open_csv
        )

    def _fit_to_model_projectopen_ligands_open_file_options(self, widget):
        """Tell the user to choose the import options for sdf/sd, mol, mol2, smi, pdd, xed files"""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = widget
        tour_widget.text = (
            "Choose the input options for the ligands being imported.<br>&nbsp;<br>"
            + "1. The Molecule type should be 'Autodetect' or 'Ligands'.<br>&nbsp;<br>"
            + "2. Choose a role for the ligands, "
            + "<b>making sure they don't end up in the same role "
            + " as the Reference</b>.<br>"
            + "If you keep 'Autodetect', the ligands will be imported "
            + " in a role named after the file you are importing.<br>&nbsp;<br>"
            + "3. <b>Choose the Protonation state:</b><br>"
            + "Choose <b>Use input protonation state</b>"
            + " if you know that your ligands are in the correct protonation state.<br>"
            + "Choose <b>Apply rule-based protonation</b> "
            + "to let Flare define the protonation state.<br>&nbsp;<br>"
            + "4. The 'Ligand file read mode option' chooses how the input files are handled. "
            + "'Autodetect' works well in most cases.<br>&nbsp;<br>"
            + "Press <b>Open</b> to import your ligands."
        )

        tour_widget.add_named_widget_shown_callback(
            "Ligands", self._fit_to_model_select_ligands_toalign
        )

    def _fit_to_model_projectopen_ligands_open_csv(self, widget):
        """Tell the user to choose csv import options."""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = widget
        tour_widget.text = (
            "Choose the input options for the ligands being imported.<br>&nbsp;<br>"
            + "1. Check the <b>Preview</b> panel to ensure that "
            + "the <b>Column Delimiter</b> is set correctly.<br>&nbsp;<br>"
            + "2. Choose a <b>Ligand Role</b> for the ligands, "
            + "<b>making sure they don't end up in the same role"
            + " as the Reference</b>.<br>&nbsp;<br>"
            + "3. <b>Choose the Protonation state:</b><br>"
            + "Choose <b>Use input protonation state</b> "
            + " if you know that your ligands are in the correct protonation state.<br>"
            + "Choose <b>Apply rule-based protonation</b> "
            + "to let Flare define the protonation state.<br>&nbsp;<br>"
            + "4. Choose the columns in the CSV file which map the <b>Structure</b>,"
            + " and the <b>Ligand Name</b>.<br>&nbsp;<br>"
            + "Press <b>Open</b> to import your ligands."
        )

        tour_widget.add_named_widget_shown_callback(
            "Ligands", self._fit_to_model_select_ligands_toalign
        )

    # Option 3: project must be opened, ligands must be loaded

    def _fit_to_model_projectnotopen(self):
        """Tell the user to open the file menu."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("File")
            tour_widget.text = (
                "Press the <b>File</b> button to import the Flare project "
                + "with the QSAR model.<br>&nbsp;<br>"
            )
            tour_widget.add_named_widget_shown_callback(
                "Open File", self._fit_to_model_projectnotopen_open_file_tab
            )
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _fit_to_model_projectnotopen_open_file_tab(self, widget):
        """Tell the user to load the Flare project with the QSAR model"""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = widget
        tour_widget.text = "Press <b>Open File</b> to load the project with the QSAR model."
        tour_widget.add_named_widget_shown_callback(
            "Choose a file", self._fit_to_model_projectnotopen_choose_project
        )

    def _fit_to_model_projectnotopen_choose_project(self, widget):
        """Tell the user to choose the project file(s)."""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = widget
        tour_widget.text = "Select the project file and press <b>Open</b>."
        tour_widget.add_named_widget_shown_callback(
            "File", self._fit_to_model_projectnotopen_reopen_file_button
        )

    def _fit_to_model_projectnotopen_reopen_file_button(self, widget):
        """Tell the user to reopen the File menu."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("File")
            tour_widget.text = (
                "Press the <b>File</b> button again to import the ligands to predict."
            )
            tour_widget.add_named_widget_shown_callback(
                "Open File", self._fit_to_model_projectnotopen_reopen_file_menu
            )
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _fit_to_model_projectnotopen_reopen_file_menu(self, widget):
        """Tell the user to press Open File in the File menu."""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = widget
        tour_widget.text = "Press <b>Open File</b> to load the ligands you want to score."
        tour_widget.add_named_widget_shown_callback(
            "Choose a file", self._fit_to_model_projectnotopen_choose_file
        )

    def _fit_to_model_projectnotopen_choose_file(self, widget):
        """Tell the user to choose a file."""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = widget
        tour_widget.text = "Browse to find the ligands file and press <b>Open</b>."

        tour_widget.add_named_widget_shown_callback(
            "Open Molecules", self._fit_to_model_projectnotopen_ligands_open_file_options
        )
        tour_widget.add_named_widget_shown_callback(
            "CSV Import", self._fit_to_model_projectnotopen_ligands_open_csv
        )

    def _fit_to_model_projectnotopen_ligands_open_file_options(self, widget):
        """Tell the user to choose the import options for sdf/sd, mol, mol2, smi, pdd, xed files"""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = widget
        tour_widget.text = (
            "Choose the input options for the ligands being imported.<br>&nbsp;<br>"
            + "1. The Molecule type should be 'Autodetect' or 'Ligands'.<br>&nbsp;<br>"
            + "2. Choose a role for the ligands, "
            + "<b>making sure they don't end up in the same role "
            + " as the Reference</b>.<br>"
            + "If you keep 'Autodetect', the ligands will be imported "
            + " in a role named after the file you are importing.<br>&nbsp;<br>"
            + "3. <b>Choose the Protonation state:</b><br>"
            + "Choose <b>Use input protonation state</b>"
            + " if you know that your ligands are in the correct protonation state.<br>"
            + "Choose <b>Apply rule-based protonation</b> "
            + "to let Flare define the protonation state.<br>&nbsp;<br>"
            + "4. The 'Ligand file read mode option' chooses how the input files are handled. "
            + "'Autodetect' works well in most cases.<br>&nbsp;<br>"
            + "Press <b>Open</b> to import your ligands."
        )

        tour_widget.add_named_widget_shown_callback(
            "Ligands", self._fit_to_model_select_ligands_toalign
        )

    def _fit_to_model_projectnotopen_ligands_open_csv(self, widget):
        """Tell the user to choose csv import options."""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = widget
        tour_widget.text = (
            "Choose the input options for the ligands being imported.<br>&nbsp;<br>"
            + "1. Check the <b>Preview</b> panel to ensure that "
            + "the <b>Column Delimiter</b> is set correctly.<br>&nbsp;<br>"
            + "2. Choose a <b>Ligand Role</b> for the ligands, "
            + "<b>making sure they don't end up in the same role"
            + " as the Reference</b>.<br>&nbsp;<br>"
            + "3. <b>Choose the Protonation state:</b><br>"
            + "Choose <b>Use input protonation state</b> "
            + " if you know that your ligands are in the correct protonation state.<br>"
            + "Choose <b>Apply rule-based protonation</b> "
            + "to let Flare define the protonation state.<br>&nbsp;<br>"
            + "4. Choose the columns in the CSV file which map the <b>Structure</b>,"
            + " and the <b>Ligand Name</b>.<br>&nbsp;<br>"
            + "Press <b>Open</b> to import your ligands."
        )

        tour_widget.add_named_widget_shown_callback(
            "Ligands", self._fit_to_model_select_ligands_toalign
        )

    def _fit_to_model_select_ligands_toalign(self, widget):
        """Tell the user to select the ligands to align."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("Ligands")
            tour_widget.text = (
                "Load additional ligands to predict by pressing File/Open File again.<br>&nbsp;<br>"
                + "When you are done, select all the the ligands you want to predict."
            )
            tour_widget.add_next_callback(self._fit_to_model_ligandtab)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    # Check whether the model is 2D or 3D
    def _fit_to_model_ligandtab(self):
        """Ask the user whether they are using a 3D QSAR model"""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = flare.tour.find_widget("QSAR")
        tour_widget.text = (
            "Are you making prediction for a 3D or 2D QSAR model?<br>&nbsp;<br>"
            + '<a href="model3D">'
            + "My model(s) were built using Cresset 3D descriptors/similarity"
            + "</a><br></a><br>"
            + '<a href="model2D">'
            + "My model(s) were built using 2D similarity/imported descriptors"
            + "</a><br></a><br>"
        )
        tour_widget.add_link_callback("model3D", self._model3D)
        tour_widget.add_link_callback("model2D", self._model2D)

    # Model was built using 2D fingerprints or custom descriptors

    def _model2D(self):
        """Tell the user to find the ligands for which they want to make predictions."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("Ligands")
            tour_widget.text = (
                "Find the ligands you want to predict. <br>"
                + "Scroll towards the right of the Ligands table to find the "
                + "columns with the QSAR model predictions.<br>&nbsp;<br>"
            )
            tour_widget.add_next_callback(self._model2D_done)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _model2D_done(self):
        """Tell the user to check the predictions in the Ligands table."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("Ligands")
            tour_widget.text = (
                "Predictions for each 2D QSAR model in the project are made"
                + " as soon as the ligands to predict are added "
                + "to the Ligands table.<br>&nbsp;<br>"
                + "If predictions are missing for one or more of your ligands, "
                + "check whether there are any missing values "
                + "in the descriptors used by the model. <br>&nbsp;<br>"
                + "<b>This tour is now complete: "
                + "press Stop to close this widget.</b>"
            )
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

        # Model was built using 3D descriptors

    # Do the conformation hunt and alignment

    def _model3D(self):
        """Tell the user to press the Conf Hunt and Align button."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("Conf Hunt and Align")
            tour_widget.text = "Press the <b>Conf Hunt and Align</b> button.<br>"
            tour_widget.add_named_widget_shown_callback(
                ["Dialog", "Conf Hunt and Align"], self._fit_to_model_confhuntalign_step1
            )
        except ValueError:
            tour_widget.text = "Go to the <b>3D Pose tab</b>. "
            tour_widget.add_named_widget_shown_callback(
                ["3D Pose", "Conf Hunt and Align"], self._fit_to_model_confhuntalign
            )
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _fit_to_model_confhuntalign(self, widget):
        """Tell the user to press the Conf Hunt and Align button."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(["3D Pose", "Conf Hunt and Align"])
            tour_widget.text = (
                "To score your ligands against a QSAR model built with Cresset "
                + "3D descriptors/similarity, they must be aligned.<br>"
                + "The alignment should be performed with the same "
                + "settings used to align the training set"
                + " compounds used to develop the model.<br>&nbsp;<br>"
                "Press the <b>Conf Hunt and Align</b> button to start the ligand alignment. <br>"
            )
            tour_widget.add_named_widget_shown_callback(
                ["Dialog", "Conf Hunt and Align", "All Ligands"],
                self._fit_to_model_confhuntalign_step1,
            )
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _fit_to_model_confhuntalign_step1(self, widget):
        """Tell the user to choose the conf hunt method."""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = widget
        tour_widget.text = (
            "<b>Set-up the conformation hunt and alignment for your ligands.</b><br>&nbsp;<br>"
            + "<b>Note:</b> it is important that you choose the same "
            + "conformation hunt and alignment"
            + " options applied for aligning the training set used to "
            + "develop the QSAR model.<br>&nbsp;<br>"
            + "<b>Step 1.</b> Choose the <b>Conformation Hunt</b> method from:<br>&nbsp;<br>"
            + "<b>No calculation</b>: don't perform the conf hunt<br>&nbsp;<br>"
            + "<b>Quick</b>: fast, only max 50 conformations kept<br>&nbsp;<br>"
            + "<b>Normal</b>: max 100 conformations: good for most molecules.<br>&nbsp;<br>"
            + "<b>Accurate but slow</b>: max 200 low energy conformations, "
            + "better for larger molecules."
            + " Significant memory usage<br>&nbsp;<br>"
            + "<b>Very Accurate but slow</b>: max 1000 low energy conformations. "
            + "The best for very large molecules, but significant time and memory usage."
        )
        tour_widget.add_next_callback(self._fit_to_model_confhuntalign_step2)

    def _fit_to_model_confhuntalign_step2(self):
        """Tell the user to choose the alignment method."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(
                ["Dialog", "Conf Hunt and Align", "All Ligands"]
            )
            tour_widget.text = (
                "<b>Step 2.</b> Choose the <b>Alignment</b> method from:</b><br>&nbsp;<br>"
                + "<b>No calculation</b>: don't calculate alignments<br>&nbsp;<br>"
                + "<b>Quick</b>: fast, but some alignments may be missed<br>&nbsp;<br>"
                + "<b>Normal</b>: recommended for normal use. "
                + "Alignment based on electrostatic and shape<br>&nbsp;<br>"
                + "<b>Substructure</b>: dominated by common substructure "
                + "to reference.<br>&nbsp;<br>"
                + "<b>Score Only</b>: only appears when molecules have "
                + "conformations and at least one alignment. "
                + "Similarity for the first alignment will be recalculated "
                + "without moving the molecules."
            )
            tour_widget.add_next_callback(self._fit_to_model_confhuntalign_step3)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _fit_to_model_confhuntalign_step3(self):
        """Tell the user to check that the reference role is set correctly"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(["Dialog", "Conf Hunt and Align"])
            tour_widget.text = (
                "<b>Step 3.</b> Check that the <b>Role Containing References</b> is set correctly."
            )
            tour_widget.add_next_callback(self._fit_to_model_confhuntalign_step4)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _fit_to_model_confhuntalign_step4(self):
        """Tell the user to choose an optional protein"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(["Dialog", "Conf Hunt and Align"])
            tour_widget.text = (
                "<b>Step 4:</b> Choose an optional protein and protein chains "
                + "to use an excluded volume.<br>"
                + "Alignments which clash with the protein will be penalized.<br>"
            )
            tour_widget.add_next_callback(self._fit_to_model_confhuntalign_step5)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _fit_to_model_confhuntalign_step5(self):
        """Tell the users to start the alignment"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(["Dialog", "Conf Hunt and Align", "Start"])
            tour_widget.text = (
                "<b>Step 6.</b> Press the <b> Start</b> button to align your ligands, "
                + "then wait for the calculation to complete."
            )
            tour_widget.add_widget_hidden_callback(self._fit_to_model_confhuntalign_alignment_done)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _fit_to_model_confhuntalign_alignment_done(self):
        """Tell the user to check the predictions in the Ligands table."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("Ligands")
            tour_widget.text = (
                "Predictions for all new ligands and for each QSAR model in "
                + "the project are made at the end of the alignment run.<br>&nbsp;<br>"
                + "Scroll towards the right of the Ligands table to find the "
                + "columns with the QSAR model predictions.<br>&nbsp;<br>"
                + "<b>This tour is now complete: "
                + "press Stop to close this widget.</b>"
            )
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )
