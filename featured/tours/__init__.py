# -*- coding: utf-8 -*-
# Copyright (C) 2023 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Adds guided tours of Flare's functionality.

Ribbon Controls:
    Help -> Guided Tours -> Align Ligands
        Run a tour on how to align ligands.

    Help -> Guided Tours -> Dock Ligands
        Run a tour on how to dock ligands.

    Help -> Guided Tours -> Build QSAR
        Run a tour on how to build QSAR models.

    Help -> Guided Tours -> Build 2D QSAR
        Run a tour on how to build 2D QSAR models.

    Help -> Guided Tours -> Fit Ligands to a QSAR Model
        Run a tour on how to fit ligands to a QSAR model.

    Help -> Guided Tours -> Create Pharmacophore
        Run a tour on how to create a pharmacophore.
"""
import os
from cresset import flare

from .alignligands import AlignLigandsTour
from .buildqsar import BuildQSARTour
from .build2Dqsar import Build2DQSARTour
from .dockligands import DockLigandsTour
from .fittomodel import FitToModelTour
from .createpharmacophore import CreatePharmacophoreTour


@flare.extension
class GuidedToursExtension:
    """Add buttons to the ribbon run tours of Flare functionality."""

    def load(self):
        """Load the extension."""
        tab = flare.main_window().ribbon["Help"]

        group = tab["Guided Tours"]

        control = group.add_button("Align Ligands", AlignLigandsTour)
        control.tooltip = "Run a tour on how to align ligands."
        control.tip_key = "A"
        control.load_icon(_package_file("align_ligands.png"))

        control = group.add_button("Dock Ligands", DockLigandsTour)
        control.tooltip = "Run a tour on how to dock ligands."
        control.tip_key = "D"
        control.load_icon(_package_file("dock_ligands.png"))

        control = group.add_button("Build QSAR", BuildQSARTour)
        control.tooltip = (
            "Run a tour on how to build QSAR models using Cresset 3D descriptors/similarity."
        )
        control.tip_key = "B"
        control.load_icon(_package_file("build_qsar.png"))

        control = group.add_button("Build 2D QSAR", Build2DQSARTour)
        control.tooltip = (
            "Run a tour on how to build QSAR models using 2D fingerprints/imported descriptors."
        )
        control.tip_key = "2"
        control.load_icon(_package_file("build_qsar.png"))

        control = group.add_button("Fit Ligands to a QSAR Model", FitToModelTour)
        control.tooltip = "Run a tour on how to fit ligands to a QSAR model."
        control.tip_key = "Q"
        control.load_icon(_package_file("fit_to_model.png"))

        control = group.add_button("Create Pharmacophore", CreatePharmacophoreTour)
        control.tooltip = "Run a tour on how to create a Pharmacophore."
        control.tip_key = "H"
        control.load_icon(_package_file("create_pharmacophore.png"))

        print(f"Loaded {self.__class__.__module__}.{self.__class__.__name__}")


def _package_file(file_name):
    """Return the path to the file `file_name` in this package."""
    return os.path.join(os.path.dirname(os.path.realpath(__file__)), file_name)
