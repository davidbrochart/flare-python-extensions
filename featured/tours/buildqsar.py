# -*- coding: utf-8 -*-
# Copyright (C) 2023 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
from cresset import flare


class BuildQSARTour:
    """A tour on how to build QSAR models using Cresset 3D descriptors/similarity."""

    def __init__(self):
        self._build_qsar()

    def _build_qsar(self):
        """Start the tour"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("File")
            tour_widget.text = (
                "<b>Build a Quantitative Structure Activity Relationships (QSAR) "
                + "model using Cresset 3D descriptors</b><br>&nbsp;<br>"
                + "Your ligands must be pre-aligned to use this tour.<br>"
                + "Try the 'Align Ligands' tour to learn how to align a set of "
                + "ligands to a reference.<br>&nbsp;<br>"
                + '<a href="link_project_open">The Flare project with the pre-aligned ligands is already open.</a><br>&nbsp;<br>'  # noqa: E501
                + '<a href="link_project_not_open">Open the Flare project with the pre-aligned ligands.</a><br>&nbsp;<br>'  # noqa: E501
                + "Make sure the Ligands table is open before starting this tour."
            )

            tour_widget.add_link_callback("link_project_open", self._build_qsar_projectopen)
            tour_widget.add_link_callback("link_project_not_open", self._build_qsar_projectnotopen)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    # Option 1: Project is open, I already have a training set

    def _build_qsar_projectopen(self):
        """Ask the user if they already have a training set"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("Ligands")
            tour_widget.text = (
                "To build a QSAR model in Flare, all the ligands you want to include "
                + "in model building must be in the same role.<br>"
                + "You can use any role as the training set, or you can use the pre-built "
                + "'Training Set' role in Flare.<br>&nbsp;<br>"
                + '<a href="trainingset">All the ligands to use in model building are already in the same role</a><br>&nbsp;<br>'  # noqa: E501
                + '<a href="movetrainingset">Move the ligands into the Training Set role.</a>'
            )
            tour_widget.add_link_callback("trainingset", self._build_qsar_testset)
            tour_widget.add_link_callback("movetrainingset", self._build_qsar_movetrainingset)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    # Move the ligands in the training set

    def _build_qsar_movetrainingset(self):
        """Ask the user to select the ligands to move to the Training set"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("Ligands")
            tour_widget.text = "Select the ligands you want to use to build the QSAR model.<br>"
            tour_widget.add_next_callback(self._build_qsar_movetrainingset_ligandtab)

        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _build_qsar_movetrainingset_ligandtab(self):
        """Ask the user to go to the Ligand tab"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(["Ligand"])
            tour_widget.text = "Go to the <b>Ligand</b> tab.<br>"
            tour_widget.add_named_widget_shown_callback(
                ["Ligand", "Roles"], self._build_qsar_movetrainingset_roles
            )

        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _build_qsar_movetrainingset_roles(self, widget):
        """Tell the user to move the reference molecules into a separate role"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(["Ligand", "Roles"])
            tour_widget.text = "Press the <b>Roles</b> button."
            tour_widget.add_named_widget_shown_callback(
                ["Menu", "Set Role"], self._build_qsar_movetrainingset_choose_role
            )
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _build_qsar_movetrainingset_choose_role(self, widget):
        """Tell the user to move the reference molecules into a separate role"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(["Menu", "Set Role"])
            tour_widget.text = (
                "Choose <b>Set Role</b>, then <b>Training Set</b>.<br>&nbsp;<br>"
                + "Alternatively, you can create a new role for your training set ligands."
            )
            tour_widget.add_next_callback(self._build_qsar_testset)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    # Ligands are already in the Training set, go ahead to check if there is a test set.

    def _build_qsar_testset(self):
        """Ask the user if they already have a test set"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("Ligands")
            tour_widget.text = (
                "Do you have already a Test Set?<br>This is used to check the predictive "
                + "ability of a QSAR model.<br>&nbsp;<br>"
                + "Note that you don't need a Test Set if you build a qualitative "
                + "Activity Atlas model.<br>&nbsp;<br>"
                + '<a href="testset">I have already a Test Set</a><br>&nbsp;<br>'
                + '<a href="noneedtestset">I do not need a Test Set</a><br>&nbsp;<br>'
                + '<a href="partition">I want to create a Test Set.</a>'
            )
            tour_widget.add_link_callback("testset", self._build_qsar_qsartab)
            tour_widget.add_link_callback("noneedtestset", self._build_qsar_qsartab)
            tour_widget.add_link_callback("partition", self._build_qsar_partition)

        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _build_qsar_partition(self):
        """Tell the users to go to the QSAR tab"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("QSAR")
            tour_widget.text = "Go to the <b>QSAR</b> tab."
            tour_widget.add_named_widget_shown_callback(
                ["QSAR", "Partition Data Set"], self._build_qsar_partition_button
            )
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _build_qsar_partition_button(self, widget):
        """Tell the users to press the Partition Data Set buttom"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(["QSAR", "Partition Data Set"])
            tour_widget.text = "Press the <b>Partition Data Set</b> button."
            tour_widget.add_named_widget_shown_callback(
                ["Dialog", "Partition the Data Set"], self._build_qsar_partition_step1
            )
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _build_qsar_partition_step1(self, widget):
        """Tell the users to choose the Training Set to partition"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(
                ["Dialog", "Partition the Data Set", "Add Role"]
            )
            tour_widget.text = "Choose the <b>Training Set Role</b> you want to partition."
            tour_widget.add_next_callback(self._build_qsar_partition_step2)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _build_qsar_partition_step2(self):
        """Tell the users to choose role for the Test Set"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(
                ["Dialog", "Partition the Data Set", "Add Role"]
            )
            tour_widget.text = (
                "Choose the <b>Test Set Role</b>.<br>&nbsp;<br>"
                + "Enter a role name to create a new role or select an existing role."
            )
            tour_widget.add_next_callback(self._build_qsar_partition_step3)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _build_qsar_partition_step3(self):
        """Tell the users to choose the % partition"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(["Dialog", "Partition the Data Set"])
            tour_widget.text = (
                "Choose the <b>percentage</b> of molecules from the Training Set "
                + "which you want to partition into the Test Set role."
            )
            tour_widget.add_next_callback(self._build_qsar_partition_step4)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _build_qsar_partition_step4(self):
        """Tell the users to choose the partition method"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(["Dialog", "Partition the Data Set"])
            tour_widget.text = (
                "Choose the <b>Partition Method</b>.<br>&nbsp;<br>"
                + "Two methods are available:<br>"
                + "<b>Random</b>: the ligands to partition into the Test Set role "
                + "are chosen at random.<br>&nbsp;<br>"
                + "<b>Activity Stratified</b>: the ligands are sorted by activity, "
                + "and then sampled across the activity range to form the Test Set.<br>&nbsp;<br>"
                + "When using the Activity Stratified method, check that the <b>By Activity</b> "
                + "option is set to the activity column you want to use"
                + " for building the QSAR model.<br>&nbsp;<br>"
                + "This can be changed by pressing the 'Settings' button "
                + "near the Partition Method choice, which will open the "
                + "Column & Activity Editor.<br>"
                + "In the Column & Activity Editor, set the desired activity "
                + "column to be the Primary Activity, then close the Editor."
            )
            tour_widget.add_next_callback(self._build_qsar_partition_step5)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _build_qsar_partition_step5(self):
        """Tell the users to do the partitioning"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(["Dialog", "Partition the Data Set", "OK"])
            tour_widget.text = "Press <b>OK</b> to do the partitioning.<br>"
            tour_widget.add_named_widget_shown_callback(
                "Build QSAR Model", self._build_qsar_buildqsarmodel
            )
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    # Option 2: Project is not open

    def _build_qsar_projectnotopen(self):
        """Tell the user to go to the File tab"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("File")
            tour_widget.text = (
                "Go to the <b>File</b> tab to load the Flare project "
                + "with the pre-aligned ligands.<br>"
            )
            tour_widget.add_named_widget_shown_callback(
                "Open File", self._build_qsar_projectnotopen_openfile
            )
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _build_qsar_projectnotopen_openfile(self, widget):
        """Tell the user to find and load the Flare project with the aligned ligands"""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = widget
        tour_widget.text = "Press <b>Open File</b> to load the Flare project."
        tour_widget.add_named_widget_shown_callback(
            "Choose a file", self._build_qsar_projectnotopen_choose_file
        )

    def _build_qsar_projectnotopen_choose_file(self, widget):
        """Tell the user to choose a file."""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = widget
        tour_widget.text = "Browse to find the Flare project file and press <b>Open</b>."
        tour_widget.add_named_widget_shown_callback(
            "Ligands", self._build_qsar_projectnotopen_asktraining
        )

    def _build_qsar_projectnotopen_asktraining(self, widget):
        """Ask the user if they already have a training set"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("Ligands")
            tour_widget.text = (
                "To build a QSAR model in Flare, all the ligands you want to include "
                + "in model building must be in the same role.<br>"
                + "You can use any role as the training set, or you can use the pre-built "
                + "'Training Set' role in Flare.<br>&nbsp;<br>"
                + '<a href="nottrainingset">All the ligands to use in model building are already in the same role</a><br>&nbsp;<br>'  # noqa: E501
                + '<a href="notmovetrainingset">Move the ligands into the Training Set role.</a>'
            )
            tour_widget.add_link_callback("nottrainingset", self._build_qsar_nottestset)
            tour_widget.add_link_callback("notmovetrainingset", self._build_qsar_notmovetrainingset)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    # Move the ligands in the training set

    def _build_qsar_notmovetrainingset(self):
        """Ask the user to select the ligands to move to the Training set"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("Ligands")
            tour_widget.text = "Select the ligands you want to use to build the QSAR model.<br>"
            tour_widget.add_next_callback(self._build_qsar_notmovetrainingset_ligandtab)

        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _build_qsar_notmovetrainingset_ligandtab(self):
        """Ask the user to go to the Ligand tab"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(["Ligand"])
            tour_widget.text = "Go to the <b>Ligand</b> tab.<br>"
            tour_widget.add_named_widget_shown_callback(
                ["Ligand", "Roles"], self._build_qsar_notmovetrainingset_roles
            )

        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _build_qsar_notmovetrainingset_roles(self, widget):
        """Tell the user to move the selected ligands into the training set"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(["Ligand", "Roles"])
            tour_widget.text = "Press the <b>Roles</b> button."
            tour_widget.add_named_widget_shown_callback(
                ["Menu", "Set Role"], self._build_qsar_notmovetrainingset_choose_role
            )
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _build_qsar_notmovetrainingset_choose_role(self, widget):
        """Tell the user to move the selected ligands into the training set"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(["Menu", "Set Role"])
            tour_widget.text = (
                "Choose <b>Set Role</b>, then <b>Training Set</b>.<br>&nbsp;<br>"
                + "Alternatively, you can create a new role for your training set ligands."
            )
            tour_widget.add_next_callback(self._build_qsar_nottestset)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    # Ligands are already in the Training set, go ahead to check if there is a test set.

    def _build_qsar_nottestset(self):
        """Ask the user if they already have a test set"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("Ligands")
            tour_widget.text = (
                "Do you have already a Test Set?<br>This is used to check the "
                + "predictive ability of a QSAR model.<br>&nbsp;<br>"
                + "Note that you don't need a Test Set if you build a qualitative "
                + "Activity Atlas model.<br>&nbsp;<br>"
                + '<a href="nottestset">I have already a Test Set</a><br>&nbsp;<br>'
                + '<a href="notnoneedtestset">I do not need a Test Set</a><br>&nbsp;<br>'
                + '<a href="notpartition">I want to create a Test Set.</a>'
            )
            tour_widget.add_link_callback("nottestset", self._build_qsar_qsartab)
            tour_widget.add_link_callback("notnoneedtestset", self._build_qsar_qsartab)
            tour_widget.add_link_callback("notpartition", self._build_qsar_notpartition)

        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _build_qsar_notpartition(self):
        """Tell the users to go to the QSAR tab"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("QSAR")
            tour_widget.text = "Go to the <b>QSAR</b> tab."
            tour_widget.add_named_widget_shown_callback(
                ["QSAR", "Partition Data Set"], self._build_qsar_partition_button
            )
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _build_qsar_notpartition_button(self, widget):
        """Tell the users to press the Partition Data Set buttom"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(["QSAR", "Partition Data Set"])
            tour_widget.text = "Press the <b>Partition Data Set</b> button."
            tour_widget.add_named_widget_shown_callback(
                ["Dialog", "Partition the Data Set"], self._build_qsar_notpartition_step1
            )
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _build_qsar_notpartition_step1(self, widget):
        """Tell the users to choose the Training Set to partition"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(
                ["Dialog", "Partition the Data Set", "Add Role"]
            )
            tour_widget.text = "Choose the <b>Training Set Role</b> you want to partition."
            tour_widget.add_next_callback(self._build_qsar_notpartition_step2)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _build_qsar_notpartition_step2(self):
        """Tell the users to choose role for the Test Set"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(
                ["Dialog", "Partition the Data Set", "Add Role"]
            )
            tour_widget.text = (
                "Choose the <b>Test Set Role</b>.<br>&nbsp;<br>"
                + "Enter a role name to create a new role or select an existing role."
            )
            tour_widget.add_next_callback(self._build_qsar_notpartition_step3)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _build_qsar_notpartition_step3(self):
        """Tell the users to choose the % partition"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(["Dialog", "Partition the Data Set"])
            tour_widget.text = (
                "Choose the <b>percentage</b> of molecules from the Training Set "
                + "which you want to partition into the Test Set role."
            )
            tour_widget.add_next_callback(self._build_qsar_notpartition_step4)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _build_qsar_notpartition_step4(self):
        """Tell the users to choose the partition method"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(["Dialog", "Partition the Data Set"])
            tour_widget.text = (
                "Choose the <b>Partition Method</b>.<br>&nbsp;<br>"
                + "Two methods are available:<br>"
                + "<b>Random</b>: the ligands to partition into the Test Set role "
                + "are chosen at random.<br>&nbsp;<br>"
                + "<b>Activity Stratified</b>: the ligands are sorted by activity, "
                + "and then sampled"
                + " across the activity range to form the Test Set.<br>"
                + "When using the Activity Stratified method, check that the <b>By Activity</b> "
                + "option is set to the activity column you want to use"
                + " for building the QSAR model.<br>&nbsp;<br>"
                + "This can be changed by pressing the 'Settings' button "
                + "near the Partition Method choice, which will open the "
                + "Column & Activity Editor.<br>"
                + "In the Column & Activity Editor, set the desired activity "
                + "column to be the Primary Activity, then close the Editor."
            )
            tour_widget.add_next_callback(self._build_qsar_notpartition_step5)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _build_qsar_notpartition_step5(self):
        """Tell the users to do the partitioning"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(["Dialog", "Partition the Data Set", "OK"])
            tour_widget.text = "Press <b>OK</b> to do the partitioning.<br>"
            tour_widget.add_named_widget_shown_callback(
                "Build QSAR Model", self._build_qsar_buildqsarmodel
            )
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    # Build the QSAR model
    def _build_qsar_qsartab(self):
        """Tell the users to go to the QSAR tab"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("QSAR")
            tour_widget.text = "Go to the <b>QSAR</b> tab."
            tour_widget.add_named_widget_shown_callback(
                "Build QSAR Model", self._build_qsar_buildqsarmodel
            )
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _build_qsar_buildqsarmodel(self, widget):
        """Tell the users to go to press the Build QSAR Model button"""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = widget
        tour_widget.text = "Press the <b>Build QSAR Model</b> button.<br>"
        tour_widget.add_named_widget_shown_callback(
            ["Dialog", "QSAR Model Building"], self._build_qsar_build_qsar_options_step1
        )

    def _build_qsar_build_qsar_options_step1(self, widget):
        """Tell the users to choose the Model Type"""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = widget
        tour_widget.text = (
            "<b>Step 1.</b> Choose the <b>Model Type</b> from:<br>&nbsp;<br>"
            + "<b>Field QSAR</b>: Build a Field QSAR model based on 3D descriptors.<br>&nbsp;<br>"
            + "<b>Activity Atlas</b>: Qualitatively analyzes and summarizes SAR using "
            + "a probabilistic approach.<br>&nbsp;<br>"
            + "<b>Regression Models</b>: Activity prediction based on machine learning "
            + "methods and 3D descriptors/similarity.<br>&nbsp;<br>"
            + "<b>Classification Models</b>: Build a classification model based on "
            + "machine learning methods and 3D descriptors/similarity.<br>&nbsp;<br>"
            + "More information about each model type can be found in the "
            + "Flare user manual."
        )
        tour_widget.add_next_callback(self._build_qsar_build_qsar_options_step2_1)

    def _build_qsar_build_qsar_options_step2_1(self):
        """Tell the users to choose the Model"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(["Dialog", "QSAR Model Building"])
            tour_widget.text = (
                "<b>Step 2.</b> Choose the <b>Model</b>.<br>"
                "Note that the choices available for this option  will change according to which "
                + "Model Type you chose.<br>&nbsp;<br>"
                + "<b>Field QSAR</b>:"
                + "<ol><li><b>Normal/Normal with Y Scrambles</b>:<br>"
                + "Do not/Perform Y scrambling of the data.<br></li>"
                + "<li><b>Weighted</b>: Use the similarity score to the<br>"
                + "reference molecule as an additional weighting<br>"
                + "to apply to molecules.<br>"
                + "Emphasizes those molecules that align<br>"
                + "with a high score.</li></ol>"
                + "<b>Activity Atlas</b>:"
                + "<ol><li><b>Normal</b>: Uses the Weighted Sum algorithm, <br>less susceptible "
                + "to outliers</li>"
                + "<li><b>Sum</b>: Original Activity Atlas algorithm.</li></ol>"
                + "Press Next for other model options."
            )
            tour_widget.add_next_callback(self._build_qsar_build_qsar_options_step2_2)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _build_qsar_build_qsar_options_step2_2(self):
        """Tell the users to choose the Model"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(["Dialog", "QSAR Model Building"])
            tour_widget.text = (
                "<b>Step 2. (continued)</b> Choose the Model.<br>&nbsp;<br>"
                "<b>Regression and Classification</b><br>"
                + "<ol><li><b>Automatic</b>: try all available regression or<br>"
                + "classification models and pick the best<br>"
                + "model for the output.</li>"
                + "<li><b>Gaussian Process: (regression only)</b>:<br>"
                + " predict activity using a Gaussian Process model "
                + "and Cresset 3D descriptors.</li>"
                + "<li><b>K-Nearest Neighbor (kNN)</b>:<br>"
                + " predict activity or category (class)"
                + "based on average activity/class of k Neighbors "
                + " using either 3D or 2D similarity.</li>"
                + "<li><b>MLP</b>:<br>"
                + " predict activity using a Multilayer Perceptron model "
                + "and Cresset 3D descriptors.</li>"
                + "<li><b>Random Forest</b>:<br>"
                + " predict activity using a Random Forest model "
                + "and Cresset 3D descriptors.</li>"
                + "<li><b>SVM</b>:<br>"
                + "predict activity or category (class) "
                + "with a Support Vector Machine model "
                + "and Cresset 3D descriptors.</li></ol>"
            )
            tour_widget.add_next_callback(self._build_qsar_build_qsar_options_step3)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _build_qsar_build_qsar_options_step3(self):
        """Tell the users to choose the Training Set Role"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(["Dialog", "QSAR Model Building"])
            tour_widget.text = (
                "<b>Step 3.</b> Choose the <b>Training Set Role</b>, containing the ligands "
                + "you want to use to build the model."
            )
            tour_widget.add_next_callback(self._build_qsar_build_qsar_options_step4)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _build_qsar_build_qsar_options_step4(self):
        """Tell the users to choose the Test Set Role"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(["Dialog", "QSAR Model Building"])
            tour_widget.text = (
                "<b>Step 4.</b> Choose the <b>Test Set Role</b>, containing the <br>"
                + "ligands you want to use to test the predictive <br>ability of "
                + "your model<br>&nbsp;<br>"
                + "This option will be greyed out when building qualitative Activity Atlas models."
            )
            tour_widget.add_next_callback(self._build_qsar_build_qsar_options_step5)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _build_qsar_build_qsar_options_step5(self):
        """Tell the users to choose the Activity to model"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(
                ["Dialog", "QSAR Model Building", "Configure"]
            )
            tour_widget.text = (
                "<b>Step 5.</b> Choose the <b>Activity</b> column. <br>&nbsp;<br>"
                + "You can use the press the <b>Configure</b> "
                + "button to open the Column & Activity Editor and review the activity columns in "
                + "your project.<br>&nbsp;<br>"
                + "More information about setting appropriate activity units for regression "
                + "and classification models can be found in the Flare user manual."
            )
            tour_widget.add_next_callback(self._build_qsar_build_qsar_options_step6)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _build_qsar_build_qsar_options_step6(self):
        """Tell the users to choose the 3D descriptors"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(
                ["Dialog", "QSAR Model Building", "Configure"]
            )
            tour_widget.text = (
                "<b>Step 6.</b> Choose the <b>3D Descriptors</b> to use. <br>&nbsp;<br>"
                + "The default is to build a model from both electrostatic "
                + "and volume fields, but if desired you can use just one of these.<br>&nbsp;<br>"
                + "This option is not available for Activity Atlas and kNN models."
            )
            tour_widget.add_next_callback(self._build_qsar_build_qsar_options_step7)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _build_qsar_build_qsar_options_step7(self):
        """Tell the users to start the QSAR model building"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(["Dialog", "QSAR Model Building", "Start"])
            tour_widget.text = (
                "<b>Step 7.</b> Press the <b> Start</b> button to start the calculation, "
                + "then wait for the calculation to complete."
            )
            tour_widget.add_widget_hidden_callback(self._build_qsar_build_qsar_done_qsardock)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _build_qsar_build_qsar_done_qsardock(self):
        """Tell the user to view the results: QSAR model activity tab"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("Ligands")
            tour_widget.text = (
                "<b>Wait until the calculation completes.</b><br>&nbsp;<br>"
                + "The QSAR model window opens at the end of model building.<br>"
                + "Model results are summarised in different tabs, however "
                + "some tabs may be greyed out for specific model types.<br>&nbsp;<br>"
                + "Full details for each calculated QSAR model "
                + "can be found in the Project Log: "
                + "press the <b>Show Log</b> button in the QSAR tab to open it.<br>&nbsp;<br>"
            )
            tour_widget.add_next_callback(self._build_qsar_build_qsar_done_activitytab)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _build_qsar_build_qsar_done_activitytab(self):
        """Tell the user to view the results: QSAR model activity tab"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("QSAR Model")
            tour_widget.text = (
                "<b>Activity tab</b>:<br>"
                + "Greyed out for qualitative Activity Atlas models.<br>&nbsp;<br>"
                + "<b>Field QSAR and Regression models:</b><br>"
                + "Shows a graph of predicted versus observed activity for all project molecules "
                + " using the selected model.<br>&nbsp;<br>"
                + "The graph contains separate data series for the Training set, "
                + "Test set, cross-validated Training set (where applicable) "
                + "plus any other role containing ligands with activity data in the "
                + "project.<br>&nbsp;<br>"
                + "Buttons toggle the display of each of these data series.<br>&nbsp;<br>"
                + "<b>Classification models:</b><br>"
                + "Shows the confusion matrices and classification statistics for the different "
                + "roles in the project.<br>"
                + "Separate confusion matrices are created for the Training set, Test set, "
                + "cross-validated Training set (where applicable) "
                + "plus any other role containing ligands with class data in the "
                + "project.<br>&nbsp;<br>"
                + "Buttons toggle the display of each of these data series.<br>&nbsp;<br>"
            )
            tour_widget.add_next_callback(self._build_qsar_build_qsar_done_q2tab)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _build_qsar_build_qsar_done_q2tab(self):
        """Tell the user to view the results: QSAR model q2 tab."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("QSAR Model")
            tour_widget.text = (
                "<b>Q2 tab</b>:<br>Only shown with Field QSAR and kNN "
                + "regression models.<br>&nbsp;<br>"
                + "Shows graphs of model performance (q2 and r2) "
                + "against the number of components/neighbors<br> in the model.<br>&nbsp;<br>"
                + "By default Flare selects the model which corresponds to the first maximum "
                + "in the q2 graph.<br>"
                + "To select a model with a different number of components/neighbours, click on "
                + "the desired location.<br>&nbsp;<br>"
                + "For kNN classification models, this tab is replaced by the "
                + "<b>Informedness tab</b>, "
                + "showing a graph of classification performance "
                + "against the number of neighbors in the model."
            )
            tour_widget.add_next_callback(self._build_qsar_build_qsar_done_rmsetab)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _build_qsar_build_qsar_done_rmsetab(self):
        """Tell the user to view the results: QSAR model rmse tab."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("QSAR Model")
            tour_widget.text = (
                "<b>RMSE tab</b>:<br>Only shown with Field QSAR and kNN regression "
                + "models.<br>&nbsp;<br>"
                + "Shows how the Root Mean Square Error (RMSE) changes with "
                + " number of components/neigbors in the model.<br>&nbsp;<br>"
                + "The <b>RMSE graph</b> (Field QSAR only) uses the model derived from the "
                + "entire Training Set to calculate the error.<br>&nbsp;<br>"
                + "The <b>RMSEpred graph</b> uses the cross-validated predicted "
                + "values.<br>&nbsp;<br>"
                + "To select a model with a different number of components/neighbours, "
                + "click on the desired location."
            )
            tour_widget.add_next_callback(self._build_qsar_build_qsar_done_pcatab)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _build_qsar_build_qsar_done_pcatab(self):
        """Tell the user to view the results: QSAR model PCA tab."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("QSAR Model")
            tour_widget.text = (
                "<b>PCA tab</b>:<br>"
                + "Greyed out for qualitative Activity Atlas models and kNN models based on "
                + "3D/2D similarity. <br>&nbsp;<br>"
                + "Useful to identify grouping of molecules and outliers in the descriptors "
                + " space for the compounds in the Training Set.<br>&nbsp;<br>"
                + "Points in the PCA graph can be selected by drawing a box around "
                + "them with the left mouse button.<br>"
                + "The corresponding ligands will be selected in the Ligands table."
            )
            tour_widget.add_next_callback(self._build_qsar_build_qsar_done_3dview)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _build_qsar_build_qsar_done_3dview(self):
        """Tell the user to view the results: QSAR model 3D view tab"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("QSAR Model")
            tour_widget.text = (
                "<b>3D View</b>:<br>"
                + "View the Field QSAR or Activity Atlas models in 3D. <br>&nbsp;<br>"
                + "Machine Learning models (kNN, RF, GP, MLP and SVM) don't have 3D views. "
            )
            tour_widget.add_next_callback(self._build_qsar_build_qsar_done_notes)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _build_qsar_build_qsar_done_notes(self):
        """Tell the user to view the results: QSAR model notes tab."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("QSAR Model")
            tour_widget.text = (
                "<b>Notes</b>:<br>"
                + "Designed as a simple notebook that can be used to record information "
                + "about your QSAR experiment.<br>"
                + "Supports simple text based interactions such as selection, copy, paste, "
                + "undo (Ctrl+Z) and redo (Ctrl+Y).<br>&nbsp;<br>"
                + "<b>This tour is now complete: "
                + "press Stop to close this widget.</b>"
            )
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )
