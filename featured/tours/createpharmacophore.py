# -*- coding: utf-8 -*-
# Copyright (C) 2023 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
from cresset import flare


class CreatePharmacophoreTour:
    """A tour on how to create a pharmacophore."""

    def __init__(self):
        self._create_pharmacophore_tour()

    def _create_pharmacophore_tour(self):
        """Start the tour"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("File")
            tour_widget.text = (
                "<b>Create a pharmacophore with FieldTemplater</b><br>&nbsp;<br>"
                + "Make sure that the Ligands table is open before you start.<br>&nbsp;<br>"
                + "Select 2 or more ligands to create the pharmacophore.<br>"
                + "The ligands can be loaded either as 2D structures, or as a "
                + "set of pre-existing conformations in a single file.<br>&nbsp;<br>"
                + "Aligning more that 4 or 5 ligands at a time is not recommended.<br>&nbsp;<br>"
                + "Make sure that the <b>Ligands</b> table is open before you "
                + "start.<br>&nbsp;<br>"
                + '<a href="link_ligands_loaded">The ligands are already in Flare.'
                + "</a><br>&nbsp;<br>"
                + '<a href="link_ligands_toload">I want to load one or more ligands.</a><br>'
            )
            tour_widget.add_link_callback("link_ligands_loaded", self._create_pharmacophore_loaded)
            tour_widget.add_link_callback("link_ligands_toload", self._create_pharmacophore_toload)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    # Option 1: ligands are already loaded

    def _create_pharmacophore_loaded(self):
        """Tell the user to select 2 or more ligands."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("Ligands")
            tour_widget.text = "Select two or more ligands, then press <b>Next</b>."
            tour_widget.add_next_callback(self._create_pharmacophore_next)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _create_pharmacophore_next(self):
        """Tell the user to go to the 3D Pose tab."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("3D Pose")
            tour_widget.text = "Go to the <b>3D Pose</b> tab."
            tour_widget.add_named_widget_shown_callback(
                ["3D Pose", "FieldTemplater"], self._create_pharmacophore_fieldtemplater
            )
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    # Option 2: ligands must be loaded

    def _create_pharmacophore_toload(self):
        """Tell the user to open the File menu."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("File")
            tour_widget.text = "Press the <b>File</b> button."
            tour_widget.add_named_widget_shown_callback(
                "Open File", self._create_pharmacophore_open_file_menu
            )
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _create_pharmacophore_open_file_menu(self, widget):
        """Tell the user to press Open File in the File menu."""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = widget
        tour_widget.text = "Press <b>Open File</b> to load a file with your ligands."
        tour_widget.add_widget_hidden_callback(self._create_pharmacophore_tour)
        tour_widget.add_named_widget_shown_callback(
            "Choose a file", self._create_pharmacophore_choose_file
        )

    def _create_pharmacophore_choose_file(self, widget):
        """Tell the user to browse and choose a file."""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = widget
        tour_widget.text = "Browse to find the ligands file and press <b>Open</b>."

        tour_widget.add_named_widget_shown_callback(
            "Open Molecules", self._create_pharmacophore_ligands_open_file_options
        )
        tour_widget.add_named_widget_shown_callback(
            "CSV Import", self._create_pharmacophore_ligands_open_csv
        )

    def _create_pharmacophore_ligands_open_file_options(self, widget):
        """Tell the user to choose import options"""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = widget
        tour_widget.text = (
            "Choose the input options for the ligands being imported.<br>&nbsp;<br>"
            + "1. The Molecule type should be 'Autodetect' or 'Ligands'.<br>&nbsp;<br>"
            + "2. Choose a role for the ligands, "
            + "If you keep 'Autodetect', the ligands will be imported "
            + " in a role named after the file you are importing.<br>&nbsp;<br>"
            + "3. <b>Choose the Protonation state:</b><br>"
            + "Choose <b>Use input protonation state</b>"
            + " if you know that your ligands are in the correct protonation state.<br>"
            + "Choose <b>Apply rule-based protonation</b> "
            + "to let Flare define the protonation state.<br>&nbsp;<br>"
            + "4. The 'Ligand file read mode option' chooses how the input files are handled. "
            + "'Autodetect' works well in most cases.<br>&nbsp;<br>"
            + "Press <b>Open</b> to import your ligands."
        )

        tour_widget.add_named_widget_shown_callback(
            "Ligands", self._create_pharmacophore_select_ligands_totemplate
        )

    def _create_pharmacophore_ligands_open_csv(self, widget):
        """Tell the user to choose csv import options."""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = widget
        tour_widget.text = (
            "Choose the input options for the ligands being imported.<br>&nbsp;<br>"
            + "1. Check the <b>Preview</b> panel to ensure that "
            + "the <b>Column Delimiter</b> is set correctly.<br>&nbsp;<br>"
            + "2. Choose a <b>Ligand Role</b> for the ligands, "
            + "or create a new role pressing the <b>Add Role"
            + " button</b>.<br>&nbsp;<br>"
            + "3. <b>Choose the Protonation state:</b><br>"
            + "Choose <b>Use input protonation state</b> "
            + " if you know that your ligands are in the correct protonation state.<br>"
            + "Choose <b>Apply rule-based protonation</b> "
            + "to let Flare define the protonation state.<br>&nbsp;<br>"
            + "4. Choose the columns in the CSV file which map the <b>Structure</b>,"
            + " and the <b>Ligand Name</b>.<br>&nbsp;<br>"
            + "Press <b>Open</b> to import your ligands."
        )

        tour_widget.add_named_widget_shown_callback(
            "Ligands", self._create_pharmacophore_select_ligands_totemplate
        )

    def _create_pharmacophore_select_ligands_totemplate(self, widget):
        """Tell the user to select 2 or more ligands to template."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("Ligands")
            tour_widget.text = (
                "Load additional ligands to template by selecting "
                + "File/Open File again.<br>&nbsp;<br>"
                + "When you are done, select 2 or more ligands to use for "
                + "creating a pharmacophore with FieldTemplater."
            )
            tour_widget.add_next_callback(self._create_pharmacophore_ligand_tab)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    # Create the FT project

    def _create_pharmacophore_ligand_tab(self):
        """Tell the user to go to the 3D Pose tab."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("3D Pose")
            tour_widget.text = "Go to the <b>3D Pose</b> tab."
            tour_widget.add_named_widget_shown_callback(
                ["3D Pose", "FieldTemplater"], self._create_pharmacophore_fieldtemplater
            )
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _create_pharmacophore_fieldtemplater(self, widget):
        """Tell the user to press the FT button"""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = widget
        tour_widget.text = "Press the <b>FieldTemplater</b> button."
        tour_widget.add_named_widget_shown_callback(
            ["Menu", "New FieldTemplater Project"], self._create_pharmacophore_fieldtemplater_new
        )

    def _create_pharmacophore_fieldtemplater_new(self, widget):
        """Tell the user to press the New FieldTemplater Project button."""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = widget
        tour_widget.text = "Press the <b>New FieldTemplater project</b> button."
        tour_widget.add_named_widget_shown_callback(
            ["Dialog", "New FieldTemplater Project"], self._create_pharmacophore_fieldtemplater_name
        )

    def _create_pharmacophore_fieldtemplater_name(self, widget):
        """Tell the user to give a name to the FT project, and then press OK."""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = widget
        tour_widget.text = (
            "Choose a name for the FieldTemplater project, then press <b>OK</b>.<br>&nbsp;<br>"
            + "Wait a few seconds for the FieldTemplater project window to open."
        )
        tour_widget.add_named_widget_shown_callback(
            ["Window", "FieldTemplater", "Ribbon", "Home", "Pairwise Constraints"],
            self._create_pharmacophore_fieldtemplater_constraints,
        )

    def _create_pharmacophore_fieldtemplater_constraints(self, widget):
        """Ask users whether they want to apply a FT constraint."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(
                ["Window", "FieldTemplater", "Ribbon", "Home", "Pairwise Constraints"]
            )
            tour_widget.text = (
                "Do you want to set pairwise constraints?<br>&nbsp;<br>"
                + "This is an optional step: setting pairwise constraints means that alignments "
                + "will be penalized unless the constrained atoms are within "
                + "the specified distances.<br>&nbsp;<br>"
                + '<a href="pairwise constraints">Set pairwise constraints</a><br>&nbsp;<br>'
                + '<a href="run FT">Do not set constraints, start the templating process.</a><br>'
            )

            tour_widget.add_link_callback(
                "pairwise constraints", self._create_pharmacophore_constraints
            )
            tour_widget.add_link_callback("run FT", self._create_pharmacophore_run)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    # Set constraints

    def _create_pharmacophore_constraints(self):
        """Tell users to press the Pairwise Constraints button."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget("Pairwise Constraints")
            tour_widget.text = (
                "Press the <b>Pairwise Constraints</b> button to set-up the constraints.<b>"
            )
            tour_widget.add_named_widget_shown_callback(
                ["Dialog", "Pairwise Constraints Editor"],
                self._create_pharmacophore_set_constraints,
            )
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _create_pharmacophore_set_constraints(self, widget):
        """Tell users to set the Constraints then press OK."""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = widget
        tour_widget.text = "Set-up the constraints. Press <b>OK</b> when you are done."
        tour_widget.add_named_widget_shown_callback(
            ["Window", "FieldTemplater", "Ribbon", "Home", "Run"],
            self._create_pharmacophore_constr_run,
        )

    def _create_pharmacophore_constr_run(self, widget):
        """Tell users to press the Run button."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(
                ["Window", "FieldTemplater", "Ribbon", "Home", "Run"]
            )
            tour_widget.text = (
                "Press the <b>Run</b> button to set-up the FieldTemplater calculation."
            )
            tour_widget.add_named_widget_shown_callback(
                ["Dialog", "FieldTemplater Processing"], self._create_pharmacophore_set_FT
            )
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    # Option 2: don't set constraints

    def _create_pharmacophore_run(self):
        """Tell users to press the Run button."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(
                ["Window", "FieldTemplater", "Ribbon", "Home", "Run"]
            )
            tour_widget.text = (
                "Press the <b>Run</b> button to set-up the FieldTemplater calculation."
            )
            tour_widget.add_named_widget_shown_callback(
                ["Dialog", "FieldTemplater Processing"], self._create_pharmacophore_set_FT
            )
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _create_pharmacophore_set_FT(self, widget):
        """Tell users to choose the FT options."""
        tour_widget = flare.tour.TourWidget()
        tour_widget.widget = widget
        tour_widget.text = (
            "<b>Choose the settings for the FieldTemplater calculation</b>.<br>&nbsp;<br>"
            + "Pre-set methods are available for Conformation Hunt, "
            + "Alignment and Templating:<br>&nbsp;<br>"
            + "<b>Quick</b>: fast, but some templates may be missed<br>&nbsp;<br>"
            + "<b>Normal</b>: recommended for normal use<br>&nbsp;<br>"
            + "<b>Normal (large mols)</b>: suitable for molecules with >6 rotatable bonds,"
            + " generates more conformations.<br>&nbsp;<br>"
            + "These pre-set methods should work well for normal uses, "
            + "however frequently the best results"
            + " are obtained by fine tuning the advanced options."
        )
        tour_widget.add_next_callback(self._create_pharmacophore_set_FT_opt1)

    def _create_pharmacophore_set_FT_opt1(self):
        """Tell users to review the advanced options in the Molecule tab."""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(["Dialog", "FieldTemplater Processing"])
            tour_widget.text = (
                "Press the <b>Show Options</b> button to view the advanced "
                + "options for FieldTemplater.<br>&nbsp;<br>"
                + "The <b>Molecules</b> tab shows all the ligands that are "
                + "currently loaded into FieldTemplater.<br>&nbsp;<br>"
                + "Only the molecules which are checked in this list will be "
                + "used to build templates.<br>"
                + "This allows you to load in more molecules than are actually "
                + "used in any one calculation "
                + "(e.g., load in 10 molecules, form templates from two groups of 5)."
            )
            tour_widget.add_next_callback(self._create_pharmacophore_set_FT_opt2)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _create_pharmacophore_set_FT_opt2(self):
        """Tell users to review the advanced options in the Conformation Hunt tab"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(["Dialog", "FieldTemplater Processing"])
            tour_widget.text = (
                "Go to the <b>Conformation Hunt</b> tab.<br>"
                + "This tab shows shows the options controlling the conformation exploration"
                + " of the ligands.<br>&nbsp;<br>"
                + "Hovering with the mouse above any option will show a tooltip giving "
                + "a quick summary of that option.<br>&nbsp;<br>"
                + "Check the <b>Delete existing conformations</b> box "
                + "to delete unwanted starting conformations.<br>&nbsp;<br>"
                + "The setting that has the largest effect is <b>Maximum number "
                + "of conformations</b>.<br>"
                + "<b>Between 50 and 200 conformations are recommended.</b><br>&nbsp;<br>"
                + "More conformations mean higher likelihood that the "
                + "bioactive conformation will be found: "
                + "however, using more conformations also means adding conformations "
                + "which act as noise in the calculations.<br>"
                + "Also, both pairwise alignments and templating may take a very long"
                + " time to complete when using too many conformations.<br>&nbsp;<br>"
                + "For molecules whose conformation space is not sufficiently "
                + "explored by 200 conformations"
                + " it becomes difficult to obtain reliable results without adding "
                + "additional information such as"
                + " using pairwise constraints."
            )
            tour_widget.add_next_callback(self._create_pharmacophore_set_FT_opt3)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _create_pharmacophore_set_FT_opt3(self):
        """Tell users to review the advanced options in the Alignment tab"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(["Dialog", "FieldTemplater Processing"])
            tour_widget.text = (
                "Go to the <b>Alignment</b> tab.<br>"
                + "This tab shows the options controlling the pairwise "
                + "alignment of the ligands"
                + " and conformations.<br>&nbsp;<br>"
                + "Hovering with the mouse above any option will show a tooltip "
                + "giving a quick summary of that option.<br>&nbsp;<br>"
                + "Ticking <b>Take major shortcuts in pairwise alignments</b> "
                + "will skip the simplex optimization of the"
                + " initial pairwise alignments generated by aligning the field points. "
                + "This is significantly faster but at the expense of accuracy "
                + "and completeness.<br>&nbsp;<br>"
                + "Ticking <b>Take moderate shortcuts in pairwise alignments</b> "
                + "uses a looser convergence criterion in the"
                + " simplex optimization of the pairwise alignments, and simplexes fewer of them. "
                + "Significantly faster, but at a small cost to quality."
            )
            tour_widget.add_next_callback(self._create_pharmacophore_set_FT_opt4)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _create_pharmacophore_set_FT_opt4(self):
        """Tell users to review the advanced options in the Templating tab"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(["Dialog", "FieldTemplater Processing"])
            tour_widget.text = (
                "Go to the <b>Templating</b> tab.<br>"
                + "This tab shows  the options controlling the generation of "
                + "templates from pairwise alignments.<br>&nbsp;<br>"
                + "Hovering with the mouse above any option will show a tooltip "
                + "giving a quick summary of that option.<br>&nbsp;<br>"
                + "The main options that affect the results are the <b>Maximum number "
                + "of comparisons per pair</b> and the "
                + "<b>Maximum score delta per pair</b>.<br>"
                + "Increasing the values of these options will give more templates "
                + "(although possibly of lower reliability).<br>&nbsp;<br>"
                + "<b>Maximum number of comparisons per pair</b>: this value controls "
                + "the maximum number of high-scoring alignments"
                + " from any pair of ligands that will be used to form a template. "
                + "Higher values usually give more templates: <b>values of 100-200 are "
                + "recommended</b>.<br>&nbsp;<br>"
                + "<b>Maximum score delta per pair</b>: specifies the maximum difference "
                + "in similarity score between the best-scoring alignment"
                + " and any alignment to be used for generating a template.<br> "
                + "Higher values usually give more templates: <b>values between 0.05 and 0.2 "
                + "are recommended</b>.<br>&nbsp;<br>"
                + "<b>Minimum link density in a template </b>: The minimum fraction of possible "
                + "pairwise links that must be used in constructing"
                + " a template.<br>"
                + "Lower values are recommended for larger sets of molecules (5 or more) "
                + "where no templates are found. "
                + "However, please note that templates with a low link density are less reliable."
            )
            tour_widget.add_next_callback(self._create_pharmacophore_run_FT)
        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )

    def _create_pharmacophore_run_FT(self):
        """Tell users to start the templating run"""
        tour_widget = flare.tour.TourWidget()
        try:
            tour_widget.widget = flare.tour.find_widget(
                ["Dialog", "FieldTemplater Processing", "Start"]
            )
            tour_widget.text = (
                "Press the <b>Start</b> button to start the FieldTemplater experiment. <br>"
                "Note that the experiment may take a while to complete.<br>&nbsp;<br>"
                "<b>This tour is now finished: press Stop to close this widget."
            )

        except ValueError:
            tour_widget.text = (
                "You may have missed a step in the tour.<br>"
                + "Please start again and carefully follow the instructions."
            )
