# -*- coding: utf-8 -*-
# Copyright (C) 2023 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Adds useful functions for homology modeling including import of FASTA files.

Ribbon Controls:
    Sequences -> Fasta -> Import FASTA File
        Read and create a protein from a FASTA file. The protein will contain 1
        carbon atom for each residue.

    Sequences -> Fasta -> Import FASTA Sequence
        Read and create a protein from a FASTA string. The protein will contain 1
        carbon atom for each residue.

    Sequences -> Fasta -> Export alignments
        Export selected chain alignments (all if none is selected) to a FASTA file.

    Sequences -> Homology Modeling -> Pick Residues across Sequences
        Expand the residue pick to include residues in other sequences which are
        aligned to the current residue pick.

    Sequences -> Homology Modeling -> Mutate Protein To Sequence
        Mutate the residues in the selected protein to match another protein.

    Sequences -> Homology Modeling -> Renumber Residues
        Renumber the seq numbers of residues. If some residues have been picked, the renumbering
        applies only to this selection. Otherwise, renumbering is performed for residues in all
        selected proteins.
        Allows setting the initial seq number and whether gaps should be counted when numbering.

Context Menu Items:
    Protein Sequence -> Set Alignment From FASTA
        Set the sequence alignment to the FASTA sequences. Residues cannot be added
        but gaps may be added or removed.

    Protein Sequence Residue -> Number Residues From Here
        Renumber the seq number of the residues starting at the residue the menu was opened on.
        Allows to set the initial seq number and whether gaps should be counted when numbering.
"""
from collections import defaultdict
from itertools import zip_longest

from PySide2 import QtWidgets

from cresset import flare


@flare.extension
class HomologyModelingExtension:
    """Add controls to help with homology modeling."""

    CODES = {
        "A": "ALA",
        "B": "ASX",
        "C": "CYS",
        "D": "ASP",
        "E": "GLU",
        "F": "PHE",
        "G": "GLY",
        "H": "HIS",
        "I": "ILE",
        "K": "LYS",
        "L": "LEU",
        "M": "MET",
        "N": "ASN",
        "P": "PRO",
        "Q": "GLN",
        "R": "ARG",
        "S": "SER",
        "T": "THR",
        "U": "SEC",
        "V": "VAL",
        "W": "TRP",
        "Y": "TYR",
        "Z": "GLX",
    }

    MAX_FASTA_LEN = 70

    def load(self):
        """Load the extension."""
        tab = flare.main_window().ribbon["Sequences"]
        group = tab["Fasta"]

        control = group.add_button("Import FASTA File", self.proteins_from_file)
        control.size = flare.UIRibbonControl.Size.Small
        control.tooltip = (
            "Read and create a protein from a FASTA file. "
            + "The protein will contain 1 carbon atom for each residue."
        )
        control.tip_key = "EI"

        control = group.add_button("Import FASTA Sequence", self.proteins_from_sequence)
        control.size = flare.UIRibbonControl.Size.Small
        control.tooltip = (
            "Read and create a protein from a FASTA string. "
            + "The protein will contain 1 carbon atom for each residue."
        )
        control.tip_key = "ES"

        control = group.add_button("Export alignments", self.sequences_to_file)
        control.size = flare.UIRibbonControl.Size.Small
        control.tooltip = (
            "Export alignment from selected (all, if none is selected) "
            + "sequences to a FASTA file. "
        )
        control.tip_key = "EA"

        group = tab["Homology Modelling"]

        control = group.add_button("Pick Residues across Sequences", self.select_sequence)
        control.size = flare.UIRibbonControl.Size.Small
        control.tooltip = (
            "Expand the residue pick to include residues in other sequences "
            + "which are aligned to the current residue pick."
        )
        control.tip_key = "EP"

        control = group.add_button(
            "Mutate Protein To Sequence", self.mutate_selected_protein_to_sequence
        )
        control.size = flare.UIRibbonControl.Size.Small
        control.tooltip = (
            "Mutate the residues in the selected protein to match the sequence of another protein."
        )
        control.tip_key = "EM"

        control = group.add_button("Renumber Protein Residues", self.renumber_residues_seq_num)
        control.size = flare.UIRibbonControl.Size.Small
        control.tooltip = "Renumber the seq number of the residues in the selected proteins."
        control.tip_key = "ER"

        flare.callbacks.protein_sequence_context_menu.add(self._protein_natural_sequence_menu)
        flare.callbacks.protein_residue_context_menu.add(self._residue_sequence_menu)

        print(f"Loaded {self.__class__.__module__}.{self.__class__.__name__}")

    @staticmethod
    def _parse_fasta_sequence(fasta_sequence):
        """Create a protein for each sequence.

        Returns
        -------
        list of (str : title, str : sequence)
            The titles and sequences create the proteins from.
        """
        sequences = []
        title = ""
        sequence = ""

        for line in fasta_sequence.split("\n"):
            line = line.strip()

            if line:
                if line[0] == ">":
                    if sequence:
                        sequences.append((title, sequence))
                        title = ""
                        sequence = ""
                    title = line[1:]
                else:
                    sequence += line.upper()

        if sequence:
            sequences.append((title, sequence))
        return sequences

    @staticmethod
    def _create_protein_from_sequence(project, title, sequence):
        """Create a protein with the title and sequence.

        Parameters
        ----------
        project : cresset.flare.project
            The project to add the proteins too.
        title : str or None
            The title for the new protein.
        sequence : str
            Sequence to add as a chain to the protein. Each residue in
            the chain will contain one carbon atom.
            The sequence looks line 'NFQKVE'
        """
        protein = project.proteins.new()
        if title:
            protein.title = title

        # Create the residues from the FASTA sequence
        with protein.batch_edit():
            for seq_num, code in enumerate(sequence):
                residue = protein.residues.new(
                    "A", HomologyModelingExtension.CODES.get(code, "XXX"), seq_num + 1
                )
                # Add a atom to each residue so its pickable
                # Set the x pos to the seq_num number so that all the atoms are
                # 1A apart to about the "Atom are to close" warnings
                # in the protein structure checker
                atom = protein.atoms.new(6, pos=(seq_num, 0, 0))
                atom.residue = residue

        protein.sequences[0].type = flare.Sequence.Type.Natural
        protein.sequences[0].alignment_group = 1

    @classmethod
    def _create_proteins_from_fasta(cls, project, fasta_sequence):
        """Create a protein for each sequence in `fasta_sequence`.

        Parameters
        ----------
        project : cresset.flare.Project
            The project to add the proteins too.
        fasta_sequence : str
            The FASTA sequences.
        """
        sequences = cls._parse_fasta_sequence(fasta_sequence)
        for title, sequence in sequences:
            cls._create_protein_from_sequence(project, title, sequence)

    @staticmethod
    def _protein_natural_sequence(protein):
        """Return the natural sequences for `protein`."""
        sequences = [seq for seq in protein.sequences if seq.type == flare.Sequence.Type.Natural]
        return sequences

    @classmethod
    def _mutate_protein_to_protein(cls, protein, target, picked_residues):
        """Mutate `protein` to match the sequence of 'target'.

        If `picked_residues` is not empty then only the residues in picked_residues
        will be mutated.
        """
        picked_residues_set = set(picked_residues)

        mutation = flare.Mutation()

        valid_names = set(HomologyModelingExtension.CODES.values())

        # Check that both proteins have the same number of Natural chains
        protein_seq = [seq for seq in protein.sequences if seq.type == flare.Sequence.Type.Natural]
        target_seq = [seq for seq in target.sequences if seq.type == flare.Sequence.Type.Natural]
        if len(protein_seq) != len(target_seq):
            QtWidgets.QMessageBox.critical(
                flare.main_window().widget(),
                "Incompatible proteins",
                "The two proteins must have the same number of protein chains: "
                "protein '{0}' has {1} chains, while protein '{2}' has {3} chains.".format(
                    protein.title, len(protein_seq), target.title, len(target_seq)
                ),
            )
            return

        last_oldres = None
        warnings = []
        for s in zip(protein_seq, target_seq):
            for oldres, newres in zip_longest(s[0], s[1]):
                oldres_picked_for_mutation = oldres is not None and (
                    len(picked_residues_set) == 0 or oldres in picked_residues_set
                )

                if newres is None and oldres is not None:
                    s[0].remove(oldres)
                elif oldres is None and last_oldres is not None and newres is not None:
                    try:
                        protein.residues.new(
                            s[0].chain, newres.name, last_oldres.seq_num + 1, last_oldres.icode
                        )
                    except flare.DuplicateError:
                        protein.residues.new(s[0].chain, newres.name, last_oldres.seq_num + 1, "Z")
                    idx = s[0].index(last_oldres) + 1
                    s[0].move(idx, protein.residues[-1])
                    last_oldres = s[0][idx]
                elif (
                    oldres is not None and oldres.name in valid_names and newres.name in valid_names
                ):
                    if oldres.name != newres.name and oldres_picked_for_mutation:
                        mutation.mutations[oldres] = newres.name
                    last_oldres = oldres
                elif (
                    oldres is not None
                    and newres is not None
                    and (newres.name not in valid_names or oldres.name not in valid_names)
                    and oldres_picked_for_mutation
                ):
                    warnings.append(
                        f"Cannot mutate residue {str(oldres)} to {str(newres)} as it is not a standard amino acid"  # noqa E203
                    )

        warnings.extend(mutation.setup_warnings())
        warnings.insert(0, "Mutating {0} residues".format(len(mutation.mutations)))
        if mutation.setup_errors():
            if flare.main_window():
                QtWidgets.QMessageBox.critical(
                    flare.main_window().widget(), "Error", "\n".join(mutation.setup_errors())
                )
        else:
            if flare.main_window():
                QtWidgets.QMessageBox.warning(
                    flare.main_window().widget(), "Mutating residues", "\n".join(warnings)
                )
            mutation.start()
            mutation.wait()

    @classmethod
    def proteins_from_file(cls):
        """Add proteins from a FASTA file path entered by the user."""
        project = flare.main_window().project
        fasta_file_path, _ = QtWidgets.QFileDialog.getOpenFileName(flare.main_window().widget())
        if fasta_file_path:
            try:
                with open(fasta_file_path, "r") as fasta_file:
                    fasta_sequence = fasta_file.read()
                    if fasta_sequence:
                        cls._create_proteins_from_fasta(project, fasta_sequence)
                    else:
                        QtWidgets.QMessageBox.critical(
                            flare.main_window().widget(), "No Data", "File was empty"
                        )
            except IOError as e:
                QtWidgets.QMessageBox.critical(
                    flare.main_window().widget(), "Exception", str(e.strerror)
                )

    @classmethod
    def sequences_to_file(cls):
        """Export sequence alignments to a FASTA file."""
        project = flare.main_window().project
        selected_sequences = {a.residue.sequence for a in flare.main_window().picked_atoms}
        selected_sequences_natural = {
            s for s in selected_sequences if s.type == flare.Sequence.Type.Natural
        }
        if selected_sequences and (not selected_sequences_natural):
            return
        proteins = {s.molecule for s in selected_sequences_natural}
        if proteins:
            proteins = [p for p in project.proteins if p in proteins]
        else:
            proteins = project.proteins
        sequence_list = sum(
            [[s for s in p.sequences if s.type == flare.Sequence.Type.Natural] for p in proteins],
            [],
        )
        if selected_sequences_natural:
            sequence_list = [s for s in sequence_list if s in selected_sequences_natural]
        if not sequence_list:
            return
        fasta_file_path, _ = QtWidgets.QFileDialog.getSaveFileName(
            flare.main_window().widget(), filter="*.fasta"
        )
        if fasta_file_path:
            try:
                with open(fasta_file_path, "w") as fasta_file:
                    p = None
                    for s in sequence_list:
                        if s.molecule != p:
                            p = s.molecule
                            title = p.title[: (cls.MAX_FASTA_LEN - 5)]
                            if len(title) < len(p.title):
                                title += "..."
                            fasta_file.write(">" + title + "\n")
                        for i in range(0, len(s.alignment), cls.MAX_FASTA_LEN):
                            fasta_file.write(
                                s.alignment[i : i + cls.MAX_FASTA_LEN] + "\n"  # noqa E203
                            )
            except IOError as e:
                QtWidgets.QMessageBox.critical(
                    flare.main_window().widget(), "Exception", str(e.strerror)
                )

    @classmethod
    def proteins_from_sequence(cls):
        """Add proteins from a FASTA string entered by the user."""
        fasta_sequence, ok = QtWidgets.QInputDialog.getMultiLineText(
            flare.main_window().widget(), "Protein FASTA sequence", "FASTA sequence:"
        )
        if fasta_sequence and ok:
            project = flare.main_window().project
            cls._create_proteins_from_fasta(project, fasta_sequence)

    @classmethod
    def mutate_selected_protein_to_sequence(cls):
        """Mutate a protein to the sequence of another protein."""
        if len(flare.main_window().project.proteins) < 2:
            QtWidgets.QMessageBox.critical(
                flare.main_window().widget(),
                "Error",
                "You must have at least two proteins to mutate the sequence of one to match the other.",  # noqa E203
            )
            return
        proteins = flare.main_window().selected_proteins

        if len(proteins) != 1:
            QtWidgets.QMessageBox.critical(
                flare.main_window().widget(), "Error", "One protein must be selected."
            )
        else:
            picked_residues = flare.main_window().picked_residues
            picked_residues = [res for res in picked_residues if res.molecule == proteins[0]]

            # Ask which protein we are mutating
            dialog = QtWidgets.QDialog(flare.main_window().widget())

            pchoice = QtWidgets.QComboBox()
            for idx, p in enumerate(flare.main_window().project.proteins):
                if p != proteins[0]:
                    pchoice.addItem(p.title, idx)

            mutate_picked_residues_only = QtWidgets.QCheckBox("Mutate picked residues only")
            mutate_picked_residues_only.setToolTip("Only mutate the picked residues")
            mutate_picked_residues_only.setChecked(False)
            mutate_picked_residues_only.setEnabled(len(picked_residues) > 0)

            buttons = QtWidgets.QDialogButtonBox(
                QtWidgets.QDialogButtonBox.Ok | QtWidgets.QDialogButtonBox.Cancel
            )
            buttons.accepted.connect(dialog.accept)
            buttons.rejected.connect(dialog.reject)

            # Layout out the widgets
            layout = QtWidgets.QVBoxLayout(dialog)
            layout.addWidget(
                QtWidgets.QLabel(
                    "This will mutate protein '{0}' to match the sequence of "
                    "another protein in this project.".format(proteins[0].title)
                )
            )

            label = QtWidgets.QLabel(
                "Which protein do you want to match the sequence of? "
                "Note that the sequences of these two proteins should be aligned!"
            )
            label.setWordWrap(True)
            layout.addWidget(label)
            layout.addWidget(pchoice)
            layout.addWidget(mutate_picked_residues_only)
            layout.addStretch(1)
            layout.addWidget(buttons)

            # Show the dialog, do the calculation if OK
            if dialog.exec_():
                idx = pchoice.currentData()
                target = flare.main_window().project.proteins[idx]
                cls._mutate_protein_to_protein(proteins[0], target, picked_residues)

            dialog.deleteLater()

    @staticmethod
    def _largest_seq_num(protein, chain):
        """Return the largest seq number in the `protein` for the `chain`."""
        largest = None
        for residue in protein.residues:
            if chain == residue.chain:
                largest = residue.seq_num if largest is None else max(largest, residue.seq_num)
        return largest

    @classmethod
    def _renumber_residues(cls, residues, number_from, num_gaps):
        """Renumber the residues starting counting from `number_from`.

        Parameters
        ----------
        residues : list(cresset.flare.Residue)
            The residues to renumber.
        number_from : int
            The number to start counting from.
        num_gaps : bool
            If True then gaps will be counted when updating the
            seq number.

        Returns
        -------
        int
            The last number+1 which was given to a residue.
        """
        cls._check_renumber_residues_allowed(residues, number_from, num_gaps)

        for residue in residues:
            if residue is not None:
                residue.icode = ""
                residue.seq_num = number_from

            if residue is not None or num_gaps:
                number_from += 1
        return number_from

    @staticmethod
    def _check_renumber_residues_allowed(residues, number_from, num_gaps):
        """Check renumber residues will not cause a duplicate id`.

        Parameters
        ----------
        residues : list(cresset.flare.Residue)
            The residues to renumber.
        number_from : int
            The number to start counting from.
        num_gaps : bool
            If True then gaps will be counted.

        Raises
        ------
        flare.DuplicateError
            If renumbering the residues will cause a duplicate id.
        """
        existing_residues_ids = set()
        for residue in residues[0].molecule.residues:
            if residue not in residues:
                existing_residues_ids.add((residue.chain, residue.seq_num, residue.icode))

        new_residues_ids = set()
        for residue in residues:
            if residue is not None:
                new_residues_ids.add((residue.chain, number_from, ""))

            if residue is not None or num_gaps:
                number_from += 1

        duplicate_residue_ids = sorted(
            existing_residues_ids.intersection(new_residues_ids), key=lambda id: id[1]
        )

        if len(duplicate_residue_ids) != 0:
            raise flare.DuplicateError(
                "renumbering will cause a duplicate of residue "
                f'"{duplicate_residue_ids[0][0]} {duplicate_residue_ids[0][1]}'
                f'{duplicate_residue_ids[0][2]}"'
            )

    @classmethod
    def _renumber_sequence_starting_at(cls, residue, number_from=1, num_gaps=False):
        """Renumber the residues in a chain starting at `residue`.

        Parameters
        ----------
        residue : cresset.flare.Residue
            The residue to start number at. This will get the seq number of `number_from`.
        number_from : int
            The number to start counting from.
        num_gaps : bool
            If True then gaps will be counted when updating the
            seq number.
        """
        is_numbering = False
        for sequence in residue.molecule.sequences:
            if sequence == residue.sequence:
                is_numbering = True
                index = sequence.index(residue)
                number_from = cls._renumber_residues(sequence[index:], number_from, num_gaps)

            elif is_numbering and sequence.chain == residue.chain:
                number_from = cls._renumber_residues(sequence, number_from, num_gaps)

    @staticmethod
    def _first_non_gap_residue(sequence):
        """Return the first residue in a sequence which is not a gap."""
        for residue in sequence:
            if residue is not None:
                return residue
        return None

    @classmethod
    def _renumber_protein_residues(cls, protein, num_start, num_gaps):
        """Renumbers off natural sequences in `protein`."""
        with protein.batch_edit():
            chains = set()
            for sequence in protein.sequences:
                if sequence.chain not in chains:
                    chains.add(sequence.chain)
                    largest = cls._largest_seq_num(protein, sequence.chain)
                    residue = cls._first_non_gap_residue(sequence)
                    if largest is not None and residue is not None:
                        # Renumber to a large value first to prevent clashes.
                        cls._renumber_sequence_starting_at(residue, largest + 1, num_gaps)
                        cls._renumber_sequence_starting_at(residue, num_start, num_gaps)

    @classmethod
    def renumber_residues_seq_num(cls):
        """Renumber picked residues or all residues in selected proteins if none are picked."""
        num_start, num_gaps = cls._show_renumber_sequence_configuration_dialog()
        if num_start is None or num_gaps is None:
            # Cancel was pressed
            return

        picked_residues = flare.main_window().picked_residues
        if picked_residues:
            if not cls._residues_in_same_protein_chain(picked_residues):
                QtWidgets.QMessageBox.critical(
                    flare.main_window().widget(),
                    "Error",
                    "All selected residues for renumbering must be "
                    "from the same protein and the same chain.",
                )
                return

            if not cls._residues_are_contiguous(picked_residues):
                QtWidgets.QMessageBox.critical(
                    flare.main_window().widget(),
                    "Error",
                    "The selected residues for renumbering must be contiguous.",
                )
                return

            try:
                cls._renumber_residues(picked_residues, num_start, num_gaps)
                message = f"Renumbering complete for {len(picked_residues)} selected residues."
                QtWidgets.QMessageBox.information(flare.main_window().widget(), "Done", message)
            except flare.DuplicateError:
                QtWidgets.QMessageBox.critical(
                    flare.main_window().widget(),
                    "Error",
                    "Sequentially renumbering just the selected residues "
                    "results in duplicate numbers elsewhere in the protein.",
                )
        else:
            proteins = flare.main_window().selected_proteins
            for protein in proteins:
                cls._renumber_protein_residues(protein, num_start, num_gaps)
            message = f"Renumbering complete for {len(proteins)} proteins."
            QtWidgets.QMessageBox.information(flare.main_window().widget(), "Done", message)

    @staticmethod
    def _residues_in_same_protein_chain(residues):
        """Check whether all residues are in the same protein and the same chain."""
        if len(residues):
            residue_0_protein = residues[0].molecule
            residue_0_chain = residues[0].chain
            for residue in residues:
                protein = residue.molecule
                chain = residue.chain
                if protein != residue_0_protein or chain != residue_0_chain:
                    return False
        return True

    @staticmethod
    def _residues_are_contiguous(residues):
        """Check whether residues are contiguous in the associated protein."""
        if len(residues):
            residues_set = set(residues)
            sequence = residues[0].sequence
            in_sequence = False
            contiguous_range_found = False
            for residue in sequence:
                if residue:  # Ignore gaps
                    if residue in residues_set:
                        if contiguous_range_found and not in_sequence:
                            return False
                        contiguous_range_found = True
                        in_sequence = True
                        residues_set.remove(residue)
                    else:
                        in_sequence = False

            return len(residues_set) == 0
        return False

    def _residue_sequence_menu(self, menu, residue):
        """Add a item to the `menu` for renumbering a chain."""
        action = menu.addAction("Number Residues From Here")
        action.triggered.connect(lambda: self._number_residues_starting_at(residue))

    def _number_residues_starting_at(self, residue):
        """Renumber residues in a chain starting at `residue`."""
        num_start, num_gaps = self._show_renumber_sequence_configuration_dialog()
        if num_start is None or num_gaps is None:
            # Cancel was pressed
            return

        try:
            residues_to_be_renumbered = self._residues_after_residue(residue)
            self._check_renumber_residues_allowed(residues_to_be_renumbered, num_start, num_gaps)

            with residue.molecule.batch_edit():
                largest = self._largest_seq_num(residue.molecule, residue.chain)
                if largest is not None and residue is not None:
                    # Renumber to a large value first to prevent clashes.
                    self._renumber_sequence_starting_at(residue, largest + 1, num_gaps)
                    self._renumber_sequence_starting_at(residue, num_start, num_gaps)

            message = f"Done over chain {residue.chain}, starting at {str(residue)}."
            QtWidgets.QMessageBox.information(flare.main_window().widget(), "Done", message)
        except flare.DuplicateError:
            QtWidgets.QMessageBox.critical(
                flare.main_window().widget(),
                "Error",
                "Renumbering these residues "
                "results in duplicate numbers elsewhere in the protein.",
            )

    @staticmethod
    def _show_renumber_sequence_configuration_dialog():
        """Show a widget which asks for the renumber config options."""
        parent_widget = flare.main_window().widget()
        dialog = QtWidgets.QDialog(parent_widget)

        button_box = QtWidgets.QDialogButtonBox(
            QtWidgets.QDialogButtonBox.Ok | QtWidgets.QDialogButtonBox.Cancel, dialog
        )

        button_box.accepted.connect(dialog.accept)
        button_box.rejected.connect(dialog.reject)

        number_from = QtWidgets.QSpinBox(dialog)
        number_from.setRange(-999999, 999999)
        number_from.setValue(1)

        number_gaps = QtWidgets.QCheckBox(dialog)
        number_gaps.setChecked(False)

        form_layout = QtWidgets.QFormLayout()
        form_layout.addRow("Number From:", number_from)
        form_layout.addRow("Number Gaps:", number_gaps)

        layout = QtWidgets.QVBoxLayout(dialog)
        layout.addLayout(form_layout)
        layout.addStretch(1)
        layout.addWidget(button_box)

        num_from = None
        num_gaps = None

        # Show the dialog and get the data out of the dialog if Ok was pressed.
        if dialog.exec_():
            num_from = number_from.value()
            num_gaps = number_gaps.isChecked()

        dialog.deleteLater()
        return num_from, num_gaps

    @staticmethod
    def _residues_after_residue(residue):
        """Return a list containing `residue` and all residues after `residue` in the same chain."""
        residues = []
        is_in_chain = False
        for sequence in residue.molecule.sequences:
            if sequence == residue.sequence:
                is_in_chain = True
                index = sequence.index(residue)
                residues.extend(sequence[index:])

            elif is_in_chain and sequence.chain == residue.chain:
                residues.extend(sequence)
        return residues

    def _protein_natural_sequence_menu(self, menu, sequence):
        """Add to the sequence menu an item for editing the sequence."""
        if sequence.type == flare.Sequence.Type.Natural:
            action = menu.addAction("Set Alignment From FASTA")
            action.triggered.connect(lambda: self._set_fasta_alignment(sequence))

    def _set_fasta_alignment(self, sequence):
        """Align the sequence to the user FASTA file."""
        parent_widget = flare.main_window().widget()

        fasta_sequence, ok = QtWidgets.QInputDialog.getMultiLineText(
            parent_widget, "Protein FASTA sequence", "FASTA sequence:", sequence.alignment
        )
        if fasta_sequence and ok:
            sequences = self._parse_fasta_sequence(fasta_sequence)
            if len(sequences) != 1:
                QtWidgets.QMessageBox.critical(
                    parent_widget, "Error", "Only 1 FASTA sequence is allowed."
                )
            else:
                _, fasta_sequence = sequences[0]
                try:
                    sequence.alignment = fasta_sequence
                except ValueError:
                    QtWidgets.QMessageBox.critical(parent_widget, "Error", "Invalid alignment.")

    @classmethod
    def select_sequence(cls):
        """Select all residues which are aligned to the picked residues."""
        picked_residues = cls._picked_residues()
        picked_columns = cls._picked_columns(picked_residues)
        cls._pick_atoms_in_columns(picked_columns)

    @staticmethod
    def _picked_residues():
        """Return residues with 1 or more picked atoms."""
        picked_atoms = flare.main_window().picked_atoms
        picked_residues = set()
        for atom in picked_atoms:
            picked_residues.add(atom.residue)
        return picked_residues

    @staticmethod
    def _picked_columns(picked_residues):
        """Return a dict of alignment group to set of picked columns indexes."""
        picked_columns = defaultdict(set)
        for residue in picked_residues:
            sequence = residue.sequence
            if sequence and sequence.type == flare.Sequence.Type.Natural:
                picked_columns[sequence.alignment_group].add(sequence.index(residue))
        return picked_columns

    @staticmethod
    def _pick_atoms_in_columns(picked_columns):
        """Pick all atoms which are in the `picked_columns`."""
        picked_atoms = []
        for protein in flare.main_window().project.proteins:
            for sequence in protein.sequences:
                if sequence.type == flare.Sequence.Type.Natural:
                    for column in picked_columns[sequence.alignment_group]:
                        if column < len(sequence) and sequence[column] is not None:
                            picked_atoms.extend(sequence[column].atoms)

        flare.main_window().picked_atoms = picked_atoms
