# -*- coding: utf-8 -*-
# Copyright (C) 2023 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset

WIDTH_TOOLTIP = """
Width of drawing in px (default = 800px).
Press return to apply changes.
""".strip()

HEIGHT_TOOLTIP = """
Height of drawing in px (default = 600px).
Press return to apply changes.
""".strip()

PLOT_TOOLTIP = """
Click circled residues to highlight them in the 3D window.
""".strip()

SIM3D_TOOLTIP = """
Generate plot coordinates from 3D.
""".strip()

ROTATE_TOOLTIP = """
Rotate plot counterclockwise in degrees.
""".strip()

COPY_TOOLTIP = """
Copy plot to clipboard in SVG format.
""".strip()

SAVE_TOOLTIP = """
Save plot to file in SVG format.
""".strip()

FLIP_TOOLTIP = """
Flip plot horizontally.
"""
