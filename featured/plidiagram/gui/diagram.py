# -*- coding: utf-8 -*-
# Copyright (C) 2023 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset

from cresset import flare
from rdkit.Geometry import Point2D, Point3D
from PySide2.QtCore import Qt, QByteArray, QMimeData, QTimer
from PySide2.QtWidgets import (
    QDialog,
    QFileDialog,
    QCheckBox,
    QPushButton,
    QLabel,
    QSpinBox,
    QGridLayout,
    QHBoxLayout,
    QVBoxLayout,
    QSpacerItem,
    QSizePolicy,
    QApplication,
)
from ..lib.contact import find_contacts, set_ligand_contact_centers
from ..lib.rdmol import get_mol_from_ligand, prepare_2d_coords, new_mol_with_residues
from ..lib.svg import create_view_and_svg
from ..lib.optimize import optimize_residues
from .tooltip import (
    SIM3D_TOOLTIP,
    COPY_TOOLTIP,
    SAVE_TOOLTIP,
    FLIP_TOOLTIP,
    ROTATE_TOOLTIP,
    WIDTH_TOOLTIP,
    HEIGHT_TOOLTIP,
)
from .svgwidget import SVGWidget
from .labeledslider import LabeledSlider
import numpy as np


DEFAULT_WIDTH = 800
DEFAULT_HEIGHT = 600
WINDOW_HEIGHT_OFFSET = 140


class DiagramWindow(QDialog):
    ContactMenu = {
        "h_bonds": dict(title="Hydrogen bonds", pos=(0, 0)),
        "w_h_bonds": dict(title="Weak hydrogen bonds", pos=(0, 1)),  # special
        "steric_clashes": dict(title="Steric clashes", pos=(0, 2)),
        "cation_pi": dict(title="Cation-Pi", pos=(0, 3)),
        "sulfur_lone_pair": dict(title="Sulfur lone pairs", pos=(1, 0)),
        "halogen_bonds": dict(title="Halogen bonds", pos=(1, 1)),
        "aromatic_aromatic": dict(title="Aromatic-Aromatic", pos=(1, 2)),
        "salt_bridge": dict(title="Salt bridge/Metal", pos=(1, 3)),
        "hydrophobic_contacts": dict(title="Hydrophobic contacts", pos=(2, 0)),
    }

    def __init__(self, pose, protein, contact_flags, ref3d_flag, parent=None):
        super().__init__(parent)
        self._pose = pose
        if isinstance(pose, flare.Pose):
            self.ligand = pose.ligand
        else:
            self.ligand = pose
        self.protein = protein
        self._contact_flags = contact_flags
        self._ref3d_flag = ref3d_flag

        self._update_timer = QTimer()
        self._update_timer.setSingleShot(True)
        self._update_timer.setInterval(200)

        # Set appearance

        self.title = f"{self.protein.title} {self.ligand.title}"
        self._set_window(self.title)
        self._ui_elements()
        self._ui_defaults()
        self._ui_tooltips()
        self._ui_layout()

        # Callback functions

        self._checkbox_3D.clicked.connect(self._update_draw)
        self._rot_edit.valueChanged.connect(self._on_rotation_change)
        self._flip_checkbox.clicked.connect(self._update_draw)

        for name in self._contact_flags.available_contacts:
            chkbox = getattr(self, f"_checkbox_{name}")
            what = self.ContactMenu[name]["title"]
            chkbox.setToolTip(f"Show/hide {what}.")
            chkbox.clicked.connect(self._update_draw)

        flare.callbacks.selected_ligands_changed.add(self._reactivate)
        self._button_copy.clicked.connect(self._to_clipboard)
        self._button_save.clicked.connect(self._to_file)
        self._rot_slider._slider.valueChanged.connect(self._on_rotation_change)
        self._size_width.valueChanged.connect(self._on_size_change)
        self._size_height.valueChanged.connect(self._on_size_change)
        self._update_timer.timeout.connect(self._update_draw)

        self.setSizeGripEnabled(True)
        self.setMinimumHeight(400)

    def _set_window(self, title):
        self.setWindowTitle(title)
        self.setModal(False)
        self.setWindowFlag(Qt.WindowMinimizeButtonHint, True)

    def _ui_elements(self):
        self._size_label = QLabel("Size:")

        self._size_width = QSpinBox()
        self._size_width.setRange(0, 3000)
        self._size_width.setSingleStep(100)
        self._size_width.setKeyboardTracking(False)

        self._size_x = QLabel("  x  ")

        self._size_height = QSpinBox()
        self._size_height.setRange(0, 3000)
        self._size_height.setSingleStep(100)
        self._size_height.setKeyboardTracking(False)

        self._checkbox_3D = QCheckBox("Simulate 3D")
        self._button_copy = QPushButton("Copy")
        self._button_save = QPushButton("Save")
        self._depict_widget = self._get_svg_widget(DEFAULT_WIDTH, DEFAULT_HEIGHT)
        self._flip_checkbox = QCheckBox("Flip")
        self._rot_label = QLabel("Rotate:")
        self._rot_slider = LabeledSlider(parent=self)
        self._rot_slider._slider.setSingleStep(5)
        self._rot_edit = QSpinBox()
        self._rot_edit.setRange(-180, 180)
        self._rot_edit.setSingleStep(self._rot_slider._slider.singleStep())

        self._checkbox_grid = QGridLayout()
        for name in self._contact_flags.available_contacts:
            setattr(self, f"_checkbox_{name}", QCheckBox(DiagramWindow.ContactMenu[name]["title"]))
            chkbox = getattr(self, f"_checkbox_{name}")
            chkbox.setChecked(getattr(self._contact_flags, name))
            self._checkbox_grid.addWidget(chkbox, *DiagramWindow.ContactMenu[name]["pos"], 1, 1)
        self._checkbox_grid.addItem(
            QSpacerItem(1, 1, QSizePolicy.Expanding, QSizePolicy.Preferred), 0, 4, 1, 1, Qt.AlignTop
        )

    def _ui_tooltips(self):
        self._size_width.setToolTip(WIDTH_TOOLTIP)
        self._size_height.setToolTip(HEIGHT_TOOLTIP)
        self._checkbox_3D.setToolTip(SIM3D_TOOLTIP)
        self._button_copy.setToolTip(COPY_TOOLTIP)
        self._button_save.setToolTip(SAVE_TOOLTIP)
        self._flip_checkbox.setToolTip(FLIP_TOOLTIP)
        self._rot_edit.setToolTip(ROTATE_TOOLTIP)
        self._rot_slider.setToolTip(ROTATE_TOOLTIP)

    def _ui_defaults(self):
        self._size_width.setValue(DEFAULT_WIDTH)
        self._size_height.setValue(DEFAULT_HEIGHT)
        self._checkbox_3D.setChecked(self._ref3d_flag.value)
        self._rot_edit.setValue(0)

    def _ui_layout(self):
        layout = QVBoxLayout()
        layout.setContentsMargins(5, 5, 5, 5)
        layout.setSpacing(0)

        hbox = QHBoxLayout()
        hbox.addWidget(self._button_copy)
        hbox.addWidget(self._button_save)
        hbox.addSpacing(20)
        hbox.addWidget(self._rot_label)
        hbox.addSpacing(5)
        hbox.addWidget(self._rot_slider)
        hbox.addSpacing(20)
        hbox.addWidget(self._rot_edit)
        hbox.addSpacing(20)
        hbox.addWidget(self._flip_checkbox)
        hbox.addSpacing(20)
        hbox.addStretch()
        hbox.addWidget(self._size_label)
        hbox.addSpacing(5)
        hbox.addWidget(self._size_width)
        hbox.addWidget(self._size_x)
        hbox.addWidget(self._size_height)

        self._depict_layout = QVBoxLayout()
        self._depict_layout.addWidget(self._depict_widget)

        layout.addLayout(self._depict_layout)
        layout.addWidget(self._checkbox_3D)
        layout.addLayout(self._checkbox_grid)
        layout.addLayout(hbox)

        self.setLayout(layout)

    def _on_rotation_change(self, value):
        if self._rot_slider._slider.value() != value:
            self._rot_slider._slider.setValue(value)
        if self._rot_edit.value() != value:
            self._rot_edit.setValue(value)

        self._update_timer.start()

    def _on_size_change(self):
        width = self._size_width.value()
        height = self._size_height.value()
        self.resize(
            width + 10, height + WINDOW_HEIGHT_OFFSET
        )  # margin*2, margin*2 + 3 or 4 lines + 50

    def _get_svg_widget(self, width, height, theta=0, flip=False):
        contacts = find_contacts(self._pose, self.protein, self._contact_flags)
        mol = get_mol_from_ligand(self._pose, contacts)
        if self._ref3d_flag.value:
            prepare_2d_coords(mol, ref=self._pose)
        else:
            prepare_2d_coords(mol, ref=None)

        conf = mol.GetConformer(0)
        ps = conf.GetPositions()
        C = ps[:, :2].mean(axis=0)
        c, s = np.cos(np.radians(theta)), np.sin(np.radians(theta))
        rot = np.array([[c, -s], [s, c]])
        if flip:
            flip = np.array([[-1, 0], [0, 1]])
        else:
            flip = np.eye(2)
        for i in range(ps.shape[0]):
            q = flip.dot(rot.dot(ps[i, 0:2] - C)) + C
            conf.SetAtomPosition(i, Point3D(q[0], q[1], 0))

        set_ligand_contact_centers(mol, contacts)
        resnames, res_pos = optimize_residues(mol, contacts)
        mol = new_mol_with_residues(mol, resnames, res_pos)
        view, svg = create_view_and_svg(mol, contacts, self.title, width, height)

        self.svg = svg

        self.res_coords = {
            name: view.GetDrawCoords(Point2D(*p)) for name, p in zip(resnames, res_pos)
        }
        depict_widget = SVGWidget(parent=self)

        return depict_widget

    def _to_clipboard(self):
        clipboard = QApplication.clipboard()
        mime = QMimeData()
        mime.setImageData(self._depict_widget.grab())
        mime.setData("image/svg+xml", QByteArray(self.svg.encode("utf-8")))

        clipboard.setMimeData(mime)

    def _to_file(self):
        path, _ = QFileDialog.getSaveFileName(
            parent=self, caption="Choose save location", filter="Scalable Vector Graphics (*.svg)"
        )
        if len(path) > 0:
            with open(path, "w") as f:
                f.write(self.svg)

    def _reactivate(self, ligands):
        for ligand in ligands:
            if ligand == self.ligand:
                self.activateWindow()

    def _update_draw(self):
        try:
            width = self._size_width.value()
        except ValueError:
            width = DEFAULT_WIDTH
            self._size_width.setValue(width)
        try:
            height = self._size_height.value()
        except ValueError:
            height = DEFAULT_HEIGHT
            self._size_height.setValue(height)
        self._ref3d_flag.value = self._checkbox_3D.isChecked()
        try:
            theta = float(self._rot_edit.value())
        except ValueError:
            theta = 0
            self._rot_edit.setValue(0)
        flip = self._flip_checkbox.isChecked()

        for name in self._contact_flags.available_contacts:
            chkbox = getattr(self, f"_checkbox_{name}")
            setattr(self._contact_flags, name, chkbox.isChecked())

        if self._checkbox_h_bonds.isChecked():
            self._checkbox_w_h_bonds.setEnabled(True)
        else:
            self._checkbox_w_h_bonds.setEnabled(False)
            self._checkbox_w_h_bonds.setChecked(False)

        self._depict_layout.takeAt(0)
        self._depict_widget.deleteLater()
        self._depict_widget = self._get_svg_widget(width, height, theta=theta, flip=flip)
        self._depict_layout.addWidget(self._depict_widget)

    def resizeEvent(self, event):
        super().resizeEvent(event)

        width = self.size().width() - 10
        height = self.size().height() - WINDOW_HEIGHT_OFFSET
        if width <= 0 or height <= 0:
            width = DEFAULT_WIDTH
            height = DEFAULT_HEIGHT

        self._size_width.blockSignals(True)
        self._size_width.setValue(width)
        self._size_width.blockSignals(False)

        self._size_height.blockSignals(True)
        self._size_height.setValue(height)
        self._size_height.blockSignals(False)

        self._update_timer.start()
