# -*- coding: utf-8 -*-
# Copyright (C) 2023 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset

from cresset import flare
from PySide2.QtWidgets import QMessageBox
from .lib.contact import ContactFlags
from .gui.diagram import DiagramWindow
import textwrap
import os


__version__ = "0.9.8"


@flare.extension
class PLIDiagram:
    """Show PLI Diagram (or Contacts in 2D) in a popup window.

    Open a window for each ligand associated by protein.
    """

    TOOLTIP = textwrap.dedent(
        """
        Show 2D Diagram of ligand-protein interactions for one or more selected ligands
        with an associated protein.
        """
    ).strip()

    def load(self):
        """Load the extension."""
        tab = flare.main_window().ribbon["Home"]
        group = tab["Measurements"]

        control = group.add_button("Interaction Map", self._run)
        control.tooltip = self.TOOLTIP
        control.tip_key = "MI"
        control.load_icon(_package_file("2D-contacts.png"))

        flare.callbacks.ligand_context_menu.add(self._ligand_menu)

        print(f"Loaded {self.__class__.__module__}.{self.__class__.__name__} {__version__}")

        self.contact_flags = ContactFlags()
        self.margin = 0
        self.ref3d_flag = Flag(False)

    def _ligand_menu(self, menu, ligand):
        action = menu.addAction("Interaction Map")
        action.triggered.connect(lambda: self._run())

    def _run(self, dx=20, dy=10, camera_zoom=10):
        main_window = flare.main_window()
        camera = main_window.camera

        ligands = [ligand for ligand in main_window.selected_ligands if ligand.protein]

        load_ligands = True
        total_ligands = len(ligands)

        if total_ligands == 0:
            QMessageBox.critical(
                main_window.widget(),
                "Error",
                "Select ligand(s) with associated protein",
                QMessageBox.Ok,
            )
            return
        elif total_ligands > 10:
            msgbox_ret = QMessageBox.warning(
                main_window.widget(),
                "Interaction Map Warning",
                f"{total_ligands} ligands with associated protein selected, this will take a"
                + " while to load. Do you want to continue?",
                QMessageBox.Yes | QMessageBox.No,
            )
            load_ligands = msgbox_ret == QMessageBox.Yes

        if load_ligands:
            app_width = main_window.widget().size().width()
            app_height = main_window.widget().size().height()
            app_x = main_window.widget().pos().x()
            app_y = main_window.widget().pos().y()
            x_start = int(app_x + app_width / 8)
            y_start = int(app_y + app_height / 8)
            for i in range(total_ligands):
                ligand = ligands[i]
                new_x = x_start + dx * i
                new_y = y_start + dy * i
                protein = ligand.protein
                main_window.selected_proteins = [protein]
                camera.fit_to_window([ligand])
                camera.zoom(camera_zoom)

                if len(ligand.poses) == 0:
                    pose = ligand
                else:
                    pose = ligand.preferred_pose

                w = DiagramWindow(
                    pose, protein, self.contact_flags, self.ref3d_flag, parent=main_window.widget()
                )
                w.show()
                w.move(new_x, new_y)


class Flag:
    def __init__(self, initial_value):
        self.value = initial_value


def _package_file(file_name):
    """Return the path to the file `file_name` in this package."""
    return os.path.join(os.path.dirname(os.path.realpath(__file__)), file_name)
