# -*- coding: utf-8 -*-
# Copyright (C) 2023 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset

from rdkit import Chem
from rdkit.Chem.rdDepictor import SetPreferCoordGen
from rdkit.Chem.rdDepictor import GenerateDepictionMatching3DStructure, Compute2DCoords


def find_atom_by_flare_index(mol, i):
    for atom in mol.GetAtoms():
        if atom.GetIntProp("flare_atom_index") == i:
            return atom
    return None


def get_mol_from_ligand(ligand, contacts):
    mol = ligand.to_rdmol()
    Chem.AssignStereochemistry(mol, cleanIt=True)
    for atom, flare_atom in zip(mol.GetAtoms(), ligand.atoms):
        atom.SetIntProp("flare_atom_index", flare_atom.index)

    in_contacts = set()
    for contact in contacts:
        for i in contact.ligand_atom_idxs:
            in_contacts.add(i)
    in_contacts = sorted(in_contacts)
    del_Hs = []
    for atom in mol.GetAtoms():
        if atom.GetAtomicNum() == 1 and atom.GetIntProp("flare_atom_index") not in in_contacts:
            del_Hs.append(atom.GetIdx())
    del_Hs.sort(reverse=True)

    tmp_mol = Chem.RWMol(mol)
    for i in del_Hs:
        H = tmp_mol.GetAtomWithIdx(i)
        parent_atom = list(H.GetNeighbors())[0]
        parent_atom.SetNumExplicitHs(parent_atom.GetNumExplicitHs() + 1)
        tmp_mol.RemoveAtom(i)
    mol = tmp_mol.GetMol()
    return mol


def prepare_2d_coords(mol, coordgen=True, ref=None):
    SetPreferCoordGen(coordgen)
    if ref:
        refmol = ref.to_rdmol()
        GenerateDepictionMatching3DStructure(mol, refmol)
    else:
        Compute2DCoords(mol)


def new_mol_with_residues(mol, resnames, res_pos):
    tmp_mol = Chem.RWMol(mol)
    conf = tmp_mol.GetConformer(0)
    for i in range(len(res_pos)):
        pos = res_pos[i]
        idx = tmp_mol.AddAtom(Chem.Atom(0))
        conf.SetAtomPosition(idx, [pos[0], pos[1], 0])
    mol = tmp_mol.GetMol()
    res_i = 0
    for atom in mol.GetAtoms():
        if atom.GetAtomicNum() == 0:
            resname = resnames[res_i]
            atom.SetProp("resname", resname)
            res_i += 1
    return mol
