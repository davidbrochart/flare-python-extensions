# -*- coding: utf-8 -*-
# Copyright (C) 2023 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset

import numpy as np
import random
from .rdmol import find_atom_by_flare_index


random.seed(0)  # to obtain reproduced results


def optimize_residues(mol, contacts):
    contact_map = _get_res_id_to_contact_map(contacts)
    resnames = [t[0] for t in contact_map]
    res_pos, favored, unfavored = _prepare_residue_positions(mol, contact_map)
    _montecarlo(mol, res_pos, favored, unfavored)
    return resnames, res_pos


def _get_res_id_to_contact_map(contacts):
    contact_map = {}
    for contact in contacts:
        res_id = contact.get_res_id()
        if res_id not in contact_map:
            contact_map[res_id] = []
        found_dup = False
        for i in range(len(contact_map[res_id])):
            ref = contact_map[res_id][i]
            if (
                contact.ligand_atom_idxs == ref.ligand_atom_idxs
                and contact.get_res_id() == ref.get_res_id()
            ):
                if contact.distance < ref.distance:
                    contact_map[res_id][i] = contact
                found_dup = True
                break
        if not found_dup:
            contact_map[res_id].append(contact)
    return sorted(contact_map.items(), key=lambda t: t[0])


def _prepare_residue_positions(mol, contact_map):
    N = len(contact_map)
    res_pos = [[0, 0] for i in range(N)]

    favored = []
    unfavored = []

    for i, (res_id, contacts) in enumerate(contact_map):
        fav_per_res = []
        mcenters = []
        unfav_per_res = []
        for contact in contacts:
            if contact.mc_flag:
                mcenters.append(contact.center)
            else:
                atom_idx = find_atom_by_flare_index(mol, contact.ligand_atom_idxs[0]).GetIdx()
                fav_per_res.append(atom_idx)
        fav_per_res = sorted(fav_per_res)
        for atom in mol.GetAtoms():
            if atom.GetIdx() not in fav_per_res:
                unfav_per_res.append(atom.GetIdx())
        unfav_per_res = sorted(unfav_per_res)

        conf = mol.GetConformer(0)

        favps = [list(conf.GetAtomPosition(i))[:2] for i in fav_per_res]
        unfavps = [list(conf.GetAtomPosition(i))[:2] for i in unfav_per_res]

        favps += mcenters
        favored.append(favps)
        unfavored.append(unfavps)
        res_pos[i] = list(np.array(favps).mean(axis=0))

    return res_pos, favored, unfavored


def _calculate_penalty(x, x0, k, is_fav=False):
    d = [x[0] - x0[0], x[1] - x0[1]]
    r = np.linalg.norm(d)
    if is_fav:
        return abs(k - r) ** 3
    else:
        if r < k:
            return (k - r) ** 3
        else:
            return 0


def _montecarlo(mol, res_pos, favored, unfavored, nrounds=2, ntimes=200, factor=3.5):
    N = len(res_pos)
    conf = mol.GetConformer(0)
    coords = conf.GetPositions()[:, :2]
    x_min, y_min = coords.min(axis=0)
    x_max, y_max = coords.max(axis=0)

    width = (x_max - x_min) / 2
    height = (y_max - y_min) / 2

    for n in range(nrounds):
        for i in range(N):
            pos_start = res_pos[i]
            fav = favored[i]
            unfav = unfavored[i] + res_pos[:i] + res_pos[i + 1 :]  # noqa: E203
            penalty_ref = 0
            for v in fav:
                penalty_ref += _calculate_penalty(pos_start, v, factor, is_fav=True)
            for v in unfav:
                penalty_ref += _calculate_penalty(pos_start, v, factor, is_fav=False)
            p_ref = None
            for j in range(ntimes):
                penalty = 0
                dx = 2 * width * (random.random() - 0.5)
                x = pos_start[0] + dx
                dy = 2 * height * (random.random() - 0.5)
                y = pos_start[1] + dy
                p = [x, y]
                try:
                    for v in fav:
                        penalty += _calculate_penalty(p, v, factor, is_fav=True)
                        if penalty_ref < penalty:
                            raise Exception("exceeded")
                except Exception:
                    continue
                try:
                    for v in unfav:
                        penalty += _calculate_penalty(p, v, factor, is_fav=False)
                        if penalty_ref < penalty:
                            raise Exception("exceeded")
                except Exception:
                    continue
                penalty_ref = penalty
                p_ref = p
            if p_ref is not None:
                res_pos[i] = p_ref
        width /= 2
        height /= 2
