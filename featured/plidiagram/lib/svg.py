# -*- coding: utf-8 -*-
# Copyright (C) 2023 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset

from cresset import flare
from rdkit.Geometry import Point2D
from rdkit.Chem.Draw import rdMolDraw2D
import math
from .svg_tags import WATER, INTERACTION, RESIDUE, TITLE


Strength = flare.Measurement.Strength

# Keep it for comparison
RASMOL_COLORS = {
    "ASP": "#E60A0A",
    "GLU": "#E60A0A",
    "CYS": "#E6E600",
    "MET": "#E6E600",
    "LYS": "#145AFF",
    "ARG": "#145AFF",
    "SER": "#FA9600",
    "THR": "#FA9600",
    "PHE": "#3232AA",
    "TYR": "#3232AA",
    "ASN": "#00DCDC",
    "GLN": "#00DCDC",
    "GLY": "#EBEBEB",
    "LEU": "#0F820F",
    "VAL": "#0F820F",
    "ILE": "#0F820F",
    "ALA": "#C8C8C8",
    "TRP": "#B45AB4",
    "HIS": "#8282D2",
    "PRO": "#DC9682",
}

# SFT-20358
CRESSET_COLORS = {
    "ALA": "#8CFF8C",
    "ARG": "#00007C",
    "ASN": "#FF7C70",
    "ASP": "#A00042",
    "CYS": "#FFFF70",
    "GLN": "#FF4C4C",
    "GLU": "#660000",
    "GLY": "#FFFFFF",
    "HIS": "#7070FF",
    "ILE": "#004C00",
    "LEU": "#455E45",
    "LYS": "#4747B8",
    "MET": "#B8A042",
    "PHE": "#534C52",
    "PRO": "#525252",
    "SER": "#FF7042",
    "THR": "#B84C00",
    "TRP": "#4F4600",
    "TYR": "#8C704C",
    "VAL": "#FF8CFF",
}

# SFT-20358
SHAPELY_COLORS = {
    "ASP": "#A00042",
    "THR": "#A00042",
    "GLU": "#660000",
    "CYS": "#FFFF70",
    "MET": "#B8A042",
    "TYR": "#B8A042",
    "LYS": "#4747B8",
    "ARG": "#00007C",
    "SER": "#FF4C4C",
    "GLN": "#FF4C4C",
    "PHE": "#534C42",
    "PRO": "#534C42",
    "TRP": "#534C42",
    "ASN": "#FF7C70",
    "GLY": "#FFFFFF",
    "VAL": "#FFFFFF",
    "ILE": "#004C00",
    "LEU": "#455E45",
    "ALA": "#8CFF8C",
    "HIS": "#7070FF",
}

RESIDUE_COLORS = SHAPELY_COLORS
LIGHT_COLORS = ("#FFFFFF", "#FFFF70", "#8CFF8C")

HBOND_LINE_COLORS = {
    Strength.Weak: "#00D2CF",
    Strength.Average: "#00831B",
    Strength.Strong: "#00D300",
}

LINE_COLORS = {
    "steric_clashes": "#FF7300",
    "hydrophobic_contacts": "#7F7F7F",
    "halogen_bonds": "#B936A9",
}

# DEFAULT_LINE_COLOR = "#9307F7"
DEFAULT_LINE_COLOR = "#AE109E"
WATER_COLOR = "#E0FFFF"


def create_view_and_svg(mol, contacts, title, width=800, height=600):
    def __add_delta(p0, p1, q0, q1, delta):
        ulen = math.sqrt((q0 - p0) * (q0 - p0) + (q1 - p1) * (q1 - p1))
        orthovec = (-(q1 - p1) / ulen, (q0 - p0) / ulen)
        return q0 + orthovec[0] * delta, q1 + orthovec[1] * delta

    def __add_margin(p0, p1, q0, q1, delta):
        ulen = math.sqrt((q0 - p0) * (q0 - p0) + (q1 - p1) * (q1 - p1))
        unitvec = ((q0 - p0) / ulen, (q1 - p1) / ulen)
        return p0 + unitvec[0] * delta, p1 + unitvec[1] * delta

    def __get_angle_in_degree(p0, p1, q0, q1):
        if q0 - p0 == 0.0:
            return 90
        else:
            return math.atan((q1 - p1) / (q0 - p0)) * 180 / math.pi

    margin = 0
    conf = mol.GetConformer(0)

    x_min, y_min = [float(x) for x in conf.GetPositions()[:, :2].min(axis=0)]
    x_max, y_max = [float(x) for x in conf.GetPositions()[:, :2].max(axis=0)]

    view = rdMolDraw2D.MolDraw2DSVG(width, height)
    view.SetScale(
        width - 2 * margin, height - 2 * margin, Point2D(x_min, y_min), Point2D(x_max, y_max), mol
    )
    view.SetOffset(margin, margin)
    view.DrawMolecule(mol)

    svg = view.GetDrawingText()

    res_xy = {}
    for atom in mol.GetAtoms():
        if atom.GetAtomicNum() == 0 and atom.HasProp("resname"):
            resname = atom.GetProp("resname")
            res_xy[resname] = [round(x, 3) for x in list(view.GetDrawCoords(atom.GetIdx()))]

    # grouping for avoiding overlap

    contact_groups = {}
    for contact in contacts:
        ctype = contact.contact_type
        distance = round(contact.distance, 1)
        res_id = contact.get_res_id()
        p = [round(x, 3) for x in list(view.GetDrawCoords(Point2D(*contact.center)))]
        if (*p, res_id) not in contact_groups:
            contact_groups[(*p, res_id)] = []
        contact_groups[(*p, res_id)].append(contact)

    for idx, groups in contact_groups.items():
        if len(groups) == 1:
            p0, p1, res = idx
            contact = groups[0]
            ctype = contact.contact_type
            distance = round(contact.distance, 1)
            q0, q1 = res_xy[res]
            xm = (p0 + q0) / 2
            ym = (p1 + q1) / 2
            if ctype == "h_bonds":
                color = HBOND_LINE_COLORS[contact.strength]
            else:
                color = LINE_COLORS.get(ctype, DEFAULT_LINE_COLOR)
            if ctype == "hydrophobic_contacts":
                distance = ""
            p0_, p1_ = __add_margin(p0, p1, q0, q1, 10)
            deg = __get_angle_in_degree(p0_, p1_, q0, q1)
            if q0 - p0 < 0:
                xm_, ym_ = __add_delta(p0_, p1_, xm, ym, 2.5)
            else:
                xm_, ym_ = __add_delta(p0_, p1_, xm, ym, -2.5)
            svg += (
                INTERACTION.format(
                    p0_, p1_, q0, q1, color, xm_, ym_, color, deg, xm_, ym_, distance
                )
                + "\n"
            )
        elif 1 < len(groups):
            D = 22.5
            n = len(groups)
            p0, p1, res = idx
            q0, q1 = res_xy[res]
            for i in range(n):
                contact = groups[i]
                ctype = contact.contact_type
                distance = round(contact.distance, 1)
                q0_, q1_ = __add_delta(p0, p1, q0, q1, 2 * D * i / (n - 1) - D)
                xm = (p0 + q0_) / 2
                ym = (p1 + q1_) / 2

                if ctype == "h_bonds":
                    color = HBOND_LINE_COLORS[contact.strength]
                else:
                    color = LINE_COLORS.get(ctype, DEFAULT_LINE_COLOR)
                    if ctype == "hydrophobic_contacts":
                        distance = ""
                p0_, p1_ = __add_margin(p0, p1, q0_, q1_, 7.5)
                deg = __get_angle_in_degree(p0_, p1_, q0_, q1_)
                if q0_ - p0_ < 0:
                    xm_, ym_ = __add_delta(p0_, p1_, xm, ym, 2.5)
                else:
                    xm_, ym_ = __add_delta(p0_, p1_, xm, ym, -2.5)
                svg += (
                    INTERACTION.format(
                        p0_, p1_, q0_, q1_, color, xm_, ym_, color, deg, xm_, ym_, distance
                    )
                    + "\n"
                )

    for resname in res_xy:
        res, sup = resname[-3:], resname[:-3]
        fill = RESIDUE_COLORS.get(res, "#FFFFFF")
        text_fill = "black" if fill in LIGHT_COLORS else "white"
        x, y = res_xy[resname]
        if res == "HOH":
            fill = WATER_COLOR
            text_fill = "gray"
            svg += (
                WATER.format(x, y, fill, x, y - 4, text_fill, res, x, y + 12, text_fill, sup) + "\n"
            )
        else:
            svg += (
                RESIDUE.format(x, y, fill, x, y - 4, text_fill, res, x, y + 12, text_fill, sup)
                + "\n"
            )

    svg += TITLE.format(title) + "\n"
    svg += "</svg>"

    return view, svg
