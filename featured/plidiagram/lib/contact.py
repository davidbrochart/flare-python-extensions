# -*- coding: utf-8 -*-
# Copyright (C) 2023 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset

from cresset import flare
import numpy as np
from .rdmol import find_atom_by_flare_index


Strength = flare.Measurement.Strength


class Contact:
    def __init__(self, contact, contact_type):
        if contact.is_intra_molecular():
            raise _IntramolecularContact()
        patoms, latoms = _identify_atomset_source(contact)
        self.ligand_atom_idxs = sorted([a.index for a in latoms])
        self.mc_flag = 1 < len(self.ligand_atom_idxs)
        self.contact_type = contact_type
        self.contains_water = contact.contains_water()
        res = patoms[0].residue
        self._resname = res.name
        self.chain = res.chain
        self._seqnum = res.seq_num
        self.strength = contact.strength
        self.distance = contact.value
        self.center = [0, 0]  # assigned later

    def get_res_id(self):
        res_id = f"{self.chain}{self._seqnum}{self._resname}"
        return res_id


class ContactFlags:
    def __init__(self):
        ContactFlags.set_available_contacts()
        for name in self.available_contacts:
            setattr(self, name, True)
        self.w_h_bonds = True
        self.hydrophobic_contacts = False

    @classmethod
    def set_available_contacts(cls):
        cls.available_contacts = [n for n in dir(flare.contacts) if not n.startswith("_")]
        cls.available_contacts.append("w_h_bonds")


def find_contacts(pose, protein, contact_flags):
    contacts = []
    weak_hb = False
    enabled_contacts = [
        name for name in contact_flags.available_contacts if getattr(contact_flags, name)
    ]

    if "w_h_bonds" in enabled_contacts:
        weak_hb = True
        enabled_contacts.remove("w_h_bonds")

    for contact_type in enabled_contacts:
        func = getattr(flare.contacts, contact_type)
        for flare_contact in func([pose, protein]):
            try:
                contact = Contact(flare_contact, contact_type)
                if contact.strength == Strength.Weak and not weak_hb:
                    continue
                contacts.append(contact)
            except _IntramolecularContact:
                continue
            except _ProteinLigandNotResolved:
                continue
            except Exception as e:
                print(e)
                continue
    contacts.sort(key=lambda c: c.get_res_id())
    return contacts


def set_ligand_contact_centers(mol, contacts):
    conf = mol.GetConformer(0)
    for contact in contacts:
        atoms = [find_atom_by_flare_index(mol, i) for i in contact.ligand_atom_idxs]
        ps = [list(conf.GetAtomPosition(atom.GetIdx()))[:2] for atom in atoms]
        contact.center = list(np.array(ps).mean(axis=0))


class _IntramolecularContact(Exception):
    pass


class _ProteinLigandNotResolved(Exception):
    pass


def _identify_atomset_source(contact):
    def is_ligand_atom(atom):
        return isinstance(atom.molecule, flare.Ligand) or isinstance(atom.molecule, flare.Pose)

    def is_protein_atom(atom):
        return isinstance(atom.molecule, flare.Protein)

    atomset1, atomset2 = contact.points
    if is_protein_atom(atomset1[0]) and is_ligand_atom(atomset2[0]):
        return atomset1, atomset2
    elif is_protein_atom(atomset2[0]) and is_ligand_atom(atomset1[0]):
        return atomset2, atomset1
    else:
        raise _ProteinLigandNotResolved()
