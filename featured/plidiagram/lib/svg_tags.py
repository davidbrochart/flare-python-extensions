# -*- coding: utf-8 -*-
# Copyright (C) 2023 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset

TITLE = """
<text
  x="5"
  y="16"
  style="font-size:14px;font-weight:bold;font-family:san-serif;text-anchor:left;"
>{}</text>
""".strip()

INTERACTION = """
<line
  stroke-dasharray="6,4"
  x1="{}"
  y1="{}"
  x2="{}"
  y2="{}"
  style="stroke-width:1.5px; stroke:{};" />
<text
  x="{}"
  y="{}"
  stroke="none"
  fill="{}"
  style="font-size:12px;font-family:serif;text-anchor:middle;"
  transform="rotate({} {} {})"
>{}</text>
""".strip()

RESIDUE = """
<ellipse
  cx="{}"
  cy="{}"
  rx="28"
  ry="28"
  style="fill:{};stroke:gray;stroke-width:1px;" />
<text
  x="{}"
  y="{}"
  stroke="none"
  fill="{}"
  style="font-size:14px;font-weight:bold;font-family:san-serif;text-anchor:middle;"
>{}</text>
<text
  x="{}"
  y="{}"
  stroke="none"
  fill="{}"
  style="font-size:14px;font-weight:bold;font-family:san-serif;text-anchor:middle;"
>{}</text>
""".strip()

WATER = """
<ellipse
  cx="{}"
  cy="{}"
  rx="24"
  ry="24"
  style="fill:{};stroke:gray;stroke-width:1px;" />
<text
  x="{}"
  y="{}"
  stroke="none"
  fill="{}"
  style="font-size:11px;font-weight:bold;font-family:san-serif;text-anchor:middle;"
>{}</text>
<text
  x="{}"
  y="{}"
  stroke="none"
  fill="{}"
  style="font-size:11px;font-weight:bold;font-family:san-serif;text-anchor:middle;"
>{}</text>
""".strip()
