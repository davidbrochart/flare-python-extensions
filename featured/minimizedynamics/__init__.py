# -*- coding: utf-8 -*-
# Copyright (C) 2023 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Copies the protein at various points in a dynamics timeline and minimizes them.

Ribbon Controls:
    Extensions -> Protein -> Minimize Dynamics
        Makes copies of the protein at various points in the dynamics timeline
        and minimizes the them using the XED forcefield.
"""
import os
import sys
from PySide2 import QtWidgets

from cresset import flare


@flare.extension
class MinimizeDynamicsExtension:
    """Add a button to the ribbon which runs the dynamics and minimization workflow."""

    def load(self):
        """Load the extension."""
        tab = flare.main_window().ribbon["Extensions"]
        tab.tip_key = "X"
        self._control = tab["Protein"].add_button("Minimize Dynamics", self._dynamics_minimization)
        self._control.tooltip = (
            "Makes copies of the protein at various points in the dynamics timeline "
            "and minimizes them using the XED forcefield."
        )
        self._control.load_icon(
            os.path.join(os.path.dirname(os.path.realpath(__file__)), "protein_minimise.png")
        )
        self._control.tip_key = "M"
        self._control.enabled = False

        flare.callbacks.selected_proteins_changed.add(self._selected_proteins_changed)

        print(f"Loaded {self.__class__.__module__}.{self.__class__.__name__}")

    def _dynamics_minimization(self):
        """Take a number of snapshots of the dynamics results and minimizes them."""
        parent = flare.main_window().widget()
        num_snapshots, ok = QtWidgets.QInputDialog.getInt(
            parent, "Minimize Dynamics", "Number of snapshots:", 5, 2, 1000, 1
        )

        if ok:
            dynamics_protein = flare.main_window().selected_proteins[0]
            cancelled = False

            for snapshot_index in range(num_snapshots):
                if cancelled:
                    break

                minimize_protein = self._take_snapshot(
                    snapshot_index, num_snapshots, dynamics_protein
                )
                cancelled = self._minimize_protein(minimize_protein)

            if cancelled:
                QtWidgets.QMessageBox.information(parent, "Cancelled", "Cancelled.")
            else:
                QtWidgets.QMessageBox.information(parent, "Complete", "Complete.")

    @classmethod
    def _take_snapshot(cls, snapshot_index, num_snapshots, dynamics_protein):
        """Return a copy of dynamics_protein at the given point in the dynamics."""
        project = dynamics_protein.project
        results = flare.DynamicsResult(dynamics_protein)

        frame = 1 + int(((results.frame_count - 1) * snapshot_index) / (num_snapshots - 1))
        results.current_frame = frame
        # Copy the protein at the frame
        minimize_protein = project.proteins.append(dynamics_protein)

        cls._set_title(minimize_protein, dynamics_protein, snapshot_index)

        # Remove the water molecules
        waters = [
            seq for seq in minimize_protein.sequences if seq.type == flare.Sequence.Type.Water
        ]
        minimize_protein.sequences.remove(waters)
        return minimize_protein

    @staticmethod
    def _set_title(minimize_protein, dynamics_protein, snapshot_index):
        """Set the protein title to include the snapshot_index."""
        for i in range(sys.maxsize):
            try:
                subfix = "" if i == 0 else f"_({i})"
                minimize_protein.title = dynamics_protein.title + f"_M_{snapshot_index+1}" + subfix
                break
            except flare.DuplicateError:
                pass

    @staticmethod
    def _minimize_protein(protein):
        """Perform a quick minimization of the whole protein."""
        minimization = flare.Minimization()

        minimization.protein = protein
        minimization.atoms = protein.atoms
        # Only do a quick minimization
        minimization.gradient_cutoff = 1.0

        if minimization.setup_errors():
            raise Exception("\n".join(minimization.setup_errors()))

        minimization.start()
        minimization.wait()  # Block until done or error

        if minimization.errors():
            raise Exception("\n".join(minimization.errors()))

        return minimization.is_cancelled()

    def _selected_proteins_changed(self, proteins):
        """Enable the control only if a protein with dynamics is selected."""
        self._control.enabled = len(proteins) == 1 and bool(
            flare.DynamicsResult(proteins[0]).trajectory_path
        )
