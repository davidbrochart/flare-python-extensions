# -*- coding: utf-8 -*-
# Copyright (C) 2023 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""The kernel for the Flare Python Notebook."""

import sys
import os
import traceback
import atexit
from tempfile import TemporaryFile
import matplotlib
from ipykernel.kernelapp import IPKernelApp
from ipykernel import version_info as ipykernel_version_info
from traitlets import Type
from ipykernel.ipkernel import IPythonKernel
from ipykernel.zmqshell import ZMQInteractiveShell
from tornado import ioloop
from traitlets.utils.importstring import import_item
from functools import partial


# monkey-patch IPython to avoid a regression
# introduced in IPython 7.2 that was fixed in 7.7
# (https://github.com/ipython/ipython/pull/11806)
def soft_define_alias(self, name, cmd):
    """Define an alias for the notebook."""
    if _soft_define_alias and (cmd not in self.shell.magics_manager.magics["line"]):
        _soft_define_alias(self, name, cmd)


import IPython  # noqa: E402

if IPython.version_info > (7, 2, 0) and IPython.version_info < (7, 7, 0):
    import IPython.core.alias  # noqa: E402

    _soft_define_alias = IPython.core.alias.AliasManager.soft_define_alias
    IPython.core.alias.AliasManager.soft_define_alias = soft_define_alias
from .common import flare_extension_dir  # noqa E402
from .flarenotebook import PythonNotebookWidget  # noqa E402
from .eventloop import EventLoopHelper, PythonRunQtEventLoop  # noqa E402
from .configurelogging import get_logger  # noqa E402

log = get_logger("flareipkernel")


class FlareZMQInteractiveShell(ZMQInteractiveShell):
    """ZMQInteractiveShell with support for processing Qt events."""

    def run_cell(self, code, **kwargs):
        """Run notebook cell and keep processing Qt events."""
        log.debug("FlareZMQInteractiveShell.run_cell")
        with PythonRunQtEventLoop(
            self._run_event_loop and code, self._py_notebook.set_script_running
        ):
            log.debug("FlareZMQInteractiveShell.run_cell run_cell")
            res = super(ZMQInteractiveShell, self).run_cell(code, **kwargs)
        return res

    def __init__(self, *args, **kwargs):
        self._run_event_loop = False
        self._py_notebook = None
        super().__init__(*args, **kwargs)


class FlareIPythonKernel(IPythonKernel):
    """IPythonKernel with support for processing Qt events."""

    shell_class = Type(FlareZMQInteractiveShell)


class FlareIPKernelApp(IPKernelApp):
    """IPKernelApp with support for processing Qt events."""

    kernel_class = Type(
        FlareIPythonKernel, help="IPythonKernel subclass with support for processing Qt events"
    ).tag(config=True)

    def init_io(self, *args, **kwargs):
        # create tempfiles for stdout / stderr
        self._stdout_tempfile = TemporaryFile()
        self._stderr_tempfile = TemporaryFile()

        # delete these files when program exits
        def close_file_atexit(f):
            try:
                f.close()
            except OSError:
                pass

        atexit.register(close_file_atexit, self._stdout_tempfile)
        atexit.register(close_file_atexit, self._stderr_tempfile)

        # monkey patch stdout/stderr st. it has a fileno method
        sys.stdout.fileno = lambda: self._stdout_tempfile.fileno()
        sys.stderr.fileno = lambda: self._stderr_tempfile.fileno()
        sys.__stdout__.fileno = lambda: self._stdout_tempfile.fileno()
        sys.__stderr__.fileno = lambda: self._stderr_tempfile.fileno()

        """Override IPKernelApp.init_io to store original sys.displayhook."""
        EventLoopHelper().original_displayhook = sys.displayhook

        if ipykernel_version_info[0] <= 5:
            # for ipykernel<= 5.0.0 we can call the unpatched init_io
            # of the baseclass
            super(FlareIPKernelApp, self).init_io(*args, **kwargs)
        else:
            # for ipykernel >= 6.0.0  we call the patched init_io
            self._super_init_io(*args, **kwargs)

        EventLoopHelper().ipykernel_displayhook = self.displayhook
        sys.displayhook = EventLoopHelper().original_displayhook

    def _super_init_io(self, *args, **kwargs):
        """For ipykernel >= 6.0.0 we patch the init_io
        function. The original implementation tries
        to redirect the loggers file-handlers content
        under certain conditions. For flare 6 this prevents
        the notebook from starting.
        Therefore we overwrite this function and remove the
        redirection.
        """
        if self.outstream_class:
            outstream_factory = import_item(str(self.outstream_class))
            if sys.stdout is not None:
                sys.stdout.flush()

            e_stdout = None if self.quiet else sys.__stdout__
            e_stderr = None if self.quiet else sys.__stderr__

            if not self.capture_fd_output:
                outstream_factory = partial(outstream_factory, watchfd=False)

            sys.stdout = outstream_factory(self.session, self.iopub_thread, "stdout", echo=e_stdout)
            if sys.stderr is not None:
                sys.stderr.flush()
            sys.stderr = outstream_factory(self.session, self.iopub_thread, "stderr", echo=e_stderr)

        if self.displayhook_class:
            displayhook_factory = import_item(str(self.displayhook_class))
            self.displayhook = displayhook_factory(self.session, self.iopub_socket)
            sys.displayhook = self.displayhook

        self.patch_io()

    def init_crash_handler(self, *args, **kwargs):
        """Override IPKernelApp.init_crash_handler to store original sys.excepthook."""
        EventLoopHelper().original_excepthook = sys.excepthook
        super().init_crash_handler(*args, **kwargs)
        EventLoopHelper().ipykernel_excepthook = self.excepthook
        sys.excepthook = EventLoopHelper().original_excepthook

    def log_connection_info(self, *args, **kwargs):
        """Do not print connection info to stderr."""
        pass

    def _init_flare_shell(self):
        """Execute notebookdefaultscript.py when a new shell is started."""
        init_script_path = os.path.join(flare_extension_dir(), "notebookdefaultscript.py")
        try:
            with open(init_script_path, "r") as hnd:
                init_script = hnd.read()
        except Exception:
            log.warning(
                "_init_flare_shell failed to open {0:s} "
                "for reading. Stack trace:\n{1:s}".format(
                    init_script_path, "".join(traceback.format_exc())
                )
            )
        else:
            self.kernel.do_execute(init_script, True, store_history=False)

    def start(self):
        """Start the kernel and set ioloop to run a single iteration."""
        if self.subapp is not None:
            return self.subapp.start()
        if self.poller is not None:
            self.poller.start()
        self.kernel.shell._py_notebook = self._py_notebook
        self.kernel.start()
        self._init_flare_shell()
        self.kernel.shell._run_event_loop = True
        self._py_notebook.set_script_running(False)
        self.io_loop = ioloop.IOLoop.current()
        self._io_loop_stopper = ioloop.PeriodicCallback(self.io_loop.stop, 1)
        self._io_loop_stopper.start()

    def __init__(self, *args, **kwargs):
        self._py_notebook = kwargs.pop(PythonNotebookWidget.KEYNAME, None)
        # this ensures that plots are shown inline in the notebook
        # and not in a separate QtWindow.
        # (showing plots in a QtWindow leads to segfaults)
        matplotlib.use("nbagg")
        super().__init__(*args, **kwargs)
