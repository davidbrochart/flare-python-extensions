# -*- coding: utf-8 -*-
# Copyright (C) 2023 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
r"""Add the Jupyter-based Python Notebook to Flare.

To install, copy the whole "pythonnotebook" directory to your "extensions" directory,
which is in the respective locations on Windows, Linux and macOS:

Windows: %LOCALAPPDATA%\Cresset BMD\Flare\python\extensions
Linux: $HOME/.com.cresset-bmd/Flare/python/extensions
macOS: $HOME/Library/Application Support/Cresset BMD/Flare/python/extensions

Make sure that jupyter and its dependencies are installed in your pyflare
installation. The location of the pyflare binary depends on where Flare was
installed on the different platforms:

Windows:
 * %PROGRAMFILES%\Cresset-BMD\Flare\pyflare.exe (system-wide install)
 * %LOCALAPPDATA%\Cresset-BMD\Flare\pyflare.exe (user-level install)

Linux:
 * /opt/cresset/Flare/bin/pyflare (system-wide RPM install)
 * /your/path/cresset/Flare/bin/pyflare (user-level TGZ install, /your/path is user-determined)

macOS:
 * /Applications/Flare.app/Contents/MacOS/pyflare (system-wide install)
 * $HOME/Applications/Flare.app/Contents/MacOS/pyflare (user-level install)

Once you have identified where the pyflare binary lives on your system, replace
/path/to in the command reported below with the relevant path on your system:

pyflare -m pip install jupyter pillow

To enable the Python Notebook, click on the Python Notebook button
(the one with the Jupyter icon) in the "Code" group of the "Python" ribbon.
"""

import sys
import os
import site
from .configurelogging import get_logger
from .common import flare_extension_dir

log = get_logger("FlareJupyter")

# The NotebookPreload.py script is executed before anything else
# This is useful, for example, for things that need to be monkey-patched
# before anything else happens
preload_script_path = os.path.join(flare_extension_dir(), "notebookpreload.py")
preload_script = None
try:
    with open(preload_script_path, "r") as hnd:
        preload_script = hnd.read()
except Exception:
    log.warning("Failed to open preload script {0:s} for reading".format(preload_script_path))
if preload_script:
    exec(preload_script)

from cresset import flare  # noqa: E402
from PySide2 import QtCore, QtWidgets  # noqa: E402


# When jupyter is installed to the user site packages then
# jupyter will fail to find the template files for nbconvert
# when exporting the scripts.
user_jupyter_path = os.path.join(site.getuserbase(), "share", "jupyter")
if "JUPYTER_PATH" in os.environ:
    os.environ["JUPYTER_PATH"] += os.pathsep + user_jupyter_path
else:
    os.environ["JUPYTER_PATH"] = user_jupyter_path


@flare.extension
class PythonNotebookExtension:
    """Flare extension class."""

    PYTHON_NOTEBOOK_APPNAME = "Python Notebook"

    def load(self):
        """Load in this Flare extension."""

        group = flare.main_window().ribbon["Python"]["Code"]
        self.py_notebook_button_ui_control = group.add_button(
            self.PYTHON_NOTEBOOK_APPNAME.replace(" ", "\n"), self.py_notebook_button_toggled
        )
        self.py_notebook_button_ui_control.tooltip = "Open the Python Notebook."
        self.py_notebook_button_ui_control.tip_key = "P"
        self.py_notebook_button_q_action = self.py_notebook_button_ui_control.action()
        self.py_notebook_button_q_action.setCheckable(True)
        self.py_notebook_button_q_action.setChecked(False)

        icon_file = os.path.join(flare_extension_dir(), "notebook.png")
        if os.path.isfile(icon_file):
            self.py_notebook_button_ui_control.load_icon(icon_file)

        self.py_notebook = None

        print("Loaded {0:s}.{1:s}".format(self.__class__.__module__, self.__class__.__name__))

    def py_notebook_button_toggled(self):
        """Call when the Python Notebook ribbon button is toggled."""
        if self.py_notebook_button_q_action.isChecked():
            if self.py_notebook is None:
                # The notebook is slow to create so its creation is delayed until it is needed
                self.create_widget()

            self.py_notebook.py_notebook_show()
        else:
            self.py_notebook.py_notebook_hide()

    def try_import_win32api(self):
        """Try importing win32api adjusting PATH as needed."""
        if sys.platform == "win32":
            win32api = None
            exc = None
            PYWIN32_SYSTEM32 = "pywin32_system32"
            import site  # noqa: E402

            os_path = os.environ["PATH"]
            for get_packages_path in (None, site.getusersitepackages, site.getsitepackages):
                if get_packages_path:
                    packages_path = get_packages_path()
                    if isinstance(packages_path, str):
                        packages_path = (packages_path,)
                    for pp in packages_path:
                        pywin32_system32_path = os.path.join(pp, PYWIN32_SYSTEM32)
                        if os.path.isdir(pywin32_system32_path):
                            os.environ["PATH"] = os.pathsep.join([os_path, pywin32_system32_path])
                            break
                try:
                    import win32api
                except ImportError as e:
                    exc = e
                    continue
                break
            if not win32api:
                raise ImportError("Could not import win32api:\n{0:s}".format(str(exc)))

    def create_widget(self):
        """Initialize PythonNotebookExtension."""
        QtWidgets.qApp = QtWidgets.QApplication
        # FIXME: the following line might not be needed any more once
        # https://github.com/jupyter/qtconsole/issues/279
        # is closed and incorporated in the main qt-console trunk
        QtWidgets.QApplication.instance().font = QtWidgets.QApplication.font

        # on Windows, make sure that win32api can be imported
        self.try_import_win32api()
        # PythonNotebookWidget is slow to import so it is only imported if this extension
        # is being loaded
        from .flarenotebook import PythonNotebookWidget  # noqa: E402

        self.py_notebook = PythonNotebookWidget(None, QtCore.Qt.Window)
        self.py_notebook.setWindowTitle(self.PYTHON_NOTEBOOK_APPNAME)

        self.py_notebook.set_py_notebook_button_action(self.py_notebook_button_q_action)
        self.py_notebook.hide()
