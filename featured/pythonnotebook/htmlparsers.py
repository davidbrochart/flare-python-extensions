# -*- coding: utf-8 -*-
# Copyright (C) 2023 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""HTMLParser and helper classes to patch Jupyter notebook standard HTML template files."""

from html.parser import HTMLParser


class HTMLBlock:
    """Helper class to store HTML block counts."""

    MENU = 1
    MENU_ENTRY = 2
    DIVIDER_ENTRY = 3
    LOGIN_WIDGET = 4

    def __init__(self):
        self.start = None
        self.end = None
        self._count = 0
        self.type = None

    def increase_count(self):
        """Increase block count by 1."""
        self._count += 1

    def decrease_count(self):
        """Decrease block count by 1."""
        if self._count == 0:
            raise ValueError("Count is already 0")
        self._count -= 1

    def is_count_zero(self):
        """Return True if count equals 0."""
        return self._count == 0


class FlareHTMLParser(HTMLParser):
    """Helper class to store HTML block counts."""

    def feed(self, data):
        """Store cumulative data before passing it to the parent class feed() method."""
        self._html_data += data
        super().feed(data)

    def dump(self, delete_blocks=True, number_lines=False):
        """Dump data, optionally deleting previously flagged blocks.

        Optionally, lines can be numbered in the output for debugging purposes.
        """
        block = None
        line = True
        n = 0
        pn = 0
        d = ""
        blocks_to_delete = list(self._blocks_to_remove)
        for line in self._html_data.split("\n"):
            if delete_blocks and (block is None) and blocks_to_delete:
                block = blocks_to_delete.pop(0)
            n += 1
            print_line = True
            if block:
                print_line = n < block.start
                if n == block.end:
                    block = None
            if print_line:
                pn += 1
                if number_lines:
                    d += "{0:6d}:".format(pn)
                d += line + "\n"
        return d

    def __init__(self, *args, **kwargs):
        self._blocks_to_remove = []
        self._html_data = ""
        self._current_block = None
        super().__init__(*args, **kwargs)


class NotebookHTMLTrimMenus(FlareHTMLParser):
    """HTMLParser class to remove whole menus or individual menu entries.

    These are removed from the standard Jupyter notebook.html template.
    """

    def handle_starttag(self, tag, attrs):
        """Do this when a start tag is found."""
        if tag == "li":
            if (self._current_block is not None) and (self._current_block.start is not None):
                self._current_block.increase_count()
            for attr in attrs:
                if (attr == ("class", "dropdown")) or (attr in self._menu_entries_to_remove):
                    if self._current_block is None:
                        self._current_block = HTMLBlock()
                    if self._current_block.start is None:
                        self._current_block.start = self.getpos()[0]
                    if attr != ("class", "dropdown"):
                        self._current_block.type = HTMLBlock.MENU_ENTRY
                        self._current_block.increase_count()
                    break
        elif (
            tag == "ul"
            and (self._current_block is not None)
            and (self._current_block.type != HTMLBlock.MENU_ENTRY)
            and (self._current_block.start is not None)
            and self._current_block.is_count_zero()
        ):
            should_remove_menu = [attr for attr in attrs if (attr in self._menus_to_remove)]
            if should_remove_menu:
                self._current_block.type = HTMLBlock.MENU
                self._current_block.increase_count()
            else:
                self._current_block.start = None

    def handle_endtag(self, tag):
        """Do this when an end tag is found."""
        if (
            (tag == "li")
            and (self._current_block is not None)
            and (self._current_block.start is not None)
        ):
            self._current_block.decrease_count()
            if self._current_block.is_count_zero():
                self._current_block.end = self.getpos()[0]
                self._blocks_to_remove.append(self._current_block)
                self._current_block = None

    def set_menus_to_remove(self, attrs):
        """Set the menus to be removed entirely."""
        self._menus_to_remove = attrs

    def set_menu_entries_to_remove(self, attrs):
        """Set the individual menu entries to be removed."""
        self._menu_entries_to_remove = attrs


class NotebookHTMLRemoveExtraDividers(FlareHTMLParser):
    """HTMLParser class to remove orphan multiple consecutive dividers.

    The orphan dividers are removed from the standard Jupyter notebook.html template.
    """

    def handle_starttag(self, tag, attrs):
        """Do this when a start tag is found."""
        if tag == "li":
            for attr in attrs:
                if attr == ("class", "divider"):
                    self._current_block = HTMLBlock()
                    self._current_block.start = self.getpos()[0]
                    self._current_block.type = HTMLBlock.DIVIDER_ENTRY
                    self._current_block.increase_count()
                    break
        if self._current_block is None or (
            self._previous_divider
            and self._blocks_to_remove
            and (self._current_block.start - self._blocks_to_remove[-1].end) > 1
        ):
            if self._previous_divider:
                self._blocks_to_remove.pop(-1)
            self._previous_divider = None

    def handle_endtag(self, tag):
        """Do this when an end tag is found."""
        if (tag == "li") and (self._current_block is not None):
            self._current_block.decrease_count()
            if self._current_block.is_count_zero():
                self._current_block.end = self.getpos()[0]
                self._blocks_to_remove.append(self._current_block)
                self._previous_divider = self._current_block
                self._current_block = None
        elif (tag == "ul") and self._previous_divider:
            self._previous_divider = None

    def __init__(self, *args, **kwargs):
        self._previous_divider = None
        super().__init__(*args, **kwargs)


class PageHTMLRemoveLoginWidget(FlareHTMLParser):
    """HTMLParser class to remove the login widget.

    The login widget is removed from the standard Jupyter page.html template.
    """

    def handle_starttag(self, tag, attrs):
        """Do this when a start tag is found."""
        if tag == "span":
            for attr in attrs:
                if attr == ("id", "login_widget"):
                    self._current_block = HTMLBlock()
                    self._current_block.start = self.getpos()[0]
                    self._current_block.type = HTMLBlock.LOGIN_WIDGET
                    self._current_block.increase_count()
                    break

    def handle_endtag(self, tag):
        """Do this when an end tag is found."""
        if (tag == "span") and (self._current_block is not None):
            self._current_block.decrease_count()
            if self._current_block.is_count_zero():
                self._current_block.end = self.getpos()[0]
                self._blocks_to_remove.append(self._current_block)
                self._current_block = None
