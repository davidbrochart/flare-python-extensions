# -*- coding: utf-8 -*-
# Copyright (C) 2023 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Configure logging level and log file location."""

import os
import logging
import tempfile
import getpass


def get_logger(logger_name="default"):
    """Return logger appropriate for the logging level."""
    is_debug_run = os.environ.get("CRESSET_IPYNB_DEBUG", None)
    logger = logging.getLogger(logger_name)
    logging_level = is_debug_run and logging.DEBUG or logging.WARNING
    logger.setLevel(logging_level)
    fh = logging.FileHandler(
        os.path.join(tempfile.gettempdir(), f"flare_{getpass.getuser()}_ipynb.log")
    )
    fh.setLevel(logging_level)
    formatter = logging.Formatter("%(asctime)s:%(levelname)s:%(name)s:%(message)s")
    fh.setFormatter(formatter)
    logger.addHandler(fh)
    return logger
