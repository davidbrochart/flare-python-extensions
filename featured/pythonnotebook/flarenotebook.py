# -*- coding: utf-8 -*-
# Copyright (C) 2023 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Classes specific to the Flare Python Notebook."""

import os
import re
import traceback
import json
import shutil
from PySide2 import QtCore, QtGui, QtWidgets
from PySide2.QtWebEngineWidgets import QWebEngineView, QWebEnginePage, QWebEngineDownloadItem
from cresset import flare

# The following import is required, see https://github.com/jupyter/notebook/issues/3056
import notebook.transutils  # noqa: F401
from .flarekernelmanager import FlareContentsManager
from .myfiledialog import MyFileDialog
from .directoryservice import DirectoryService
from .eventloop import EventLoopHelper, PythonRunQtEventLoop
from .common import (
    flare_is_shutting_down,
    GettingNotebookReady,
    flare_extension_dir,
    DisableWidget,
    FileStatMonitor,
    SafeFileRemover,
)
from .configurelogging import get_logger

log = get_logger("flarenotebook")


class RestartButton(QtWidgets.QPushButton):
    """A QPushButton class with a context menu."""

    RESTART_LABEL = "Restart"
    RESTART_TOOLTIP = "Restart kernel.<br>Click on the arrow to see more restart options."
    ARROW_WIDTH = 32

    def __init__(self, parent):
        super().__init__(parent)
        self.restart_menu = RestartMenu(self)
        self.restart_action = self.restart_menu.addAction(self.RESTART_LABEL)
        self.restart_action.setObjectName("PyNotebookRestartAction")
        self.restart_action.setToolTip(self.RESTART_TOOLTIP)
        self.restart_and_clear_output_action = self.restart_menu.addAction(
            "Restart && Clear Output"
        )
        self.restart_and_clear_output_action.setObjectName("PyNotebookRestartAndClearOutputAction")
        self.restart_and_clear_output_action.setToolTip("Restart kernel and clear all cell output.")
        self.restart_and_clear_all_action = self.restart_menu.addAction("Restart && Clear All")
        self.restart_and_clear_all_action.setObjectName("PyNotebookRestartAndClearAllAction")
        self.restart_and_clear_all_action.setToolTip("Restart kernel and clear all cells.")
        self.restart_and_run_all_action = self.restart_menu.addAction("Restart && Run All")
        self.restart_and_run_all_action.setObjectName("PyNotebookRestartAndRunAllAction")
        self.restart_and_run_all_action.setToolTip("Restart kernel and run all cells.")
        self.setText(self.RESTART_LABEL)
        self.setToolTip(self.RESTART_TOOLTIP)
        self.setObjectName("PyNotebookRestartStopPushButton")
        self.tool_button = QtWidgets.QToolButton(self)
        self.tool_button.setObjectName("PyNotebookRestartStopToolButton")
        self.tool_button.setPopupMode(QtWidgets.QToolButton.InstantPopup)
        self.tool_button.setArrowType(QtCore.Qt.UpArrow)
        self.tool_button.setStyleSheet(
            "QToolButton { border: none; }" "QToolButton::menu-indicator { image: none; }"
        )
        self.tool_button.setFixedWidth(self.ARROW_WIDTH)
        self.setFixedWidth(self.sizeHint().width() + self.tool_button.width())
        self.tool_button.setMenu(self.restart_menu)
        self.tool_button.move(QtCore.QPoint(self.width() - self.tool_button.width(), 0))
        self.restart_menu.set_restart_button(self)

    def connect_signals(self, slot):
        """Connect the signals of both buttons."""
        self.clicked.connect(slot)
        self.tool_button.triggered.connect(slot)


class RestartMenu(QtWidgets.QMenu):
    """A pop-up QMenu class which pops upwards only."""

    def event(self, event):
        """Reimplemented to pop up upwrds only."""
        if (
            event.type() == QtCore.QEvent.Show or event.type() == QtCore.QEvent.Move
        ) and self._restart_button:
            button_pos = self._restart_button.mapToGlobal(QtCore.QPoint(0, 0))
            self.setGeometry(
                button_pos.x(), button_pos.y() - self.height(), self.width(), self.height()
            )
        return super().event(event)

    def set_restart_button(self, widget):
        """Set the _restart_button to widget."""
        self._restart_button = widget

    def __init(self, *args, **kwargs):
        self._restart_button = None
        super().__init(*args, **kwargs)


class PythonNotebookWidget(QtWidgets.QWidget):
    """Container widget to hold FlareEngineTabbedView and buttons."""

    KEYNAME = "python_notebook_widget"
    PROGRESSBAR_INTERVAL_MS = 300
    PROGRESSBAR_INCREMENT = 3
    COPY_SUFFIX = "-Copy"
    COPY_REGEX = re.compile("(^.*" + COPY_SUFFIX + ").*$")
    JUPYTER_NOTEBOOK_NS = "Jupyter.notebook."
    CLEAR_ALL_OUTPUT_JS = JUPYTER_NOTEBOOK_NS + "clear_all_output();"
    RESTART_KERNEL_JS = JUPYTER_NOTEBOOK_NS + "restart_kernel({confirm: false});"
    STOP_KERNEL_JS = JUPYTER_NOTEBOOK_NS + "kernel.interrupt();"
    EXECUTE_ALL_JS = JUPYTER_NOTEBOOK_NS + "execute_all_cells();"
    CLOSE_AND_HALT_JS = JUPYTER_NOTEBOOK_NS + "close_and_halt();"
    SAVE_AND_CHECKPOINT_JS = JUPYTER_NOTEBOOK_NS + "save_checkpoint();"
    RENAME_JS = JUPYTER_NOTEBOOK_NS + 'rename("{0:s}");'
    FROM_JSON_JS = JUPYTER_NOTEBOOK_NS + "fromJSON(JSON.parse({0:s}));"
    ALLOW_OP_MS = 100
    IOLOOP_INTERVAL_MS = 100
    DEFERRED_INIT_DELAY_MS = 100
    CONTENTS_MARGINS = (2, 2, 2, 2)
    SHORTCUT_EVENTS = {
        "Ctrl+O": "on_load",
        "Ctrl+S": "on_save",
        "Ctrl+Shift+S": "on_save_as",
        "Ctrl+W": "on_tab_close",
    }

    class GetOpLock:
        """Context manager class to run a single concurrent load/save/close op."""

        class AlreadyLocked(Exception):
            """Exception thrown when a load/save/close op is already running."""

            pass

        def __init__(self, parent):
            self._parent = parent
            if not self._parent.block_op():
                raise self.AlreadyLocked

        def __enter__(self):
            return self

        def __exit__(self, exc_type, exc_value, trace_back):
            self._parent.allow_op_shortly()

    def set_script_running(self, state):
        """Set to True if the notebook is currently running a script, to False if it isn't."""
        self.restart_button_prev_state = not state
        self.restart_button.setEnabled(self.restart_button_prev_state)
        self.stop_button_prev_state = state
        self.stop_button.setEnabled(self.stop_button_prev_state)
        self.update()

    def on_tab_close(self):
        """Exception thrown when a load/save/close op is already running."""
        self.call_op_impl(self._on_tab_close_impl)

    def _on_tab_close_impl(self):
        i = self.flare_web_tabbed_view.currentIndex()
        if i:
            self.flare_web_tabbed_view.on_tab_close(i)

    def py_notebook_show(self):
        """Bring to front and set focus on PythonNotebookWidget."""
        self.show()
        self.raise_()
        self.activateWindow()
        self.setFocus()

    def py_notebook_hide(self):
        """Hide PythonNotebookWidget."""
        self.hide()

    def set_py_notebook_button_action(self, action):
        """Set Python Notebook button QAction."""
        self.py_notebook_button_q_action = action

    def closeEvent(self, event):
        """When the Python Notebook window is closed keep the ribbon button in sync."""
        if self.py_notebook_button_q_action:
            self.py_notebook_button_q_action.setChecked(False)
        event.accept()

    def confirm_restart_dialog(self, extra_text=""):
        """Return True if the user confirms restart."""
        res = QtWidgets.QMessageBox.warning(
            self,
            "Restart kernel",
            "Do you want to restart the current kernel{0:s}?\n"
            "All variables will be lost.".format(extra_text),
            QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No,
            QtWidgets.QMessageBox.StandardButton(QtWidgets.QMessageBox.No),
        )
        return res == QtWidgets.QMessageBox.Yes

    def on_restart_and_clear_output(self):
        """Call when the output of a notebook is to be cleared."""
        log.debug("on_restart_and_clear_output")
        i, view = self.flare_web_tabbed_view.get_current_index_and_view()
        if self.confirm_restart_dialog("\nand clear all output"):
            if i == 0:
                self.kernelapp.shell.reset()
                view._view.page().runJavaScript(self.CLEAR_ALL_OUTPUT_JS)
            else:
                view._view.page().runJavaScript(self.CLEAR_ALL_OUTPUT_JS + self.RESTART_KERNEL_JS)

    def on_restart_and_clear_all(self):
        """Call when a notebook is to be cleared completely."""
        log.debug("on_restart_and_clear_all")
        if self.confirm_restart_dialog("\nand clear all cells"):
            i, view = self.flare_web_tabbed_view.get_current_index_and_view()
            view._file_path = None
            if i == 0:
                self.kernelapp.shell.reset()
                self.kernelapp._init_flare_shell()
            else:
                view._view.page().runJavaScript(self.RESTART_KERNEL_JS)
            current_ipynb, _ = view.get_notebook_path_and_basename()
            view.replace_notebook(current_ipynb, content=FlareContentsManager._flare_new_notebook())

    def on_restart_and_run_all(self):
        """Call to restart a notebook's kernel and run all cells."""
        log.debug("on_restart_and_run_all")
        i, view = self.flare_web_tabbed_view.get_current_index_and_view()
        if self.confirm_restart_dialog("\nand re-execute the whole notebook"):
            if i == 0:
                self.kernelapp.shell.reset()
                view._view.page().runJavaScript(self.EXECUTE_ALL_JS)
            else:
                view._view.page().runJavaScript(self.RESTART_KERNEL_JS + self.EXECUTE_ALL_JS)

    def on_restart(self):
        """Call to restart a notebook's kernel."""
        log.debug("on_restart")
        i, view = self.flare_web_tabbed_view.get_current_index_and_view()
        if self.confirm_restart_dialog():
            if i == 0:
                self.kernelapp.shell.reset()
            else:
                view._view.page().runJavaScript(self.RESTART_KERNEL_JS)

    def on_restart_triage(self, action):
        """Call to determine which restart action should be carried out."""
        if isinstance(self.sender(), QtWidgets.QPushButton):
            action = self.restart_button.restart_action
        self.on_restart_action(action)

    def on_stop(self):
        """Call when the user clicks the Stop button."""
        log.debug("Stop clicked")
        i, view = self.flare_web_tabbed_view.get_current_index_and_view()
        if i == 0:
            locker = QtCore.QMutexLocker(EventLoopHelper().stop_event_loop_mutex)
            EventLoopHelper().stop_button_clicked = True
            locker.unlock()
        else:
            view._view.page().runJavaScript(self.STOP_KERNEL_JS)

    def get_last_visited_dir(self):
        """Return the last visited dir as QtCore.QDir, or None."""
        view = self.flare_web_tabbed_view.currentWidget()
        last_visited_dir = None
        if view._file_path:
            last_visited_dir = QtCore.QFileInfo(view._file_path).absoluteDir()
        if not last_visited_dir:
            last_visited_dir = DirectoryService.getLastVisitedDir()
        return last_visited_dir

    @property
    def attach_lock(self):
        """Return abs path to the attach lock file."""
        return self._attach_lock

    @staticmethod
    def generate_copy_basename_no_ext(file_path_basename, curr_notebooks):
        """Generate a unique -Copy# notebook name."""
        basename_no_ext, ext = os.path.splitext(file_path_basename)
        m = PythonNotebookWidget.COPY_REGEX.match(basename_no_ext)
        if (not m) or (not m.groups()):
            copy_basename_no_ext = basename_no_ext + PythonNotebookWidget.COPY_SUFFIX
        else:
            copy_basename_no_ext = m.groups()[0]
        if (copy_basename_no_ext + ext) in curr_notebooks:
            suffixes = [
                os.path.splitext(nb)[0].replace(copy_basename_no_ext, "")
                for nb in curr_notebooks
                if nb.startswith(copy_basename_no_ext)
            ]
            n = 0
            n_strlen = str(len(str(n)))
            for suffix in suffixes:
                try:
                    i = int(suffix)
                except ValueError:
                    continue
                if i > n:
                    n = i
                    n_strlen = str(len(suffix))
            copy_basename_no_ext += ("{0:0" + n_strlen + "d}").format(n + 1)
        return copy_basename_no_ext

    def _allow_op(self):
        """Load/save/close op has finished so a new one can take place."""
        locker = QtCore.QMutexLocker(self._op_mutex)
        try:
            self._op_ongoing = False
        finally:
            locker.unlock()

    def allow_op_shortly(self):
        """Allow load/save/close op within ALLOW_OP_MS."""
        QtCore.QTimer.singleShot(self.ALLOW_OP_MS, self._allow_op)

    def block_op(self):
        """Return True if successful, False if already blocked."""
        locker = QtCore.QMutexLocker(self._op_mutex)
        try:
            if self._op_ongoing:
                return False
            self._op_ongoing = True
            return True
        finally:
            locker.unlock()

    def call_op_impl(self, op_impl):
        """No-op if another op is ongoing, otherwise call op_impl."""
        try:
            with self.GetOpLock(self):
                op_impl()
        except self.GetOpLock.AlreadyLocked:
            pass

    def on_load(self):
        """Call when user wants to load a notebook."""
        self.call_op_impl(self._on_load_impl)

    def _on_load_impl(self):
        """Private on_load implementation."""
        log.debug("Load clicked")
        view = self.flare_web_tabbed_view.currentWidget()
        try:
            file_path, _ = MyFileDialog.getOpenFileName(
                None,
                "Load Python Notebook as...",
                cwd=self.get_last_visited_dir(),
                filter="Jupyter Notebook (*.ipynb)",
            )
        except Exception:
            QtWidgets.QMessageBox.warning(
                self,
                "Failed to load notebook",
                "An error occurred while attempting to load a notebook:\n"
                "{0:s}".format("".join(traceback.format_exc())),
                QtWidgets.QMessageBox.Ok,
                QtWidgets.QMessageBox.StandardButton(QtWidgets.QMessageBox.Ok),
            )
        if not file_path or not os.path.exists(file_path):
            return
        log.debug('on_load() file_path = "{0:s}"'.format(file_path))
        view._file_path = file_path
        src_basename_no_ext = None
        file_path_basename = os.path.basename(file_path)
        curr_notebooks = os.listdir(self.launcher.notebook_dir)
        if file_path_basename != view._ipynb_basename and file_path_basename in curr_notebooks:
            src_basename_no_ext = self.generate_copy_basename_no_ext(
                file_path_basename, curr_notebooks
            )
            res = QtWidgets.QMessageBox.warning(
                self,
                "Notebook already exists",
                "There is already an open notebook with this name,\n"
                "so this notebook will be opened as\n"
                "{0:s}.\n".format(src_basename_no_ext),
                QtWidgets.QMessageBox.Ok | QtWidgets.QMessageBox.Cancel,
                QtWidgets.QMessageBox.StandardButton(QtWidgets.QMessageBox.Ok),
            )
            if res == QtWidgets.QMessageBox.Cancel:
                return
        view.replace_notebook(file_path, src_basename_no_ext=src_basename_no_ext)

    def on_save(self):
        """Call when user wants to save a notebook.

        Return True if save is confirmed, False if save was cancelled or failed.
        """
        return self.call_op_impl(self._on_save_impl)

    def _on_save_impl(self):
        """Private on_save implementation.

        Return True if save is confirmed, False if save was cancelled or failed.
        """
        log.debug("Save clicked")
        view = self.flare_web_tabbed_view.currentWidget()
        if not (view._file_path):
            return self._on_save_as_impl()
        else:
            return view.save_notebook()

    def check_if_ipynb_exists(self, file_path):
        """Display a warning dialog and return True if the notebook exists."""
        view = self.flare_web_tabbed_view.currentWidget()
        file_path_basename = os.path.basename(file_path)
        exists = False
        for i in range(self.flare_web_tabbed_view.count()):
            v = self.flare_web_tabbed_view.widget(i)
            if v == view:
                continue
            _, current_ipynb_basename = v.get_notebook_path_and_basename()
            if current_ipynb_basename == file_path_basename:
                exists = True
                break
        if exists:
            QtWidgets.QMessageBox.warning(
                self,
                "Notebook already exists",
                "There is already an open notebook with this name in another tab.\n"
                "Please choose a different filename for your notebook.",
                QtWidgets.QMessageBox.Ok,
                QtWidgets.QMessageBox.StandardButton(QtWidgets.QMessageBox.Ok),
            )
        return exists

    def on_save_as(self):
        """Call when user wants to save a notebook with a name.

        Return True if save was confirmed, False if save was cancelled or failed.
        """
        return self.call_op_impl(self._on_save_as_impl)

    def _on_save_as_impl(self):
        """Private on_save_as implementation.

        Return True if save was confirmed, False if save was cancelled or failed.
        """
        log.debug("SaveAs clicked")
        view = self.flare_web_tabbed_view.currentWidget()
        keep_looping = True
        while keep_looping:
            keep_looping = False
            file_path = None
            try:
                file_path, _ = MyFileDialog.getSaveFileName(
                    None,
                    "Save Python Notebook as...",
                    cwd=self.get_last_visited_dir(),
                    filter="Jupyter Notebook (*.ipynb)",
                    filename=view.url_to_filename(view._view.url().url()),
                )
            except Exception:
                QtWidgets.QMessageBox.warning(
                    self,
                    "Failed to save notebook",
                    "An error occurred while attempting to save your notebook:\n"
                    "{0:s}".format("".join(traceback.format_exc())),
                    QtWidgets.QMessageBox.Ok,
                    QtWidgets.QMessageBox.StandardButton(QtWidgets.QMessageBox.Ok),
                )
            if file_path:
                keep_looping = self.check_if_ipynb_exists(file_path)
                log.debug('on_save_as() file_path = "{0:s}"'.format(file_path))
                if not keep_looping:
                    view._file_path = file_path
                    view.save_notebook()
        return bool(file_path)

    def eventFilter(self, target, event):
        """Event filter to find when we are about to be shut down."""
        if target == self.q_main_window:
            if event.type() == QtCore.QEvent.Close:
                log.debug("PythonNotebookExtension.eventFilter Close")
            elif event.type() == QtCore.QEvent.Hide and flare_is_shutting_down():
                log.debug("1) PythonNotebookExtension.eventFilter Hide")
                self.about_to_shutdown()
                log.debug("2) PythonNotebookExtension.eventFilter Hide")
                self.stop_jupyter_notebook()
                log.debug("3) PythonNotebookExtension.eventFilter Hide")
                self.deleteLater()
                log.debug("4) PythonNotebookExtension.eventFilter Hide")
        return super().eventFilter(target, event)

    def start_jupyter_notebook(self):
        """Start the Jupyter Notebook launcher.

        Find out which port the Jupyter Notebook is listening on
        and the authentication token, then start a blank notebook.
        """
        log.debug("1) start_jupyter_notebook")
        try:
            self.launcher.start_launcher()
        except Exception:
            if not flare_is_shutting_down():
                raise
        try:
            self.launcher.set_port_and_token(func=flare_is_shutting_down)
        except Exception:
            log.critical(
                "JupyterNotebookLauncher.set_port_and_token "
                "raised an exception:\n{0:s}".format("".join(traceback.format_exc()))
            )
            if not flare_is_shutting_down():
                raise
        untitled_notebook_file = os.path.join(
            self.launcher.notebook_dir, self.launcher.UNTITLED_NOTEBOOK_FILE
        )
        try:
            with open(untitled_notebook_file, "w") as untitled_notebook_hnd:
                json.dump(
                    FlareContentsManager._flare_new_notebook(),
                    untitled_notebook_hnd,
                    indent=1,
                    sort_keys=True,
                )
        except Exception:
            log.critical(
                "start_jupyter_notebook failed to open {0:s} "
                "for writing. Stack trace:\n{1:s}".format(
                    untitled_notebook_file, "".join(traceback.format_exc())
                )
            )
            if not flare_is_shutting_down():
                raise
        self.flare_web_tabbed_view.main_tab.set_notebook_file(untitled_notebook_file)

    def about_to_shutdown(self):
        """Call when the notebook is about to be shut down.

        Displays confirmation dialogs for unsaved notebooks and closes tabs one by one.
        """
        if self.flare_web_tabbed_view:
            index_list = list(range(self.flare_web_tabbed_view.count()))
            index_list.pop(self.flare_web_tabbed_view.currentIndex())
            index_list.insert(0, self.flare_web_tabbed_view.currentIndex())
            for i in index_list:
                view = self.flare_web_tabbed_view.widget(i)
                if view._ipynb_basename is None:
                    continue
                self.flare_web_tabbed_view.setCurrentIndex(i)
                self.flare_web_tabbed_view.on_tab_close(i, can_cancel=False)

    def stop_jupyter_notebook(self):
        """Clean up and shut down the notebook when Flare quits."""
        if self.launcher:
            self.launcher.rmtempdir()

    def do_one_ioloop_iter(self):
        """Run a single Tornado ioloop iteration."""
        try:
            self.kernelapp.io_loop.start()
        except KeyboardInterrupt:
            pass

    def progressbar_increment(self):
        """Increment th progress bar for starting the notebook."""
        self.GettingNotebookReady.setValue(
            self.GettingNotebookReady.value() + self.PROGRESSBAR_INCREMENT
        )

    def deferred_init(self):
        """Deferred IPKernelApp import and instantiation."""
        with PythonRunQtEventLoop(), GettingNotebookReady(
            parent=self, text="Getting the notebook ready..."
        ):
            progressbar_timer = QtCore.QTimer(self)
            progressbar_timer.timeout.connect(self.progressbar_increment)
            progressbar_timer.start(self.PROGRESSBAR_INTERVAL_MS)
            self._shortcuts = {}
            for k, v in self.SHORTCUT_EVENTS.items():
                shortcut = QtWidgets.QShortcut(QtGui.QKeySequence(k), self)
                shortcut.setContext(QtCore.Qt.WindowShortcut)
                shortcut.activated.connect(getattr(self, v))
                self._shortcuts[v] = shortcut
            if flare_is_shutting_down():
                return
            log.debug("1) deferred_init")
            from . import nblauncher
            from .flareipkernel import FlareIPKernelApp

            log.debug("2) deferred_init")
            self.launcher = nblauncher.JupyterNotebookLauncher(self.flare_pid, True)
            os.environ["JUPYTER_RUNTIME_DIR"] = self.launcher.runtime_dir
            log.debug("3) deferred_init")
            if flare_is_shutting_down():
                self.launcher.rmtempdir()
                return
            self.kernelapp = FlareIPKernelApp.instance(**{self.KEYNAME: self})
            self.kernelapp.initialize()
            self.kernelapp.start()
            self.ioloop_timer = QtCore.QTimer(self)
            self.ioloop_timer.timeout.connect(self.do_one_ioloop_iter)
            self.ioloop_timer.start(self.IOLOOP_INTERVAL_MS)
            self.start_jupyter_notebook()
            log.debug("4) deferred_init")

    def __init__(self, *args, **kwargs):
        self.py_notebook_button_q_action = None
        self.flare_pid = os.getpid()
        self.launcher = None
        self.launcher_popen = None
        self.port = None
        self.token = None
        self._op_mutex = QtCore.QMutex()
        self._op_ongoing = False
        self.jupyter_notebook_binary = None
        self.ioloop_timer = None
        self.flare_web_tabbed_view = None
        super().__init__(*args, **kwargs)
        self.q_main_window = flare.main_window().widget()
        self.q_main_window.installEventFilter(self)
        self.setObjectName("PythonNotebookWidget")
        self.container = QtWidgets.QWidget(self)
        self.buttons = QtWidgets.QWidget(self.container)
        self.buttons.setObjectName("PyNotebookButtonsWidget")
        self.restart_button_prev_state = None
        self.restart_button = RestartButton(self.container)
        self.restart_button.connect_signals(self.on_restart_triage)
        self._on_action_dict = {
            self.restart_button.restart_action: self.on_restart,
            self.restart_button.restart_and_clear_output_action: self.on_restart_and_clear_output,  # noqa: E501
            self.restart_button.restart_and_clear_all_action: self.on_restart_and_clear_all,
            self.restart_button.restart_and_run_all_action: self.on_restart_and_run_all,
        }
        self.on_restart_action = lambda action: self._on_action_dict[action]()
        self.stop_button_prev_state = None
        self.stop_button = QtWidgets.QPushButton("Stop", self.buttons)
        self.stop_button.setToolTip("Interrupt kernel.")
        self.stop_button.clicked.connect(self.on_stop)
        self.load_button = QtWidgets.QPushButton("Load...", self.buttons)
        self.load_button.setObjectName("PyNotebookLoadButton")
        self.load_button.setToolTip("Load notebook from a file\n(Ctrl+O).")
        self.load_button.clicked.connect(self.on_load)
        self.save_button = QtWidgets.QPushButton("Save", self.buttons)
        self.save_button.setObjectName("PyNotebookSaveButton")
        self.save_button.setToolTip("Save notebook\n(Ctrl+S).")
        self.save_button.clicked.connect(self.on_save)
        self.save_as_button = QtWidgets.QPushButton("Save As...", self.buttons)
        self.save_as_button.setObjectName("PyNotebookSaveAsButton")
        self.save_as_button.setToolTip("Save notebook to a file\n(Ctrl+Shift+S).")
        self.save_as_button.clicked.connect(self.on_save_as)
        self.buttons.setSizePolicy(QtWidgets.QSizePolicy.Ignored, QtWidgets.QSizePolicy.Preferred)
        self.button_layout = QtWidgets.QHBoxLayout(self.buttons)
        self.button_layout.setContentsMargins(*self.CONTENTS_MARGINS)
        self.button_layout.addWidget(self.restart_button)
        self.button_layout.addWidget(self.stop_button)
        self.button_layout.addStretch(1)
        self.button_layout.addWidget(self.load_button)
        self.button_layout.addWidget(self.save_button)
        self.button_layout.addWidget(self.save_as_button)
        self.flare_web_tabbed_view = FlareEngineTabbedView(**{self.KEYNAME: self})
        self.layout = QtWidgets.QVBoxLayout(self.container)
        self.layout.setSpacing(2)
        self.layout.setContentsMargins(*self.CONTENTS_MARGINS)
        self.layout.addWidget(self.flare_web_tabbed_view)
        self.layout.addWidget(self.buttons)
        self.stacked_layout = QtWidgets.QStackedLayout(self)
        self.stacked_layout.addWidget(self.container)
        self.stacked_layout.setStackingMode(QtWidgets.QStackedLayout.StackAll)
        self.layout.addLayout(self.stacked_layout)
        QtCore.QTimer.singleShot(self.DEFERRED_INIT_DELAY_MS, self.deferred_init)


class FlareEngineTabbedView(QtWidgets.QTabWidget):
    """Multi-tabbed FlareEngineViewStackedLayout manager."""

    MAIN_TAB_TITLE = "Flare Notebook"
    LOADING_TAB_TITLE = "Loading..."

    def __init__(self, *args, **kwargs):
        self.py_notebook = kwargs.pop(PythonNotebookWidget.KEYNAME, None)
        super().__init__(*args, **kwargs)
        self.main_tab = FlareEngineViewStackedLayout(self)
        self.addTab(
            self.main_tab,
            QtGui.QIcon(os.path.join(flare_extension_dir(), "flare_logo_16x16.png")),
            self.MAIN_TAB_TITLE,
        )
        self.setTabsClosable(True)
        self.tabBar().tabButton(0, QtWidgets.QTabBar.RightSide).deleteLater()
        self.tabBar().setTabButton(0, QtWidgets.QTabBar.RightSide, None)
        self.currentChanged.connect(self.on_tab_change)
        self.tabCloseRequested.connect(self.on_tab_close)

    def get_current_index_and_view(self):
        """Return current index and widget."""
        return self.currentIndex(), self.currentWidget()

    def on_tab_change(self, *args, **kwargs):
        """Call when current tab has changed."""
        i = self.currentIndex()
        if i == 0:
            if self.py_notebook.restart_button_prev_state is not None:
                self.py_notebook.restart_button.setEnabled(
                    self.py_notebook.restart_button_prev_state
                )
            if self.py_notebook.stop_button_prev_state is not None:
                self.py_notebook.stop_button.setEnabled(self.py_notebook.stop_button_prev_state)
        elif i > 0:
            self.py_notebook.restart_button.setEnabled(True)
            self.py_notebook.stop_button.setEnabled(True)

    def on_tab_close(self, i, can_cancel=True):
        """Call when a tab is about to be closed."""
        view = self.widget(i)
        if view._ipynb_basename is not None:
            view._view.page().runJavaScript(PythonNotebookWidget.CLOSE_AND_HALT_JS)
            current_ipynb, current_ipynb_basename = view.get_notebook_path_and_basename()
            if current_ipynb:
                SafeFileRemover.remove(
                    [
                        current_ipynb,
                        os.path.join(
                            self.py_notebook.launcher.notebook_cmp_dir, current_ipynb_basename
                        ),
                    ],
                    parent=view,
                )
        self.removeTab(i)

    def create_tab(self, view):
        """Call when a new tab is to be created."""
        i = self.addTab(view, self.LOADING_TAB_TITLE)
        self.setCurrentIndex(i)
        log.debug("view {0:s} to tab {1:d}\n".format(str(view), i))


class FlareEngineView(QWebEngineView):
    """Override QWebEngineView."""

    ZOOM_STEP = 0.1
    MIN_ZOOM_FACTOR = 0.3
    MAX_ZOOM_FACTOR = 5.0

    class ZoomPopup(QtWidgets.QDialog):
        """Display a pop-up with the zoom factor for PERSISTENCE_TIME_MS ms.

        Signal the parent after the timeout has occurred.
        """

        PERSISTENCE_TIME_MS = 500

        def __init__(self, parent, zoom_factor):
            super().__init__(parent, QtCore.Qt.Widget | QtCore.Qt.FramelessWindowHint)
            self.setModal(False)
            layout = QtWidgets.QVBoxLayout(self)
            label = QtWidgets.QLabel("{0:d}%".format(int(round(zoom_factor * 100))), self)
            layout.addWidget(label)
            self.show()
            QtCore.QTimer.singleShot(self.PERSISTENCE_TIME_MS, parent.close_zoom_popup)

    def __init__(self, *args, **kwargs):
        self._partial_wheel_step = 0.0
        self._popup = None
        super().__init__(*args, **kwargs)
        self.setPage(FlareEnginePage(self))
        self.titleChanged.connect(self.parent().on_title_changed)
        self.urlChanged.connect(self.parent().on_url_changed)
        self.loadFinished.connect(self.parent().on_load_finished)

    def wheelEvent(self, event):
        """Call when a wheelEvent is received.

        Sets the appropriate zoom factor and briefly displays it in a pop-up.
        """
        if not (event.modifiers() & QtCore.Qt.ControlModifier):
            event.ignore()
            return
        deg = event.angleDelta().y() / 8.0
        step = deg / 15.0
        self._partial_wheel_step += step
        if abs(self._partial_wheel_step) < 1.0:
            return
        n_step = int(self._partial_wheel_step)
        self._partial_wheel_step -= n_step
        zf = self.zoomFactor() + n_step * self.ZOOM_STEP
        zf = max(min(zf, self.MAX_ZOOM_FACTOR), self.MIN_ZOOM_FACTOR)
        self.setZoomFactor(zf)
        self.close_zoom_popup()
        self._popup = self.ZoomPopup(self, zf)
        event.accept()

    def close_zoom_popup(self):
        """Call when the zoom factor pop-up is to be closed."""
        if self._popup is not None:
            self._popup.deleteLater()
            self._popup = None

    def showEvent(self, event):
        """Check button state at each showEvent."""
        log.debug("showEvent")
        self.parent().set_button_state()
        super().showEvent(event)

    def createWindow(self, type):
        """Override createWindow."""
        log.debug("FlareEngineView.createWindow() url:{0:s}".format(self.url().url()))
        view = None
        if type == QWebEnginePage.WebBrowserTab:
            view = FlareEngineViewStackedLayout(self.parent()._flare_web_tabbed_view)
            self.parent()._flare_web_tabbed_view.create_tab(view)
            view = view._view
        else:
            view = super().createWindow(self, type)
        return view


class FlareEngineViewStackedLayout(QtWidgets.QWidget):
    """Stacked layout of FlareEngineView and a FileStatMonitor."""

    IPYNB_URL_REGEX = re.compile("http://localhost:(\\d+)/notebooks/(.*\\.ipynb).*$")

    def __init__(self, *args, **kwargs):
        self._ipynb_basename = None
        self._prev_ipynb_basename = None
        self._file_path = None
        super().__init__(*args, **kwargs)
        self._view = FlareEngineView(self)
        self.stacked_layout = QtWidgets.QStackedLayout(self)
        self.stacked_layout.addWidget(self._view)
        self.stacked_layout.setStackingMode(QtWidgets.QStackedLayout.StackAll)
        self.setLayout(self.stacked_layout)
        self._flare_web_tabbed_view = self.parent()
        self._view.setPage(FlareEnginePage(self))
        self._view.titleChanged.connect(self.on_title_changed)
        self._view.urlChanged.connect(self.on_url_changed)
        self._view.loadFinished.connect(self.on_load_finished)

    @staticmethod
    def get_notebook_cells(ipynb_filename):
        """Return the 'cells' item from the notebook JSON dict, or None."""
        try:
            with open(ipynb_filename, "r") as ipynb_hnd:
                ipynb_json = json.load(ipynb_hnd)
        except Exception:
            log.warning(
                "get_notebook_cells failed to open {0:s} "
                "for reading. Stack trace:\n{1:s}".format(
                    ipynb_filename, "".join(traceback.format_exc())
                )
            )
            return None
        return ipynb_json.get("cells", None)

    def has_notebook_changed(self):
        """Return True if notebook has changed from the last save."""
        # we need to save upfront or latest changes wouldn't be picked up
        current_ipynb, current_ipynb_basename = self.get_notebook_path_and_basename()
        self.save_notebook(save_path=current_ipynb, op="checkpoint")
        current_ipynb_cmp = os.path.join(
            self._flare_web_tabbed_view.py_notebook.launcher.notebook_cmp_dir,
            current_ipynb_basename,
        )
        current_ipynb_cells = self.get_notebook_cells(current_ipynb)
        current_ipynb_cmp_cells = self.get_notebook_cells(current_ipynb_cmp)
        if str(current_ipynb_cells) != str(current_ipynb_cmp_cells):
            log.debug(
                "has_notebook_changed\n{0:s}\n{1:s}".format(
                    str(current_ipynb_cells), str(current_ipynb_cmp_cells)
                )
            )
        # if the user quits Flare early the cmp file has not been generated yet
        if current_ipynb_cmp_cells is None:
            return False
        return (not current_ipynb_cells) or (
            str(current_ipynb_cells) != str(current_ipynb_cmp_cells)
        )

    def url_to_filename(self, url_string):
        """Return notebook basename from URL."""
        res = None
        m = self.IPYNB_URL_REGEX.match(url_string)
        log.debug("url_to_filename {0:s} {1:s}".format(str(m), str(m.groups() if m else None)))
        if m and len(m.groups()) > 1:
            res = m.groups()[1]
        return res

    def set_ipynb_basename(self, url_string):
        """Set the notebook basename from a URL string."""
        self._ipynb_basename = self.url_to_filename(url_string)

    def get_notebook_path_and_basename(self):
        """Return (notebook abs path, basename), or (None, None) if no notebook."""
        if not self._ipynb_basename:
            return None, None
        launcher = self._flare_web_tabbed_view.py_notebook.launcher
        return os.path.join(launcher.notebook_dir, self._ipynb_basename), self._ipynb_basename

    def set_notebook_file(self, notebook_file):
        """Load a notebook file using setUrl().

        This must be used only to load the initial blank notebook in the main tab.
        Will throw an AssertionError if called from another tab.
        """
        assert self == self._flare_web_tabbed_view.main_tab
        notebook_base = os.path.basename(notebook_file)
        launcher = self._flare_web_tabbed_view.py_notebook.launcher
        try:
            with open(launcher.attach_lock, "w"):
                pass
        except Exception:
            log.critical(
                "set_notebook_file failed to open {0:s} "
                "for writing. Stack trace:\n{1:s}".format(
                    launcher.attach_lock, "".join(traceback.format_exc())
                )
            )
            if not flare_is_shutting_down():
                raise
        if launcher.port and launcher.token:
            log.debug(
                "set_notebook_file main_tab http://localhost:{0:s}/notebooks/{1:s}{2:s}".format(
                    launcher.port, notebook_base, launcher.token
                )
            )
            self._view.page().setUrl(
                "http://localhost:{0:s}/notebooks/{1:s}{2:s}".format(
                    launcher.port, str(notebook_base), launcher.token
                )
            )
        self._ipynb_basename = notebook_base

    def replace_notebook(self, src_path, src_basename_no_ext=None, content=None):
        """Replace the contents of a notebook.

        If content is None, content will be read from src_path.
        If src_basename_no_ext is None, it will be set to the basename of src_path
        without extension.
        """
        log.debug(
            "replace_notebook index = {0:d}".format(self._flare_web_tabbed_view.indexOf(self))
        )
        py_notebook = self._flare_web_tabbed_view.py_notebook
        if self == self._flare_web_tabbed_view.main_tab:
            log.debug("1) replace_notebook main_tab")
            py_notebook.kernelapp.shell.reset()
            py_notebook.kernelapp._init_flare_shell()
        if src_basename_no_ext is None:
            src_basename = os.path.basename(src_path)
            src_basename_no_ext = os.path.splitext(src_basename)[0]
        else:
            src_basename = src_basename_no_ext + ".ipynb"
        if content is None:
            try:
                with open(src_path, "r") as hnd:
                    content = json.load(hnd)
            except Exception:
                log.critical(
                    "replace_notebook failed to open {0:s} "
                    "for reading. Stack trace:\n{1:s}".format(
                        src_path, "".join(traceback.format_exc())
                    )
                )
                raise
        for cell in content.get("cells", []):
            source = cell.get("source", None)
            if isinstance(source, list):
                cell["source"] = "".join(source)
        data = json.dumps(
            json.dumps({"content": content, "name": src_basename, "path": src_basename})
        )
        log.debug("replace_notebook data:\n{0:s}".format(data))
        current_ipynb, current_ipynb_basename = self.get_notebook_path_and_basename()
        cmd = PythonNotebookWidget.FROM_JSON_JS.format(data)
        if current_ipynb_basename != src_basename_no_ext:
            cmd = PythonNotebookWidget.RENAME_JS.format(src_basename_no_ext) + cmd
        self._view.page().runJavaScript(cmd)
        prev_checkpoint = os.path.join(
            self._flare_web_tabbed_view.py_notebook.launcher.notebook_cmp_dir,
            current_ipynb_basename,
        )
        try:
            os.remove(prev_checkpoint)
        except Exception:
            pass
        self._ipynb_basename = src_basename
        self.save_notebook(
            save_path=os.path.join(
                self._flare_web_tabbed_view.py_notebook.launcher.notebook_cmp_dir, src_basename
            ),
            op="checkpoint",
        )

    def save_and_checkpoint_js(self):
        """Save and checkpoint notebook calling the JavaScript API."""
        log.debug("1) save_and_checkpoint_js")
        self._view.page().runJavaScript(PythonNotebookWidget.SAVE_AND_CHECKPOINT_JS)
        log.debug("2) save_and_checkpoint_js")

    def save_notebook(self, save_path=None, op="save"):
        """Save notebook to save_path.

        If save_path is None, the notebook will be saved to the file_path
        currently stored in the object.
        'op' is used to customize the messages.
        """
        log.debug("save_notebook save_path={0:s}, op={1:s}".format(str(save_path), op))
        if save_path is None:
            save_path = self._file_path
        current_ipynb, current_ipynb_basename = self.get_notebook_path_and_basename()
        file_path_basename = os.path.basename(save_path)
        success = False
        with DisableWidget(self._flare_web_tabbed_view.py_notebook.buttons), FileStatMonitor(
            file=current_ipynb,
            parent=self,
            text="Checkpointing notebook...",
            init_func=self.save_and_checkpoint_js,
        ) as fs:
            try:
                fs.wait_until_stable()
            except FileStatMonitor.CannotAccess:
                QtWidgets.QMessageBox.warning(
                    self,
                    "Failed to {0:s} notebook".format(op),
                    "The checkpoint file\n{0:s}\ncould not be accessed.\n".format(current_ipynb),
                    QtWidgets.QMessageBox.Ok,
                )
                log.warning(
                    "Failed to {0:s} notebook to {1:s} as the checkpoint file {2:s} "
                    "could not be accessed.".format(op, save_path, current_ipynb)
                )
            except FileStatMonitor.NoChange:
                # There has been no changes since the last save
                success = True
            except FileStatMonitor.Empty:
                QtWidgets.QMessageBox.warning(
                    self,
                    "Failed to {0:s} notebook".format(op),
                    "The checkpoint file {0:s} is empty.\n".format(current_ipynb),
                    QtWidgets.QMessageBox.Ok,
                )
                log.warning(
                    "Failed to {0:s} notebook to {1:s} as the checkpoint file {2:s} "
                    "is empty.".format(op, save_path, current_ipynb)
                )
            else:
                success = True
            if success and current_ipynb != save_path:
                log.debug(
                    "save_notebook Now copying {0:s} to {1:s}".format(current_ipynb, save_path)
                )
                try:
                    shutil.copyfile(current_ipynb, save_path)
                except shutil.SameFileError:
                    msg = "{0:s} and {1:s} are the same file.".format(current_ipynb, save_path)
                    success = False
                except Exception:
                    msg = (
                        "there was an error while attempting to copy\n"
                        "{0:s}\nto {1:s}:\n{2:s}".format(
                            current_ipynb, save_path, "".join(traceback.format_exc())
                        )
                    )
                    success = False
                if not success:
                    QtWidgets.QMessageBox.warning(
                        self,
                        "Failed to {0:s} notebook".format(op),
                        "Failed to {0:s} notebook:\n{1:s}.".format(op, msg),
                        QtWidgets.QMessageBox.Ok,
                    )
                    log.warning(
                        "Failed to copy notebook from '{0:s}' to '{1:s}': {2:s}".format(
                            current_ipynb, save_path, msg
                        )
                    )
            if success and current_ipynb_basename != file_path_basename:
                file_path_basename_no_ext = os.path.splitext(file_path_basename)[0]
                self._view.page().runJavaScript(
                    PythonNotebookWidget.RENAME_JS.format(file_path_basename_no_ext)
                )
                os.rename(
                    os.path.join(
                        self._flare_web_tabbed_view.py_notebook.launcher.notebook_cmp_dir,
                        current_ipynb_basename,
                    ),
                    os.path.join(
                        self._flare_web_tabbed_view.py_notebook.launcher.notebook_cmp_dir,
                        file_path_basename,
                    ),
                )
                current_ipynb_basename = file_path_basename
            if op == "save":
                shutil.copyfile(
                    current_ipynb,
                    os.path.join(
                        self._flare_web_tabbed_view.py_notebook.launcher.notebook_cmp_dir,
                        current_ipynb_basename,
                    ),
                )

        return success

    def set_button_state(self):
        """Enable buttons only if a notebook is associated to this view."""
        self._flare_web_tabbed_view.py_notebook.buttons.setEnabled(self._ipynb_basename is not None)

    def on_title_changed(self, title):
        """Call when the tab title has changed."""
        log.debug("on_title_changed {0:s}".format(title))
        i = self._flare_web_tabbed_view.indexOf(self)
        if i != -1:
            title = title.strip()
            short_title = title if (len(title) <= 12) else (title[:10] + "...")
            self._flare_web_tabbed_view.setTabText(i, short_title)
            self._flare_web_tabbed_view.setTabToolTip(
                i,
                "This tab {0:s} access to flare.main_window()\n{1:s}".format(
                    "has" if i == 0 else "does not have", title
                ),
            )

    def on_load_finished(self, ok):
        """Call when the page has finished to load."""
        log.debug("on_load_finished {0:s}".format(str(ok)))
        current_ipynb, current_ipynb_basename = self.get_notebook_path_and_basename()
        if not current_ipynb:
            return
        if current_ipynb_basename != self._prev_ipynb_basename:
            self._prev_ipynb_basename = current_ipynb_basename
            self.save_notebook(save_path=current_ipynb, op="checkpoint")
            shutil.copyfile(
                current_ipynb,
                os.path.join(
                    self._flare_web_tabbed_view.py_notebook.launcher.notebook_cmp_dir,
                    current_ipynb_basename,
                ),
            )

    def on_url_changed(self, url):
        """Call when the URL has changed."""
        log.debug("on_url_changed {0:s}".format(url.url()))
        _, prev_ipynb_basename = self.get_notebook_path_and_basename()
        self.set_ipynb_basename(url.url())
        _, curr_ipynb_basename = self.get_notebook_path_and_basename()
        if (
            prev_ipynb_basename
            and curr_ipynb_basename
            and curr_ipynb_basename != prev_ipynb_basename
        ):
            try:
                os.rename(
                    os.path.join(
                        self._flare_web_tabbed_view.py_notebook.launcher.notebook_cmp_dir,
                        prev_ipynb_basename,
                    ),
                    os.path.join(
                        self._flare_web_tabbed_view.py_notebook.launcher.notebook_cmp_dir,
                        curr_ipynb_basename,
                    ),
                )
            except FileNotFoundError:
                pass
        if self._file_path and os.path.basename(self._file_path) != self._ipynb_basename:
            self._file_path = None
        self.set_button_state()


class FlareEnginePage(QWebEnginePage):
    """Override QWebEnginePage."""

    EXTENSION_TO_FILE_TYPE_DICT = {
        ".ipynb": "Jupyter Notebook",
        ".py": "Python script",
        ".html": "HTML document",
        ".md": "Markdown",
        ".rst": "reStructuredText",
        ".tex": "LaTeX",
        ".pdf": "PDF via LaTeX",
    }
    DOWNLOAD_URL_REGEX = re.compile("http://localhost:(\\d+)/nbconvert/\\w+/(.*\\.ipynb).*$")

    @staticmethod
    def extension_to_file_type(ext):
        """Return a description of the file type based on the extension."""
        return FlareEnginePage.EXTENSION_TO_FILE_TYPE_DICT.get(ext, "Unknown file type")

    def javaScriptConfirm(self, *args, **kwargs):
        """Do not display confirmation dialogs before running JavaScript."""
        return True

    def triggerAction(self, action, checked):
        """Override triggerAction to load the latest checkpoint page upon refresh."""
        if action == QWebEnginePage.Reload and isinstance(self.parent(), FlareEngineView):
            view = self.parent()
            current_ipynb, current_ipynb_basename = view.get_notebook_path_and_basename()
            if current_ipynb:
                view.save_notebook(save_path=current_ipynb, op="checkpoint")
            log.debug("triggerAction reload")
        return super().triggerAction(action, checked)

    def on_download_requested(self, download_item):
        """Call when an export action is requested."""
        if download_item.state() != QWebEngineDownloadItem.DownloadRequested:
            return
        url_string = download_item.url().url()
        m = self.DOWNLOAD_URL_REGEX.match(url_string)
        log.debug(
            "on_download_requested self:{0:s} sender:{1:s} url:{2:s} match:{3:s}".format(
                str(self), str(self.sender()), url_string, str(m)
            )
        )
        if not m:
            return
        filename = os.path.basename(download_item.path())
        ext = os.path.splitext(filename)[1]
        kwargs = {}
        if self._view:
            kwargs["cwd"] = self._view.get_last_visited_dir()
        try:
            file_path, _ = MyFileDialog.getSaveFileName(
                None,
                "Export Python Notebook as...",
                filter="{0:s} (*{1:s})".format(self.extension_to_file_type(ext), ext),
                filename=filename,
                **kwargs
            )
        except Exception as e:
            QtWidgets.QMessageBox.warning(
                self,
                "Failed to export notebook",
                "An error occurred while attempting to export your notebook:\n"
                "{0:s}".format(str(e)),
                QtWidgets.QMessageBox.Ok,
                QtWidgets.QMessageBox.StandardButton(QtWidgets.QMessageBox.Ok),
            )
        if file_path:
            download_item.setPath(file_path)
            download_item.finished.connect(self.on_download_finished)
            download_item.accept()
        else:
            download_item.cancel()
            self.on_download_finished()

    def on_download_finished(self):
        """Call when an export action is finished."""
        log.debug("on_download_finished")
        if (not self.parent()) or (not hasattr(self.parent(), "_flare_web_tabbed_view")):
            return
        tabbed_view = self.parent()._flare_web_tabbed_view
        if tabbed_view.count() > 0:
            i = tabbed_view.count() - 1
            if tabbed_view.widget(i)._ipynb_basename is None:
                tabbed_view.removeTab(i)

    def __init__(self, *args, **kwargs):
        self._view = kwargs.get("parent", None)
        super().__init__(*args, **kwargs)
        log.debug(
            "FlareEnginePage.__init__() self:{0:s} self.profile():{1:s}".format(
                str(self), str(self.profile())
            )
        )
        self.profile().downloadRequested.connect(self.on_download_requested)
