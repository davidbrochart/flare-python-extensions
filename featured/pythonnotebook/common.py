# -*- coding: utf-8 -*-
# Copyright (C) 2023 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Common utility classes."""

import os
import traceback
from PySide2 import QtCore, QtGui, QtWidgets
from .configurelogging import get_logger
from cresset import flare

log = get_logger("common")


def flare_extension_dir():
    """Return the directory we are in, i.e. the Flare Python extensions dir."""
    return os.path.dirname(os.path.realpath(__file__))


def flare_is_shutting_down():
    """Return True if flare is shutting down."""
    return (not flare.main_window()) or (not flare.main_window().widget().isVisible())


class DisableWidget:
    """Disable widget on enter; re-enable it on exit."""

    def __init__(self, widget):
        self._widget = widget

    def __enter__(self):
        if self._widget:
            self._widget.setEnabled(False)
        return self

    def __exit__(self, exc_type, exc_value, trace_back):
        if self._widget:
            self._widget.setEnabled(True)


class ProgressBar:
    """Context manager which shows a semi-transparent progress bar."""

    class ProgressBarWidget(QtWidgets.QWidget):
        """Widget which contains a semi-transparent progress bar."""

        ALPHA = 200

        def __init__(self, **kwargs):
            self._parent = kwargs.get("parent", None)
            text = kwargs.get("text", "Preparing...")
            super().__init__(self._parent)
            self.container = QtWidgets.QWidget(self)
            self.v_layout = QtWidgets.QVBoxLayout(self.container)
            self.label = QtWidgets.QLabel(text, self.container)
            self.progress_bar = QtWidgets.QProgressBar(self.container)
            self.progress_bar.setTextVisible(False)
            self.progress_bar.setRange(0, 100)
            self.v_layout.addStretch(1)
            self.v_layout.addWidget(self.label)
            self.v_layout.addWidget(self.progress_bar)
            self.v_layout.addStretch(1)
            self.h_layout = QtWidgets.QHBoxLayout(self)
            self.h_layout.addStretch(1)
            self.h_layout.addWidget(self.container)
            self.h_layout.addStretch(1)
            self._parent.stacked_layout.addWidget(self)
            self._parent.stacked_layout.setCurrentIndex(1)

        def setValue(self, v):
            """Set the value of the progress bar."""
            self.progress_bar.setValue(v)

        def value(self):
            """Return the value of the progress bar."""
            return self.progress_bar.value()

        def paintEvent(self, event):
            """Paint the semi-transparent progress bar."""
            bg_color = self.palette().light().color()
            bg_color.setAlpha(self.ALPHA)
            painter = QtGui.QPainter(self)
            painter.fillRect(self.rect(), bg_color)

    def __init__(self, **kwargs):
        self._parent = kwargs["parent"]
        cache = kwargs.get("cache", type(self).__name__)
        if hasattr(self._parent, cache):
            self.widget = getattr(self._parent, cache)
        else:
            self.widget = self.ProgressBarWidget(**kwargs)
            setattr(self._parent, cache, self.widget)
        self.setValue(0)
        self._parent.stacked_layout.addWidget(self.widget)
        self._parent.stacked_layout.setCurrentWidget(self.widget)

    def __enter__(self):
        return self

    def setValue(self, v):
        """Set the value of the progress bar."""
        self.widget.progress_bar.setValue(v)

    def value(self):
        """Return the value of the progress bar."""
        return self.widget.progress_bar.value()

    def __exit__(self, exc_type, exc_value, trace_back):
        self._parent.stacked_layout.removeWidget(self.widget)


class GettingNotebookReady(ProgressBar):
    """Context manager which shows a semi-transparent progress bar."""

    pass


class FileStatMonitor(ProgressBar):
    """Class which monitors change in a file and displays a progressbar."""

    MAX_ATTEMPTS = 5
    WAIT_FOR_MS = 100
    FIRST_TIMEOUT = 1000
    SECOND_TIMEOUT = 500

    class CannotAccess(Exception):
        """Exception thrown if the file cannot be accessed."""

        pass

    class NoChange(Exception):
        """Exception thrown if the file has not changed."""

        pass

    class Empty(Exception):
        """Exception thrown if the file has zero content."""

        pass

    def _start_monitoring(self):
        """Get the initial stat of the file, then call _init_func if not None."""
        if self._prev_stat is None:
            try:
                self._prev_stat = os.stat(self._file)
            except Exception:
                pass
        if self._init_func:
            self._init_func()

    def wait_until_stable(self, max_attempts=MAX_ATTEMPTS):
        """Wait until the file stat has stabilized or timeout occurs.

        MAX_ATTEMPTS to detect file change are made; if no change takes place,
        a NoChange exception is thrown. The other exceptions are thrown upon
        first failure.
        """
        attempts = 0
        max_attempts = self.MAX_ATTEMPTS
        success = False
        p = 0
        maxp = self.FIRST_TIMEOUT + self.SECOND_TIMEOUT
        has_changed = False
        still_changing = False
        now_existing = False
        while (not success) and (attempts < max_attempts):
            attempts += 1
            self._start_monitoring()
            t = self.FIRST_TIMEOUT
            with DisableWidget(self._parent._flare_web_tabbed_view.py_notebook.buttons):
                while t:
                    v = min(100, int(100.0 * p / maxp))
                    log.debug("wait_until_stable v = {0:d}".format(v))
                    self.setValue(v)
                    QtWidgets.QApplication.instance().processEvents()
                    if os.path.isfile(self._file):
                        if not now_existing:
                            log.debug("now existing")
                            t = self.FIRST_TIMEOUT
                        now_existing = True
                        try:
                            current_stat = os.stat(self._file)
                        except Exception:
                            log.warning(
                                "wait_until_stable exception:\n{0:s}".format(
                                    "".join(traceback.format_exc())
                                )
                            )
                            raise self.CannotAccess
                        still_changing = (not self._prev_stat) or (
                            current_stat.st_mtime != self._prev_stat.st_mtime
                        )
                    if still_changing:
                        log.debug("wait_until_stable still_changing")
                        if not has_changed:
                            log.debug("wait_until_stable has_changed = True")
                            has_changed = True
                        self._prev_stat = current_stat
                        t = self.SECOND_TIMEOUT
                    t -= self.WAIT_FOR_MS
                    p += self.WAIT_FOR_MS / attempts
                    log.debug("wait_until_stable t = {0:d}".format(t))
                    QtCore.QThread.msleep(self.WAIT_FOR_MS)
                if not self._prev_stat.st_size:
                    raise self.Empty
            success = has_changed and (not still_changing)
        self.setValue(100)
        # if Flare is shutting down no surprise the notebook did not get any update
        if (not flare_is_shutting_down()) and (not has_changed):
            raise self.NoChange

    def __init__(self, **kwargs):
        self._file = kwargs["file"]
        self._init_func = kwargs.get("init_func", None)
        self._prev_stat = None
        super().__init__(**kwargs)


class SafeFileRemover(ProgressBar):
    """Class which tries multiple times to remove one or more files.

    Multiple attempts can be needed as the file may still be in use.
    A progressbar is displayed meanwhile.
    Use it as follows:
    SafeFileRemover.remove(filename or [filenames], parent=parent)
    """

    MAX_ATTEMPTS = 30
    WAIT_FOR_MS = 100

    class CannotRemove(Exception):
        """Exception thrown if the file cannot be removed."""

        pass

    @classmethod
    def remove(cls, files=[], **kwargs):
        """Try to remove the files."""
        files = {files} if isinstance(files, str) else set(files)
        if "text" not in kwargs:
            kwargs["text"] = "Cleaning up temporary files..."
        with cls(**kwargs) as self:
            self._attempts = {f: self.MAX_ATTEMPTS for f in files}
            while files:
                to_be_removed = set()
                for f in files:
                    try:
                        os.remove(f)
                        to_be_removed.add(f)
                    except FileNotFoundError:
                        to_be_removed.add(f)
                    except Exception:
                        if not self._attempts[f]:
                            to_be_removed.add(f)
                            break
                        else:
                            self._attempts[f] -= 1
                files = files.difference(to_be_removed)
                if files:
                    QtCore.QThread.msleep(self.WAIT_FOR_MS)
                    self.setValue(
                        self.MAX_ATTEMPTS
                        - int(max(self._attempts.values()) / self.MAX_ATTEMPTS * 100)
                    )
            failures = [k for k, v in self._attempts.items() if (not v)]
            if failures:
                raise self.CannotRemove(
                    "Failed to remove the following " "files:\n{0:s}".format("\n".join(failures))
                )

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
