# -*- coding: utf-8 -*-
# Copyright (C) 2023 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Classes to manage integration between IPython kernel and Qt event loop."""

import sys
import ctypes
from PySide2 import QtCore, QtWidgets
from .configurelogging import get_logger
from .common import flare_is_shutting_down

log = get_logger("eventloop")


class EventLoopHelper(object):
    """Singleton class to process Qt events while Python code is running in the IPython kernel."""

    MAX_RESOURCES = 1
    _instance = None

    def __new__(cls):
        """Call upon class creation."""
        if cls._instance is None:
            cls._instance = super().__new__(cls)
            cls._instance.original_displayhook = None
            cls._instance.ipykernel_displayhook = None
            cls._instance.original_excepthook = None
            cls._instance.ipykernel_excepthook = None
            cls._instance.stop_event_loop = True
            cls._instance.stop_button_clicked = False
            cls._instance.add_run_event_loop_pending_call_thread = None
            cls._instance.event_loop_semaphore = QtCore.QSemaphore(EventLoopHelper.MAX_RESOURCES)
            cls._instance.stop_event_loop_mutex = QtCore.QMutex(mode=QtCore.QMutex.Recursive)
            cls._instance.session_loading = False
        return cls._instance

    def set_loading_session(self, state):
        """Set a boolean flag if a QtConsole session is being loaded."""
        locker = QtCore.QMutexLocker(self.stop_event_loop_mutex)
        self.session_loading = state
        locker.unlock()

    def is_loading_session(self):
        """Return True if a QtConsole session is being loaded."""
        locker = QtCore.QMutexLocker(self.stop_event_loop_mutex)
        res = self.session_loading
        locker.unlock()
        return res

    def run_event_loop(self, c_ptr):
        """Process Qt events while Python code is running in the PythonNotebook."""
        log.debug("1) EventLoopHelper.run_event_loop()")
        locker = QtCore.QMutexLocker(self.stop_event_loop_mutex)
        log.debug("2) EventLoopHelper.run_event_loop()")
        if self.stop_button_clicked or flare_is_shutting_down():
            log.debug("3) EventLoopHelper.run_event_loop() stop")
            stop_button_clicked = self.stop_button_clicked
            self.stop_button_clicked = False
            self.session_loading = False
            locker.unlock()
            self.shutdown_event_loop()
            if stop_button_clicked:
                ctypes.pythonapi.PyErr_SetInterrupt()
            return 0
        if not self.stop_event_loop:
            locker.unlock()
            log.debug("4) EventLoopHelper.run_event_loop() processEvents()")
            QtWidgets.QApplication.instance().processEvents()
        else:
            log.debug("5) EventLoopHelper.run_event_loop() stopped running event loop")
        self.event_loop_semaphore.release()
        return 0

    def shutdown_event_loop(self):
        """Stop processing Qt events while Python code is running in the PythonNotebook."""
        log.debug("EventLoopHelper.shutdown_event_loop()")
        if self.add_run_event_loop_pending_call_thread:
            log.debug("EventLoopHelper.shutdown_event_loop() acquire mutex")
            locker = QtCore.QMutexLocker(self.stop_event_loop_mutex)
            if self.original_displayhook:
                sys.displayhook = self.original_displayhook
            if self.original_excepthook:
                sys.excepthook = self.original_excepthook
            if not self.stop_event_loop:
                log.debug("EventLoopHelper.shutdown_event_loop() requested stopping event loop")
                self.stop_event_loop = True
            locker.unlock()
            log.debug("1) EventLoopHelper.shutdown_event_loop()")
            self.add_run_event_loop_pending_call_thread.wait()
            log.debug("2) EventLoopHelper.shutdown_event_loop()")
            del self.add_run_event_loop_pending_call_thread
            self.add_run_event_loop_pending_call_thread = None
            log.debug("EventLoopHelper.shutdown_event_loop() event loop stopped")


class BlockEventLoop:
    """Context manager class to prevent the Qt event loop from running."""

    def __init__(self, kernel_client):
        self.kernel_client = kernel_client

    def __enter__(self):
        self.kernel_client.run_event_loop = False
        return self

    def __exit__(self, exc_type, exc_value, trace_back):
        self.kernel_client.run_event_loop = True


class PythonRunQtEventLoop:
    """Context manager class to run a Qt event loop while Python code is being executed."""

    def __init__(self, run_event_loop=True, callback=None):
        self.run_event_loop = run_event_loop
        self.elh = EventLoopHelper()
        self.callback = callback

    def __enter__(self):
        if self.run_event_loop:
            locker = QtCore.QMutexLocker(self.elh.stop_event_loop_mutex)
            if self.elh.ipykernel_displayhook:
                sys.displayhook = self.elh.ipykernel_displayhook
            if self.elh.ipykernel_excepthook:
                sys.excepthook = self.elh.ipykernel_excepthook
            if (
                QtWidgets.QApplication
                and QtWidgets.QApplication.instance()
                and self.elh.stop_event_loop
            ):
                log.debug("PythonRunQtEventLoop.__enter__(): started running event loop")
                self.elh.stop_event_loop = False
                self.elh.add_run_event_loop_pending_call_thread = AddRunEventLoopPendingCall()
                self.elh.add_run_event_loop_pending_call_thread.start()
            locker.unlock()
            if self.callback:
                self.callback(True)
        return self

    def __exit__(self, exc_type, exc_value, trace_back):
        log.debug("PythonRunQtEventLoop.__exit__()")
        if self.run_event_loop:
            self.elh.shutdown_event_loop()
            if self.callback:
                self.callback(False)


class AddRunEventLoopPendingCall(QtCore.QThread):
    """QThread class to call Py_AddPendingCall()."""

    RunEventLoopCType = ctypes.CFUNCTYPE(ctypes.c_int, ctypes.c_void_p)
    runEventLoopCFuncPtr = RunEventLoopCType(EventLoopHelper().run_event_loop)
    ShutdownEventLoopCType = ctypes.CFUNCTYPE(None)
    shutdownEventLoopCFuncPtr = ShutdownEventLoopCType(EventLoopHelper().shutdown_event_loop)
    SLEEP_INTERVAL_MS = 10

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.elh = EventLoopHelper()
        self.setObjectName("PythonNotebookProcessEvents")

    def run(self):
        """Call Py_AddPendingCall()."""
        if self.elh.event_loop_semaphore.available() != EventLoopHelper.MAX_RESOURCES:
            log.warning(
                "AddProcessEventsPendingCall.run(): thread started "
                "with invalid resource count of {0:d}, expected "
                "{1:d}".format(
                    self.elh.event_loop_semaphore.available(), EventLoopHelper.MAX_RESOURCES
                )
            )
            self.elh.event_loop_semaphore.release(
                EventLoopHelper.MAX_RESOURCES - self.elh.event_loop_semaphore.available()
            )
        self.setPriority(QtCore.QThread.LowestPriority)
        while 1:
            locker = QtCore.QMutexLocker(self.elh.stop_event_loop_mutex)
            if self.elh.stop_event_loop:
                log.debug("AddProcessEventsPendingCall.run() stop_event_loop is True")
                break
            locker.unlock()
            if self.elh.event_loop_semaphore.tryAcquire(1, 250):
                log.debug("AddProcessEventsPendingCall.run() before Py_AddPendingCall")
                result = ctypes.pythonapi.Py_AddPendingCall(
                    AddRunEventLoopPendingCall.runEventLoopCFuncPtr, None
                )
                log.debug("AddProcessEventsPendingCall.run() after Py_AddPendingCall")
                if result != 0:
                    log.warning(
                        "AddRunEventLoopPendingCall.run(): "
                        "failed to add pending call, error code {0:d}".format(result)
                    )
                    self.elh.event_loop_semaphore.release()
            QtCore.QThread.msleep(self.SLEEP_INTERVAL_MS)
        log.debug("AddProcessEventsPendingCall.run() stopped running event loop")
