#!/usr/bin/env pyflare
# -*- coding: utf-8 -*-
# Copyright (C) 2023 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Classes to start/stop a jupyter-notebook process.

The status of an upstream Flare process is monitored; if the the Flare process dies,
the jupyter-notebook process is shut down.
"""

import os
import tempfile
import sys
import site
import shutil
import subprocess
import re
from PySide2 import QtCore, QtWidgets

if sys.platform == "win32":
    import ctypes
    import ctypes.wintypes
import_dir = os.path.dirname(os.path.realpath(__file__))
if import_dir not in sys.path:
    sys.path.append(import_dir)
from htmlparsers import (  # noqa E402
    NotebookHTMLTrimMenus,
    NotebookHTMLRemoveExtraDividers,
    PageHTMLRemoveLoginWidget,
)
from configurelogging import get_logger  # noqa E402

log = get_logger("nblauncher")


class StartupInfo(object):
    """A singleton class to obtain a STARTUPINFO instance."""

    _instance = None

    def __new__(cls):
        """Call upon class creation."""
        if cls._instance is None:
            cls._instance = super().__new__(cls)
            si = None
            if sys.platform == "win32":
                si = subprocess.STARTUPINFO()
                si.dwFlags |= subprocess.STARTF_USESHOWWINDOW
                si.wShowWindow = subprocess.SW_HIDE
            setattr(StartupInfo._instance, "_si", si)
        return cls._instance._si


class JupyterNotebookLauncher(object):
    """A class to start/stop a Jupyter notebook."""

    UNTITLED_NOTEBOOK_FILE = "Untitled.ipynb"
    TEMPLATES_DIR = "templates"
    RUNTIME_DIR = "runtime"
    NOTEBOOK_DIR = "ipynb"
    NOTEBOOK_CMP_DIR = ".ipynb_cmp"
    HTTP_REGEX = re.compile("^\\s*http://.*:(\\d+)/(\\S*\\?*\\S*)&*\\S*\\s*$")
    ALIVE_POLL_INTERVAL_MS = 500
    TIMEOUT_JUPYTER_NOTEBOOK_MS = 30000
    SLEEP_MS = 100

    class BinaryNotFound(Exception):
        """Raised when jupyter-notebook binary was not found."""

        pass

    class CannotStart(Exception):
        """Raised when jupyter-notebook fails to start."""

        pass

    class CannotFindPortToken(Exception):
        """Raised when jupyter-notebook port and token cannot be found/parsed."""

        pass

    class CannotCreateTempDir(Exception):
        """Raised when a temporary directory cannot be created."""

        pass

    class CannotOpenNotebookStdoutFile(Exception):
        """Raised when a the file to redirect notebook stdout to cannot be created."""

        pass

    class CannotOpenNotebookStderrFile(Exception):
        """Raised when a the file to redirect notebook stderr to cannot be created."""

        pass

    class CannotOpenLauncherStdoutFile(Exception):
        """Raised when a the file to redirect launcher stdout to cannot be created."""

        pass

    class CannotOpenLauncherStderrFile(Exception):
        """Raised when a the file to redirect launcher stderr to cannot be created."""

        pass

    class LauncherNotRunning(Exception):
        """Raised when the launcher is not running."""

        pass

    @staticmethod
    def _flare_ipynb_dir(pid):
        """Return abs path to the flare_ipynb directory given a PID."""
        return os.path.join(tempfile.gettempdir(), "flare_ipynb.{0:d}".format(pid))

    @staticmethod
    def flare_ipynb_dir():
        """Return abs path to the flare_ipynb directory given current PID."""
        return JupyterNotebookLauncher._flare_ipynb_dir(os.getpid())

    def find_binary(self):
        """Find jupyter-notebook binary."""
        if self._jupyter_notebook_binary and os.path.isfile(self._jupyter_notebook_binary):
            return
        else:
            self._jupyter_notebook_binary = None
        jupyter_dirs = set()
        JUPYTER_NOTEBOOK_BINARY = "jupyter-notebook"
        if sys.platform == "win32":
            JUPYTER_NOTEBOOK_BINARY += ".exe"
            jupyter_dirs.update(
                set(
                    os.path.join(os.path.dirname(os.path.dirname(p)), "Scripts")
                    for p in sys.path
                    if "site-packages" in p
                )
            )
            jupyter_dirs.add(os.path.join(os.path.dirname(site.USER_SITE), "Scripts"))
        else:
            jupyter_dirs.add(os.path.join(sys.prefix, "bin"))
            jupyter_dirs.add(os.path.join(site.USER_BASE, "bin"))
        jupyter_notebook_binary = None
        for jupyter_dir in jupyter_dirs:
            jupyter_notebook_binary = os.path.join(jupyter_dir, JUPYTER_NOTEBOOK_BINARY)
            if not os.path.isfile(jupyter_notebook_binary):
                jupyter_notebook_binary = None
            else:
                break
        if not jupyter_notebook_binary:
            raise self.BinaryNotFound(
                "Cannot find a jupyter-notebook binary "
                "in any of these directories: {0:s}".format(", ".join(list(jupyter_dirs)))
            )
        self._jupyter_notebook_binary = jupyter_notebook_binary

    def env(self):
        """Return a customized environment to run processes in."""
        CRESSET_FLARE_PROCESS_PID = "CRESSET_FLARE_PROCESS_PID"
        JUPYTER_RUNTIME_DIR = "JUPYTER_RUNTIME_DIR"
        PYTHONPATH = "PYTHONPATH"
        CRESSET_NO_LOGFILE = "CRESSET_NO_LOGFILE"
        env = os.environ.copy()
        pythonpath = env.get(PYTHONPATH, "")
        import_dir = self.module_dir
        if pythonpath:
            pythonpath_as_list = pythonpath.split(os.pathsep)
            if import_dir in pythonpath_as_list:
                import_dir = ""
            else:
                import_dir += os.pathsep
            import_dir += pythonpath
        dllpath_dir = None
        if sys.platform == "win32":
            dllpath_dir = "PATH"
            dll_dir = os.path.dirname(os.path.realpath(sys.executable))
        elif sys.platform == "linux":
            dllpath_dir = "LD_LIBRARY_PATH"
            dll_dir = os.path.join(
                os.path.dirname(os.path.dirname(os.path.realpath(sys.executable))), "lib"
            )
        if dllpath_dir:
            dllpath = env.get(dllpath_dir, "")
            if dllpath:
                pythonpath_as_list = pythonpath.split(os.pathsep)
                if dll_dir in pythonpath_as_list:
                    dll_dir = ""
                else:
                    dll_dir += os.pathsep
                dll_dir += dllpath
            env[dllpath_dir] = dll_dir
        env.update(
            {
                PYTHONPATH: import_dir,
                CRESSET_FLARE_PROCESS_PID: str(self.flare_pid),
                JUPYTER_RUNTIME_DIR: self.runtime_dir,
                CRESSET_NO_LOGFILE: "1",
            }
        )
        return env

    def patch_notebook_html_template(self, notebook_html_template):
        """Generate a patched copy of the standard Jupyter notebook.html template."""
        parser = NotebookHTMLTrimMenus()
        parser.set_menus_to_remove([("id", "kernel_menu")])
        parser.set_menu_entries_to_remove(
            [("id", "open_notebook"), ("id", "close_and_halt"), ("id", "save_notebook_as")]
        )
        try:
            parser.feed(open(notebook_html_template, "r").read())
        except Exception:
            log.warning(
                "Failed to patch menus in notebook " "template {0:s}".format(notebook_html_template)
            )
            return
        parser.close()
        html = parser.dump()
        parser = NotebookHTMLRemoveExtraDividers()
        try:
            parser.feed(html)
        except Exception:
            log.warning(
                "Failed to patch dividers in notebook "
                "template {0:s}".format(notebook_html_template)
            )
            return
        parser.close()
        extra_notebook_template = os.path.join(self.templates_dir, "notebook.html")
        log.debug("extra_notebook_template = {0:s}".format(extra_notebook_template))
        try:
            open(extra_notebook_template, "w").write(
                parser.dump().replace("Download as", "Export as")
            )
        except Exception:
            log.warning(
                "Could not create patched notebook "
                "template {0:s}".format(extra_notebook_template)
            )

    def patch_page_html_template(self, page_html_template):
        """Generate a patched copy of the standard Jupyter page.html template."""
        parser = PageHTMLRemoveLoginWidget()
        try:
            parser.feed(open(page_html_template, "r").read())
        except Exception:
            log.warning(
                "Failed to patch login widget in page " "template {0:s}".format(page_html_template)
            )
            return
        parser.close()
        extra_page_template = os.path.join(self.templates_dir, "page.html")
        log.debug("extra_page_template = {0:s}".format(extra_page_template))
        try:
            open(extra_page_template, "w").write(parser.dump())
        except Exception:
            log.warning(
                "Could not create patched page " "template {0:s}".format(extra_page_template)
            )

    def patch_html_templates(self):
        """Generate patched copies of the standard Jupyter HTML templates."""
        html_template_dirs = set(
            os.path.join(p, "notebook", "templates") for p in sys.path if "site-packages" in p
        )
        log.debug("html_template_dirs = {0:s}".format(str(html_template_dirs)))
        for html_template_dir in html_template_dirs:
            notebook_html_template = os.path.join(html_template_dir, "notebook.html")
            if os.path.isfile(notebook_html_template):
                self.patch_notebook_html_template(notebook_html_template)
            page_html_template = os.path.join(html_template_dir, "page.html")
            if os.path.isfile(page_html_template):
                self.patch_page_html_template(page_html_template)

    def start_notebook(self):
        """Start jupyter-notebook."""
        self.find_binary()
        self.patch_html_templates()
        JUPYTER_OPTS = [
            "--NotebookApp.kernel_manager_class=flarekernelmanager.FlareKernelManager",
            "--NotebookApp.contents_manager_class=flarekernelmanager.FlareContentsManager",
            "--NotebookApp.session_manager_class=flarekernelmanager.FlareSessionManager",
            "--NotebookApp.kernel_spec_manager_class=flarekernelmanager.FlareKernelSpecManager",
            "--NotebookApp.open_browser=False",
            "--NotebookApp.extra_template_paths={0:s}".format(str([self.templates_dir])),
            "--NotebookApp.allow_remote_access=False",
            '--Session.key=b""',
            "--notebook-dir={0:s}".format(self.notebook_dir),
            "--log-level=DEBUG",
        ]
        with open(self.kernel_lock, "w"):
            pass
        self._jupyter_notebook_cmd = [self.jupyter_notebook_binary] + JUPYTER_OPTS
        self._notebook_stdout_hnd = open(self._notebook_stdout_file, "w")
        if not self._notebook_stdout_hnd:
            raise self.CannotOpenNotebookStdoutFile("Cannot open notebook stdout file.")
        self._notebook_stderr_hnd = open(self._notebook_stderr_file, "w")
        if not self._notebook_stderr_hnd:
            raise self.CannotOpenNotebookStderrFile("Cannot open notebook stderr file.")
        self.notebook_popen = subprocess.Popen(
            self._jupyter_notebook_cmd,
            stdout=self._notebook_stdout_hnd,
            stderr=self._notebook_stderr_hnd,
            env=self.env(),
            startupinfo=StartupInfo(),
            creationflags=self.process_creation_flags,
            cwd=os.path.dirname(self.jupyter_notebook_binary),
        )

    def _is_flare_alive_unix(self):
        """Check if the parent Flare process is still alive (Unix)."""
        try:
            os.kill(self.flare_pid, 0)
        except OSError as e:
            return e.errno == os.errno.EPERM
        return True

    def _is_flare_alive_win32(self):
        """Check if the parent Flare process is still alive (Windows)."""
        # GetExitCodeProcess uses a special exit code to indicate that the process is
        # still running.
        STILL_ACTIVE = 259
        PROCESS_QUERY_LIMITED_INFORMATION = int("0x1000", 16)
        DONT_INHERIT_HANDLE = 0
        kernel32 = ctypes.windll.kernel32
        hnd = kernel32.OpenProcess(
            PROCESS_QUERY_LIMITED_INFORMATION, DONT_INHERIT_HANDLE, self.flare_pid
        )
        if hnd == 0:
            log.debug("kernel32.OpenProcess() returned 0")
            log.debug("kernel32.GetLastError() {0:s}".format(str(kernel32.GetLastError())))
            return False
        # If the process exited recently, a pid may still exist for the handle.
        # So, check if we can get the exit code.
        exit_code = ctypes.wintypes.DWORD()
        res = kernel32.GetExitCodeProcess(hnd, ctypes.byref(exit_code))
        # res == 0 if we couldn't get the exit code
        is_running = res == 0 or exit_code.value == STILL_ACTIVE
        kernel32.CloseHandle(hnd)
        return is_running

    def loop(self):
        """Loop until the child jupyter-notebook and the parent Flare processes are alive.

        Return None if the process has not terminated yet.
        A negative value -N indicates that the child was terminated by signal N (POSIX only).
        """
        ret = None
        while 1:
            # FIXME: if jupyter-notebook died but Flare is still alive
            # we should restart jupyter-notebook
            ret = self.notebook_popen.poll()
            if os.path.isfile(self.kernel_lock) and ret is None and self.is_flare_alive():
                QtCore.QThread.msleep(self.ALIVE_POLL_INTERVAL_MS)
            else:
                break
        return ret

    def stop_notebook(self):
        """Kill the notebook process and close its stdout/stderr handles."""
        self.notebook_popen.kill()
        timeout = self.TIMEOUT_JUPYTER_NOTEBOOK_MS
        while timeout and self.notebook_popen.poll() is None:
            timeout -= self.SLEEP_MS
            QtCore.QThread.msleep(self.SLEEP_MS)
        if self._notebook_stdout_hnd:
            self._notebook_stdout_hnd.close()
        if self._notebook_stderr_hnd:
            self._notebook_stderr_hnd.close()

    def set_port_and_token(self, func=None):
        """Set the port the notebook is running on and the token required to access it."""
        timeout = self.TIMEOUT_JUPYTER_NOTEBOOK_MS
        m = None
        notebook_stderr_data = ""
        hnd = None
        has_flare_been_alive = False
        while timeout and self.is_launcher_alive():
            if func:
                if not func():
                    has_flare_been_alive = True
                elif has_flare_been_alive:
                    return
            if not hnd:
                try:
                    hnd = open(self._notebook_stderr_file, "r")
                except Exception:
                    pass
            if hnd:
                line = hnd.readline()
                while (not m) and line:
                    notebook_stderr_data += line
                    m = self.HTTP_REGEX.match(line)
                    line = hnd.readline()
                if m:
                    break
            timeout -= self.SLEEP_MS
            QtWidgets.QApplication.instance().processEvents()
            QtCore.QThread.msleep(self.SLEEP_MS)
        if hnd:
            hnd.close()
        if not self.is_launcher_alive():
            try:
                with open(self._launcher_stderr_file, "r") as hnd:
                    launcher_stderr = hnd.read()
            except Exception:
                launcher_stderr = "<None>"
            raise self.LauncherNotRunning(
                "Launcher '{0:s}' is not running.\n"
                "Launcher stderr:\n'{1:s}'".format(" ".join(self._launcher_cmd), launcher_stderr)
            )
        if not hnd:
            raise self.CannotStart(
                "Cannot start jupyter-notebook '{0:s}'.\n"
                "Notebook stderr:\n'{1:s}'".format(
                    " ".join(self.jupyter_notebook_binary), notebook_stderr_data
                )
            )
        if (not m) or len(m.groups()) != 2:
            raise self.CannotFindPortToken(
                "Cannot find jupyter-notebook port "
                "and token information.\nNotebook stderr:\n'{0:s}'".format(notebook_stderr_data)
            )
        self._port, self._token = m.groups()

    def start_launcher(self):
        """Start the Jupyter notebook launcher."""
        self._launcher_cmd = [sys.executable, __file__, "--pid", str(self.flare_pid)]
        self._launcher_stdout_hnd = open(self._launcher_stdout_file, "w")
        if not self._launcher_stdout_hnd:
            raise self.CannotOpenLauncherStdoutFile("Cannot open launcher stdout file.")
        self._launcher_stderr_hnd = open(self._launcher_stderr_file, "w")
        if not self._launcher_stderr_hnd:
            raise self.CannotOpenLauncherStderrFile("Cannot open launcher stderr file.")
        self.launcher_popen = subprocess.Popen(
            self._launcher_cmd,
            stdout=self._launcher_stdout_hnd,
            stderr=self._launcher_stderr_hnd,
            env=self.env(),
            startupinfo=StartupInfo(),
            creationflags=self.process_creation_flags,
            cwd=os.path.dirname(os.path.realpath(__file__)),
        )

    def is_launcher_alive(self):
        """Return True if the launcher is still alive."""
        return self.launcher_popen is not None and self.launcher_popen.poll() is None

    def mktempdir(self):
        """Create temporary directory tree."""
        CANNOT_MKDIR_MSG = "Cannot create temporary directory {0:s}."
        if os.path.exists(self._tempdir):
            if os.path.isfile(self._tempdir):
                os.remove(self._tempdir)
            elif os.path.isdir(self._tempdir):
                shutil.rmtree(self._tempdir)
            else:
                raise self.CannotCreateTempDir(CANNOT_MKDIR_MSG.format(self._tempdir))
        for d in (
            self._tempdir,
            self._templates_dir,
            self._runtime_dir,
            self._notebook_dir,
            self._notebook_cmp_dir,
        ):
            try:
                os.mkdir(d)
            except Exception:
                raise self.CannotCreateTempDir(CANNOT_MKDIR_MSG.format(d))

    def rmtempdir(self):
        """Remove temporary directory tree."""
        if not self._tempdir:
            return
        if self._launcher_stdout_hnd:
            self._launcher_stdout_hnd.close()
        if self._launcher_stderr_hnd:
            self._launcher_stderr_hnd.close()
        if self._notebook_stdout_hnd:
            self._notebook_stdout_hnd.close()
        if self._notebook_stderr_hnd:
            self._notebook_stderr_hnd.close()
        try:
            shutil.rmtree(self._tempdir)
        except Exception:
            pass

    @property
    def port(self):
        """Return port on which the notebook is running."""
        return self._port

    @property
    def token(self):
        """Return token needed to connect to the notebook."""
        return self._token

    @property
    def jupyter_notebook_binary(self):
        """Return abs path to the jupyter-notebook binary."""
        self.find_binary()
        return self._jupyter_notebook_binary

    @property
    def attach_lock(self):
        """Return abs path to the attach lock file."""
        return self._attach_lock

    @property
    def kernel_lock(self):
        """Return abs path to the kernel lock file."""
        return self._kernel_lock

    @property
    def module_dir(self):
        """Return abs path to the module dir."""
        return self._module_dir

    @property
    def notebook_dir(self):
        """Return abs path to the notebook dir."""
        return self._notebook_dir

    @property
    def notebook_cmp_dir(self):
        """Return abs path to the notebook compare dir."""
        return self._notebook_cmp_dir

    @property
    def templates_dir(self):
        """Return abs path to the templates dir."""
        return self._templates_dir

    @property
    def runtime_dir(self):
        """Return abs path to the runtime dir."""
        return self._runtime_dir

    def __init__(self, flare_pid, mktempdir=False):
        """Initialize class."""
        self.flare_pid = flare_pid
        self._tempdir = os.path.join(self._flare_ipynb_dir(self.flare_pid))
        self._notebook_dir = os.path.join(self._tempdir, self.NOTEBOOK_DIR)
        self._notebook_cmp_dir = os.path.join(self._tempdir, self.NOTEBOOK_CMP_DIR)
        self._templates_dir = os.path.join(self._tempdir, self.TEMPLATES_DIR)
        self._runtime_dir = os.path.join(self._tempdir, self.RUNTIME_DIR)
        self._module_dir = os.path.dirname(os.path.realpath(__file__))
        if mktempdir:
            self.mktempdir()
        self.notebook_popen = None
        self._notebook_stdout_file = os.path.join(self._tempdir, "jupyter_notebook.out")
        self._notebook_stderr_file = os.path.join(self._tempdir, "jupyter_notebook.err")
        self._notebook_stdout_hnd = None
        self._notebook_stderr_hnd = None
        self.launcher_popen = None
        self._launcher_stdout_file = os.path.join(self._tempdir, "launcher.out")
        self._launcher_stderr_file = os.path.join(self._tempdir, "launcher.err")
        self._launcher_stdout_hnd = None
        self._launcher_stderr_hnd = None
        self._kernel_lock = os.path.join(self._tempdir, "kernel.lock")
        self._attach_lock = os.path.join(self._tempdir, "attach.lock")
        self._port = None
        self._token = None
        self._launcher_cmd = []
        self._jupyter_notebook_cmd = []
        self._jupyter_notebook_binary = None

    if sys.platform == "win32":
        is_flare_alive = _is_flare_alive_win32
        process_creation_flags = subprocess.CREATE_NEW_PROCESS_GROUP
    else:
        is_flare_alive = _is_flare_alive_unix
        process_creation_flags = 0


if __name__ == "__main__":
    # If we are being started as a script, start a Jupyter Notebook and
    # enter a loop periodically checking if Flare and Jupyter Notebook
    # processes are still alive. When Flare quits, stop Jupyter Notebook,
    # remove temporary directory and exit.
    CANNOT_FIND_PID_MSG = "Cannot find Flare pid in command-line parameters."
    try:
        i = sys.argv.index("--pid")
    except ValueError as e:
        raise ValueError(CANNOT_FIND_PID_MSG) from e
    try:
        flare_pid = sys.argv[i + 1]
    except IndexError as e:
        raise ValueError(CANNOT_FIND_PID_MSG) from e
    try:
        flare_pid = int(flare_pid)
    except ValueError as e:
        raise ValueError("Flare pid should be an integer.") from e
    launcher = JupyterNotebookLauncher(flare_pid)

    launcher.start_notebook()
    ret_code = launcher.loop()
    if ret_code is None:
        # if ret_code is None, jupyter-notebook is still running
        launcher.stop_notebook()
    while launcher.is_flare_alive():
        log.debug("1) launcher.is_flare_alive()")
        QtCore.QThread.msleep(launcher.ALIVE_POLL_INTERVAL_MS)
    # close own file descriptors or on Windows we won't be able
    # to clean after ourselves as our stdout/stderr is redirected
    # to the directory we want to delete
    os.close(0)
    os.close(1)
    os.close(2)
    launcher.rmtempdir()
