# -*- coding: utf-8 -*-
# Copyright (C) 2023 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Extend the static QFileDialog methods to set the last visited directory."""

import os
import re
from PySide2 import QtCore, QtWidgets
from .directoryservice import DirectoryService
from .configurelogging import get_logger

log = get_logger("myfiledialog")


class MyFileDialog:
    """A simple extension to the static QFileDialog methods."""

    @staticmethod
    def getSaveFileName(
        parent=None,
        caption="",
        cwd=DirectoryService.getLastVisitedDir(),
        filter="",
        options=0,
        filename=None,
    ):
        """Return a (filename, selected_filter) tuple where either can be None."""
        log.debug('getSaveFileName cwd ="{0:s}"'.format(cwd.absolutePath()))
        confirm_overwrite = not options & QtWidgets.QFileDialog.DontConfirmOverwrite
        options |= QtWidgets.QFileDialog.DontConfirmOverwrite
        keep_looping = True
        while keep_looping:
            selected_filter = ""
            file_path, selected_filter = QtWidgets.QFileDialog.getSaveFileName(
                parent,
                caption,
                cwd.absoluteFilePath(filename) if filename else cwd.absolutePath(),
                filter,
                selected_filter,
                options,
            )
            if not file_path:
                return None, None
            ext = ""
            ext_start = selected_filter.rfind(".")
            if ext_start != -1:
                m = re.search("[\\s\\)]", selected_filter[ext_start:])
                ext_end = m.start()
                if ext_end != -1:
                    ext_end += ext_start
                    ext = selected_filter[ext_start:ext_end]
            if file_path and ext and os.path.splitext(file_path)[1] != ext:
                file_path += ext
            if os.path.exists(file_path) and confirm_overwrite:
                res = QtWidgets.QMessageBox.warning(
                    parent,
                    "Confirm Save As",
                    "{0:s} already exists.\nDo you want to replace it?".format(file_path),
                    QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No,
                    QtWidgets.QMessageBox.StandardButton(QtWidgets.QMessageBox.No),
                )
                if res == QtWidgets.QMessageBox.Yes:
                    keep_looping = False
            else:
                keep_looping = False
        file_path = QtCore.QDir.toNativeSeparators(file_path)
        fileinfo = QtCore.QFileInfo(file_path)
        DirectoryService.setLastVisitedDir(fileinfo)
        log.debug('getSaveFileName setLastVisitedDir "{0:s}"'.format(fileinfo.absolutePath()))
        return file_path, selected_filter

    @staticmethod
    def getOpenFileNames(
        parent=None,
        caption="",
        cwd=DirectoryService.getLastVisitedDir(),
        filter="",
        single_file_mode=False,
    ):
        """Return a list of (filename, selected_filter) tuples where either can be None."""
        file_list = []
        log.debug('getOpenFileNames cwd ="{0:s}"'.format(cwd.absolutePath()))
        if single_file_mode:
            file_path, selected_filter = QtWidgets.QFileDialog.getOpenFileName(
                parent, caption, cwd.absolutePath(), filter
            )
            if file_path:
                file_list = [(file_path, selected_filter)]
        else:
            file_list = QtWidgets.QFileDialog.getOpenFileNames(
                parent, caption, cwd.absolutePath(), filter
            )
        if file_list:
            for i, file_tuple in enumerate(file_list):
                file_list[i] = (QtCore.QDir.toNativeSeparators(file_tuple[0]), file_tuple[1])
            fileinfo = QtCore.QFileInfo(file_list[0][0])
            DirectoryService.setLastVisitedDir(fileinfo)
            log.debug('getOpenFileNames setLastVisitedDir "{0:s}"'.format(fileinfo.absolutePath()))
        return file_list if file_list else [(None, None)]

    @staticmethod
    def getOpenFileName(
        parent=None, caption="", cwd=DirectoryService.getLastVisitedDir(), filter=""
    ):
        """Return a (filename, selected_filter) tuple where either can be None."""
        return MyFileDialog.getOpenFileNames(parent, caption, cwd, filter, True)[0]
