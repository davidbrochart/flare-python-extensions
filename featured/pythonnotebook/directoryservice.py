# -*- coding: utf-8 -*-
# Copyright (C) 2023 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Classes to get/set the last visited directory."""

from PySide2 import QtCore


class DirectoryService:
    """Static class to get/set last visited directory."""

    @staticmethod
    def getLastVisitedDir():
        """Get the last visited directory.

        Returns None if none available, or a QDir if available.
        """
        settings = QtCore.QSettings()
        cwd = settings.value("lastVisitedDir", None)
        if cwd:
            cwd = QtCore.QDir(cwd)
        if (not cwd) or (not cwd.exists()):
            documents = QtCore.QStandardPaths.standardLocations(
                QtCore.QStandardPaths.DocumentsLocation
            )
            if documents:
                cwd = QtCore.QDir(documents[0])
        return cwd

    @staticmethod
    def _setLastVisitedDirFromQDir(qdir):
        """Set the last visited directory from an argument of type QDir."""
        if qdir.exists():
            settings = QtCore.QSettings()
            settings.setValue("lastVisitedDir", qdir.absolutePath())

    @staticmethod
    def setLastVisitedDir(arg):
        """Set the last visited directory from an argument of type QDir or QFileInfo."""
        if isinstance(arg, QtCore.QDir):
            DirectoryService._setLastVisitedDirFromQDir(arg)
        elif isinstance(arg, QtCore.QFileInfo):
            if arg.isDir():
                DirectoryService._setLastVisitedDirFromQDir(arg.absoluteDir())
            else:
                DirectoryService._setLastVisitedDirFromQDir(arg.dir())
        else:
            raise TypeError("Expected QDir or QFileInfo, got {0:s}".format(type(arg)))
