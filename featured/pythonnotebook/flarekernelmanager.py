# -*- coding: utf-8 -*-
# Copyright (C) 2023 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Custom classes loaded by jupyter-notebook at startup."""

import sys
import os
from notebook.services.kernels.kernelmanager import MappingKernelManager
from notebook.services.contents.largefilemanager import LargeFileManager
from notebook.services.sessions.sessionmanager import SessionManager
from jupyter_client.kernelspec import KernelSpecManager
import asyncio
from tornado import gen
from traitlets import Set, Instance
import nbformat.v4

import_dir = os.path.dirname(os.path.realpath(__file__))
if import_dir not in sys.path:
    sys.path.append(import_dir)
from configurelogging import get_logger  # noqa: E402


class FlareKernelManager(MappingKernelManager):
    """A Kernel manager that connects to an in-process IPython kernel started by Flare."""

    @property
    def attach_lock(self):
        """Return the abs path to the 'attach.lock' file."""
        if not hasattr(self, "_attach_lock"):
            self._attach_lock = os.path.join(os.path.dirname(self.connection_dir), "attach.lock")
        return self._attach_lock

    def _attach_to_flare_kernel(self, kernel_id):
        """Attach to the Flare kernel."""
        kernel = self._kernels[kernel_id]
        port_names = ("shell_port", "stdin_port", "iopub_port", "hb_port", "control_port")
        for port_name in port_names:
            setattr(kernel, port_name, 0)
        kernel_json = os.path.join(self.connection_dir, "kernel-{0:s}.json".format(self._flare_pid))
        kernel.load_connection_file(kernel_json)

    def _should_attach_to_flare_kernel(self):
        """Return True is this kernel should attach to the Flare kernel."""
        should_attach = os.path.isfile(self.attach_lock)
        if should_attach:
            os.remove(self.attach_lock)
        return should_attach

    @asyncio.coroutine
    async def start_kernel(self, **kwargs):
        """Start a kernel and attach to the Flare kernel if attach.lock exists."""
        should_attach = self._should_attach_to_flare_kernel()
        self._log.debug("1) start_kernel() should_attach = {0:s}".format(str(should_attach)))
        kernel_id = None
        if (not self._kernels) or (not should_attach):
            self._log.debug("2) start_kernel()")
            kernel_id = await super().start_kernel(**kwargs)
            if should_attach:
                self._log.debug("2) should_attach")
                self._flare_kernel_id = kernel_id
                self._attach_to_flare_kernel(kernel_id)
        else:
            self._log.debug("3) start_kernel() kernel_id = self._flare_kernel_id")
            kernel_id = self._flare_kernel_id
        return kernel_id

    def __init__(self, *args, **kwargs):
        self._flare_pid = os.environ["CRESSET_FLARE_PROCESS_PID"]
        self._flare_kernel_id = None
        self._log = get_logger("flarekernelmanager")
        super().__init__(*args, **kwargs)


class FlareKernelSpecManager(KernelSpecManager):
    """A KernelSpecManager which only allows Python3 kernel."""

    whitelist = Set(["python3"], config=True, help="Only Python3 kernel is allowed")


class FlareContentsManager(LargeFileManager):
    """A LargeFileManager which serves a new Flare notebook."""

    @staticmethod
    def _flare_blank_cell():
        return {
            "cell_type": "code",
            "execution_count": None,
            "metadata": {},
            "outputs": [],
            "source": [],
        }

    @staticmethod
    def _flare_new_notebook():
        """Return a customized new notebook dictionary."""
        import_flare_cell = FlareContentsManager._flare_blank_cell()
        import_flare_cell["source"] = "from cresset import flare"
        notebook = nbformat.v4.new_notebook()
        notebook["cells"] = [import_flare_cell, FlareContentsManager._flare_blank_cell()]
        return notebook

    def new(self, model=None, path=""):
        """Return a customized new notebook model."""
        model = super().new(model, path)
        if "content" in model:
            model["content"] = self._flare_new_notebook()
            model = self.save(model, path)
        return model


class FlareSessionManager(SessionManager):
    """A SessionManager which uses FlareKernelManager and FlareContentsManager."""

    kernel_manager = Instance("flarekernelmanager.FlareKernelManager")
    contents_manager = Instance("flarekernelmanager.FlareContentsManager")

    @gen.coroutine
    def create_session(self, **kwargs):
        """Create a session and return its model."""
        kernel_id = kwargs.get("kernel_id", None)
        self._log.debug(
            "1) FlareSessionManager.create_session kernel_id = {0:s}".format(str(kernel_id))
        )
        if (
            kernel_id is not None
            and kernel_id in self.kernel_manager
            and kernel_id == self.kernel_manager._flare_kernel_id
        ):
            kwargs["kernel_id"] = None
            self._log.debug(
                "2) FlareSessionManager.create_session kernel_id = {0:s}".format(
                    str(kwargs["kernel_id"])
                )
            )
        result = yield super().create_session(**kwargs)
        return result

    def __init__(self, *args, **kwargs):
        self._log = get_logger("FlareSessionManager")
        super().__init__(*args, **kwargs)
