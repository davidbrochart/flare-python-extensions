# -*- coding: utf-8 -*-
# Copyright (C) 2023 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Script executed by the __init__ module."""

try:
    import gevent.monkey
except Exception:
    pass
else:
    gevent.monkey.patch_all(thread=False)
    try:
        import gevent.libuv.watcher
    except Exception:
        pass
    else:

        def _watcher_ffi_init_patched(self, args):
            self._watcher_init(self.loop._ptr, self._watcher)
            self._after, self._repeat = args
            if self._after and self._after < 0.001:
                self._after = 0.001
            if self._repeat and self._repeat < 0.001:
                self._repeat = 0.001

        gevent.libuv.watcher.timer._watcher_ffi_init = _watcher_ffi_init_patched
