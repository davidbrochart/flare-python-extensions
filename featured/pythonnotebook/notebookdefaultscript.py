# -*- coding: utf-8 -*-
# Copyright (C) 2023 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Script run when a notebook starts."""

import sys
from IPython.external.qt_loaders import ImportDenier
from nblauncher import JupyterNotebookLauncher


class FlareImportDenier(ImportDenier):
    """Override ImportDenier to make the load_module() method a no-op."""

    def load_module(*args, **kwargs):
        """No-op."""
        pass


for i, item in enumerate(sys.meta_path):
    if isinstance(item, ImportDenier):
        sys.meta_path[i] = FlareImportDenier()

# set flare_ipynb_dir if the notebook namespace
flare_ipynb_dir = JupyterNotebookLauncher.flare_ipynb_dir()

del sys
del ImportDenier
del JupyterNotebookLauncher
