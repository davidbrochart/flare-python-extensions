# Featured Extensions

The featured extensions is a collection of free Python scripts which add powerful new functionality to Flare (TM). After installing these extensions, a new ribbon tab titled "Extensions" should appear in the Flare GUI, containing buttons to access this new functionality.

If you need help with any of these extensions or have suggestions of your own please contact [Cresset support](https://www.cresset-group.com/about-us/contact-us/).

## Prerequisites

Some extensions require additional Python packages which are not installed by Flare. These packages can be installed from the command line by using the pyflare executable:

> pyflare -m pip install --user -r featured/requirements.txt

Alternatively, these packages can be installed from within Flare "Python Console" window.

```python
import sys
import subprocess
subprocess.check_call([sys.executable, "-m", "pip", "install", "--user", "-r", "featured/requirements.txt"])
```

Restart Flare to load the new packages.

## Installing

See the [installation guide](../README.md).

## Extensions Descriptions

### Calculate RMSD (calculatermsd)
Calculates the RMSD of ligands and poses using RDKit.

**New Ribbon Controls:**

*Extensions -> Ligand -> Calculate RMSD* - Calculates the RMSD between the selected ligand or pose and all the other ligands and poses with the same structure. A "RMSD" column will be added to the Ligands table with the results.


### Cresset Integration (cressetintegration)
Adds buttons and menus which allow other Cresset products to be access from Flare.

**New Context Menu Items:**

*Ligands table -> Send to Forge* - Open the selected molecules in Forge. This control is only shown if a Forge installation is detected.

*Ligands table -> Send to Spark* - Open the selected ligand to Spark. This menu item is only shown if a Spark installation is detected.


### Export Model Data (exportmodeldata)
Adds functions for exporting QSAR model data.

**New Ribbon Controls:**

*QSAR -> Import/Export -> QSAR Models -> Export Displayed QSAR Model Data* - Export the data used to build the displayed QSAR model.

*QSAR -> Import/Export -> QSAR Models -> Export Field Samples for Selected Ligands* - Export the field sample data for the selected ligands for use with external statistics packages.


### Homology Modeling (homologymodeling.py)
Adds useful functions for homology modeling including import of FASTA files.

**New Ribbon Controls:**

*Sequences -> Fasta -> Import FASTA File* - Read and create a protein from a FASTA file. The protein will contain 1 carbon atom for each residue.

*Sequences -> Fasta -> Import FASTA Sequence* - Read and create a protein from a FASTA string. The protein will contain 1 carbon atom for each residue.

*Sequences -> Fasta -> Export alignments* - Export selected chain alignments (all if none is selected) to a FASTA file.

*Sequences -> Homology Modeling -> Pick Residues across Sequences* - Expand the residue pick to include residues in other sequences which are aligned to the current residue pick.

*Sequences -> Homology Modeling -> Mutate Protein To Sequence* - Mutate the residues in the selected protein to match another protein.

*Sequences -> Homology Modeling -> Renumber Residues* - Renumber the seq numbers of residues. If some residues have been picked, the renumbering applies only to this selection. Otherwise, renumbering is performed for residues in all selected proteins. Allows setting the initial seq number and whether gaps should be counted when numbering.

**New Context Menu Items:**

*Protein Sequence -> Set Alignment From FASTA* - Set the sequence alignment to the FASTA sequences. Residues cannot be added but gaps may be added or removed.

*Protein Sequence Residue -> Number Residues From Here* - Renumber the seq number of the residues starting at the residue the menu was opened on. Allows to set the initial seq number and whether gaps should be counted when numbering.


### Loop Modeling (loopmodeling)
Predicts loop structures using PyFREAD and a specified database.

**New Ribbon Controls:**

*Protein -> Structure -> Loop Modeling* - Predicts loop structures using the database search loop modeling method PyFREAD.


### Minimize Dynamics (minimizedynamics)
Copies the protein at various points in a dynamics timeline and minimizes them.

**New Ribbon Controls:**

*Extensions -> Protein -> Minimize Dynamics* - Makes copies of the protein at various points in the dynamics timeline and minimizes the them using the XED forcefield.


### Open in RCSB (openinrcsb.py)
Adds functionality to show the RCSB web page for a protein.

**New Context Menu Items:**

*Protein -> Show in RCSB* - Opens a new dialog which shows the RCSB web page for the protein.


### Plot (plot)
Adds a ligand scatter plot, Ramachandran plot and protein contact map plot to Flare.

**New Ribbon Controls:**

*Ligand -> Table -> Plot* - Open a scatter plot for 2 or 3 columns in the ligand table. The plot shows which ligands are selected and allows ligands in the plot to be selected by clicking them or drawing a lasso around them.

*Ligand -> Table -> Histogram* - Open a histogram plot for a column in the ligand table.

*Ligand -> Table -> Box Plot* - Open a boxplot for columns in the ligand table.

*Protein -> Structure -> Ramachandran* - Open a Ramachandran plot for the selected proteins. The plot shows which residues are picked and allows residues in the plot to be picked by clicking them or drawing a lasso around them.

*Protein -> Structure -> Contact Map* - Open a Protein Contact Map which shows the distance between the protein's alpha carbons.

*Home -> Appearance -> Color -> Color by Ramachandran* - Color the protein residues accordingly to which region they fall in the Ramachandran plot.

*Home -> Appearance -> Ribbon Color -> Color Ribbon by Ramachandran* - Color the ribbon accordingly to which region the residues fall in the the Ramachandran plot.


### Python Notebook (pythonnotebook)
Add the Jupyter-based Python Notebook to Flare.

**New Ribbon Controls:**

*Python -> Code -> Python Notebook* - Open the Python Notebook.


### QSAR Models (qsarmodels)
Adds a selection of machine learning QSAR extensions that run scikit-learn to produce models that are then converted to ONNX format to run predictions. Rather than adding buttons to the extensions ribbon tab, controls are added to the QSAR Model Building dialog under the Regression and Classification model types.

**New Ribbon Controls:**

*QSAR -> Build QSAR Model -> Model -> SVM* - Run either a Regression or Classification SVM model.

*QSAR -> Build QSAR Model -> Model -> Random Forest* - Run either a Regression or Classification Random Forest model.

*QSAR -> Build QSAR Model -> Model -> MLP* - Run either a Regression or Classification Multi-Layer Perceptron model.

*QSAR -> Build QSAR Model -> Model -> Gaussian Process* - Run a Regression Gaussian Process model.

*QSAR -> Build QSAR Model -> Model -> Consensus* - Run either a Regression or Classification Consensus model.


### Sequence Coloring (sequencecoloring.py)
Allows proteins atoms to be colored by the sequence alignment.

**New Ribbon Controls:**

*Sequences -> Sequences View -> Color by Sequence* - Colors the selected proteins atoms by the sequence similarity to another protein.


### 2D Sim (sim2d)
Calculates the 2D similarity of all ligands to the selected ligands.

**New Ribbon Controls:**

*Extensions -> Ligand -> Calculate 2D Sim* - Calculates the 2D Sim between the selected ligands and all the other ligands. A "2D Sim" column will be added to the Ligands table with the results.


### Total Formal Charge (totalformalcharge.py)
Adds a column to the ligand table which displays the ligand total formal charge.


### Tours (tours)
Adds guided tours of Flare's functionality.

**New Ribbon Controls:**

*Help -> Guided Tours -> Align Ligands* - Run a tour on how to align ligands.

*Help -> Guided Tours -> Dock Ligands* - Run a tour on how to dock ligands.

*Help -> Guided Tours -> Build QSAR* - Run a tour on how to build QSAR models.

*Help -> Guided Tours -> Build 2D QSAR* - Run a tour on how to build 2D QSAR models.

*Help -> Guided Tours -> Fit Ligands to a QSAR Model* - Run a tour on how to fit ligands to a QSAR model.

*Help -> Guided Tours -> Create Pharmacophore* - Run a tour on how to create a pharmacophore.

