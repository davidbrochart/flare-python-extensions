#!/usr/bin/env pyflare
# -*- coding: utf-8 -*-
# Copyright (C) 2023 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Install the Python extensions for Flare.

examples:
  pyflare installextensions.py
    Asks where to install the extensions and installs the "featured" and "additional" extensions
    to that location. If a location is not given they are installed to the default location.
    If a custom directory is added then it will needed to be added to the
    'FLARE_PYTHON_EXTENSIONS_PATH' environment variable.
    See Flare Python documentation for more details.

  pyflare installextensions.py -t `path`
    Installs the "featured" and "additional" extensions to `path`

  pyflare installextensions.py -e "featured,additional"
    Installs the  "featured" and "additional" extensions.

  pyflare installextensions.py -u
    Uninstall the extensions. You will be asked to the directory to uninstall from.

  pyflare installextensions.py --help
    Prints the help text including all available options and exits.
"""
import os
import sys
import shutil
import argparse
import subprocess

INSTALL_LOG_NAME = "installed_files.txt"
"""The name of the log file which list all files and directories what were installed.

This is used by the uninstaller to remove the files.
"""


def main(argv):
    """Install or uninstalls the extensions."""
    try:
        args = _parse_args(argv)

        install_dir = args.target
        if install_dir is None:
            message = "Install directory" if not args.uninstall else "Uninstall from directory"
            install_dir = _ask(message, _default_install_dir())

        install_log_path = os.path.join(install_dir, INSTALL_LOG_NAME)
        has_installation = os.path.isfile(install_log_path)

        if args.uninstall:
            if has_installation:
                _uninstall(install_dir, silent=True)
                print("Uninstall complete.")
            else:
                raise Exception(f"No installation found at '{install_dir}'")
        else:  # Install
            ok = True
            if has_installation:
                print("A previous installation has been detected.")
                ok = _uninstall(install_dir)

            if ok:
                extensions_to_install = []
                if args.extensions is not None:
                    extensions_to_install = [
                        f"{type_dir.strip()}" for type_dir in args.extensions.split(",")
                    ]
                else:
                    if _ask("Install Featured Extensions", "y", "y/n").lower() == "y":
                        extensions_to_install.append("featured")
                    if (
                        _ask(
                            "Install Additional Extensions (may affect stability)", "n", "y/n"
                        ).lower()
                        == "y"
                    ):
                        extensions_to_install.append("additional")

                if not extensions_to_install:
                    raise Exception("No extensions have been selected to install")

                os.makedirs(install_dir, exist_ok=True)
                _install_extensions(install_dir, extensions_to_install, args.requirements)
                print("Installation complete.")

    except Exception as err:
        print(err, file=sys.stderr)
        return -1
    return 0


def _parse_args(argv):
    """Parse the command line arguments."""
    parser = argparse.ArgumentParser(
        description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter
    )
    parser.add_argument(
        "-e",
        "--extensions",
        type=str,
        default=None,
        help="The types of extensions to install as a comma separated list. The available options are 'featured' and 'additional'. If not set then the user is ask what extensions to install.",  # noqa: E501
    )
    parser.add_argument(
        "-u", "--uninstall", action="store_true", help="Removed a previous install"  # noqa: E501
    )
    parser.add_argument(
        "-t",
        "--target",
        help="The directory to install the extensions to. If this is not part of Flare search path then it will need to be added to 'FLARE_PYTHON_EXTENSIONS_PATH'. Defaults to a directory in the home directory which is on the Flare search path.",  # noqa: E501
    )
    parser.add_argument(
        "-r",
        "--requirements",
        choices=["a", "y", "n"],
        default="a",
        type=str.lower,
        help="Install 3rd party requirements from Python Package Index. y: install the packages, n: do not install the packages, a (default): asks if the packages should be installed.",  # noqa: E501
    )
    args = parser.parse_args(argv)
    return args


def _default_install_dir():
    """Return the default install directory."""
    return os.environ["FLARE_EXTENSIONS_DIR"]


def _uninstall(install_dir, silent=False):
    """Uninstall the scripts from `install_dir`."""
    install_log_path = os.path.join(install_dir, INSTALL_LOG_NAME)
    uninstall_files = _read_installed_files(install_log_path)

    file_paths = [os.path.join(install_dir, file) for file in uninstall_files]

    if silent:
        should_uninstall = True
    else:
        should_uninstall = _ask("Remove previous installation", "n", "y/n").lower() == "y"

    if should_uninstall:
        # Loop in reversed order so files and sub-directories are removed before
        # parent directories
        for path in reversed(file_paths):
            if os.path.isfile(path):
                os.remove(path)

            # Remove the directories, if they are not empty then someone else has added a file so
            # raise an error
            if os.path.isdir(path):
                # __pycache__ is auto generated so always delete it
                pycache_dir = os.path.join(path, "__pycache__")
                if os.path.isdir(pycache_dir):
                    shutil.rmtree(pycache_dir)
                os.rmdir(path)
        return True

    print("Installation canceled.")
    return False


def _install_extensions(install_dir, extensions, install_requirements):
    """Install the extensions to `install_dir`."""
    source_dir = os.path.dirname(os.path.realpath(__file__))
    _install_extension(source_dir, install_dir, extensions)
    _install_third_party(source_dir, extensions, install_requirements)


def _install_extension(source_dir, install_dir, types):
    """Install the given types of extensions from `source_dir` to `install_dir`."""
    type_files = {}

    for type_dir in types:
        type_files[type_dir] = _list_files(os.path.join(source_dir, type_dir))

    for files in type_files.values():
        _check_does_not_exist(install_dir, files)

    all_files = []
    for type_dir, files in type_files.items():
        _copy_extensions(os.path.join(source_dir, type_dir), install_dir, files)
        all_files.extend(files)

    _write_installed_files(install_dir, all_files)


def _list_files(root_dir):
    """List all files and directories in `root_dir`."""
    objects = []
    for folder, _, files in os.walk(root_dir):
        dir_path = folder[len(root_dir) + 1 :]  # noqa: E203
        if "__pycache__" not in folder:
            if dir_path:
                objects.append(dir_path)
            for file in files:
                objects.append(os.path.join(dir_path, file))
    return objects


def _check_does_not_exist(install_dir, files):
    """Throw excepotion if a file in `files` exists in `install_dir`."""
    for file in files:
        path = os.path.join(install_dir, file)
        if os.path.isfile(path):
            raise Exception(f"Failed to install extension due to existing file '{path}'.")
        elif os.path.isdir(path):
            raise Exception(f"Failed to install extension due to existing directory '{path}'.")


def _copy_extensions(source_dir, install_dir, files):
    """Copy each file and directory in `files` from `source_dir` to `install_dir`."""
    for file in files:
        source_path = os.path.join(source_dir, file)
        install_path = os.path.join(install_dir, file)

        if os.path.isdir(source_path):
            os.makedirs(install_path)
        else:
            shutil.copyfile(source_path, install_path)  # Replaces install_path if exists
            shutil.copymode(source_path, install_path)  # Copy file permissions


def _write_installed_files(install_dir, files):
    """Write all files and directories that have been installed to the log."""
    with open(os.path.join(install_dir, INSTALL_LOG_NAME), "w") as install_log:
        for file in files:
            install_log.write(f"{file}\n")
        install_log.write(f"{INSTALL_LOG_NAME}\n")


def _read_installed_files(install_log_path):
    """Return a list of all files and directories listed in the `install_log_path` file."""
    files = []
    with open(install_log_path, "r") as install_log:
        for file in install_log:
            files.append(file.strip())
    return files


def _install_third_party(source_dir, types, install_requirements):
    """Ask then downloads the 3rd party packages from The Python Package Index."""
    requirments = []

    for type_dir in types:
        requirment_path = os.path.join(source_dir, type_dir, "requirements.txt")
        if os.path.isfile(requirment_path):
            requirments.append(requirment_path)

    if requirments:
        if install_requirements.lower() == "a":
            print(
                "Some extensions require 3rd party Python packages to be installed from the "
                + "Python Package Index."
            )
            should_download = (
                _ask("Download and install these packages now", "y", "y/n").lower() == "y"
            )
        else:
            should_download = install_requirements.lower() == "y"

        if should_download:
            for requirment in requirments:
                subprocess.check_call(
                    [
                        sys.executable,
                        "-m",
                        "pip",
                        "install",
                        "--disable-pip-version-check",
                        "--no-warn-script-location",
                        "--user",
                        "-r",
                        requirment,
                    ]
                )


def _ask(question, default, options=None):
    """Ask a quest and return the user input."""
    if options is None:
        result = input(f"{question} [{default}]: ")
    else:
        result = input(f"{question} ({options}) [{default}]: ")
    return result if result else default


if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
