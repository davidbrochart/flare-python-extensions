# Changelog

## 2023-05-10 for Flare 7.0
### Changed
- Added an automatic mode to loop modeling which generates loops in all selected proteins where there are three or more missing residues in a row (loopmodeling)
- The ligand plots now allows annotations to be permanently displayed (plot)
- Added consensus QSAR regression and classification models (qsarmodels)
- Updated the tours for Flare 7 (tours)

## 2022-11-15 for Flare 6.1
### Added
- Added plot to show ligand data as a box plot. The button is located at `Ligand -> Table -> Boxplot` (plot)
- Added QSAR MLP regression and classification models (qsarmodels)
- Added option to mutate only part of the sequence (homologymodeling.py)

### Changed
- Improved the Ligand scatter plot at `Ligand -> Table -> Plot` (plot)
  - The plot points color, shape and size can be changed based of ligand column values the ligand tags or the ligand role.
  - The background color can now be changed
- The histogram plot at `Ligand -> Table -> Histogram` background color can now be changed (plot)

## 2022-06-14 for Flare 6.0
### Added
- Button `Home -> Measurements -> Interaction Map` which shows the interactions between a ligand and protein as a 2D image (plidiagram)
- An automatic mode for loop modeling which generates loops for all gaps in the selected proteins (loopmodeling)
- "Plot All Ligands" button to the scatter and histogram plot (plot)
- Button `Help -> Guided Tours -> Build 2D QSAR` which shows how to create 2D QSAR models (tour)

### Changed
- `Export Residues Delta G Values` has been moved to `3D Pose -> Export` (export)
- `QSAR -> Import/Export -> QSAR Models` has been renamed and moved to `QSAR -> Model -> Export Model Data` (exportmodeldata)
- `Extensions -> Sequences` has been moved to `Sequences -> Fasta` and `Sequences ->  Homology Modeling` (homologymodeling.py)
- `Extensions -> Homology Modeling` has been moved to `Sequences -> Fasta` and `Sequences ->  Homology Modeling` (homologymodeling.py)
- `Extensions -> Sequences -> Color by Sequence` has been moved to `Sequences -> Sequences View -> Color by Sequence` (sequencecoloring.py)

## 2021-06-28 for Flare 5.0
### Added
- Added plot to show ligand data in a histogram. The button is located at `Ligand -> Table -> Histogram` (plot)
- Button for calculating 2D similarity of ligands using RDKit fingerprints. The button is located at `Extensions -> Ligand -> Calculate 2D Sim` (sim2d)
- Added several tours to Flare GUI. The tours are located at `Help -> Guided Tours` (tours)

### Changed
- Updates to support the Flare 5.0 Python API
- Added configuration options to the Ligand scatter plot (plot)
- Moved the Ligand scatter plot to `Ligand -> Table -> Plot` (plot)
- Moved the Ramachandran Plot and Protein Contact Map to `Protein -> Structure` (plot)
- Moved the send to Forge button to the ligand context menu (cressetintegration)
- Moved the Python Notebook to the additional extensions (pythonnotebook)

### Removed
- The button `Extensions -> Import/Export -> Export to csv` as CSV export is now built into
  Flare and can be done via `File -> Export Ligands -> Export Selected Ligands` and selecting the CSV file type. (importexport)
- The button `Extensions -> Import/Export -> Import from csv` as CSV import is now built into
  Flare and can be done via `File -> Open` and selecting a CSV file. (importexport)

## 2020-05-02 for Flare 4.0
### Changed
- Updates to support the Flare 4.0 Python API
- `Extensions -> Protein -> Prep and Align` and `Extensions -> Protein -> Ligand View` now allow the settings
  for the generated surfaces and z-clip to be changed.
- Improved the visibility of selected residues in the Ramachandran plot.
- The column `Total Formal Charge` is now updated when poses are added to ligands.

### Removed
- The button `Ligand -> Binding -> ΔG Converter` as when setting up a FEP calculation the units
  the activity is in can be set.

## 2020-01-30 for Flare 3.0
### Added
- Widget which adds the Jupyter-based Python Notebook to Flare. The Python Notebook can be opened
  using the button `Python -> Code -> Python Notebook` (pythonnotebook)
- Button for converting activity data to delta G in kcal/mol so it can be used in FEP calculations.
  The button is located at `Ligand -> Binding -> ΔG Converter` (convertactivityunits)
- The column `Total Formal Charge` which contains each ligand total formal charge can be added to the ligand table (totalformalcharge.py)
- Button for creating copies of the protein at various points in the dynamics timeline. The copies
  are then minimized using the XED forcefield. The button is located at
  `Extensions -> Protein -> Minimize Dynamics` (minimizedynamics)
- Buttons for coloring protein residues according to their region in the Ramachandran
  plot. The buttons are located at `Home -> Appearance -> Color -> Color by Ramachandran` and
  `Home -> Appearance -> Ribbon Color -> Color Ribbon by Ramachandran` (plot)
- Button for predicting loop structures of proteins. The button is located at
  `Extensions -> Homology Modeling -> Build Loops with FREAD` (loopmodeling)

### Changed
- Updates to support the Flare 3.0 Python API
- If residues are picked when the button `Extensions -> Homology Modeling -> Renumber Protein Residues`
  is pressed only the picked residues will be renumbered. (homologymodeling)
- Rearranged and renamed the ribbon buttons
- `Extensions -> Protein -> Prep and Align` and `Extensions -> Protein -> Ligand View` now operate on
  selected proteins rather than all proteins in the project
- `Extensions -> Coloring -> Color By Sequence` now has Ident as the default matrix type rather than BLOSUM62

### Removed
- The button `Extensions -> Cresset -> Align` which aligns molecules using Forge falign binary.
  Flare 3.0 has built in support for aligning molecules (cressetintegration)
- The button `Extensions -> Cresset -> XedeX` which performs conformation hunt using XedTool's
  xedex binary. Flare 3.0 has built in support for generating conformations (cressetintegration)
- The button to rescore poses `Extensions -> Re-score -> Re-score Poses`. Flare 3.0 has built-in
  support for scoring poses (rescoreposes.py)
- The QtConsole widget - this has been replaced by the Python Notebook (pythonqtconsole)

## 2018-07-25 for Flare 2.0
### Added
#### Featured
- Buttons which integrate other Cresset products into Flare (TM). The button is located at
  `Extensions -> Cresset -> Send To Forge` and a context menu item is added to
  `Ligands table -> Send To Spark` (cressetintegration)
- Ligand Scatter Plot, Ramachandran Plot and Protein Contact Map. The buttons are located at
  `Extensions -> Plot -> Ligand Scatter Plot`, `Extensions -> Plot -> Ramachandran Plot` and
   `Extensions -> Plot -> Protein Contact Map` (plot)
- Widget which adds the Jupyter QtConsole to Flare. The button is located at
  `Python -> Code -> Python Qt Console` (pythonqtconsole)
- Button for calculating RMSD of ligands and poses using RDKit. The button is located at
  `Extensions -> RMSD -> Calculate RMSD` (calculatermsd.py)
- Buttons for importing FASTA files, mutating sequences and renumbering protein residues. The
  buttons are located at `Extensions -> FASTA -> Import FASTA File`,
  `Extensions -> FASTA -> Import FASTA Sequence`, `Extensions -> FASTA -> Export alignments`,
  `Extensions -> Homology Modeling -> Mutate Protein To FASTA Sequence`,
  `Extensions -> Homology Modeling -> Re-number Protein Residues` and
  `Extensions -> Homology Modeling -> Pick Residues Across Sequences`. Context menu items are added at
  `Protein Sequence -> Set Alignment From FASTA` and
  `Protein Sequence Residue -> Number Residues From Here`(homologymodeling.py)
- Buttons for importing and exporting to CSV files. The buttons are located at
  `Extensions -> Import/Export -> Export Ligands Table`, `Extensions -> Import/Export -> Import Ligands`
  and `Extensions -> Import/Export -> Export Residues Delta G Values` (importexport.py)
- Menu item which shows the RCSB entry for the protein. The menu item is located at
  `Protein -> Show in RCSB`(openinrcsb.py)
- Button which aligns, superposes, prepares and adds surfaces to all proteins in the project. The
  button is located at `Extensions -> Prep And Align` (prepalignview.py)
- Button which scores the poses in the project using Lead Finder (TM) (rescoreposes.py)
- Button which colors the protein by sequence alignment. The button is located at
  `Extensions -> Coloring -> Color By Sequence` (sequencecoloring.py)

#### Additional
- Button which opens the selected molecules in PyMol. The button is located at
  `Extensions -> PyMOL -> Send to PyMOL ` (openpymol.py)
